-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 07, 2020 at 09:58 AM
-- Server version: 10.2.33-MariaDB-log
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `camsedu_examination`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_last_login`
--

CREATE TABLE `admin_last_login` (
  `id` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `answer_status`
--

CREATE TABLE `answer_status` (
  `id` int(20) NOT NULL,
  `code` varchar(1024) DEFAULT '',
  `name` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answer_status`
--

INSERT INTO `answer_status` (`id`, `code`, `name`, `status`, `created_by`, `reason`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, '', 'Active', 1, 1, '', '2020-10-07 12:32:42', NULL, '2020-10-07 12:32:42'),
(2, '', 'In-active', 1, 1, '', '2020-10-07 12:34:26', NULL, '2020-10-07 12:34:26'),
(3, '', 'Draft', 1, 1, '', '2020-10-07 12:34:26', NULL, '2020-10-07 12:34:26'),
(4, '', 'Moderated', 1, 1, '', '2020-10-07 12:34:26', NULL, '2020-10-07 12:34:26'),
(5, '', 'Active But Re-moderated', 1, 1, '', '2020-10-07 12:34:26', NULL, '2020-10-07 12:34:26');

-- --------------------------------------------------------

--
-- Table structure for table `bank_registration`
--

CREATE TABLE `bank_registration` (
  `id` int(20) NOT NULL,
  `name` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `account_no` varchar(512) DEFAULT '',
  `bank_id` varchar(50) DEFAULT '',
  `address` varchar(580) DEFAULT '',
  `landmark` varchar(580) DEFAULT '',
  `city` varchar(580) DEFAULT '',
  `id_state` int(20) DEFAULT NULL,
  `id_country` int(20) DEFAULT NULL,
  `zipcode` int(10) DEFAULT NULL,
  `cr_fund` varchar(50) DEFAULT '',
  `cr_department` varchar(50) DEFAULT '',
  `cr_activity` varchar(50) DEFAULT '',
  `cr_account` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bloom_taxonomy`
--

CREATE TABLE `bloom_taxonomy` (
  `id` int(20) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `name_optional_language` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bloom_taxonomy`
--

INSERT INTO `bloom_taxonomy` (`id`, `name`, `code`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Recall', 'Recall', '', 1, 1, '2020-08-14 18:17:14', NULL, '2020-08-14 18:17:14'),
(2, 'Understanding', 'Understanding', '', 1, 1, '2020-08-14 18:18:53', NULL, '2020-08-14 18:18:53'),
(3, 'Application', 'Application', '', 1, 1, '2020-08-14 18:19:05', NULL, '2020-08-14 18:19:05'),
(4, 'Analysis', 'Analysis', '', 1, 1, '2020-08-14 18:19:18', 1, '2020-08-14 18:19:18');

-- --------------------------------------------------------

--
-- Table structure for table `communication_group`
--

CREATE TABLE `communication_group` (
  `id` int(20) NOT NULL,
  `id_template` int(20) DEFAULT 0,
  `name` varchar(1024) DEFAULT '',
  `type` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `communication_group`
--

INSERT INTO `communication_group` (`id`, `id_template`, `name`, `type`, `status`, `created_by`, `reason`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 'Student Final Year 2020', 'Student', 1, 1, '', '2020-10-02 15:52:05', NULL, '2020-10-02 15:52:05');

-- --------------------------------------------------------

--
-- Table structure for table `communication_group_recepients`
--

CREATE TABLE `communication_group_recepients` (
  `id` int(20) NOT NULL,
  `id_group` int(20) DEFAULT 0,
  `id_recepient` int(20) DEFAULT 0,
  `type` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `communication_group_recepients`
--

INSERT INTO `communication_group_recepients` (`id`, `id_group`, `id_recepient`, `type`, `status`, `created_by`, `reason`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 1, 4, 'Student', 1, 1, '', '2020-10-02 15:52:22', NULL, '2020-10-02 15:52:22');

-- --------------------------------------------------------

--
-- Table structure for table `communication_template`
--

CREATE TABLE `communication_template` (
  `id` int(20) NOT NULL,
  `name` text DEFAULT NULL,
  `subject` text DEFAULT NULL,
  `message` text DEFAULT NULL,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `communication_template`
--

INSERT INTO `communication_template` (`id`, `name`, `subject`, `message`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Fee Instruction', 'Fee Payment', '<p>Pay Fee Amount</p>\r\n', 1, 1, '2020-10-02 15:51:01', NULL, '2020-10-02 15:51:01');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(20) NOT NULL,
  `name` varchar(120) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(2) DEFAULT 0,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(2) DEFAULT 0,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `name`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Malaysia', 1, 0, '2020-02-27 15:59:29', 0, '2020-02-27 15:59:29');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `id` int(20) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `name_optional_language` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `name`, `code`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Foundation Shariah', 'Foundation Shariah', '', 1, 1, '2020-08-14 18:04:41', 1, '2020-08-14 18:04:41'),
(2, 'CIIF Code of Ethics', 'CIIF Code of Ethics', '', 1, 1, '2020-08-14 18:04:49', 1, '2020-08-14 18:04:49'),
(3, 'CPIF – Foundation Shariah (Question Bank)', 'CPIF – Foundation Shariah (Question Bank)', '', 1, 1, '2020-08-14 18:04:56', 1, '2020-08-14 18:04:56'),
(4, 'CPIF – Intermediate Applied Shariah in Finance', 'CPIF – Intermediate Applied Shariah in Finance', '', 1, 1, '2020-08-14 18:05:07', 1, '2020-08-14 18:05:07');

-- --------------------------------------------------------

--
-- Table structure for table `course_learning_objective`
--

CREATE TABLE `course_learning_objective` (
  `id` int(20) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `name_optional_language` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_learning_objective`
--

INSERT INTO `course_learning_objective` (`id`, `name`, `code`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Understanding', 'Understanding', '', 1, 1, '2020-08-14 18:17:50', NULL, '2020-08-14 18:17:50'),
(2, 'Application', 'Application', '', 1, 1, '2020-08-14 18:18:04', NULL, '2020-08-14 18:18:04'),
(3, 'Recall', 'Recall', '', 1, 1, '2020-08-14 18:18:17', NULL, '2020-08-14 18:18:17'),
(4, 'Analysis', 'Analysis', '', 1, 1, '2020-08-14 18:18:33', NULL, '2020-08-14 18:18:33');

-- --------------------------------------------------------

--
-- Table structure for table `currency_rate_setup`
--

CREATE TABLE `currency_rate_setup` (
  `id` int(20) NOT NULL,
  `id_currency` int(20) DEFAULT 0,
  `exchange_rate` varchar(200) DEFAULT '',
  `min_rate` varchar(200) DEFAULT '',
  `max_rate` varchar(200) DEFAULT '',
  `effective_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `currency_setup`
--

CREATE TABLE `currency_setup` (
  `id` int(20) NOT NULL,
  `code` varchar(2048) DEFAULT '',
  `name` varchar(2048) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `prefix` varchar(200) DEFAULT '',
  `suffix` varchar(200) DEFAULT '',
  `decimal_place` int(20) DEFAULT 0,
  `default` int(2) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `difficult_level`
--

CREATE TABLE `difficult_level` (
  `id` int(20) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `name_optional_language` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `difficult_level`
--

INSERT INTO `difficult_level` (`id`, `name`, `code`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Easy', 'Easy', '', 1, 1, '2020-08-14 18:21:39', NULL, '2020-08-14 18:21:39'),
(2, 'Medium', 'Medium', '', 1, 1, '2020-08-14 18:21:48', NULL, '2020-08-14 18:21:48'),
(3, 'Difficult', 'Difficult', '', 1, 1, '2020-08-14 18:21:58', NULL, '2020-08-14 18:21:58');

-- --------------------------------------------------------

--
-- Table structure for table `examset`
--

CREATE TABLE `examset` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `instructions` text DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `pass_grade` int(11) DEFAULT NULL,
  `attempts` varchar(100) DEFAULT NULL,
  `grading_method` int(11) DEFAULT NULL,
  `layout` int(11) DEFAULT NULL,
  `behaviour` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `updated_by` bigint(20) NOT NULL,
  `created_dt_tm` datetime NOT NULL,
  `updated_dt_tm` datetime NOT NULL,
  `attempt_duration` int(11) DEFAULT NULL,
  `id_tos` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `examset`
--

INSERT INTO `examset` (`id`, `name`, `instructions`, `duration`, `pass_grade`, `attempts`, `grading_method`, `layout`, `behaviour`, `status`, `created_by`, `updated_by`, `created_dt_tm`, `updated_dt_tm`, `attempt_duration`, `id_tos`) VALUES
(1, 'Test', '<ol>\r\n <li>Instuction goes here point number 1</li>\r\n <li>Instuction goes here point number 2</li>\r\n <li>Instuction goes here point number 3</li>\r\n <li>Instuction goes here point number 4</li>\r\n <li>Instuction goes here point number 5</li>\r\n</ol>\r\n', 100, 10, '4', 1, 1, 1, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 6);

-- --------------------------------------------------------

--
-- Table structure for table `examset_questions`
--

CREATE TABLE `examset_questions` (
  `id` bigint(20) NOT NULL,
  `examset_code` varchar(50) DEFAULT NULL,
  `id_examset` bigint(20) DEFAULT NULL,
  `id_question` bigint(20) DEFAULT NULL,
  `id_user` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_dt_tm` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_dt_tm` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `examset_questions`
--

INSERT INTO `examset_questions` (`id`, `examset_code`, `id_examset`, `id_question`, `id_user`, `status`, `created_dt_tm`, `updated_dt_tm`) VALUES
(19, 'EXM742', 6, 4, 3, 1, '2020-09-04 13:43:03', '2020-09-04 13:43:03'),
(20, 'EXM742', 6, 6, 3, 1, '2020-09-04 13:43:03', '2020-09-04 13:43:03'),
(21, 'EXM742', 6, 8, 3, 1, '2020-09-04 13:43:03', '2020-09-04 13:43:03'),
(22, 'EXM742', 6, 7, 3, 1, '2020-09-04 13:43:03', '2020-09-04 13:43:03'),
(23, 'EXM742', 6, 1, 3, 1, '2020-09-04 13:43:03', '2020-09-04 13:43:03'),
(24, 'EXM742', 6, 10, 3, 1, '2020-09-04 13:43:03', '2020-09-04 13:43:03'),
(25, 'EXM518', 6, 5, 3, 1, '2020-09-04 13:43:13', '2020-09-04 13:43:13'),
(26, 'EXM518', 6, 4, 3, 1, '2020-09-04 13:43:13', '2020-09-04 13:43:13'),
(27, 'EXM518', 6, 8, 3, 1, '2020-09-04 13:43:13', '2020-09-04 13:43:13'),
(28, 'EXM518', 6, 7, 3, 1, '2020-09-04 13:43:13', '2020-09-04 13:43:13'),
(29, 'EXM518', 6, 2, 3, 1, '2020-09-04 13:43:13', '2020-09-04 13:43:13'),
(30, 'EXM518', 6, 10, 3, 1, '2020-09-04 13:43:13', '2020-09-04 13:43:13'),
(31, 'EXM202009040146pm', 6, 5, 3, 1, '2020-09-04 13:46:24', '2020-09-04 13:46:24'),
(32, 'EXM202009040146pm', 6, 4, 3, 1, '2020-09-04 13:46:24', '2020-09-04 13:46:24'),
(33, 'EXM202009040146pm', 6, 8, 3, 1, '2020-09-04 13:46:24', '2020-09-04 13:46:24'),
(34, 'EXM202009040146pm', 6, 7, 3, 1, '2020-09-04 13:46:24', '2020-09-04 13:46:24'),
(35, 'EXM202009040146pm', 6, 2, 3, 1, '2020-09-04 13:46:24', '2020-09-04 13:46:24'),
(36, 'EXM202009040146pm', 6, 10, 3, 1, '2020-09-04 13:46:24', '2020-09-04 13:46:24'),
(37, 'EXM202009040146', 6, 6, 3, 1, '2020-09-04 13:46:46', '2020-09-04 13:46:46'),
(38, 'EXM202009040146', 6, 5, 3, 1, '2020-09-04 13:46:46', '2020-09-04 13:46:46'),
(39, 'EXM202009040146', 6, 8, 3, 1, '2020-09-04 13:46:46', '2020-09-04 13:46:46'),
(40, 'EXM202009040146', 6, 7, 3, 1, '2020-09-04 13:46:46', '2020-09-04 13:46:46'),
(41, 'EXM202009040146', 6, 2, 3, 1, '2020-09-04 13:46:46', '2020-09-04 13:46:46'),
(42, 'EXM202009040146', 6, 10, 3, 1, '2020-09-04 13:46:46', '2020-09-04 13:46:46'),
(43, 'EXM20200904084547', 6, 5, 3, 1, '2020-09-04 20:45:47', '2020-09-04 20:45:47'),
(44, 'EXM20200904084547', 6, 6, 3, 1, '2020-09-04 20:45:47', '2020-09-04 20:45:47'),
(45, 'EXM20200904084547', 6, 8, 3, 1, '2020-09-04 20:45:47', '2020-09-04 20:45:47'),
(46, 'EXM20200904084547', 6, 7, 3, 1, '2020-09-04 20:45:47', '2020-09-04 20:45:47'),
(47, 'EXM20200904084547', 6, 2, 3, 1, '2020-09-04 20:45:47', '2020-09-04 20:45:47'),
(48, 'EXM20200904084547', 6, 10, 3, 1, '2020-09-04 20:45:47', '2020-09-04 20:45:47'),
(49, 'EXM20200906011804', 6, 6, 3, 1, '2020-09-06 13:18:04', '2020-09-06 13:18:04'),
(50, 'EXM20200906011804', 6, 4, 3, 1, '2020-09-06 13:18:04', '2020-09-06 13:18:04'),
(51, 'EXM20200906011804', 6, 8, 3, 1, '2020-09-06 13:18:04', '2020-09-06 13:18:04'),
(52, 'EXM20200906011804', 6, 7, 3, 1, '2020-09-06 13:18:04', '2020-09-06 13:18:04'),
(53, 'EXM20200906011804', 6, 1, 3, 1, '2020-09-06 13:18:04', '2020-09-06 13:18:04'),
(54, 'EXM20200906011804', 6, 10, 3, 1, '2020-09-06 13:18:04', '2020-09-06 13:18:04'),
(55, 'EXM20200906054151', 6, 5, 4, 1, '2020-09-06 17:41:51', '2020-09-06 17:41:51'),
(56, 'EXM20200906054151', 6, 8, 4, 1, '2020-09-06 17:41:51', '2020-09-06 17:41:51'),
(57, 'EXM20200906054151', 6, 7, 4, 1, '2020-09-06 17:41:51', '2020-09-06 17:41:51'),
(58, 'EXM20200906054151', 6, 2, 4, 1, '2020-09-06 17:41:51', '2020-09-06 17:41:51'),
(59, 'EXM20200906054151', 6, 10, 4, 1, '2020-09-06 17:41:51', '2020-09-06 17:41:51'),
(60, 'EXM20200906055844', 6, 6, 4, 1, '2020-09-06 05:28:44', '2020-09-06 05:28:44'),
(61, 'EXM20200906055844', 6, 8, 4, 1, '2020-09-06 05:28:44', '2020-09-06 05:28:44'),
(62, 'EXM20200906055844', 6, 7, 4, 1, '2020-09-06 05:28:44', '2020-09-06 05:28:44'),
(63, 'EXM20200906055844', 6, 2, 4, 1, '2020-09-06 05:28:44', '2020-09-06 05:28:44'),
(64, 'EXM20200906055844', 6, 10, 4, 1, '2020-09-06 05:28:44', '2020-09-06 05:28:44'),
(65, 'EXM20200923082352', 6, 5, 4, 1, '2020-09-22 19:53:52', '2020-09-22 19:53:52'),
(66, 'EXM20200923082352', 6, 8, 4, 1, '2020-09-22 19:53:52', '2020-09-22 19:53:52'),
(67, 'EXM20200923082352', 6, 7, 4, 1, '2020-09-22 19:53:52', '2020-09-22 19:53:52'),
(68, 'EXM20200923082352', 6, 1, 4, 1, '2020-09-22 19:53:52', '2020-09-22 19:53:52'),
(69, 'EXM20200923082352', 6, 10, 4, 1, '2020-09-22 19:53:52', '2020-09-22 19:53:52'),
(70, 'EXM20200923082835', 6, 4, 4, 1, '2020-09-22 19:58:35', '2020-09-22 19:58:35'),
(71, 'EXM20200923082835', 6, 8, 4, 1, '2020-09-22 19:58:35', '2020-09-22 19:58:35'),
(72, 'EXM20200923082835', 6, 7, 4, 1, '2020-09-22 19:58:35', '2020-09-22 19:58:35'),
(73, 'EXM20200923082835', 6, 1, 4, 1, '2020-09-22 19:58:35', '2020-09-22 19:58:35'),
(74, 'EXM20200923082835', 6, 10, 4, 1, '2020-09-22 19:58:35', '2020-09-22 19:58:35'),
(75, 'EXM20200923083056', 6, 5, 4, 1, '2020-09-22 20:00:56', '2020-09-22 20:00:56'),
(76, 'EXM20200923083056', 6, 8, 4, 1, '2020-09-22 20:00:56', '2020-09-22 20:00:56'),
(77, 'EXM20200923083056', 6, 7, 4, 1, '2020-09-22 20:00:56', '2020-09-22 20:00:56'),
(78, 'EXM20200923083056', 6, 2, 4, 1, '2020-09-22 20:00:56', '2020-09-22 20:00:56'),
(79, 'EXM20200923083056', 6, 10, 4, 1, '2020-09-22 20:00:56', '2020-09-22 20:00:56'),
(80, 'EXM20200923083058', 6, 5, 4, 1, '2020-09-22 20:00:58', '2020-09-22 20:00:58'),
(81, 'EXM20200923083058', 6, 8, 4, 1, '2020-09-22 20:00:58', '2020-09-22 20:00:58'),
(82, 'EXM20200923083058', 6, 7, 4, 1, '2020-09-22 20:00:58', '2020-09-22 20:00:58'),
(83, 'EXM20200923083058', 6, 2, 4, 1, '2020-09-22 20:00:58', '2020-09-22 20:00:58'),
(84, 'EXM20200923083058', 6, 10, 4, 1, '2020-09-22 20:00:58', '2020-09-22 20:00:58');

-- --------------------------------------------------------

--
-- Table structure for table `exam_center`
--

CREATE TABLE `exam_center` (
  `id` int(20) NOT NULL,
  `name` varchar(580) DEFAULT '',
  `id_tos` int(20) DEFAULT 0,
  `contact_person` varchar(512) DEFAULT '',
  `contact_number` varchar(20) DEFAULT '',
  `email` varchar(512) DEFAULT '',
  `user_name` varchar(512) DEFAULT '',
  `password` varchar(512) DEFAULT '',
  `exam_type` varchar(512) DEFAULT '',
  `rooms` int(20) DEFAULT 0,
  `room_capacity` int(20) DEFAULT 0,
  `id_country` int(20) DEFAULT NULL,
  `id_state` int(20) DEFAULT NULL,
  `city` varchar(250) DEFAULT '',
  `id_location` int(20) DEFAULT 0,
  `zipcode` varchar(50) DEFAULT '',
  `address` varchar(580) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_center`
--

INSERT INTO `exam_center` (`id`, `name`, `id_tos`, `contact_person`, `contact_number`, `email`, `user_name`, `password`, `exam_type`, `rooms`, `room_capacity`, `id_country`, `id_state`, `city`, `id_location`, `zipcode`, `address`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'CIIF', 6, 'Center 1', 'Center 1', 'examcenter1@gmail.com', '123', '202cb962ac59075b964b07152d234b70', 'Both', 2, 0, 1, 1, 'Kuala Lumpur', 1, '3213123', 'B01-A-06-1, Level 6', 1, 1, '2020-08-15 11:55:57', 1, '2020-08-15 11:55:57'),
(2, 'Martinal Central Exam Hall', 6, 'Yagandar', '32321123', 'mc@ec.com', '1123', '202cb962ac59075b964b07152d234b70', 'Face To Face', 10, 0, 1, 1, 'Bengaluru', 1, '123', '123, Bengaluru', 1, 1, '2020-08-15 09:24:13', NULL, '2020-08-15 09:24:13'),
(3, 'University Center 2', 0, 'Muzamil', '368271231', 'econe@exam.com', '1288', 'd9b1d7db4cd6e70935368a1efb10e377', 'Face to Face', 1, 0, 1, 2, 'KL', 1, '12312', 'KL', 1, 1, '2020-10-03 00:53:56', NULL, '2020-10-03 00:53:56');

-- --------------------------------------------------------

--
-- Table structure for table `exam_center_has_room`
--

CREATE TABLE `exam_center_has_room` (
  `id` int(20) NOT NULL,
  `id_exam_center` int(20) DEFAULT 0,
  `room` varchar(120) DEFAULT '',
  `capacity` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(2) DEFAULT 0,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(2) DEFAULT 0,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_center_has_room`
--

INSERT INTO `exam_center_has_room` (`id`, `id_exam_center`, `room`, `capacity`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(4, 1, 'Room 1', 12, 1, 1, '2020-08-15 13:11:43', 0, '2020-08-15 13:11:43'),
(5, 1, 'Room 2', 10, 1, 1, '2020-08-15 13:12:21', 0, '2020-08-15 13:12:21');

-- --------------------------------------------------------

--
-- Table structure for table `exam_center_last_login`
--

CREATE TABLE `exam_center_last_login` (
  `id` bigint(20) NOT NULL,
  `id_exam_center` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exam_center_last_login`
--

INSERT INTO `exam_center_last_login` (`id`, `id_exam_center`, `session_data`, `machine_ip`, `user_agent`, `agent_string`, `platform`, `created_dt_tm`) VALUES
(0, 2, '{\"exam_center_name\":\"Martinal Central Exam Hall\",\"id_exam_center_location\":\"1\"}', '117.230.9.7', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-08-15 09:24:54');

-- --------------------------------------------------------

--
-- Table structure for table `exam_center_location`
--

CREATE TABLE `exam_center_location` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_center_location`
--

INSERT INTO `exam_center_location` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Kuala Lumpur', '', 1, NULL, '2020-08-15 11:50:10', NULL, '2020-08-15 11:50:10');

-- --------------------------------------------------------

--
-- Table structure for table `exam_center_start_exam`
--

CREATE TABLE `exam_center_start_exam` (
  `id` int(20) NOT NULL,
  `id_exam_event` int(20) DEFAULT NULL,
  `exam_start_time` datetime DEFAULT NULL,
  `exam_status` int(20) DEFAULT NULL,
  `id_exam_center` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_center_start_exam`
--

INSERT INTO `exam_center_start_exam` (`id`, `id_exam_event`, `exam_start_time`, `exam_status`, `id_exam_center`) VALUES
(1, 1, '2020-10-04 12:36:19', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `exam_event`
--

CREATE TABLE `exam_event` (
  `id` int(20) NOT NULL,
  `id_location` int(20) DEFAULT 0,
  `id_exam_center` int(20) DEFAULT 0,
  `id_exam_set` int(20) DEFAULT 0,
  `max_count` int(20) DEFAULT 0,
  `name` varchar(250) DEFAULT '',
  `type` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `exam_date` date DEFAULT NULL,
  `to_dt` varchar(50) DEFAULT '',
  `from_tm` varchar(50) DEFAULT '',
  `to_tm` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL,
  `id_exam_name` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_event`
--

INSERT INTO `exam_event` (`id`, `id_location`, `id_exam_center`, `id_exam_set`, `max_count`, `name`, `type`, `code`, `exam_date`, `to_dt`, `from_tm`, `to_tm`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`, `id_exam_name`) VALUES
(1, 1, 1, 1, 12, 'CIIF-OCt-3-KL', 'Exam', '', '2020-10-04', '', '15:00', '20:00', 1, 1, '2020-08-15 16:10:20', 1, '2020-08-15 16:10:20', 1),
(2, 1, 3, 1, 10, 'Examination Dec 2020', 'Exam', '', '2020-12-01', '', '10:00', '12:00', 1, 1, '2020-10-03 00:58:41', NULL, '2020-10-03 00:58:41', 1);

-- --------------------------------------------------------

--
-- Table structure for table `exam_has_question`
--

CREATE TABLE `exam_has_question` (
  `id` int(20) NOT NULL,
  `id_exam_name` int(20) DEFAULT 0,
  `id_exam_has_count_details` int(20) DEFAULT 0,
  `id_question` int(20) DEFAULT 0,
  `id_course` int(20) DEFAULT 0,
  `id_topic` int(20) DEFAULT 0,
  `id_course_learning_objective` int(20) DEFAULT 0,
  `id_bloom_taxonomy` int(20) DEFAULT 0,
  `id_difficult_level` int(20) DEFAULT 0,
  `question_count` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `exam_has_question_count`
--

CREATE TABLE `exam_has_question_count` (
  `id` int(20) NOT NULL,
  `id_exam_name` int(20) DEFAULT 0,
  `id_course` int(20) DEFAULT 0,
  `id_topic` int(20) DEFAULT 0,
  `id_course_learning_objective` int(20) DEFAULT 0,
  `id_bloom_taxonomy` int(20) DEFAULT 0,
  `id_difficult_level` int(20) DEFAULT 0,
  `question_count` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_has_question_count`
--

INSERT INTO `exam_has_question_count` (`id`, `id_exam_name`, `id_course`, `id_topic`, `id_course_learning_objective`, `id_bloom_taxonomy`, `id_difficult_level`, `question_count`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 0, 2, 1, 2, 3, 1, 1, 1, 1, '2020-08-15 16:47:00', NULL, '2020-08-15 16:47:00'),
(2, 0, 2, 1, 2, 3, 1, 1, 1, 1, '2020-08-15 16:47:50', NULL, '2020-08-15 16:47:50'),
(3, 0, 2, 1, 2, 3, 1, 1, 1, 1, '2020-08-15 16:49:02', NULL, '2020-08-15 16:49:02');

-- --------------------------------------------------------

--
-- Table structure for table `exam_name`
--

CREATE TABLE `exam_name` (
  `id` int(20) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `description` varchar(4096) DEFAULT '',
  `total_marks` int(20) DEFAULT 0,
  `total_question` int(20) DEFAULT 0,
  `total_time` int(20) DEFAULT 0,
  `min_marks` int(20) DEFAULT 0,
  `display_result_immediately` int(2) DEFAULT 0,
  `image` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_name`
--

INSERT INTO `exam_name` (`id`, `name`, `description`, `total_marks`, `total_question`, `total_time`, `min_marks`, `display_result_immediately`, `image`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Online CIIF Exam', 'Online CIIF Exam', 0, 0, 0, 0, 0, '', 1, 1, '2020-08-15 16:50:46', 1, '2020-08-15 16:50:46');

-- --------------------------------------------------------

--
-- Table structure for table `exam_session`
--

CREATE TABLE `exam_session` (
  `id` int(20) NOT NULL,
  `name` varchar(512) DEFAULT '',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `id_exam_center` int(20) DEFAULT 0,
  `id_exam_set` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(2) DEFAULT 0,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(2) DEFAULT 0,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `exam_set`
--

CREATE TABLE `exam_set` (
  `id` int(20) NOT NULL,
  `name` varchar(512) DEFAULT '',
  `instruction` varchar(5120) DEFAULT '',
  `capacity` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(2) DEFAULT 0,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(2) DEFAULT 0,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_set`
--

INSERT INTO `exam_set` (`id`, `name`, `instruction`, `capacity`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Nam', '', 0, 1, 0, '2020-08-15 13:35:15', 0, '2020-08-15 13:35:15');

-- --------------------------------------------------------

--
-- Table structure for table `exam_student_tagging`
--

CREATE TABLE `exam_student_tagging` (
  `id` int(20) NOT NULL,
  `id_student` int(20) DEFAULT 0,
  `id_exam_event` int(20) DEFAULT 0,
  `attendence_status` int(20) DEFAULT NULL,
  `extra_time` varchar(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_student_tagging`
--

INSERT INTO `exam_student_tagging` (`id`, `id_student`, `id_exam_event`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`, `attendence_status`, `extra_time`) VALUES
(1, 1, 1, 1, 1, '2020-10-02 23:11:10', NULL, '2020-10-02 23:11:10', 1, NULL),
(2, 1, 1, 1, 1, '2020-10-03 00:57:43', NULL, '2020-10-03 00:57:43', NULL, NULL),
(3, 3, 2, 1, 1, '2020-10-03 01:06:44', NULL, '2020-10-03 01:06:44', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fee_category`
--

CREATE TABLE `fee_category` (
  `id` int(20) NOT NULL,
  `code` varchar(520) DEFAULT '',
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `fee_group` varchar(200) DEFAULT '',
  `sequence` int(2) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fee_category`
--

INSERT INTO `fee_category` (`id`, `code`, `name`, `name_optional_language`, `fee_group`, `sequence`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'SF', 'Student Fee', 'Student Fee', 'Rental', 1, 1, NULL, '2020-09-30 02:23:33', NULL, '2020-09-30 02:23:33');

-- --------------------------------------------------------

--
-- Table structure for table `fee_setup`
--

CREATE TABLE `fee_setup` (
  `id` int(20) NOT NULL,
  `code` varchar(50) DEFAULT '',
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `id_fee_category` int(10) DEFAULT NULL,
  `id_amount_calculation_type` int(10) DEFAULT NULL,
  `id_frequency_mode` int(10) DEFAULT NULL,
  `account_code` varchar(50) DEFAULT '',
  `is_refundable` int(2) DEFAULT NULL,
  `is_non_invoice` int(2) DEFAULT NULL,
  `is_gst` int(2) DEFAULT NULL,
  `gst_tax` varchar(50) DEFAULT '',
  `effective_date` datetime DEFAULT current_timestamp(),
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fee_setup`
--

INSERT INTO `fee_setup` (`id`, `code`, `name`, `name_optional_language`, `id_fee_category`, `id_amount_calculation_type`, `id_frequency_mode`, `account_code`, `is_refundable`, `is_non_invoice`, `is_gst`, `gst_tax`, `effective_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'RF', 'Registration Fee', 'Registration Fee', 1, 0, 0, '1', 0, 0, 0, '123', '2020-09-19 00:00:00', 1, NULL, '2020-09-30 02:24:40', NULL, '2020-09-30 02:24:40');

-- --------------------------------------------------------

--
-- Table structure for table `financial_account_code`
--

CREATE TABLE `financial_account_code` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `type` varchar(512) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `short_code` varchar(50) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `level` int(2) DEFAULT 0,
  `id_parent` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `financial_account_code`
--

INSERT INTO `financial_account_code` (`id`, `name`, `type`, `name_optional_language`, `short_code`, `code`, `level`, `id_parent`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, '', 'Permanent', '', '', '002288', 0, 0, 1, 1, '2020-09-30 02:24:17', NULL, '2020-09-30 02:24:17');

-- --------------------------------------------------------

--
-- Table structure for table `grade`
--

CREATE TABLE `grade` (
  `id` int(20) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `name_optional_language` varchar(1024) DEFAULT '',
  `min_percentage` int(20) DEFAULT NULL,
  `max_percentage` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grade`
--

INSERT INTO `grade` (`id`, `name`, `code`, `name_optional_language`, `min_percentage`, `max_percentage`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Pass', '', '', 70, 100, 1, 1, '2020-10-01 20:52:41', 1, '2020-10-01 20:52:41'),
(2, 'Fail', '', '', 1, 69, 1, 1, '2020-10-01 20:52:50', 1, '2020-10-01 20:52:50');

-- --------------------------------------------------------

--
-- Table structure for table `main_invoice`
--

CREATE TABLE `main_invoice` (
  `id` int(20) NOT NULL,
  `type` varchar(50) DEFAULT '',
  `invoice_number` varchar(50) DEFAULT '',
  `date_time` datetime DEFAULT current_timestamp(),
  `remarks` varchar(520) DEFAULT '',
  `id_student` int(10) DEFAULT NULL,
  `currency` varchar(200) DEFAULT '',
  `total_amount` float(20,2) DEFAULT 0.00,
  `invoice_total` float(20,2) DEFAULT 0.00,
  `total_discount` float(20,2) DEFAULT 0.00,
  `balance_amount` float(20,2) DEFAULT 0.00,
  `paid_amount` float(20,2) DEFAULT 0.00,
  `reason` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_invoice`
--

INSERT INTO `main_invoice` (`id`, `type`, `invoice_number`, `date_time`, `remarks`, `id_student`, `currency`, `total_amount`, `invoice_total`, `total_discount`, `balance_amount`, `paid_amount`, `reason`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Student', 'INV000001/2020', '2020-09-30 02:25:10', '', 1, '1', 1470.00, 1470.00, 0.00, 1347.00, 123.00, '', 1, 1, '2020-09-30 02:25:10', NULL, '2020-09-30 02:25:10'),
(2, 'Student', 'INV000002/2020', '2020-10-01 02:28:49', '', 4, '1', 12.00, 12.00, 0.00, 12.00, 0.00, '', 1, 1, '2020-10-01 02:28:49', NULL, '2020-10-01 02:28:49');

-- --------------------------------------------------------

--
-- Table structure for table `main_invoice_details`
--

CREATE TABLE `main_invoice_details` (
  `id` int(20) NOT NULL,
  `id_main_invoice` int(10) DEFAULT NULL,
  `id_fee_item` int(10) DEFAULT NULL,
  `price` int(20) DEFAULT 0,
  `quantity` int(20) DEFAULT 0,
  `amount` float(20,2) DEFAULT NULL,
  `id_reference` int(20) DEFAULT 0,
  `description` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_invoice_details`
--

INSERT INTO `main_invoice_details` (`id`, `id_main_invoice`, `id_fee_item`, `price`, `quantity`, `amount`, `id_reference`, `description`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 0, 0, 1290.00, 0, '', NULL, NULL, '2020-09-30 02:24:57', NULL, '2020-09-30 02:24:57'),
(2, 1, 1, 0, 0, 180.00, 0, '', NULL, NULL, '2020-09-30 02:25:06', NULL, '2020-09-30 02:25:06'),
(3, 2, 1, 0, 0, 12.00, 0, '', NULL, NULL, '2020-10-01 02:28:43', NULL, '2020-10-01 02:28:43');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(20) NOT NULL,
  `menu_name` varchar(200) DEFAULT NULL,
  `module_name` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `menu_name`, `module_name`) VALUES
(1, 'User', 'Setup'),
(2, 'Course', NULL),
(3, 'Topic', 'Setup'),
(4, 'Bloom Taxonomy', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payment_type`
--

CREATE TABLE `payment_type` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `description` varchar(2048) DEFAULT '',
  `description_optional_language` varchar(2048) DEFAULT '',
  `payment_group` varchar(512) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `name_in_malay` varchar(520) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_type`
--

INSERT INTO `payment_type` (`id`, `name`, `description`, `description_optional_language`, `payment_group`, `code`, `name_in_malay`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, '', 'Cash Payment', '', 'Cash', 'Cash', '', 1, NULL, '2020-08-20 10:08:10', NULL, '2020-08-20 10:08:10'),
(2, '', 'Bank Checue Transfer', '', 'Checque', 'Bank Transfer', '', 1, NULL, '2020-08-20 10:09:30', NULL, '2020-08-20 10:09:30');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) NOT NULL,
  `code` varchar(100) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `id_menu` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `code`, `description`, `status`, `id_menu`) VALUES
(5, 'student.studentexams', 'Student Exam page view', 1, NULL),
(6, 'role.list', 'List Role', 1, NULL),
(7, 'permission.add', 'Add Permission', 1, NULL),
(8, 'permission.list', 'View Permission', 1, NULL),
(9, 'permission.edit', 'Edit Permission', 1, NULL),
(10, 'user.edit', 'Edit User', 1, 1),
(11, 'user.add', 'Add User', 1, 1),
(12, 'user.list', 'View User', 1, 1),
(13, 'course.list', 'View Course', 1, 2),
(14, 'course.add', 'Add Course', 1, 2),
(15, 'course.edit', 'Edit Course', 1, 2),
(16, 'topic.edit', 'Edit Topic', 1, NULL),
(17, 'topic.add', 'Add Topic', 1, NULL),
(18, 'topic.list', 'View Topic', 1, NULL),
(19, 'learningobjective.list', 'View Learning Objective', 1, NULL),
(20, 'learningobjective.add', 'Add Learning Objective', 1, NULL),
(21, 'learningobjective.list', 'View Learning Objective', 1, NULL),
(22, 'taxonomy.list', 'View Blood Taxonomy', 1, NULL),
(23, 'taxonomy.add', 'Add Blood Taxonomy', 1, NULL),
(24, 'taxonomy.edit', 'Edit Blood Taxonomy', 1, NULL),
(25, 'difficultylevel.edit', 'Edit Difficulty Level', 1, NULL),
(26, 'difficultylevel.add', 'Add Difficulty Level', 1, NULL),
(27, 'difficultylevel.list', 'View Difficulty Level', 1, NULL),
(28, 'question.list', 'View Question', 1, NULL),
(29, 'question.add', 'Add Question', 1, NULL),
(30, 'question.edit', 'Edit Question', 1, NULL),
(31, 'questionhasoption.add', 'Add Question Options', 1, NULL),
(33, 'exam_name.list', 'List Exam Name', 1, NULL),
(34, 'exam_name.add', 'Add Exam Name', 1, NULL),
(35, 'exam_name.edit', 'Edit Exam Name', 1, NULL),
(36, 'exam_center.list', 'View Exam Center', 1, NULL),
(37, 'exam_center.add', 'Add Exam Center', 1, NULL),
(38, 'exam_center.edit', 'Edit Exam Center', 1, NULL),
(39, 'exam_event.edit', 'Edit Exam Event', 1, NULL),
(40, 'exam_event.list', 'View Exam Event', 1, NULL),
(41, 'exam_event.add', 'Add Exam Event', 1, NULL),
(42, 'exam_has_question.add', 'Add Exam Has Question', 1, NULL),
(43, 'exam_has_question.edit', 'Edit Exam Has Question', 1, NULL),
(44, 'role.add', 'Add Role', 1, NULL),
(45, 'exam_has_question.list', 'View Exam Has Question', 1, NULL),
(46, 'grade.list', 'View Grade', 1, NULL),
(47, 'grade.add', 'Add Grade', 1, NULL),
(48, 'grade.edit', 'Edit Grade', 1, NULL),
(49, 'location.edit', 'Edit Location', 1, NULL),
(50, 'location.add', 'Add Location', 1, NULL),
(51, 'location.list', 'View Location', 1, NULL),
(52, 'questionpool.list', 'List Question Pool', 1, NULL),
(53, 'questionpool.add', 'Add Question Pool', 1, NULL),
(54, 'questionpool.edit', 'Edit Question Pool', 1, NULL),
(55, 'tos.add', 'Add TOS', 1, NULL),
(56, 'tos.list', 'View TOS', 1, NULL),
(57, 'tos.edit', 'Edit TOS', 1, NULL),
(58, 'examset.add', 'Add Exam Set', 1, NULL),
(59, 'examset.list', 'View Exam Set', 1, NULL),
(60, 'examset.edit', 'Edit Exam Set', 1, NULL),
(61, 'student.list', 'List Student', 1, NULL),
(62, 'student.add', 'Add Student', 1, NULL),
(63, 'student.edit', 'Edit Student', 1, NULL),
(64, 'exam_student_tagging.add', 'Add Exam Student Tagging', 1, NULL),
(65, 'exam_student_tagging.list', 'View Exam Student Tagging', 1, NULL),
(66, 'exam_student_tagging.edit', 'Edit Exam Student Tagging', 1, NULL),
(67, 'role.edit', 'Role Edit', 1, NULL),
(68, 'student.examanswers', 'Student Exam answer', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE `question` (
  `id` int(20) NOT NULL,
  `question` text NOT NULL,
  `id_pool` bigint(20) NOT NULL,
  `id_course` int(20) DEFAULT 0,
  `id_topic` int(20) DEFAULT 0,
  `id_course_learning_objective` int(20) DEFAULT 0,
  `id_bloom_taxonomy` int(20) DEFAULT 0,
  `id_difficult_level` int(20) DEFAULT 0,
  `image` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL,
  `marks` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`id`, `question`, `id_pool`, `id_course`, `id_topic`, `id_course_learning_objective`, `id_bloom_taxonomy`, `id_difficult_level`, `image`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`, `marks`) VALUES
(1, '<p>The word ‘<strong>Shariah</strong>’ has been defined differently by Islamic Scholars in the past. Which of the following statements are correct technical definitions of Shariah?</p>\n\n<ol>\n <li>Canon law of Islam which stipulated all the commandments of Allah (SWT) to mankind.</li>\n <li>Sum of Islamic legal rulings.</li>\n <li>Divine law enacted for creatures of Allah (SWT) which are deduced either from the Qur’an or the Sunnah of the Prophet (SAW) from his sayings, actions or tacit approval.</li>\n <li>Sum of commandments and divine laws relating to belief and conduct which adoption in Islam has been made compulsory to attain its welfare objective in the community.</li>\n</ol>\n', 1, 1, 5, 0, 1, 1, '', 1, 1, '2020-08-14 18:36:32', 1, '2020-08-14 18:36:32', 1),
(2, '<p>Which of the following statements pertaining to Shariah compliance are correct?</p>\r\n\r\n<ol>\r\n <li>Shariah compliance refers to products or businesses that satisfy Shariah requirements irrespective of whether the products originate from conventional market or Islamic finance market. </li>\r\n <li>Shariah-compliant products are products that originate from recognised Shariah concepts and contract.</li>\r\n <li>Shariah-based products are products which originate from the conventional market and have passed through various screenings and modifications to satisfy the Shariah requirements of valid financial contracts. </li>\r\n <li>The application of Shariah compliance is stated in the Islamic Financial Services Act 2013.</li>\r\n</ol>\r\n', 1, 1, 5, 4, 1, 1, '', 1, 1, '2020-08-14 18:37:29', 1, '2020-08-14 18:37:29', NULL),
(4, '<p>Islamic Scholars have classified the sources of (Shariah) rulings into various categories based on different perspectives and considerations. In terms of classification of rulings based on origin, choose which of the following is a correct primary source of rulings.</p>\n', 1, 1, 2, 4, 1, 2, '', 1, 1, '2020-08-14 19:15:38', 1, '2020-08-14 19:15:38', NULL),
(5, '<p>Which of the following statements are TRUE pertaining to Fiqh</p>\r\n\r\n<ol>\r\n <li>Fiqh is an understanding of Shariah rulings, and not the Shariah.</li>\r\n <li>Fiqh may develop its ruling from the Qur’an and Sunnah as well as other supplementary sources of rulings recognised by Shariah.</li>\r\n <li>Fiqh is mutable because its is to some extent subjective from person to person, based on their understanding of Shariah rules.</li>\r\n <li>Fiqh is the knowledge and understanding of all Shariah rulings pertaining to financial transactions only</li>\r\n</ol>\r\n', 1, 1, 2, 2, 1, 2, '', 1, 1, '2020-08-14 19:16:12', 1, '2020-08-14 19:16:12', NULL),
(6, '<p>Which of the following statements is NOT TRUE pertaining to the salient features of Shariah</p>\r\n', 1, 1, 2, 2, 1, 2, '', 1, 1, '2020-08-14 19:16:13', 1, '2020-08-14 19:16:13', NULL),
(7, '<p>Muslim jurists and scholars asserts that there are ____________ dimensions which Maqasid as-Shariah relates to. <br>\r\nFill in the blank with the correct answer</p>\r\n', 1, 1, 3, 3, 1, 3, '', 1, 1, '2020-08-14 19:16:42', 1, '2020-08-14 19:16:42', NULL),
(8, '<p>Which of the following transactions displays riba al-nasiah?</p>\r\n\r\n<ol>\r\n <li>An exchange of good quality dates with bad quality dates.</li>\r\n <li>An exchange of unequal amount of gold.</li>\r\n <li>Surcharge of deferred payment in an agreement of sale and purchase.</li>\r\n <li>The interest amount on top of the principal amount of a conventional loan agreement.</li>\r\n</ol>\r\n', 1, 1, 2, 3, 1, 3, '', 1, 1, '2020-08-14 19:17:06', 1, '2020-08-14 19:17:06', NULL),
(10, '<p>Every commercial transactions that involves ______________ has element of ________________. <br>\r\nFill in the blanks with the correct word:</p>\r\n', 2, 2, 1, 4, 4, 3, '', 1, 1, '2020-08-14 19:47:23', 1, '2020-08-14 19:47:23', NULL),
(11, '<p>Which of the following is NOT a bilateral contract?</p>\r\n', 1, 2, 1, 4, 4, 3, '', 1, 1, '2020-10-01 20:02:57', NULL, '2020-10-01 20:02:57', NULL),
(12, '<p>Which of the following contract is void?</p>\r\n', 1, 2, 6, 4, 4, 3, '', 1, 1, '2020-10-01 20:05:09', NULL, '2020-10-01 20:05:09', NULL),
(13, '<p>Which of the following situations indicate that the intention to purchase and sell a merchandise has been concluded?</p>\r\n', 1, 2, 1, 4, 4, 1, '', 1, 1, '2020-10-01 20:06:54', NULL, '2020-10-01 20:06:54', NULL),
(14, '<p>Which of the following statements are TRUE pertaining to the theory of contract within the context of Shariah law?</p>\r\n', 1, 2, 1, 0, 4, 3, '', 1, 1, '2020-10-02 14:51:28', NULL, '2020-10-02 14:51:28', 12),
(15, '<p>There are four (4) major Schools of Fiqh which have been reigning schools of thoughts in Islamic law. These major Schools of Fiqh are Hanafi, Hanbali, Shafi’i and ________________.<br>\r\n </p>\r\n', 1, 3, 7, 0, 4, 1, '', 1, 1, '2020-10-02 14:52:33', NULL, '2020-10-02 14:52:33', 1),
(16, '<p>Fiqh maxims (Qawaid Fiqhiyyah) can be defined as comprehensive or predominant rule which is applicable to all or most of its relevant cases by which their rulings can be derived. In total, there are _____________leading Fiqh maxims.</p>\r\n', 1, 2, 1, 0, 4, 3, '', 1, 1, '2020-10-02 14:53:22', NULL, '2020-10-02 14:53:22', 1),
(17, '<p>Of the following sources of the Shariah, identify the one which is acceptable to and agreed upon by all the scholars:</p>\r\n', 1, 2, 1, 0, 3, 3, '', 1, 1, '2020-10-02 14:54:08', NULL, '2020-10-02 14:54:08', 1),
(18, '<p>Accountability, confidentiality, competency, integrity, and objectivity are the five (5) key principles that underpin CIIF’s __________________ which all CIIF<br>\r\n members are expected to adhere to at all time. </p>\r\n', 1, 2, 1, 0, 4, 3, '', 1, 1, '2020-10-02 14:55:05', NULL, '2020-10-02 14:55:05', 1),
(19, '<p>The CIIF Ethical Framework aims to promote professionalism that is centred on the role of individuals as _________________ that has implications in all aspects of life including economics, business, and finance.</p>\r\n', 1, 2, 1, 0, 4, 3, '', 1, 1, '2020-10-02 14:56:37', NULL, '2020-10-02 14:56:37', 1),
(20, '<p>Apart from the CIIF’s overarching Guiding Philosophy, Code of Ethics, and Standards of Professional Conduct, Members needs to also refer to the CIIF’s Members’ Handbook. The CIIF’s Members’ Handbook is essentially a set of comprehensive rules and procedures that governs matters relating to CIIF’s _______________.</p>\r\n', 1, 2, 1, 0, 4, 1, '', 1, 1, '2020-10-02 14:57:24', NULL, '2020-10-02 14:57:24', 1),
(21, '<p>Which of the following are NOT the rules of professional conduct according to the CIIF Standards of Professional Conduct? </p>\r\n', 1, 2, 1, 0, 4, 1, '', 1, 1, '2020-10-02 14:58:12', NULL, '2020-10-02 14:58:12', 1),
(22, '<p>Being truthful, honest and to deal fairly and equitably in all professional and business relationships is describing which of the following principles under the CIIF’s Code of Ethics.</p>\r\n', 1, 2, 1, 0, 4, 3, '', 1, 1, '2020-10-02 15:01:23', NULL, '2020-10-02 15:01:23', 1),
(23, '<p>What is the primary role of the Islamic Finance Professional?</p>\r\n', 1, 2, 1, 0, 4, 3, '', 1, 1, '2020-10-02 15:10:43', NULL, '2020-10-02 15:10:43', 1);

-- --------------------------------------------------------

--
-- Table structure for table `question_has_option`
--

CREATE TABLE `question_has_option` (
  `id` int(20) NOT NULL,
  `id_question` int(20) DEFAULT 0,
  `option_description` varchar(1024) DEFAULT '',
  `is_correct_answer` int(2) DEFAULT 0,
  `image` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question_has_option`
--

INSERT INTO `question_has_option` (`id`, `id_question`, `option_description`, `is_correct_answer`, `image`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(30, 10, 'Option 1', 0, '', NULL, NULL, '2020-09-06 17:36:31', NULL, '2020-09-06 17:36:31'),
(31, 10, 'Option 3', 0, '', NULL, NULL, '2020-09-06 17:36:34', NULL, '2020-09-06 17:36:34'),
(32, 10, 'Option 2', 0, '', NULL, NULL, '2020-09-06 17:36:36', NULL, '2020-09-06 17:36:36'),
(33, 10, 'Option 4', 1, '', NULL, NULL, '2020-09-06 17:36:42', NULL, '2020-09-06 17:36:42'),
(34, 1, '1, 2 and 3', 0, '', NULL, NULL, '2020-10-01 19:10:02', NULL, '2020-10-01 19:10:02'),
(35, 1, '1, 3 and 4', 0, '', NULL, NULL, '2020-10-01 19:10:28', NULL, '2020-10-01 19:10:28'),
(36, 1, '3 and 4', 0, '', NULL, NULL, '2020-10-01 19:11:03', NULL, '2020-10-01 19:11:03'),
(37, 1, 'All of the above', 1, '', NULL, NULL, '2020-10-01 19:11:11', NULL, '2020-10-01 19:11:11'),
(38, 2, '1, 2 and 3', 0, '', NULL, NULL, '2020-10-01 19:12:30', NULL, '2020-10-01 19:12:30'),
(39, 2, '2 and 3', 0, '', NULL, NULL, '2020-10-01 19:12:42', NULL, '2020-10-01 19:12:42'),
(40, 2, '1 and 4', 1, '', NULL, NULL, '2020-10-01 19:12:47', NULL, '2020-10-01 19:12:47'),
(41, 2, 'All of the above', 0, '', NULL, NULL, '2020-10-01 19:12:54', NULL, '2020-10-01 19:12:54'),
(42, 4, 'Qiyas', 0, '', NULL, NULL, '2020-10-01 19:15:42', NULL, '2020-10-01 19:15:42'),
(43, 4, 'Sunnah', 1, '', NULL, NULL, '2020-10-01 19:15:48', NULL, '2020-10-01 19:15:48'),
(44, 4, 'Urf', 0, '', NULL, NULL, '2020-10-01 19:15:52', NULL, '2020-10-01 19:15:52'),
(45, 4, 'Istihsan', 0, '', NULL, NULL, '2020-10-01 19:15:56', NULL, '2020-10-01 19:15:56'),
(46, 5, '1, 2 and 3', 1, '', NULL, NULL, '2020-10-01 19:54:20', NULL, '2020-10-01 19:54:20'),
(47, 5, '1, 2 and 4', 0, '', NULL, NULL, '2020-10-01 19:54:30', NULL, '2020-10-01 19:54:30'),
(48, 5, '2, 3 and 4', 0, '', NULL, NULL, '2020-10-01 19:54:37', NULL, '2020-10-01 19:54:37'),
(49, 5, 'All of the above', 0, '', NULL, NULL, '2020-10-01 19:55:01', NULL, '2020-10-01 19:55:01'),
(50, 6, 'Gradual approach in Shariah is a rule that need not be followed by practitioners in the implementation of Shariah Principles, in the instances where full implementation of Shariah is not practical.', 1, '', NULL, NULL, '2020-10-01 19:56:50', NULL, '2020-10-01 19:56:50'),
(51, 6, 'Shariah is a principle that can be applied to the whole of mankind regardless of nationality, race or tribe', 0, '', NULL, NULL, '2020-10-01 19:57:09', NULL, '2020-10-01 19:57:09'),
(52, 6, 'Shariah is about making life easy for all mankind through its divine rule which prioritises human needs and comforts.', 0, '', NULL, NULL, '2020-10-01 19:57:17', NULL, '2020-10-01 19:57:17'),
(53, 6, 'Shariah goes beyond religious aspects and encompasses all stages of life and all human conduct including public and personal affairs. ', 0, '', NULL, NULL, '2020-10-01 19:57:26', NULL, '2020-10-01 19:57:26'),
(54, 7, 'Three (3)', 0, '', NULL, NULL, '2020-10-01 19:58:30', NULL, '2020-10-01 19:58:30'),
(55, 7, 'Four (4)', 0, '', NULL, NULL, '2020-10-01 19:58:35', NULL, '2020-10-01 19:58:35'),
(56, 7, 'Five (5) ', 1, '', NULL, NULL, '2020-10-01 19:58:40', NULL, '2020-10-01 19:58:40'),
(57, 7, 'Six (6) ', 0, '', NULL, NULL, '2020-10-01 19:58:46', NULL, '2020-10-01 19:58:46'),
(58, 8, '1, 2', 0, '', NULL, NULL, '2020-10-01 20:00:55', NULL, '2020-10-01 20:00:55'),
(59, 8, '2, 4', 0, '', NULL, NULL, '2020-10-01 20:01:03', NULL, '2020-10-01 20:01:03'),
(60, 8, '1, 4', 0, '', NULL, NULL, '2020-10-01 20:01:09', NULL, '2020-10-01 20:01:09'),
(61, 8, '3, 4', 1, '', NULL, NULL, '2020-10-01 20:01:17', NULL, '2020-10-01 20:01:17'),
(62, 10, 'Maysir; Gharar', 1, '', NULL, NULL, '2020-10-01 20:02:11', NULL, '2020-10-01 20:02:11'),
(63, 10, 'Gharar; Maysir', 0, '', NULL, NULL, '2020-10-01 20:02:17', NULL, '2020-10-01 20:02:17'),
(64, 10, 'Riba; Gharar', 0, '', NULL, NULL, '2020-10-01 20:02:21', NULL, '2020-10-01 20:02:21'),
(65, 10, 'Riba; Maysir', 0, '', NULL, NULL, '2020-10-01 20:02:25', NULL, '2020-10-01 20:02:25'),
(66, 11, 'Contract of Exchange', 0, '', NULL, NULL, '2020-10-01 20:03:06', NULL, '2020-10-01 20:03:06'),
(67, 11, 'Contract for the Utilization of Usufruct', 0, '', NULL, NULL, '2020-10-01 20:03:10', NULL, '2020-10-01 20:03:10'),
(68, 11, 'Contract of Partnership', 0, '', NULL, NULL, '2020-10-01 20:03:15', NULL, '2020-10-01 20:03:15'),
(69, 11, 'Contract of Hibah', 1, '', NULL, NULL, '2020-10-01 20:03:20', NULL, '2020-10-01 20:03:20'),
(70, 12, 'Salim sells a house belonging to his mother to Alex upon receiving consent from his mother to sell the house.', 0, '', NULL, NULL, '2020-10-01 20:05:22', NULL, '2020-10-01 20:05:22'),
(71, 12, 'Chan entered into an agreement at RM700,000 with Sarah to sell a house which Sarah will buy in the future.', 1, '', NULL, NULL, '2020-10-01 20:05:28', NULL, '2020-10-01 20:05:28'),
(72, 12, 'Musa borrows from Raju RM1,000 and promises to repay him with 100kg of durian in 8 months time.', 0, '', NULL, NULL, '2020-10-01 20:05:33', NULL, '2020-10-01 20:05:33'),
(73, 12, 'Jpay Sdn Bhd entered into an agreement to purchase a factory developed by AJM Berhad which will only be completed in the coming 24 months.', 0, '', NULL, NULL, '2020-10-01 20:05:38', NULL, '2020-10-01 20:05:38'),
(74, 13, 'The purchaser made queries of the price of the product and the seller respond to the purchaser.', 0, '', NULL, NULL, '2020-10-01 20:07:04', NULL, '2020-10-01 20:07:04'),
(75, 13, 'The purchaser received the letter of offer from the seller and replied with a letter of acceptance.', 1, '', NULL, NULL, '2020-10-01 20:07:10', NULL, '2020-10-01 20:07:10'),
(76, 13, 'A seller issued letter of offer to the purchaser and the purchaser replied requesting for a lower price.', 0, '', NULL, NULL, '2020-10-01 20:07:14', NULL, '2020-10-01 20:07:14'),
(77, 13, 'A seller informed the price and the purchaser request for deferred payment.', 0, '', NULL, NULL, '2020-10-01 20:07:19', NULL, '2020-10-01 20:07:19'),
(78, 14, 'Islamic law of contract were developed through the work of Muslim jurists, based on the principles laid down by the Qur’an and the tradition of Prophet Muhammad (SAW).', 0, '', NULL, NULL, '2020-10-02 14:51:45', NULL, '2020-10-02 14:51:45'),
(79, 14, 'Islamic theory of contract does not allow for contracting arrangements which do not fall in the categories of recognized nominate contracts (e.g. sale and purchase.).', 0, '', NULL, NULL, '2020-10-02 14:51:52', NULL, '2020-10-02 14:51:52'),
(80, 14, 'Islamic contract law embraces some dispositions which are not considered “contract” in the English or civil legal systems such as Waqf and Hibah.', 1, '', NULL, NULL, '2020-10-02 14:51:58', NULL, '2020-10-02 14:51:58'),
(81, 14, 'Under Shariah law, a contract is generally understood to be a declaration of offer and acceptance that binds parties with consequent rights and obligations.', 0, '', NULL, NULL, '2020-10-02 14:52:03', NULL, '2020-10-02 14:52:03'),
(82, 15, 'Hamdani', 0, '', NULL, NULL, '2020-10-02 14:52:41', NULL, '2020-10-02 14:52:41'),
(83, 15, 'Maliki', 1, '', NULL, NULL, '2020-10-02 14:52:47', NULL, '2020-10-02 14:52:47'),
(84, 15, 'Kasani', 0, '', NULL, NULL, '2020-10-02 14:52:51', NULL, '2020-10-02 14:52:51'),
(85, 15, 'Shaybani', 0, '', NULL, NULL, '2020-10-02 14:52:55', NULL, '2020-10-02 14:52:55'),
(86, 16, 'Four (4)', 0, '', NULL, NULL, '2020-10-02 14:53:33', NULL, '2020-10-02 14:53:33'),
(87, 16, 'Five (5)', 1, '', NULL, NULL, '2020-10-02 14:53:40', NULL, '2020-10-02 14:53:40'),
(88, 16, 'Six (6)', 0, '', NULL, NULL, '2020-10-02 14:53:44', NULL, '2020-10-02 14:53:44'),
(89, 16, 'Seven (7)', 0, '', NULL, NULL, '2020-10-02 14:53:48', NULL, '2020-10-02 14:53:48'),
(90, 17, 'Urf', 0, '', NULL, NULL, '2020-10-02 14:54:16', NULL, '2020-10-02 14:54:16'),
(91, 17, 'Istishab', 0, '', NULL, NULL, '2020-10-02 14:54:21', NULL, '2020-10-02 14:54:21'),
(92, 17, 'Ijma', 1, '', NULL, NULL, '2020-10-02 14:54:27', NULL, '2020-10-02 14:54:27'),
(93, 17, 'Istihsan', 0, '', NULL, NULL, '2020-10-02 14:54:30', NULL, '2020-10-02 14:54:30'),
(94, 18, ' Professional Philosophy', 0, '', NULL, NULL, '2020-10-02 14:55:17', NULL, '2020-10-02 14:55:17'),
(95, 18, 'Code of Ethics', 1, '', NULL, NULL, '2020-10-02 14:55:24', NULL, '2020-10-02 14:55:24'),
(96, 18, 'Mission Statement', 0, '', NULL, NULL, '2020-10-02 14:55:31', NULL, '2020-10-02 14:55:31'),
(97, 18, 'Standards of Professional Conduct', 0, '', NULL, NULL, '2020-10-02 14:55:38', NULL, '2020-10-02 14:55:38'),
(98, 19, 'Leaders', 0, '', NULL, NULL, '2020-10-02 14:56:47', NULL, '2020-10-02 14:56:47'),
(99, 19, 'Managers', 0, '', NULL, NULL, '2020-10-02 14:56:50', NULL, '2020-10-02 14:56:50'),
(100, 19, 'Khalifah', 1, '', NULL, NULL, '2020-10-02 14:56:55', NULL, '2020-10-02 14:56:55'),
(101, 19, 'Muslims', 0, '', NULL, NULL, '2020-10-02 14:56:59', NULL, '2020-10-02 14:56:59'),
(102, 20, 'Professional Philosophy', 0, '', NULL, NULL, '2020-10-02 14:57:39', NULL, '2020-10-02 14:57:39'),
(103, 20, 'Membership', 1, '', NULL, NULL, '2020-10-02 14:57:46', NULL, '2020-10-02 14:57:46'),
(104, 20, 'Vision & Mission', 0, '', NULL, NULL, '2020-10-02 14:57:51', NULL, '2020-10-02 14:57:51'),
(105, 20, 'Chartership', 0, '', NULL, NULL, '2020-10-02 14:57:54', NULL, '2020-10-02 14:57:54'),
(106, 21, 'Conformity to Shariah', 0, '', NULL, NULL, '2020-10-02 14:58:33', NULL, '2020-10-02 14:58:33'),
(107, 21, 'Ensuring maximum profit for the organisation', 1, '', NULL, NULL, '2020-10-02 14:58:40', NULL, '2020-10-02 14:58:40'),
(108, 21, 'Fulfilment of duties and responsibilities', 0, '', NULL, NULL, '2020-10-02 14:58:44', NULL, '2020-10-02 14:58:44'),
(109, 21, 'Exhibit highest standard of professionalism', 0, '', NULL, NULL, '2020-10-02 14:58:48', NULL, '2020-10-02 14:58:48'),
(110, 22, 'Integrity', 1, '', NULL, NULL, '2020-10-02 15:01:33', NULL, '2020-10-02 15:01:33'),
(111, 22, 'Accountability', 0, '', NULL, NULL, '2020-10-02 15:02:37', NULL, '2020-10-02 15:02:37'),
(112, 22, 'Confidentiality', 0, '', NULL, NULL, '2020-10-02 15:03:19', NULL, '2020-10-02 15:03:19'),
(113, 22, 'Objectivity', 0, '', NULL, NULL, '2020-10-02 15:03:27', NULL, '2020-10-02 15:03:27');

-- --------------------------------------------------------

--
-- Table structure for table `question_pool`
--

CREATE TABLE `question_pool` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `updated_by` bigint(20) NOT NULL,
  `created_dt_tm` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_dt_tm` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question_pool`
--

INSERT INTO `question_pool` (`id`, `name`, `code`, `status`, `created_by`, `updated_by`, `created_dt_tm`, `updated_dt_tm`) VALUES
(1, 'Pool 1', 'Pool 1', 1, 0, 0, '2020-08-14 18:22:16', '2020-08-14 18:22:16'),
(2, 'Pool 2', 'Pool 2', 1, 0, 0, '2020-08-14 18:22:25', '2020-08-14 18:22:25');

-- --------------------------------------------------------

--
-- Table structure for table `receipt`
--

CREATE TABLE `receipt` (
  `id` int(20) NOT NULL,
  `type` varchar(50) DEFAULT '',
  `receipt_number` varchar(50) DEFAULT '',
  `receipt_amount` float(20,2) DEFAULT NULL,
  `remarks` varchar(520) DEFAULT '',
  `id_student` int(20) DEFAULT NULL,
  `id_examination` int(20) DEFAULT 0,
  `receipt_date` datetime DEFAULT current_timestamp(),
  `approval_status` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `receipt`
--

INSERT INTO `receipt` (`id`, `type`, `receipt_number`, `receipt_amount`, `remarks`, `id_student`, `id_examination`, `receipt_date`, `approval_status`, `status`, `reason`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Student', 'REC000001/2020', 123.00, '', 1, 0, '2020-10-01 02:28:24', '', 1, '', NULL, '2020-10-01 02:28:24', NULL, '2020-10-01 02:28:24');

-- --------------------------------------------------------

--
-- Table structure for table `receipt_details`
--

CREATE TABLE `receipt_details` (
  `id` int(20) NOT NULL,
  `id_receipt` int(10) DEFAULT NULL,
  `id_main_invoice` int(10) DEFAULT NULL,
  `invoice_amount` float DEFAULT NULL,
  `paid_amount` float DEFAULT NULL,
  `approval_status` int(10) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `receipt_details`
--

INSERT INTO `receipt_details` (`id`, `id_receipt`, `id_main_invoice`, `invoice_amount`, `paid_amount`, `approval_status`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 0, 123, NULL, 1, 1, '2020-10-01 02:28:24', NULL, '2020-10-01 02:28:24');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(20) NOT NULL,
  `role` varchar(250) DEFAULT '',
  `status` int(20) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`, `status`) VALUES
(1, 'Administrator', 1),
(13, 'Examination Chief', 1);

-- --------------------------------------------------------

--
-- Table structure for table `role_permissions`
--

CREATE TABLE `role_permissions` (
  `id` bigint(20) NOT NULL,
  `id_role` bigint(20) DEFAULT NULL,
  `id_permission` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_permissions`
--

INSERT INTO `role_permissions` (`id`, `id_role`, `id_permission`) VALUES
(84, 12, 5),
(85, 12, 7),
(459, 1, 5),
(460, 1, 6),
(461, 1, 7),
(462, 1, 8),
(463, 1, 9),
(464, 1, 10),
(465, 1, 11),
(466, 1, 12),
(467, 1, 13),
(468, 1, 14),
(469, 1, 15),
(470, 1, 16),
(471, 1, 17),
(472, 1, 18),
(473, 1, 19),
(474, 1, 20),
(475, 1, 21),
(476, 1, 22),
(477, 1, 23),
(478, 1, 24),
(479, 1, 25),
(480, 1, 26),
(481, 1, 27),
(482, 1, 28),
(483, 1, 29),
(484, 1, 30),
(485, 1, 31),
(486, 1, 33),
(487, 1, 34),
(488, 1, 35),
(489, 1, 36),
(490, 1, 37),
(491, 1, 38),
(492, 1, 39),
(493, 1, 40),
(494, 1, 41),
(495, 1, 42),
(496, 1, 43),
(497, 1, 44),
(498, 1, 45),
(499, 1, 46),
(500, 1, 47),
(501, 1, 48),
(502, 1, 49),
(503, 1, 50),
(504, 1, 51),
(505, 1, 52),
(506, 1, 53),
(507, 1, 54),
(508, 1, 55),
(509, 1, 56),
(510, 1, 57),
(511, 1, 58),
(512, 1, 59),
(513, 1, 60),
(514, 1, 61),
(515, 1, 62),
(516, 1, 63),
(517, 1, 64),
(518, 1, 65),
(519, 1, 66),
(520, 1, 67),
(532, 13, 14);

-- --------------------------------------------------------

--
-- Table structure for table `salutation_setup`
--

CREATE TABLE `salutation_setup` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `sequence` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salutation_setup`
--

INSERT INTO `salutation_setup` (`id`, `name`, `sequence`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Mr', 1, 1, NULL, '2020-07-30 21:10:18', NULL, '2020-07-30 21:10:18'),
(2, 'Ms', 2, 1, NULL, '2020-07-30 21:10:51', NULL, '2020-07-30 21:10:51'),
(3, 'Mrs', 3, 1, NULL, '2020-07-30 21:10:51', NULL, '2020-07-30 21:10:51');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `id` int(20) NOT NULL,
  `name` varchar(120) DEFAULT '',
  `id_country` int(10) DEFAULT NULL,
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT NULL,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`id`, `name`, `id_country`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Kuala Lumpur', 1, 1, NULL, NULL, NULL, '2020-02-10 23:10:24'),
(2, 'Kedah', 1, 1, NULL, NULL, NULL, '2020-02-10 23:22:35'),
(3, 'Selangoor', 1, 1, NULL, NULL, NULL, '2020-02-27 17:13:26'),
(5, 'Sidney', 5, 1, NULL, NULL, NULL, '2020-03-29 19:15:12');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(20) NOT NULL,
  `full_name` varchar(580) DEFAULT '',
  `salutation` varchar(120) DEFAULT '',
  `first_name` varchar(120) DEFAULT '',
  `last_name` varchar(120) DEFAULT '',
  `nric` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `email_id` varchar(120) DEFAULT '',
  `password` varchar(120) DEFAULT '',
  `gender` varchar(120) DEFAULT '',
  `permanent_address1` varchar(120) DEFAULT '',
  `permanent_address2` varchar(120) DEFAULT '',
  `permanent_country` int(120) DEFAULT NULL,
  `permanent_state` int(120) DEFAULT NULL,
  `permanent_city` varchar(120) DEFAULT '',
  `permanent_zipcode` varchar(120) DEFAULT '',
  `applicant_status` varchar(50) DEFAULT 'Draft',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `full_name`, `salutation`, `first_name`, `last_name`, `nric`, `phone`, `email_id`, `password`, `gender`, `permanent_address1`, `permanent_address2`, `permanent_country`, `permanent_state`, `permanent_city`, `permanent_zipcode`, `applicant_status`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Hazna Ahmed', '1', 'Hazna', 'Ahmed', '23123123123', '3123123', 'hazna@gmail.com', 'hazna', '', '', '', 1, 1, 'KL', '2342342', 'Draft', 1, 1, '2020-10-02 23:08:30', NULL, '2020-10-02 23:08:30'),
(2, 'Mrs. Mustafiquer Rahim', '3', 'Mustafiquer', 'Rahim', '8788', '890899', 'mr@exam.com', '202cb962ac59075b964b07152d234b70', '', '', '', 1, 1, 'KL', '123123', 'Draft', 1, 1, '2020-10-03 01:01:19', 1, '2020-10-03 01:01:19'),
(3, 'Mr. Ubedullah Syed', '1', 'Ubedullah', 'Syed', 'NR989989', '898989', 'us@exam.com', '202cb962ac59075b964b07152d234b70', '', '', '', 1, 1, 'KL', '8989', 'Draft', 1, 1, '2020-10-03 01:05:42', NULL, '2020-10-03 01:05:42');

-- --------------------------------------------------------

--
-- Table structure for table `student_exam_attempt`
--

CREATE TABLE `student_exam_attempt` (
  `id` int(20) NOT NULL,
  `id_student` int(20) DEFAULT NULL,
  `id_exam_student_tagging` int(20) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `attempt_number` int(20) DEFAULT NULL,
  `extra_time` varchar(20) DEFAULT NULL,
  `exam_result` varchar(20) DEFAULT NULL,
  `exam_submitted` int(20) DEFAULT NULL COMMENT '0:Not submitted, 1:submitted',
  `submit_type` int(10) DEFAULT NULL COMMENT '1:manual submit,2:auto submit',
  `submitted_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_exam_attempt`
--

INSERT INTO `student_exam_attempt` (`id`, `id_student`, `id_exam_student_tagging`, `start_time`, `end_time`, `attempt_number`, `extra_time`, `exam_result`, `exam_submitted`, `submit_type`, `submitted_time`) VALUES
(1, 1, NULL, '2020-10-03 12:36:38', NULL, 1, NULL, NULL, 1, 1, '2020-10-05 09:58:10'),
(2, 1, NULL, '2020-10-05 09:57:58', NULL, 2, NULL, NULL, 1, 1, '2020-10-05 09:58:10'),
(3, 1, NULL, '2020-10-07 13:03:40', NULL, 3, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `student_question_set`
--

CREATE TABLE `student_question_set` (
  `id` bigint(20) NOT NULL,
  `id_student` int(20) DEFAULT NULL,
  `id_question` int(20) DEFAULT NULL,
  `question_order` int(20) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `id_exam_student_tagging` int(20) DEFAULT NULL,
  `id_answer` int(20) DEFAULT NULL,
  `id_student_exam_attempt` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_question_set`
--

INSERT INTO `student_question_set` (`id`, `id_student`, `id_question`, `question_order`, `datetime`, `id_exam_student_tagging`, `id_answer`, `id_student_exam_attempt`) VALUES
(1, 1, 6, 1, '2020-10-05 09:50:10', NULL, 50, 1),
(2, 1, 5, 2, '2020-10-05 09:50:14', NULL, 46, 1),
(3, 1, 8, 3, '2020-10-05 09:50:16', NULL, 58, 1),
(4, 1, 7, 4, NULL, NULL, 0, 1),
(5, 1, 2, 5, '2020-10-05 09:58:05', NULL, 40, 1),
(6, 1, 16, 6, '2020-10-05 09:50:42', NULL, 88, 1),
(7, 1, 5, 1, '2020-10-07 13:03:45', NULL, 46, 2),
(8, 1, 4, 2, '2020-10-07 13:03:49', NULL, 44, 2),
(9, 1, 8, 3, NULL, NULL, 0, 2),
(10, 1, 7, 4, NULL, NULL, 0, 2),
(11, 1, 1, 5, NULL, NULL, 0, 2),
(12, 1, 18, 6, NULL, NULL, 0, 2),
(13, 1, 4, 1, NULL, NULL, 0, 3),
(14, 1, 6, 2, NULL, NULL, 0, 3),
(15, 1, 8, 3, NULL, NULL, 0, 3),
(16, 1, 7, 4, NULL, NULL, 0, 3),
(17, 1, 1, 5, NULL, NULL, 0, 3),
(18, 1, 10, 6, NULL, NULL, 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `temp_main_invoice_details`
--

CREATE TABLE `temp_main_invoice_details` (
  `id` int(20) NOT NULL,
  `id_session` varchar(512) DEFAULT NULL,
  `id_fee_item` int(10) DEFAULT NULL,
  `price` int(20) DEFAULT 0,
  `quantity` int(20) DEFAULT 0,
  `amount` float(20,2) DEFAULT NULL,
  `id_reference` int(20) DEFAULT 0,
  `description` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_receipt_details`
--

CREATE TABLE `temp_receipt_details` (
  `id` int(20) NOT NULL,
  `id_session` varchar(120) DEFAULT '',
  `id_main_invoice` int(10) DEFAULT NULL,
  `invoice_amount` float DEFAULT NULL,
  `paid_amount` float DEFAULT NULL,
  `approval_status` int(10) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `topic`
--

CREATE TABLE `topic` (
  `id` int(20) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `id_course` bigint(20) NOT NULL,
  `name_optional_language` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `topic`
--

INSERT INTO `topic` (`id`, `name`, `code`, `id_course`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Topic 1', 'Topic 1', 2, '', 1, 1, '2020-08-14 18:10:00', 1, '2020-08-14 18:10:00'),
(2, 'Topic 2', 'Topic 2', 1, '', 1, 1, '2020-08-14 18:10:56', NULL, '2020-08-14 18:10:56'),
(3, 'Topic 3', 'Topic 3', 1, '', 1, 1, '2020-08-14 18:11:09', NULL, '2020-08-14 18:11:09'),
(4, 'Topic 4', 'Topic 4', 1, '', 1, 1, '2020-08-14 18:11:20', NULL, '2020-08-14 18:11:20'),
(5, 'Topic 1', 'Topic 1', 1, '', 1, 1, '2020-08-14 18:12:16', 1, '2020-08-14 18:12:16'),
(6, 'Topic 2', 'Topic 2', 2, '', 1, 1, '2020-08-14 18:14:49', NULL, '2020-08-14 18:14:49'),
(7, 'Topic 1', 'Topic 1', 3, '', 1, 1, '2020-08-14 18:15:20', NULL, '2020-08-14 18:15:20'),
(8, 'Topic 2', 'Topic 2', 3, '', 1, 1, '2020-08-14 18:15:34', NULL, '2020-08-14 18:15:34'),
(9, 'Topic 3', 'Topic 3', 3, '', 1, 1, '2020-08-14 18:15:44', NULL, '2020-08-14 18:15:44'),
(10, 'Topic 4', 'Topic 4', 3, '', 1, 1, '2020-08-14 18:16:03', NULL, '2020-08-14 18:16:03'),
(11, 'Topic 1', 'Topic 1', 4, '', 1, 1, '2020-08-14 18:16:14', NULL, '2020-08-14 18:16:14'),
(12, 'Topic 2', 'Topic 2', 4, '', 1, 1, '2020-08-14 18:16:25', NULL, '2020-08-14 18:16:25');

-- --------------------------------------------------------

--
-- Table structure for table `tos`
--

CREATE TABLE `tos` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `id_pool` varchar(50) NOT NULL,
  `question_count` bigint(20) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_dt_tm` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_dt_tm` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tos`
--

INSERT INTO `tos` (`id`, `name`, `id_pool`, `question_count`, `status`, `created_by`, `updated_by`, `created_dt_tm`, `updated_dt_tm`) VALUES
(6, 'Test TOS', '1,2', 6, 1, 1, 1, '2020-08-15 00:28:05', '2020-08-15 00:28:05');

-- --------------------------------------------------------

--
-- Table structure for table `tos_details`
--

CREATE TABLE `tos_details` (
  `id_details` bigint(20) NOT NULL,
  `id_course` int(11) NOT NULL,
  `id_topic` int(11) NOT NULL,
  `id_bloom_taxonomy` int(11) NOT NULL,
  `id_difficult_level` int(11) NOT NULL,
  `id_pool` int(11) NOT NULL,
  `questions_available` int(11) NOT NULL,
  `questions_selected` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `id_tos` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tos_details`
--

INSERT INTO `tos_details` (`id_details`, `id_course`, `id_topic`, `id_bloom_taxonomy`, `id_difficult_level`, `id_pool`, `questions_available`, `questions_selected`, `status`, `id_tos`) VALUES
(6, 1, 2, 1, 2, 1, 3, 2, 1, 6),
(7, 1, 2, 1, 3, 1, 1, 1, 1, 6),
(8, 1, 3, 1, 3, 1, 1, 1, 1, 6),
(9, 1, 5, 1, 1, 1, 2, 1, 1, 6),
(10, 2, 1, 4, 3, 2, 1, 1, 1, 6);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(20) NOT NULL,
  `email` varchar(1024) DEFAULT '',
  `password` varchar(500) DEFAULT '',
  `name` varchar(1024) DEFAULT '',
  `salutation` varchar(20) DEFAULT '',
  `first_name` varchar(1024) DEFAULT '',
  `last_name` varchar(1024) DEFAULT '',
  `mobile` varchar(50) DEFAULT '',
  `id_role` int(20) DEFAULT NULL,
  `is_deleted` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `name`, `salutation`, `first_name`, `last_name`, `mobile`, `id_role`, `is_deleted`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'virat@exam.com', '7fef6171469e80d32c0559f88b377245', 'Mr. Virat Kohli', '1', 'Virat', 'Kohli', '88888888', 1, 0, 1, 1, '2020-07-26 23:20:26', NULL, '2020-07-26 23:20:26');

-- --------------------------------------------------------

--
-- Table structure for table `user_last_login`
--

CREATE TABLE `user_last_login` (
  `id` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_last_login`
--

INSERT INTO `user_last_login` (`id`, `id_user`, `session_data`, `machine_ip`, `user_agent`, `agent_string`, `platform`, `created_dt_tm`) VALUES
(1, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(2, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(3, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '157.45.19.112', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(4, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '157.45.19.112', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(5, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(6, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(7, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(8, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(9, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(10, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(11, 1, '{\"role\":\"2\",\"roleText\":\"Ex\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(12, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(13, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(14, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(15, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(16, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(17, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(18, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(19, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(20, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '117.230.180.35', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(21, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(22, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(23, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(24, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(25, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(26, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(27, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(28, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(29, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(30, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(31, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(32, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(33, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(34, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(35, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '157.49.131.37', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(36, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '157.49.167.112', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(37, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(38, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(39, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '157.49.87.249', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(40, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(41, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '157.49.67.173', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(42, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '157.49.94.242', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(43, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(44, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '112.79.50.205', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(45, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(46, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(47, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(48, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(49, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '157.49.91.150', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(50, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '112.79.49.123', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(51, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(52, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '157.49.83.87', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(53, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '157.45.46.116', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(54, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '157.45.38.193', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(55, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_last_login`
--
ALTER TABLE `admin_last_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `answer_status`
--
ALTER TABLE `answer_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_registration`
--
ALTER TABLE `bank_registration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bloom_taxonomy`
--
ALTER TABLE `bloom_taxonomy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_group`
--
ALTER TABLE `communication_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_group_recepients`
--
ALTER TABLE `communication_group_recepients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_template`
--
ALTER TABLE `communication_template`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_learning_objective`
--
ALTER TABLE `course_learning_objective`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency_rate_setup`
--
ALTER TABLE `currency_rate_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency_setup`
--
ALTER TABLE `currency_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `difficult_level`
--
ALTER TABLE `difficult_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `examset`
--
ALTER TABLE `examset`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `examset_questions`
--
ALTER TABLE `examset_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_center`
--
ALTER TABLE `exam_center`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_center_has_room`
--
ALTER TABLE `exam_center_has_room`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_center_location`
--
ALTER TABLE `exam_center_location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_center_start_exam`
--
ALTER TABLE `exam_center_start_exam`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_event`
--
ALTER TABLE `exam_event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_has_question`
--
ALTER TABLE `exam_has_question`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_has_question_count`
--
ALTER TABLE `exam_has_question_count`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_name`
--
ALTER TABLE `exam_name`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_session`
--
ALTER TABLE `exam_session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_set`
--
ALTER TABLE `exam_set`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_student_tagging`
--
ALTER TABLE `exam_student_tagging`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_category`
--
ALTER TABLE `fee_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_setup`
--
ALTER TABLE `fee_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `financial_account_code`
--
ALTER TABLE `financial_account_code`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grade`
--
ALTER TABLE `grade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_invoice`
--
ALTER TABLE `main_invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_invoice_details`
--
ALTER TABLE `main_invoice_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_type`
--
ALTER TABLE `payment_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question_has_option`
--
ALTER TABLE `question_has_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question_pool`
--
ALTER TABLE `question_pool`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt`
--
ALTER TABLE `receipt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt_details`
--
ALTER TABLE `receipt_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `role` (`role`);

--
-- Indexes for table `role_permissions`
--
ALTER TABLE `role_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salutation_setup`
--
ALTER TABLE `salutation_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_exam_attempt`
--
ALTER TABLE `student_exam_attempt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_question_set`
--
ALTER TABLE `student_question_set`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_main_invoice_details`
--
ALTER TABLE `temp_main_invoice_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_receipt_details`
--
ALTER TABLE `temp_receipt_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `topic`
--
ALTER TABLE `topic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tos`
--
ALTER TABLE `tos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tos_details`
--
ALTER TABLE `tos_details`
  ADD PRIMARY KEY (`id_details`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_last_login`
--
ALTER TABLE `user_last_login`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_last_login`
--
ALTER TABLE `admin_last_login`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `answer_status`
--
ALTER TABLE `answer_status`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `bank_registration`
--
ALTER TABLE `bank_registration`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bloom_taxonomy`
--
ALTER TABLE `bloom_taxonomy`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `communication_group`
--
ALTER TABLE `communication_group`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `communication_group_recepients`
--
ALTER TABLE `communication_group_recepients`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `communication_template`
--
ALTER TABLE `communication_template`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `course_learning_objective`
--
ALTER TABLE `course_learning_objective`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `currency_rate_setup`
--
ALTER TABLE `currency_rate_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `currency_setup`
--
ALTER TABLE `currency_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `difficult_level`
--
ALTER TABLE `difficult_level`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `examset`
--
ALTER TABLE `examset`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `examset_questions`
--
ALTER TABLE `examset_questions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `exam_center`
--
ALTER TABLE `exam_center`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `exam_center_has_room`
--
ALTER TABLE `exam_center_has_room`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `exam_center_location`
--
ALTER TABLE `exam_center_location`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `exam_center_start_exam`
--
ALTER TABLE `exam_center_start_exam`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `exam_event`
--
ALTER TABLE `exam_event`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `exam_has_question`
--
ALTER TABLE `exam_has_question`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `exam_has_question_count`
--
ALTER TABLE `exam_has_question_count`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `exam_name`
--
ALTER TABLE `exam_name`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `exam_session`
--
ALTER TABLE `exam_session`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `exam_set`
--
ALTER TABLE `exam_set`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `exam_student_tagging`
--
ALTER TABLE `exam_student_tagging`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `fee_category`
--
ALTER TABLE `fee_category`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fee_setup`
--
ALTER TABLE `fee_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `financial_account_code`
--
ALTER TABLE `financial_account_code`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `grade`
--
ALTER TABLE `grade`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `main_invoice`
--
ALTER TABLE `main_invoice`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `main_invoice_details`
--
ALTER TABLE `main_invoice_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `payment_type`
--
ALTER TABLE `payment_type`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `question`
--
ALTER TABLE `question`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `question_has_option`
--
ALTER TABLE `question_has_option`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;

--
-- AUTO_INCREMENT for table `question_pool`
--
ALTER TABLE `question_pool`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `receipt`
--
ALTER TABLE `receipt`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `receipt_details`
--
ALTER TABLE `receipt_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `role_permissions`
--
ALTER TABLE `role_permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=533;

--
-- AUTO_INCREMENT for table `salutation_setup`
--
ALTER TABLE `salutation_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `student_exam_attempt`
--
ALTER TABLE `student_exam_attempt`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `student_question_set`
--
ALTER TABLE `student_question_set`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `temp_main_invoice_details`
--
ALTER TABLE `temp_main_invoice_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `temp_receipt_details`
--
ALTER TABLE `temp_receipt_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `topic`
--
ALTER TABLE `topic`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tos`
--
ALTER TABLE `tos`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tos_details`
--
ALTER TABLE `tos_details`
  MODIFY `id_details` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_last_login`
--
ALTER TABLE `user_last_login`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 04, 2020 at 10:18 PM
-- Server version: 10.2.33-MariaDB-log
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `camsedu_election`
--

-- --------------------------------------------------------

--
-- Table structure for table `communication_group`
--

CREATE TABLE `communication_group` (
  `id` int(20) NOT NULL,
  `id_template` int(20) DEFAULT 0,
  `name` varchar(1024) DEFAULT '',
  `type` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `communication_group`
--

INSERT INTO `communication_group` (`id`, `id_template`, `name`, `type`, `status`, `created_by`, `reason`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 'Student Final Year 2020', 'Student', 1, 1, '', '2020-10-02 15:52:05', NULL, '2020-10-02 15:52:05');

-- --------------------------------------------------------

--
-- Table structure for table `communication_group_message`
--

CREATE TABLE `communication_group_message` (
  `id` int(20) NOT NULL,
  `id_template` int(20) DEFAULT 0,
  `name` varchar(1024) DEFAULT '',
  `type` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `communication_group_message_recepients`
--

CREATE TABLE `communication_group_message_recepients` (
  `id` int(20) NOT NULL,
  `id_group` int(20) DEFAULT 0,
  `id_recepient` int(20) DEFAULT 0,
  `type` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `communication_group_recepients`
--

CREATE TABLE `communication_group_recepients` (
  `id` int(20) NOT NULL,
  `id_group` int(20) DEFAULT 0,
  `id_recepient` int(20) DEFAULT 0,
  `type` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `communication_group_recepients`
--

INSERT INTO `communication_group_recepients` (`id`, `id_group`, `id_recepient`, `type`, `status`, `created_by`, `reason`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 1, 4, 'Student', 1, 1, '', '2020-10-02 15:52:22', NULL, '2020-10-02 15:52:22');

-- --------------------------------------------------------

--
-- Table structure for table `communication_template`
--

CREATE TABLE `communication_template` (
  `id` int(20) NOT NULL,
  `name` text DEFAULT NULL,
  `subject` text DEFAULT NULL,
  `message` text DEFAULT NULL,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `communication_template`
--

INSERT INTO `communication_template` (`id`, `name`, `subject`, `message`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Fee Instruction', 'Fee Payment', '<p>Pay Fee Amount</p>\r\n', 1, 1, '2020-10-02 15:51:01', NULL, '2020-10-02 15:51:01');

-- --------------------------------------------------------

--
-- Table structure for table `communication_template_message`
--

CREATE TABLE `communication_template_message` (
  `id` int(20) NOT NULL,
  `name` text DEFAULT NULL,
  `subject` text DEFAULT NULL,
  `message` text DEFAULT NULL,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(20) NOT NULL,
  `name` varchar(120) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(2) DEFAULT 0,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(2) DEFAULT 0,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `name`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'India', 1, 0, '2020-02-27 15:59:29', 0, '2020-02-27 15:59:29');

-- --------------------------------------------------------

--
-- Table structure for table `currency_rate_setup`
--

CREATE TABLE `currency_rate_setup` (
  `id` int(20) NOT NULL,
  `id_currency` int(20) DEFAULT 0,
  `exchange_rate` varchar(200) DEFAULT '',
  `min_rate` varchar(200) DEFAULT '',
  `max_rate` varchar(200) DEFAULT '',
  `effective_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `currency_setup`
--

CREATE TABLE `currency_setup` (
  `id` int(20) NOT NULL,
  `code` varchar(2048) DEFAULT '',
  `name` varchar(2048) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `prefix` varchar(200) DEFAULT '',
  `suffix` varchar(200) DEFAULT '',
  `decimal_place` int(20) DEFAULT 0,
  `default` int(2) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `election`
--

CREATE TABLE `election` (
  `id` int(20) NOT NULL,
  `name` varchar(2048) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `election_date` varchar(200) DEFAULT '',
  `from_time` varchar(200) DEFAULT '',
  `to_time` varchar(200) DEFAULT '',
  `organisation` varchar(2048) DEFAULT '',
  `department` varchar(2048) DEFAULT '',
  `address1` varchar(200) DEFAULT '',
  `id_country` int(20) DEFAULT 0,
  `id_state` int(20) DEFAULT 0,
  `city` varchar(200) DEFAULT '',
  `zipcode` int(20) DEFAULT 0,
  `contact_person` varchar(1024) DEFAULT '',
  `contact_email` varchar(1024) DEFAULT '',
  `phone` varchar(20) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `election`
--

INSERT INTO `election` (`id`, `name`, `name_optional_language`, `election_date`, `from_time`, `to_time`, `organisation`, `department`, `address1`, `id_country`, `id_state`, `city`, `zipcode`, `contact_person`, `contact_email`, `phone`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Election for 2020`', '', '2020-11-05', '10:00', '17:00', 'RV-VLSI', 'VLSI', 'CA-17, 36th cross Road, 26th Main Rd, 4th T Block East, Jayanagar', 1, 1, 'Bangalore', 560091, 'Akshatha', 'akshatha@rv-vlsi.com', '9538130954', 1, 1, '2020-11-03 13:09:01', 1, '2020-11-03 13:09:01'),
(2, 'Election Mar 2021', '', '2021-03-01', '09:00', '17:00', 'RVVLSI', 'Automation', 'JA', 1, 1, 'Bengaluru', 560001, 'Rahul', 'rahul@rvvlsi.com', '123312312', 1, 1, '2020-11-04 21:59:48', 1, '2020-11-04 21:59:48');

-- --------------------------------------------------------

--
-- Table structure for table `election_results`
--

CREATE TABLE `election_results` (
  `id` int(20) NOT NULL,
  `id_title` int(20) DEFAULT 0,
  `id_election` int(20) DEFAULT 0,
  `id_participant` int(20) DEFAULT 0,
  `votes` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `election_title`
--

CREATE TABLE `election_title` (
  `id` int(20) NOT NULL,
  `name` varchar(2048) DEFAULT '',
  `id_election` int(20) DEFAULT 0,
  `is_completed` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(2) DEFAULT 0,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(2) DEFAULT 0,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `election_title`
--

INSERT INTO `election_title` (`id`, `name`, `id_election`, `is_completed`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'President', 1, 0, 1, 1, '2020-11-03 13:09:18', 1, '2020-11-03 13:09:18'),
(2, 'Vice president', 2, 0, 1, 1, '2020-11-04 21:36:06', 1, '2020-11-04 21:36:06');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(20) NOT NULL,
  `menu_name` varchar(200) DEFAULT NULL,
  `module_name` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `menu_name`, `module_name`) VALUES
(1, 'User', 'Setup'),
(2, 'Course', NULL),
(3, 'Topic', 'Setup'),
(4, 'Bloom Taxonomy', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `participants`
--

CREATE TABLE `participants` (
  `id` int(20) NOT NULL,
  `name` varchar(2048) DEFAULT '',
  `phone` varchar(20) DEFAULT '',
  `email` varchar(1024) DEFAULT '',
  `description` varchar(2048) DEFAULT '',
  `id_election` int(20) DEFAULT 0,
  `id_title` int(20) DEFAULT 0,
  `address1` varchar(200) DEFAULT '',
  `id_country` int(20) DEFAULT 0,
  `id_state` int(20) DEFAULT 0,
  `city` varchar(200) DEFAULT '',
  `zipcode` int(20) DEFAULT 0,
  `image` varchar(1024) DEFAULT '',
  `logo` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `participants`
--

INSERT INTO `participants` (`id`, `name`, `phone`, `email`, `description`, `id_election`, `id_title`, `address1`, `id_country`, `id_state`, `city`, `zipcode`, `image`, `logo`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Sadhashivayya', '9853441290', 'sadashiva@rvvlsi.com', 'Serving as a Comitee Member From Last 10 Years.', 1, 1, '', 0, 0, '', 0, '76b634d49f5b2b3702c681a8636304ed.svg', '87b888e85d7632d40a31e8905ce63215.svg', 1, 1, '2020-11-04 12:21:16', NULL, '2020-11-04 12:21:16'),
(2, 'Premnath AV', '9809012345', 'premnath@rvvlsi.com', 'Aspirant For the President Post', 1, 2, '', 0, 0, '', 0, '356487a5f09afee3b830bee3578b5ccb.svg', 'b5a75e95242f2e9fc77e3a4647b171c0.png', 1, 1, '2020-11-04 12:22:27', 1, '2020-11-04 12:22:27'),
(3, 'Rajaram HV', '88888888', 'rhv@rv.com', 'Contesting 8', 2, 2, '', 0, 0, '', 0, '82cd857c15e9b09fa8425f6f5ff6d61d.png', 'da3280f37b83c7171f8fbfb1cbe4dd4c.svg', 1, 1, '2020-11-04 22:03:44', 1, '2020-11-04 22:03:44'),
(4, 'Ekanth', '9999999999', 'e@rv.com', 'Particioant', 2, 2, '', 0, 0, '', 0, 'ecda6ad9710841fc924110354d590ea5.png', 'e9a6ca303e04cde534266dc21831b52b.png', 1, 1, '2020-11-04 22:14:03', NULL, '2020-11-04 22:14:03');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) NOT NULL,
  `code` varchar(100) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `id_menu` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `code`, `description`, `status`, `id_menu`) VALUES
(5, 'student.studentexams', 'Student Exam page view', 1, NULL),
(6, 'role.list', 'List Role', 1, NULL),
(7, 'permission.add', 'Add Permission', 1, NULL),
(8, 'permission.list', 'View Permission', 1, NULL),
(9, 'permission.edit', 'Edit Permission', 1, NULL),
(10, 'user.edit', 'Edit User', 1, 1),
(11, 'user.add', 'Add User', 1, 1),
(12, 'user.list', 'View User', 1, 1),
(13, 'course.list', 'View Course', 1, 2),
(14, 'course.add', 'Add Course', 1, 2),
(15, 'course.edit', 'Edit Course', 1, 2),
(16, 'topic.edit', 'Edit Topic', 1, NULL),
(17, 'topic.add', 'Add Topic', 1, NULL),
(18, 'topic.list', 'View Topic', 1, NULL),
(19, 'learningobjective.list', 'View Learning Objective', 1, NULL),
(20, 'learningobjective.add', 'Add Learning Objective', 1, NULL),
(21, 'learningobjective.list', 'View Learning Objective', 1, NULL),
(22, 'taxonomy.list', 'View Blood Taxonomy', 1, NULL),
(23, 'taxonomy.add', 'Add Blood Taxonomy', 1, NULL),
(24, 'taxonomy.edit', 'Edit Blood Taxonomy', 1, NULL),
(25, 'difficultylevel.edit', 'Edit Difficulty Level', 1, NULL),
(26, 'difficultylevel.add', 'Add Difficulty Level', 1, NULL),
(27, 'difficultylevel.list', 'View Difficulty Level', 1, NULL),
(28, 'question.list', 'View Question', 1, NULL),
(29, 'question.add', 'Add Question', 1, NULL),
(30, 'question.edit', 'Edit Question', 1, NULL),
(31, 'questionhasoption.add', 'Add Question Options', 1, NULL),
(33, 'exam_name.list', 'List Exam Name', 1, NULL),
(34, 'exam_name.add', 'Add Exam Name', 1, NULL),
(35, 'exam_name.edit', 'Edit Exam Name', 1, NULL),
(36, 'exam_center.list', 'View Exam Center', 1, NULL),
(37, 'exam_center.add', 'Add Exam Center', 1, NULL),
(38, 'exam_center.edit', 'Edit Exam Center', 1, NULL),
(39, 'exam_event.edit', 'Edit Exam Event', 1, NULL),
(40, 'exam_event.list', 'View Exam Event', 1, NULL),
(41, 'exam_event.add', 'Add Exam Event', 1, NULL),
(42, 'exam_has_question.add', 'Add Exam Has Question', 1, NULL),
(43, 'exam_has_question.edit', 'Edit Exam Has Question', 1, NULL),
(44, 'role.add', 'Add Role', 1, NULL),
(45, 'exam_has_question.list', 'View Exam Has Question', 1, NULL),
(46, 'grade.list', 'View Grade', 1, NULL),
(47, 'grade.add', 'Add Grade', 1, NULL),
(48, 'grade.edit', 'Edit Grade', 1, NULL),
(49, 'location.edit', 'Edit Location', 1, NULL),
(50, 'location.add', 'Add Location', 1, NULL),
(51, 'location.list', 'View Location', 1, NULL),
(52, 'questionpool.list', 'List Question Pool', 1, NULL),
(53, 'questionpool.add', 'Add Question Pool', 1, NULL),
(54, 'questionpool.edit', 'Edit Question Pool', 1, NULL),
(55, 'tos.add', 'Add TOS', 1, NULL),
(56, 'tos.list', 'View TOS', 1, NULL),
(57, 'tos.edit', 'Edit TOS', 1, NULL),
(58, 'examset.add', 'Add Exam Set', 1, NULL),
(59, 'examset.list', 'View Exam Set', 1, NULL),
(60, 'examset.edit', 'Edit Exam Set', 1, NULL),
(61, 'student.list', 'List Student', 1, NULL),
(62, 'student.add', 'Add Student', 1, NULL),
(63, 'student.edit', 'Edit Student', 1, NULL),
(64, 'exam_student_tagging.add', 'Add Exam Student Tagging', 1, NULL),
(65, 'exam_student_tagging.list', 'View Exam Student Tagging', 1, NULL),
(66, 'exam_student_tagging.edit', 'Edit Exam Student Tagging', 1, NULL),
(67, 'role.edit', 'Role Edit', 1, NULL),
(68, 'student.examanswers', 'Student Exam answer', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(20) NOT NULL,
  `role` varchar(250) DEFAULT '',
  `status` int(20) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`, `status`) VALUES
(1, 'Administrator', 1),
(13, 'Examination Chief', 1);

-- --------------------------------------------------------

--
-- Table structure for table `role_permissions`
--

CREATE TABLE `role_permissions` (
  `id` bigint(20) NOT NULL,
  `id_role` bigint(20) DEFAULT NULL,
  `id_permission` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_permissions`
--

INSERT INTO `role_permissions` (`id`, `id_role`, `id_permission`) VALUES
(84, 12, 5),
(85, 12, 7),
(459, 1, 5),
(460, 1, 6),
(461, 1, 7),
(462, 1, 8),
(463, 1, 9),
(464, 1, 10),
(465, 1, 11),
(466, 1, 12),
(467, 1, 13),
(468, 1, 14),
(469, 1, 15),
(470, 1, 16),
(471, 1, 17),
(472, 1, 18),
(473, 1, 19),
(474, 1, 20),
(475, 1, 21),
(476, 1, 22),
(477, 1, 23),
(478, 1, 24),
(479, 1, 25),
(480, 1, 26),
(481, 1, 27),
(482, 1, 28),
(483, 1, 29),
(484, 1, 30),
(485, 1, 31),
(486, 1, 33),
(487, 1, 34),
(488, 1, 35),
(489, 1, 36),
(490, 1, 37),
(491, 1, 38),
(492, 1, 39),
(493, 1, 40),
(494, 1, 41),
(495, 1, 42),
(496, 1, 43),
(497, 1, 44),
(498, 1, 45),
(499, 1, 46),
(500, 1, 47),
(501, 1, 48),
(502, 1, 49),
(503, 1, 50),
(504, 1, 51),
(505, 1, 52),
(506, 1, 53),
(507, 1, 54),
(508, 1, 55),
(509, 1, 56),
(510, 1, 57),
(511, 1, 58),
(512, 1, 59),
(513, 1, 60),
(514, 1, 61),
(515, 1, 62),
(516, 1, 63),
(517, 1, 64),
(518, 1, 65),
(519, 1, 66),
(520, 1, 67),
(532, 13, 14);

-- --------------------------------------------------------

--
-- Table structure for table `salutation_setup`
--

CREATE TABLE `salutation_setup` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `sequence` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salutation_setup`
--

INSERT INTO `salutation_setup` (`id`, `name`, `sequence`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Mr', 1, 1, NULL, '2020-07-30 21:10:18', NULL, '2020-07-30 21:10:18'),
(2, 'Ms', 2, 1, NULL, '2020-07-30 21:10:51', NULL, '2020-07-30 21:10:51'),
(3, 'Mrs', 3, 1, NULL, '2020-07-30 21:10:51', NULL, '2020-07-30 21:10:51');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `id` int(20) NOT NULL,
  `name` varchar(120) DEFAULT '',
  `id_country` int(10) DEFAULT NULL,
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT NULL,
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`id`, `name`, `id_country`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'Andra Pradesh', 1, 1, NULL, NULL, NULL, '2020-02-10 23:10:24'),
(2, 'Bihar', 1, 1, NULL, NULL, NULL, '2020-02-10 23:22:35'),
(3, 'Karnataka', 1, 1, NULL, NULL, NULL, '2020-02-27 17:13:26'),
(5, 'Sidney', 5, 1, NULL, NULL, NULL, '2020-03-29 19:15:12');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(20) NOT NULL,
  `full_name` varchar(580) DEFAULT '',
  `salutation` varchar(120) DEFAULT '',
  `first_name` varchar(120) DEFAULT '',
  `last_name` varchar(120) DEFAULT '',
  `nric` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `email_id` varchar(120) DEFAULT '',
  `password` varchar(120) DEFAULT '',
  `gender` varchar(120) DEFAULT '',
  `permanent_address1` varchar(120) DEFAULT '',
  `permanent_address2` varchar(120) DEFAULT '',
  `permanent_country` int(120) DEFAULT NULL,
  `permanent_state` int(120) DEFAULT NULL,
  `permanent_city` varchar(120) DEFAULT '',
  `permanent_zipcode` varchar(120) DEFAULT '',
  `applicant_status` varchar(50) DEFAULT 'Draft',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `full_name`, `salutation`, `first_name`, `last_name`, `nric`, `phone`, `email_id`, `password`, `gender`, `permanent_address1`, `permanent_address2`, `permanent_country`, `permanent_state`, `permanent_city`, `permanent_zipcode`, `applicant_status`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Mr. Test one', '1', 'Test', 'one', '1', '1', '1@gmail.com', 'b1a5b64256e27fa5ae76d62b95209ab3', '', '', '', 1, 2, 'city', '123123', 'Draft', 1, 1, '2020-10-27 10:31:39', 1, '2020-10-27 10:31:39'),
(2, 'Mr. Student two', '1', 'Student', 'two', '2', '123123123123', '2@gmail.com', 'b1a5b64256e27fa5ae76d62b95209ab3', '', '', '', 1, 2, 'city', '213123', 'Draft', 1, 1, '2020-10-27 17:17:51', NULL, '2020-10-27 17:17:51'),
(3, 'Mr. Student Three', '1', 'Student', 'Three', '3', '3', '3@gmail.com', 'b1a5b64256e27fa5ae76d62b95209ab3', '', '', '', 1, 1, 'city', '123123', 'Draft', 1, 1, '2020-10-27 17:25:53', NULL, '2020-10-27 17:25:53'),
(4, 'Mr. Student Four', '1', 'Student', 'Four', '4', '4', '4@gmail.com', 'b1a5b64256e27fa5ae76d62b95209ab3', '', '', '', 1, 2, 'city', '123123', 'Draft', 1, 1, '2020-10-27 18:15:57', NULL, '2020-10-27 18:15:57');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(20) NOT NULL,
  `email` varchar(1024) DEFAULT '',
  `password` varchar(500) DEFAULT '',
  `name` varchar(1024) DEFAULT '',
  `salutation` varchar(20) DEFAULT '',
  `first_name` varchar(1024) DEFAULT '',
  `last_name` varchar(1024) DEFAULT '',
  `mobile` varchar(50) DEFAULT '',
  `id_role` int(20) DEFAULT NULL,
  `is_deleted` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `name`, `salutation`, `first_name`, `last_name`, `mobile`, `id_role`, `is_deleted`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'virat@election.com', '7fef6171469e80d32c0559f88b377245', 'Mr. Virat Kohli', '1', 'Virat', 'Kohli', '88888888', 1, 0, 1, 1, '2020-07-26 23:20:26', NULL, '2020-07-26 23:20:26');

-- --------------------------------------------------------

--
-- Table structure for table `user_last_login`
--

CREATE TABLE `user_last_login` (
  `id` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_last_login`
--

INSERT INTO `user_last_login` (`id`, `id_user`, `session_data`, `machine_ip`, `user_agent`, `agent_string`, `platform`, `created_dt_tm`) VALUES
(1, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(2, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(3, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '157.45.19.112', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(4, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '157.45.19.112', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(5, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(6, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(7, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(8, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(9, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(10, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(11, 1, '{\"role\":\"2\",\"roleText\":\"Ex\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(12, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(13, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(14, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(15, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(16, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(17, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(18, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(19, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(20, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '117.230.180.35', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(21, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(22, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(23, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(24, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(25, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(26, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(27, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(28, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(29, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(30, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(31, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(32, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(33, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(34, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(35, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '157.49.131.37', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(36, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '157.49.167.112', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(37, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(38, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(39, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '157.49.87.249', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(40, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(41, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '157.49.67.173', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(42, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '157.49.94.242', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(43, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(44, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '112.79.50.205', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(45, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(46, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(47, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(48, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(49, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '157.49.91.150', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(50, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '112.79.49.123', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(51, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(52, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(53, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(54, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(55, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(56, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(57, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(58, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(59, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(60, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '157.49.91.172', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(61, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(62, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '117.230.26.46', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(63, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '117.230.176.78', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(64, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '157.49.68.8', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(65, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(66, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '42.105.125.56', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `viewers`
--

CREATE TABLE `viewers` (
  `id` int(20) NOT NULL,
  `name` varchar(2048) DEFAULT '',
  `phone` varchar(20) DEFAULT '',
  `email` varchar(1024) DEFAULT '',
  `random_number` varchar(2048) DEFAULT '',
  `browser_location` varchar(2048) DEFAULT '',
  `id_election` int(20) DEFAULT 0,
  `id_title` int(20) DEFAULT 0,
  `address1` varchar(200) DEFAULT '',
  `id_country` int(20) DEFAULT 0,
  `id_state` int(20) DEFAULT 0,
  `city` varchar(200) DEFAULT '',
  `zipcode` int(20) DEFAULT 0,
  `image` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `viewers`
--

INSERT INTO `viewers` (`id`, `name`, `phone`, `email`, `random_number`, `browser_location`, `id_election`, `id_title`, `address1`, `id_country`, `id_state`, `city`, `zipcode`, `image`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'VIewer One', '12321312', 'v1@cms.com', '', '', 1, 0, '', 0, 0, '', 0, '', 1, 1, '2020-11-04 01:52:01', 1, '2020-11-04 01:52:01'),
(2, 'Viewer 2', '3729139', 'v2@rvvlsi.com', '', '', 1, 0, '', 0, 0, '', 0, '', 1, 1, '2020-11-04 12:23:15', NULL, '2020-11-04 12:23:15'),
(3, 'Viewer 3', '423424', 'v3@rvvlsi.com', '', '', 1, 0, '', 0, 0, '', 0, '', 1, 1, '2020-11-04 12:23:35', NULL, '2020-11-04 12:23:35'),
(4, 'Viewer 4', '243232342', 'v4@rvvlsi.com', '', '', 1, 0, '', 0, 0, '', 0, '', 1, 1, '2020-11-04 12:23:57', NULL, '2020-11-04 12:23:57'),
(5, 'Viewer 5', '842948293', 'v5@rvvlsi.com', '', '', 1, 0, '', 0, 0, '', 0, '', 1, 1, '2020-11-04 12:24:18', NULL, '2020-11-04 12:24:18'),
(6, 'VLSI one', '123123123123', 'vlsi1@gmailcom', '', '', 1, 0, '', 0, 0, '', 0, '', 1, 1, '2020-11-04 21:39:58', 1, '2020-11-04 21:39:58');

-- --------------------------------------------------------

--
-- Table structure for table `viewer_vote_pooling`
--

CREATE TABLE `viewer_vote_pooling` (
  `id` int(20) NOT NULL,
  `id_viewer` int(20) DEFAULT 0,
  `id_election` int(20) DEFAULT 0,
  `id_title` int(20) DEFAULT 0,
  `is_voted` int(20) DEFAULT 0,
  `id_participant` int(20) DEFAULT 0,
  `random_number` varchar(2048) DEFAULT '',
  `machine_ip` varchar(1024) DEFAULT '',
  `browser_info` varchar(1024) DEFAULT '',
  `agent_string` varchar(1024) DEFAULT '',
  `platform` varchar(1024) DEFAULT '',
  `voting_date_time` varchar(246) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `viewer_vote_pooling`
--

INSERT INTO `viewer_vote_pooling` (`id`, `id_viewer`, `id_election`, `id_title`, `is_voted`, `id_participant`, `random_number`, `machine_ip`, `browser_info`, `agent_string`, `platform`, `voting_date_time`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 2, 1, 1, 2, 2, 'a9d8cbb7b0e3430dc99daf58ddfeba8b', '157.49.68.8', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '2020-11-05 01:57:55', 1, 1, '2020-11-04 12:26:13', NULL, '2020-11-04 12:26:13'),
(3, 3, 1, 1, 2, 2, '6cb007e17bda37ee3fc028595371956d', '157.49.68.8', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '2020-11-05 02:05:42', 1, 1, '2020-11-04 12:26:13', NULL, '2020-11-04 12:26:13'),
(4, 4, 1, 1, 0, 0, 'bd646340d8547b1260c336bb4f7e1ced', '', '', '', '', '', 1, 1, '2020-11-04 12:26:13', NULL, '2020-11-04 12:26:13'),
(5, 5, 1, 1, 0, 0, '887d23e5923ec592926a91c76ea3045c', '', '', '', '', '', 1, 1, '2020-11-04 12:26:13', NULL, '2020-11-04 12:26:13'),
(6, 1, 1, 1, 0, 0, '2cd2418db7bcacfeb98c8f0da848d94e', '', '', '', '', '', 1, 1, '2020-11-04 12:26:13', NULL, '2020-11-04 12:26:13'),
(7, 1, 2, 2, 0, 0, '178acfaf00cba60e4cf160d8d80eda54', '', '', '', '', '', 1, 1, '2020-11-04 22:12:04', NULL, '2020-11-04 22:12:04'),
(8, 6, 2, 2, 3, 3, 'fe40919bc345820e8b01d32f9801c33f', '42.105.121.79', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '2020-11-05 11:44:34', 1, 1, '2020-11-04 22:12:04', NULL, '2020-11-04 22:12:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `communication_group`
--
ALTER TABLE `communication_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_group_message`
--
ALTER TABLE `communication_group_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_group_message_recepients`
--
ALTER TABLE `communication_group_message_recepients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_group_recepients`
--
ALTER TABLE `communication_group_recepients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_template`
--
ALTER TABLE `communication_template`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_template_message`
--
ALTER TABLE `communication_template_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency_rate_setup`
--
ALTER TABLE `currency_rate_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency_setup`
--
ALTER TABLE `currency_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `election`
--
ALTER TABLE `election`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `election_results`
--
ALTER TABLE `election_results`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `election_title`
--
ALTER TABLE `election_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `participants`
--
ALTER TABLE `participants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `role` (`role`);

--
-- Indexes for table `role_permissions`
--
ALTER TABLE `role_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salutation_setup`
--
ALTER TABLE `salutation_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_last_login`
--
ALTER TABLE `user_last_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `viewers`
--
ALTER TABLE `viewers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `viewer_vote_pooling`
--
ALTER TABLE `viewer_vote_pooling`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `communication_group`
--
ALTER TABLE `communication_group`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `communication_group_message`
--
ALTER TABLE `communication_group_message`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `communication_group_message_recepients`
--
ALTER TABLE `communication_group_message_recepients`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `communication_group_recepients`
--
ALTER TABLE `communication_group_recepients`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `communication_template`
--
ALTER TABLE `communication_template`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `communication_template_message`
--
ALTER TABLE `communication_template_message`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `currency_rate_setup`
--
ALTER TABLE `currency_rate_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `currency_setup`
--
ALTER TABLE `currency_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `election`
--
ALTER TABLE `election`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `election_results`
--
ALTER TABLE `election_results`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `election_title`
--
ALTER TABLE `election_title`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `participants`
--
ALTER TABLE `participants`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `role_permissions`
--
ALTER TABLE `role_permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=533;

--
-- AUTO_INCREMENT for table `salutation_setup`
--
ALTER TABLE `salutation_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_last_login`
--
ALTER TABLE `user_last_login`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `viewers`
--
ALTER TABLE `viewers`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `viewer_vote_pooling`
--
ALTER TABLE `viewer_vote_pooling`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

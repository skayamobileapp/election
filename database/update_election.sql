

CREATE TABLE `election_title` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(2048) DEFAULT '',
  `id_election` int(20) DEFAULT 0,
  `status` int(2) DEFAULT '1',
  `created_by` int(2) DEFAULT '0',
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(2) DEFAULT '0',
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `participants` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(2048) DEFAULT '',
  `phone` varchar(20) DEFAULT '',
  `email` varchar(1024) DEFAULT '',
  `description` varchar(2048) DEFAULT '',
  `id_election` int(20) DEFAULT '0',
  `id_title` int(20) DEFAULT '0',
  `address1` varchar(200) DEFAULT '',
  `id_country` int(20) DEFAULT '0',
  `id_state` int(20) DEFAULT '0',
  `city` varchar(200) DEFAULT '',
  `zipcode` int(20) DEFAULT '0',
  `image` varchar(1024) DEFAULT '',
  `logo` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `viewers` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(2048) DEFAULT '',
  `phone` varchar(20) DEFAULT '',
  `email` varchar(1024) DEFAULT '',
  `random_number` varchar(2048) DEFAULT '',
  `browser_location` varchar(2048) DEFAULT '',
  `id_election` int(20) DEFAULT '0',
  `id_title` int(20) DEFAULT '0',
  `address1` varchar(200) DEFAULT '',
  `id_country` int(20) DEFAULT '0',
  `id_state` int(20) DEFAULT '0',
  `city` varchar(200) DEFAULT '',
  `zipcode` int(20) DEFAULT '0',
  `image` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `viewer_vote_pooling` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_viewer` int(20) DEFAULT 0,
  `id_election` int(20) DEFAULT 0,
  `id_title` int(20) DEFAULT 0,
  `is_voted` int(20) DEFAULT 0,
  `id_participant` int(20) DEFAULT 0,
  `random_number` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `viewer_vote_pooling` ADD `machine_ip` VARCHAR(1024) NULL DEFAULT '' AFTER `random_number`, ADD `browser_info` VARCHAR(1024) NULL DEFAULT '' AFTER `machine_ip`, ADD `agent_string` VARCHAR(1024) NULL DEFAULT '' AFTER `browser_info`, ADD `platform` VARCHAR(1024) NULL DEFAULT '' AFTER `agent_string`, ADD `voting_date_time` VARCHAR(246) NULL DEFAULT '' AFTER `platform`;


ALTER TABLE `election_title` ADD `is_completed` INT(20) NULL DEFAULT '0' AFTER `id_election`;


CREATE TABLE `election_results` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_title` int(20) DEFAULT 0,
  `id_election` int(20) DEFAULT 0,
  `id_participant` int(20) DEFAULT 0,
  `votes` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--------------------- Update Here From Server -------------------------

































<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class : BaseController
 * Base Class to control over all the classes
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class BaseController extends MX_Controller
{
    protected $role = '';
    protected $vendorId = '';
    protected $name = '';
    protected $roleText = '';
    protected $global = array();
    protected $lastLogin = '';

    public function __construct()
    {
        // $this->isLoggedIn();
    }
    /**
     * Takes mixed data and optionally a status code, then creates the response
     *
     * @access public
     * @param array|NULL $data
     *        	Data to output to the user
     *        	running the script; otherwise, exit
     */
    public function response($data = NULL)
    {
        $this->output->set_status_header(200)->set_content_type('application/json', 'utf-8')->set_output(json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))->_display();
        exit();
    }

    function isLoggedIn()
    {
        $enteredURL = current_url(); //http://myhost/main
        // echo $currentURL;exit();

        $this->load->library('session');
        $this->session->set_flashdata('entered_url', $enteredURL);

        $isLoggedIn = $this->session->userdata('isLoggedIn');

        if (!isset($isLoggedIn) || $isLoggedIn != TRUE) {
            redirect('login');
        } else {
            $this->role = $this->session->userdata('role');
            $this->vendorId = $this->session->userdata('userId');
            $this->name = $this->session->userdata('name');
            $this->roleText = $this->session->userdata('roleText');
            $this->lastLogin = $this->session->userdata('lastLogin');

            $this->global['name'] = $this->name;
            $this->global['role'] = $this->role;
            $this->global['role_text'] = $this->roleText;
            $this->global['last_login'] = $this->lastLogin;
        }
    }

    function isStudentLoggedIn()
    {
        // echo "string";exit();
        $this->load->library('session');
        $isStudentLoggedIn = $this->session->userdata('isStudentLoggedIn');

        if (!isset($isStudentLoggedIn) || $isStudentLoggedIn != TRUE) {
            redirect('studentLogin');
        } else {
            $this->student_name = $this->session->userdata('student_name');
            $this->id_student = $this->session->userdata('id_student');
            $this->student_last_login = $this->session->userdata('student_last_login');

            $this->global['student_name'] = $this->student_name;
            $this->global['id_student'] = $this->id_student;
            $this->global['student_last_login'] = $this->student_last_login;
        }
    }

    function isApplicantLoggedIn()
    {
        // echo "string";exit();
        $this->load->library('session');
        $isApplicantLoggedIn = $this->session->userdata('isApplicantLoggedIn');

        if (!isset($isApplicantLoggedIn) || $isApplicantLoggedIn != TRUE) {
            redirect('applicantLogin');
        } else {
            $this->applicant_name = $this->session->userdata('applicant_name');
            $this->id_applicant = $this->session->userdata('id_applicant');
            $this->applicant_last_login = $this->session->userdata('applicant_last_login');

            $this->global['applicant_name'] = $this->applicant_name;
            $this->global['id_applicant'] = $this->id_applicant;
            $this->global['applicant_last_login'] = $this->applicant_last_login;
        }
    }


    function checkAccess($code)
    {
        return 0;
        $this->load->model('setup/role_model');
        $canAccess = $this->role_model->checkAccess($this->role, $code);
        return $canAccess;
    }
    function hasExamActive()
    {
        $this->load->model('setup/role_model');
        $canAccess = $this->role_model->hasExamActive($this->session->id_student);
        return $canAccess;
    }


    function isTicketter()
    {
        if ($this->role != ROLE_ADMIN || $this->role != ROLE_MANAGER) {
            return true;
        } else {
            return false;
        }
    }

    function loadAccessRestricted()
    {
        $this->global['pageTitle'] = 'Examination Management System : Access Denied';

        $this->load->view('includes/header', $this->global);
        $this->load->view('access');
        $this->load->view('includes/footer');
    }
    function loadNoExams()
    {
        $this->global['pageTitle'] = 'Examination Management System : No Exams';

        $this->load->view('includes/header', $this->global);
        $this->load->view('noexams');
        $this->load->view('includes/footer');
    }
    function logout()
    {
        $this->session->sess_destroy();

        redirect('login');
    }

    function loadViews($viewName = "", $headerInfo = NULL, $pageInfo = NULL, $footerInfo = NULL)
    {

        $this->load->view('includes/header', $headerInfo);
        $this->load->view('includes/sidebar', $headerInfo);
        $this->load->view('includes/footer', $footerInfo);
        $this->load->view($viewName, $pageInfo);
    }
    function loadExamViews($viewName = "", $headerInfo = NULL, $pageInfo = NULL, $footerInfo = NULL)
    {

        $this->load->view('includes/sidebar', $headerInfo);
        $this->load->view($viewName, $pageInfo);
    }

    function paginationCompress($link, $count, $perPage = 10, $segment = SEGMENT)
    {
        $this->load->library('pagination');

        $config['base_url'] = base_url() . $link;
        $config['total_rows'] = $count;
        $config['uri_segment'] = $segment;
        $config['per_page'] = $perPage;
        $config['num_links'] = 5;
        $config['full_tag_open'] = '<nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav>';
        $config['first_tag_open'] = '<li class="arrow">';
        $config['first_link'] = 'First';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="arrow">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="arrow">';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="arrow">';
        $config['last_link'] = 'Last';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $page = $config['per_page'];
        $segment = $this->uri->segment($segment);

        return array(
            "page" => $page,
            "segment" => $segment
        );
    }

    function getDomainName()
    {
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $domainName = $_SERVER['HTTP_HOST'] . '/';
        return $protocol . $domainName;
    }

    function getMpdfLibrary()
    {
        $base_url = $_SERVER['HTTP_HOST'];

        // $main_invoice = $this->main_invoice_model->getApplicantInformation('28');
        // print_r($base_url);exit;
        
        switch ($base_url)
        {
            case 'examination.com':
            case 'https://examination.com':

            include("/var/www/html/examination/assets/mpdf/vendor/autoload.php");
            break;
            

            case 'examination.camsedu.com':
            case 'https://examination.camsedu.com':
            include("/home/camsedu/examination.camsedu.com/assets/mpdf/vendor/autoload.php");

            break;


            case 'exam.camsedu.com':
            case 'https://exam.camsedu.com':
            include("/home/camsedu/exam.camsedu.com/assets/mpdf/vendor/autoload.php");
            break;


            case 'college.com':
            case 'https://college.com':
            include("/var/www/html/college/assets/mpdf/vendor/autoload.php");
            
            break;
    
            default:
            break;
        }
    }


    // 3 Parameters 
    // 1. File Name
    // 2. File Temp Name
    // 2. File Key (To Show Erroe msz)

    function uploadFile($file, $file_tmp, $key)
    {
        // echo microtime();exit;
        // $date = date('dmY_his');

        $uniqueId = rand(0000000000, 9999999999);
        $upload_name = md5($uniqueId);

        $domain = $this->getDomainName();

        $root = $_SERVER['DOCUMENT_ROOT'];
        // echo $root;exit;

        $upload_path = $root . '/assets/images/';


        $fileinfo = pathinfo($file);

        $extension = $fileinfo['extension'];
        $file_name = $fileinfo['filename'];

        $uploaded_file_name = $upload_name . '.' . $extension;
        $file_path = $upload_path . $uploaded_file_name;

        // $data['certificate'] = $uploaded_file_name;
        // echo "<Pre>";print_r($file_path);exit();


        if (move_uploaded_file($file_tmp, $file_path)) {
            return $uploaded_file_name;
        } else {
            echo "Unable To Uplaod" . $key . " Try After Some Time";
        }
    }

    // For Validation Of Size & Formats
    // 1. File Extension
    // 2. File Size
    // 2. File Key (To Show Erroe msz)


    function fileFormatNSizeValidation($file_ext, $file_size, $key)
    {
        $extensions = array("jpeg", "jpg", "png", "docx", "pdf");

        if (in_array($file_ext, $extensions) === false) {
            $errors[] = $key . "File Format Not Allowed, Choose a JPEG/ PNG/ JPG/ DOCX/ PDF file.";
        }

        if ($file_size > 2097152) {
            $errors[] = $key . 'File size must be Below 2 MB';
        }
    }


    function deleteServerFile($file)
    {
        $root = $_SERVER['DOCUMENT_ROOT'];
        $path = $root . '/assets/images/ ' . $file;
        // $path="uploads/file_01.jpg";
        if (unlink($path)) {
            echo "File Deleted";
        } else {
            echo "Not Deleted";
        }
    }
}

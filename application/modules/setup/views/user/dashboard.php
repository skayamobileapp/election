<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
            <h1 class="h3">Listing</h1>
            <button type="button" class="btn btn-primary ml-auto">+ Add Listing</button>
          </div>
          <div class="page-container">
            <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
              <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingOne">
                      <h4 class="panel-title">
                          <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="">
                              Advanced Search
                              <i class="fa fa-angle-right" aria-hidden="true"></i>
                              <i class="fa fa-angle-down" aria-hidden="true"></i>
                          </a>
                      </h4>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true" style="">
                  <div class="panel-body p-2">
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Name</label>
                          <div class="col-sm-9">
                            <input type="Name" class="form-control" id="colFormLabel" placeholder="">
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Email</label>
                          <div class="col-sm-9">
                            <input type="email" class="form-control" id="colFormLabel" placeholder="">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Mobile</label>
                          <div class="col-sm-9">
                            <input type="Name" class="form-control" id="colFormLabel" placeholder="">
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Role</label>
                          <div class="col-sm-9">
                            <select name="" id="" class="form-control">
                              <option value="">Select</option>
                          </select>
                          </div>
                        </div>
                      </div>
                    </div>          
                    <hr />          
                      <div class="d-flex justify-content-center">
                          <button type="button" class="btn btn-primary">Search</button>
                          <button type="button" class="btn btn-link">Clear All Fields</button>                            
                      </div>
                  </div>
                  </div>
              </div>
          </div>            
            <div class="custom-table">
              <table class="table">
                  <thead>
                      <tr>
                          <th>Full Name</th>
                          <th>Staff Id</th>
                          <th>Email</th>
                          <th>Department</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td>
                              <a href="#">Abdul Ghani Bin Ibrahim</a>
                          </td>
                          <td>560531055007</td>
                          <td>a_ghani56@yahoo.com</td>
                          <td>Admission</td>
                          <td>
                            <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Edit"><i
                                class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Delete"><i
                                  class="fa fa-trash-o" aria-hidden="true"></i></button>
                                  <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Download"><i
                                    class="fa fa-download" aria-hidden="true"></i></button>                                                                  
                          </td>
                      </tr>
                      <tr>
                          <td>
                              <a href="#">Abdul Ghani Bin Ibrahim</a>
                          </td>
                          <td>560531055007</td>
                          <td>a_ghani56@yahoo.com</td>
                          <td>Admission</td>
                          <td>
                            <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Edit"><i
                                class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Delete"><i
                                  class="fa fa-trash-o" aria-hidden="true"></i></button>
                                  <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Download"><i
                                    class="fa fa-download" aria-hidden="true"></i></button>                                                                  
                          </td>
                      </tr>
                      <tr>
                          <td>
                              <a href="#">Abdul Ghani Bin Ibrahim</a>
                          </td>
                          <td>560531055007</td>
                          <td>a_ghani56@yahoo.com</td>
                          <td>Admission</td>
                          <td>
                            <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Edit"><i
                                class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Delete"><i
                                  class="fa fa-trash-o" aria-hidden="true"></i></button>
                                  <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Download"><i
                                    class="fa fa-download" aria-hidden="true"></i></button>                                                                  
                          </td>
                      </tr>
                      <tr>
                          <td>
                              <a href="#">Abdul Ghani Bin Ibrahim</a>
                          </td>
                          <td>560531055007</td>
                          <td>a_ghani56@yahoo.com</td>
                          <td>Admission</td>
                          <td>
                            <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Edit"><i
                                class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Delete"><i
                                  class="fa fa-trash-o" aria-hidden="true"></i></button>
                                  <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Download"><i
                                    class="fa fa-download" aria-hidden="true"></i></button>                                                                  
                          </td>
                      </tr>
                      <tr>
                          <td>
                              <a href="#">Abdul Ghani Bin Ibrahim</a>
                          </td>
                          <td>560531055007</td>
                          <td>a_ghani56@yahoo.com</td>
                          <td>Admission</td>
                          <td>
                            <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Edit"><i
                                class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Delete"><i
                                  class="fa fa-trash-o" aria-hidden="true"></i></button>
                                  <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Download"><i
                                    class="fa fa-download" aria-hidden="true"></i></button>                                                                  
                          </td>
                      </tr>
                      <tr>
                          <td>
                              <a href="#">Abdul Ghani Bin Ibrahim</a>
                          </td>
                          <td>560531055007</td>
                          <td>a_ghani56@yahoo.com</td>
                          <td>Admission</td>
                          <td>
                            <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Edit"><i
                                class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Delete"><i
                                  class="fa fa-trash-o" aria-hidden="true"></i></button>
                                  <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Download"><i
                                    class="fa fa-download" aria-hidden="true"></i></button>                                                                  
                          </td>
                      </tr>
                      <tr>
                          <td>
                              <a href="#">Abdul Ghani Bin Ibrahim</a>
                          </td>
                          <td>560531055007</td>
                          <td>a_ghani56@yahoo.com</td>
                          <td>Admission</td>
                          <td>
                            <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Edit"><i
                                class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Delete"><i
                                  class="fa fa-trash-o" aria-hidden="true"></i></button>
                                  <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Download"><i
                                    class="fa fa-download" aria-hidden="true"></i></button>                                                                  
                          </td>
                      </tr>
                      <tr>
                          <td>
                              <a href="#">Abdul Ghani Bin Ibrahim</a>
                          </td>
                          <td>560531055007</td>
                          <td>a_ghani56@yahoo.com</td>
                          <td>Admission</td>
                          <td>
                            <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Edit"><i
                                class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Delete"><i
                                  class="fa fa-trash-o" aria-hidden="true"></i></button>
                                  <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Download"><i
                                    class="fa fa-download" aria-hidden="true"></i></button>                                                                  
                          </td>
                      </tr>
                      <tr>
                          <td>
                              <a href="#">Abdul Ghani Bin Ibrahim</a>
                          </td>
                          <td>560531055007</td>
                          <td>a_ghani56@yahoo.com</td>
                          <td>Admission</td>
                          <td>
                            <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Edit"><i
                                class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Delete"><i
                                  class="fa fa-trash-o" aria-hidden="true"></i></button>
                                  <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Download"><i
                                    class="fa fa-download" aria-hidden="true"></i></button>                                                                  
                          </td>
                      </tr>
                      <tr>
                          <td>
                              <a href="#">Abdul Ghani Bin Ibrahim</a>
                          </td>
                          <td>560531055007</td>
                          <td>a_ghani56@yahoo.com</td>
                          <td>Admission</td>
                          <td>
                            <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Edit"><i
                                class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Delete"><i
                                  class="fa fa-trash-o" aria-hidden="true"></i></button>
                                  <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Download"><i
                                    class="fa fa-download" aria-hidden="true"></i></button>                                                                  
                          </td>
                      </tr>
                      <tr>
                          <td>
                              <a href="#">Abdul Ghani Bin Ibrahim</a>
                          </td>
                          <td>560531055007</td>
                          <td>a_ghani56@yahoo.com</td>
                          <td>Admission</td>
                          <td>
                            <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Edit"><i
                                class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Delete"><i
                                  class="fa fa-trash-o" aria-hidden="true"></i></button>
                                  <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Download"><i
                                    class="fa fa-download" aria-hidden="true"></i></button>                                                                  
                          </td>
                      </tr>
                      <tr>
                          <td>
                              <a href="#">Abdul Ghani Bin Ibrahim</a>
                          </td>
                          <td>560531055007</td>
                          <td>a_ghani56@yahoo.com</td>
                          <td>Admission</td>
                          <td>
                            <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Edit"><i
                                class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Delete"><i
                                  class="fa fa-trash-o" aria-hidden="true"></i></button>
                                  <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Download"><i
                                    class="fa fa-download" aria-hidden="true"></i></button>                                                                  
                          </td>
                      </tr>
                      <tr>
                          <td>
                              <a href="#">Abdul Ghani Bin Ibrahim</a>
                          </td>
                          <td>560531055007</td>
                          <td>a_ghani56@yahoo.com</td>
                          <td>Admission</td>
                          <td>
                            <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Edit"><i
                                class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Delete"><i
                                  class="fa fa-trash-o" aria-hidden="true"></i></button>
                                  <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Download"><i
                                    class="fa fa-download" aria-hidden="true"></i></button>                                                                  
                          </td>
                      </tr>
                      <tr>
                          <td>
                              <a href="#">Abdul Ghani Bin Ibrahim</a>
                          </td>
                          <td>560531055007</td>
                          <td>a_ghani56@yahoo.com</td>
                          <td>Admission</td>
                          <td>
                            <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Edit"><i
                                class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Delete"><i
                                  class="fa fa-trash-o" aria-hidden="true"></i></button>
                                  <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Download"><i
                                    class="fa fa-download" aria-hidden="true"></i></button>                                                                  
                          </td>
                      </tr>
                      <tr>
                          <td>
                              <a href="#">Abdul Ghani Bin Ibrahim</a>
                          </td>
                          <td>560531055007</td>
                          <td>a_ghani56@yahoo.com</td>
                          <td>Admission</td>
                          <td>
                            <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Edit"><i
                                class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Delete"><i
                                  class="fa fa-trash-o" aria-hidden="true"></i></button>
                                  <button class="btn" type="" data-toggle="tooltip" data-placement="top" title="Download"><i
                                    class="fa fa-download" aria-hidden="true"></i></button>                                                                  
                          </td>
                      </tr>

                  </tbody>
              </table>
          </div>
          <div class="table-footer pb-2">
                    <ul class="pagination">
                        <li class="page-item">
                            <a href="#" aria-label="Previous" class="page-link">
                                <span aria-hidden="true">«</span>
                            </a>
                        </li>
                        <li class="page-item active"><a href="#" class="page-link">1</a></li>
                        <li class="page-item"><a href="#" class="page-link">2</a></li>
                        <li class="page-item"><a href="#" class="page-link">3</a></li>
                        <li class="page-item"><a href="#" class="page-link">4</a></li>
                        <li class="page-item"><a href="#" class="page-link">5</a></li>
                        <li class="page-item">
                            <a href="#" aria-label="Next" class="page-link">
                                <span aria-hidden="true">»</span>
                            </a>
                        </li>
                    </ul>
                    <div class="entries">
                        Show
                        <select class="form-control">
                            <option value="10">10</option>
                            <option value="15" selected="">15</option>
                        </select>
                        entries
                    </div>
                    <p class="entries-count mr-3">1 - 15 out of 45 entries</p>
                </div>                      
          </div>
        </main>
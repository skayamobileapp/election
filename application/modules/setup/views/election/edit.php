<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Edit Election Details</h1>
        
        <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    
    <form id="form_main" action="" method="post">

        <div class="page-container">

          <div>
            <h4 class="form-title">Election details</h4>
          </div>

            <div class="form-container">


                <div class="row">

                    
                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Name <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo $election->name; ?>">
                        </div>
                      </div>
                    </div>


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Date <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control datepicker" id="election_date" name="election_date" placeholder="Election Date" value="<?php echo date('d-m-Y', strtotime($election->election_date)); ?>">
                        </div>
                      </div>
                    </div>



                </div>





                <div class="row">

                    
                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Start Time <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="time" class="form-control" id="from_time" name="from_time" placeholder="Start Time" value="<?php echo $election->from_time; ?>">
                        </div>
                      </div>
                    </div>


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">End Time <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="time" class="form-control" id="to_time" name="to_time" placeholder="End Time" value="<?php echo $election->to_time; ?>">
                        </div>
                      </div>
                    </div>



                </div>


                <div class="row">

                    
                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Organisation <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="organisation" name="organisation" placeholder="Organisation" value="<?php echo $election->organisation; ?>">
                        </div>
                      </div>
                    </div>


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Department <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="department" name="department" placeholder="Department" value="<?php echo $election->department; ?>">
                        </div>
                      </div>
                    </div>



                </div>





                <div class="row">

                    
                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Address <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="address1" name="address1" placeholder="Address" value="<?php echo $election->address1; ?>">
                        </div>
                      </div>
                    </div>



                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Country <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <select name="id_country" id="id_country" class="form-control" onchange="getStateByCountry(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($countryList))
                            {
                                foreach ($countryList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                <?php
                                if($record->id == $election->id_country)
                                {
                                    echo 'selected';
                                }
                                ?>
                                >
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                          </select>
                          </div>
                        </div>
                    </div>


                </div>



                <div class="row">

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">State <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <span id="view_state">
                                <select class="form-control" id='id_state' name='id_state'>
                                  <option value=''></option>
                                </select>
                            </span>
                          </div>
                        </div>
                    </div>


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">City <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="city" name="city" placeholder="City" value="<?php echo $election->city; ?>">
                        </div>
                      </div>
                    </div>

                </div>



                <div class="row">

                  <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Zipcode <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="number" class="form-control" id="zipcode" name="zipcode" placeholder="Zipcode" value="<?php echo $election->zipcode; ?>">
                        </div>
                      </div>
                    </div>



                </div>





                <!-- <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row align-items-center">
                        <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1" checked="checked">
                            <label class="custom-control-label" for="customRadioInline1">Active</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0">
                            <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                          </div>
                        </div>
                      </div>
                    </div>

                </div>        -->


                  

            


            </div>                                
        </div>


        <div class="page-container">

          <div>
            <h4 class="form-title">Contact details</h4>
          </div>

            <div class="form-container">


                <div class="row">

                    
                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Contact Person <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="contact_person" name="contact_person" placeholder="Name" value="<?php echo $election->contact_person; ?>">
                        </div>
                      </div>
                    </div>


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Contact Email <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="email" class="form-control" id="contact_email" name="contact_email" placeholder="Email" value="<?php echo $election->contact_email; ?>">
                        </div>
                      </div>
                    </div>



                </div>





                <div class="row">

                    
                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Contact Phone <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="number" class="form-control" id="phone" name="phone" placeholder="Phone No." value="<?php echo $election->phone; ?>">
                        </div>
                      </div>
                    </div>


                </div>


              </div>


        </div>



        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <!-- <a href='add' class="btn btn-link">Clear All Fields</a> -->
                
            </div>

        </div>


    </form>
</main>

<script>

  $( function() {
      $( ".datepicker" ).datepicker({
          changeYear: true,
          changeMonth: true,
      });
    } );

    $('select').select2();


    function getStateByCountry(id)
    {
      if(id != '')
      {
        $.get("/setup/election/getStateByCountry/"+id,

        function(data, status)
        {
            $("#view_state").html(data);
            // $("#view_programme_details").html(data);
            // $("#view_programme_details").show();
        });
      }
      // else
      // {
      //     $("#view_state").html('');
      // }
    }




    $(document).ready(function()
    {
        var id_country = "<?php echo $election->id_country; ?>";
        // alert(id_country);
        if(id_country != '')
        {
            $.get("/setup/election/getStateByCountry/"+id_country,

            function(data, status)
            {
                // alert(data);
                $("#view_state").html(data);

                var id_state = "<?php echo $election->id_state; ?>";
                // alert(id_state);
                // if(id_state < 0)
                // {
                    $("#id_state").find('option[value="'+id_state+'"]').attr('selected',true);
                    $('select').select2();
                // }
                
                // $("#view_programme_details").html(data);
                // $("#view_programme_details").show();
            });
        }


        $("#form_main").validate({
            rules: {
                name: {
                    required: true
                },
                election_date: {
                    required: true
                },
                from_time: {
                    required: true
                },
                to_time: {
                    required: true
                },
                organisation: {
                    required: true
                },
                department: {
                    required: true
                },
                address1: {
                    required: true
                },
                id_country: {
                    required: true
                },
                id_state: {
                    required: true
                },
                city: {
                    required: true
                },
                zipcode: {
                    required: true
                },
                contact_person: {
                    required: true
                },
                contact_email: {
                    required: true
                },
                phone: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                election_date: {
                    required: "<p class='error-text'>Select Election Date</p>",
                },
                from_time: {
                    required: "<p class='error-text'>Select Start Time</p>",
                },
                to_time: {
                    required: "<p class='error-text'>Select End Time</p>",
                },
                organisation: {
                    required: "<p class='error-text'>Organisation Required</p>",
                },
                department: {
                    required: "<p class='error-text'>Department Required</p>",
                },
                address1: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                id_country: {
                    required: "<p class='error-text'>Select Country</p>",
                },
                id_state: {
                    required: "<p class='error-text'>Select State</p>",
                },
                city: {
                    required: "<p class='error-text'>Select City</p>",
                },
                zipcode: {
                    required: "<p class='error-text'>Zipcode Required</p>",
                },
                contact_person: {
                    required: "<p class='error-text'>Contact Person Required</p>",
                },
                contact_email: {
                    required: "<p class='error-text'>Contact Email Required</p>",
                },
                phone: {
                    required: "<p class='error-text'>Phone No. Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
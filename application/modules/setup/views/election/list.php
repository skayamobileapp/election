<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Election List</h1>
    <a href="add" class="btn btn-primary ml-auto">+ Add Election</a>
  </div>
  <div class="page-container">
    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="">
              Advanced Search
              <i class="fa fa-angle-right" aria-hidden="true"></i>
              <i class="fa fa-angle-down" aria-hidden="true"></i>
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">

          <div id="collapseOne" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true" style="">
            <div class="panel-body p-2">

              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Election</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo $searchParam['name'] ?>">
                    </div>
                  </div>
                </div>
              </div>

              <hr />
              <div class="d-flex justify-content-center">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href='list' class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>

        </form>

      </div>
    </div>
    <?php
    if ($this->session->flashdata('success')) {
    ?>
      <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
      </div>
    <?php
    }
    if ($this->session->flashdata('error')) {
    ?>
      <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Failure!</strong> <?php echo $this->session->flashdata('error'); ?>
      </div>
    <?php
    }
    ?>
    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Name</th>
            <th>Election Date</th>
            <th>Start Time</th>
            <th>End Time</th>
            <th>Organisation</th>
            <th>Department</th>
            <th>City</th>
            <th>Contact Person</th>
            <th>Created By</th>
            <th>Created On</th>
            <!-- <th>Name In Other Language</th> -->
            <!-- <th class="text-center">Status</th> -->
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($electionList)) {
            $i = 1;
            foreach ($electionList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->name; ?></td>
                <td><?php 
                if($record->created_dt_tm)
                { 
                  echo date('d-m-Y', strtotime($record->election_date)); 
                } ?> 
                </td>
                <td><?php echo $record->from_time; ?></td>
                <td><?php echo $record->to_time; ?></td>
                <td><?php echo $record->organisation; ?></td>
                <td><?php echo $record->department; ?></td>
                <td><?php echo $record->city; ?></td>
                <td><?php echo $record->contact_person; ?></td>
                <td><?php echo $record->created_by; ?></td>
                <td><?php 
                if($record->created_dt_tm)
                { 
                  echo date('d-m-Y', strtotime($record->created_dt_tm)); 
                } ?> 
                </td>
                <!-- <td><?php 
                if($record->created_dt_tm)
                { 
                  echo date('d-m-Y', strtotime($record->created_dt_tm)); 
                } ?> 
                </td>
                <td><?php if($record->updated_dt_tm){ echo date('d-m-Y', strtotime($record->updated_dt_tm)); } ?></td> -->
                <!-- <td style="text-align: center;">
                  <?php if ($record->status == '1')
                  {
                    echo "Active";
                  } else {
                    echo "In-Active";
                  }
                  ?>
                </td> -->
                <td class="text-center">

                  <a href="<?php echo 'edit/' . $record->id; ?>" class="btn" type="" data-toggle="tooltip" data-placement="top" title="Edit">
                    <i class="fa fa-pencil-square-o" aria-hidden="true">
                    </i>
                  </a>

                </td>
              </tr>
          <?php
              $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</main>
<script>
  $('select').select2();

  function clearSearchForm() {
    window.location.reload();
  }
</script>
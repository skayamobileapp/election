<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Edit Participants Details</h1>
        
        <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    
    <form id="form_main" action="" method="post" enctype="multipart/form-data">

        <div class="page-container">

          <div>
            <h4 class="form-title">Participants details</h4>
          </div>

            <div class="form-container">


                <div class="row">

                    
                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Name <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo $participants->name; ?>">
                        </div>
                      </div>
                    </div>


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Description <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="description" name="description" placeholder="Description" value="<?php echo $participants->description; ?>">
                        </div>
                      </div>
                    </div>



                </div>





                <div class="row">


                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Election <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <select name="id_election" id="id_election" class="form-control" onchange="getElectionTitleByElectionId(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($electionList))
                            {
                                foreach ($electionList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                              <?php
                                if($record->id == $participants->id_election)
                                {
                                    echo 'selected';
                                }
                                ?>
                                >
                                <?php echo date('d-m-Y', strtotime($record->election_date)) . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                          </select>
                          </div>
                        </div>
                    </div>


                  

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Election Title <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <span id="view_election_title">
                                <select class="form-control" id='id_title' name='id_title'>
                                  <option value=''></option>
                                </select>
                            </span>
                          </div>
                        </div>
                    </div>

                </div>



                <div class="row">

                  <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Image <span class="text-danger">*</span>
                        <?php
                        if($participants->image)
                        {
                            ?>
                            <a href="<?php echo '/assets/images/' . $participants->image; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $participants->image; ?>)" title="<?php echo $participants->image; ?>">
                                ( View )
                            </a> 

                            <?php
                        }
                        ?>
                        </label>
                        <div class="col-sm-8">
                        <input type="file" class="form-control" id="image" name="image" placeholder="Zipcode">
                        </div>
                      </div>
                    </div>


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Logo <span class="text-danger">*</span>
                        <?php
                        if($participants->logo)
                        {
                            ?>
                            <a href="<?php echo '/assets/images/' . $participants->logo; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $participants->logo; ?>)" title="<?php echo $participants->logo; ?>">
                                ( View )
                            </a> 

                            <?php
                        }
                        ?>
                        </label>
                        <div class="col-sm-8">
                        <input type="file" class="form-control" id="logo" name="logo" placeholder="Logo">
                        </div>
                      </div>
                    </div>



                </div>                  

            


            </div>                                
        </div>


        <div class="page-container">

          <div>
            <h4 class="form-title">Contact details</h4>
          </div>

            <div class="form-container">


                <div class="row">


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Contact Email <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?php echo $participants->email; ?>">
                        </div>
                      </div>
                    </div>


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Contact Phone <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="number" class="form-control" id="phone" name="phone" placeholder="Phone No." value="<?php echo $participants->phone; ?>">
                        </div>
                      </div>
                    </div>




                </div>





                <div class="row">

                  

                </div>


              </div>


        </div>



        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <!-- <a href='add' class="btn btn-link">Clear All Fields</a> -->
                
            </div>

        </div>


    </form>
</main>

<script>

  $( function() {
      $( ".datepicker" ).datepicker({
          changeYear: true,
          changeMonth: true,
      });
    } );

    $('select').select2();


    function getElectionTitleByElectionId(id)
    {
      if(id != '')
      {
        $.get("/setup/participants/getElectionTitleByElectionId/"+id,

        function(data, status)
        {
            $("#view_election_title").html(data);
            // $("#view_programme_details").html(data);
            // $("#view_programme_details").show();
        });
      }
      // else
      // {
      //     $("#view_state").html('');
      // }
    }




    $(document).ready(function()
    {

      var id_election = "<?php echo $participants->id_election; ?>";
        // alert(id_country);
        if(id_election != '')
        {
            $.get("/setup/participants/getElectionTitleByElectionId/"+id_election,

            function(data, status)
            {
                $("#view_election_title").html(data);

                var id_title = "<?php echo $participants->id_title; ?>";
                $("#id_title").find('option[value="'+id_title+'"]').attr('selected',true);
                $('select').select2();
            });
        }


        $("#form_main").validate({
            rules: {
                name: {
                    required: true
                },
                description: {
                    required: true
                },
                id_election: {
                    required: true
                },
                id_title: {
                    required: true
                },
                email: {
                    required: true
                },
                phone: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                description: {
                    required: "<p class='error-text'>Description Required</p>",
                },
                id_election: {
                    required: "<p class='error-text'>Select Election</p>",
                },
                id_title: {
                    required: "<p class='error-text'>Select Title</p>",
                },
                email: {
                    required: "<p class='error-text'>Email Required</p>",
                },
                phone: {
                    required: "<p class='error-text'>Phone Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
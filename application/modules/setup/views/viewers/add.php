<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Add Viewers</h1>
        
        <a href='list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    
    <form id="form_main" action="" method="post">

        <div class="page-container">

          <div>
            <h4 class="form-title">Viewers details</h4>
          </div>

            <div class="form-container">


                <div class="row">

                    
                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Name <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                        </div>
                      </div>
                    </div>


                    <!-- <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Election <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <select name="id_election" id="id_election" class="form-control"
                             
                             >
                            <option value="">Select</option>
                            <?php
                            // onchange="getElection(this.value)"
                            if (!empty($electionList))
                            {
                                foreach ($electionList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo date('d-m-Y', strtotime($record->election_date)) . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                          </select>
                          </div>
                        </div>
                    </div> -->

                


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Email <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                        </div>
                      </div>
                    </div>


                  </div>



                  <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Phone <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="number" class="form-control" id="phone" name="phone" placeholder="Phone No.">
                        </div>
                      </div>
                    </div>



                </div>             

            


            </div>                                
        </div>




        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a href='add' class="btn btn-link">Clear All Fields</a>
                
            </div>

        </div>


    </form>
</main>

<script>

  $( function() {
      $( ".datepicker" ).datepicker({
          changeYear: true,
          changeMonth: true,
      });
    } );

    $('select').select2();


    function getStateByCountry(id)
    {
      if(id != '')
      {
        $.get("/setup/election/getStateByCountry/"+id,

        function(data, status)
        {
            $("#view_state").html(data);
            // $("#view_programme_details").html(data);
            // $("#view_programme_details").show();
        });
      }
      // else
      // {
      //     $("#view_state").html('');
      // }
    }




    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                name: {
                    required: true
                },
                id_election: {
                    required: true
                },
                email: {
                    required: true
                },
                phone: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                id_election: {
                    required: "<p class='error-text'>Select Election</p>",
                },
                email: {
                    required: "<p class='error-text'>Email Required</p>",
                },
                phone: {
                    required: "<p class='error-text'>Phone Number Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<style>
  .menu-tree,
.menu-tree ul {
  list-style: none;
  padding-left: 0px;
}
.menu-tree li {
  padding-left: 50px;
  position: relative;
  margin-bottom: 15px;
}
.menu-tree li .icon {
  position: absolute;
  left: 20px;
  top: 0px;
  width: 20px;
  height: 20px;
  content: "";
  z-index: 1;
  display: block;
  opacity: 0.6;
}
.menu-tree li .icon.folder {
  background: url("<?php echo BASE_PATH;?>assets/img/folder.svg") no-repeat center center transparent;
}
.menu-tree li .icon.folder::before {
  position: absolute;
  left: -20px;
  top: 0px;
  width: 20px;
  height: 20px;
  content: "";
  z-index: 1;
  display: block;
  opacity: 0.9;
  background: url("<?php echo BASE_PATH;?>assets/img/dark_arrow.svg") no-repeat center center transparent;
  transform: rotate(90deg);
  transition: all 0.15s ease-in;
  background-size: 10px;
}
.menu-tree li .icon.folder.collapsed::before {
  transform: rotate(0deg);
}
.menu-tree li .icon.page {
  background: url(../img/page.svg) no-repeat center center transparent;
}
</style>
<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Edit Role</h1>

    <a href='../list' class="btn btn-link ml-auto">
      <i class="fa fa-chevron-left" aria-hidden="true"></i>
      Back
    </a>

  </div>

  <form id="form_main" action="" method="post">

    <div class="page-container">

      <div>
        <h4 class="form-title">Role details</h4>
      </div>

      

      <div class="form-container">


          <div class="clearfix">
              <ul class="menu-tree">
                                    
                  <li>
                      <span class="icon folder collapsed" data-toggle="collapse" href="#menu99" aria-expanded="false" aria-controls="menu99"></span>
                      <div class="checkbox">
                          <label>
                            <input type="checkbox">
                            <span class="check-radio"></span>
                            User Management
                          </label>
                      </div> 
                        
                      <div class="collapse" id="menu99">
                          <ul>

                              <?php for($i=0;$i<count($role);$i++) { ?>


                                <li>
                                  <span class="icon folder collapsed" data-toggle="collapse" href="#menu<?php echo $role[$i]->id;?>" aria-expanded="false" aria-controls="menu0"></span>
                                  <div class="checkbox">
                                      <label>
                                        <input type="checkbox">
                                        <span class="check-radio"></span>
                                        <?php echo $role[$i]->menu_name;?>                                            
                                    </label>
                                  </div> 
                                    
                                  <div class="collapse" id="menu<?php echo $role[$i]->id;?>">
                                      <ul>
                                      <?php 
                                      $getrolemenu = $this->role_model->getUsermenu($role[$i]->id); 
                                     ?>

                                     <?php for($m=0;$m<count($getrolemenu);$m++)
                                     {
                                        $permissionGranted = $this->role_model->checkpermission($idrole,$getrolemenu[$m]->id);
                                           $checked = "";
                                        if($permissionGranted)
                                        {
                                           $checked="checked=checked";
                                        }?>
                                          <li>
                                            <span class="icon page"></span>
                                              <div class="checkbox">
                                                  <label>
                                                    <input type="checkbox" name="checkrole[]" value="<?php echo $getrolemenu[$m]->id;?>" <?php echo $checked;?>>
                                                    <span class="check-radio"></span>
                                                    <?php echo $getrolemenu[$m]->description;?>
                                                  </label>
                                              </div>                                         
                                          </li>
                                        <?php
                                      }
                                      ?> 
                                         
                                      </ul>
                                  </div>                              
                              </li>

                            <?php }  ?> 
                              
                                                          
                          </ul>
                      </div>                              
                  </li>
              </ul>
          
          </div>


      </div>



      <div class="button-block clearfix">

          <div class="bttn-group">
              <button type="submit" class="btn btn-primary">Save</button>
              <a href="../list" class="btn btn-link">Back</a>
          </div>

      </div>


        </form>

      </div>
    </div>
  </form>
</main>

<script>
  $(document).ready(function() {
    $("#form_main").validate({
      rules: {
        role: {
          required: true
        },
        status: {
          required: true
        }
      },
      messages: {
        role: {
          required: "<p class='error-text'>Code Required</p>",
        },
        status: {
          required: "<p class='error-text'>Name Required</p>",
        }
      },
      errorElement: "span",
      errorPlacement: function(error, element) {
        error.appendTo(element.parent());
      }

    });
  });
</script>
<script type="text/javascript">
 $('#permissions').select2({
    placeholder: "Select the permissions",
    allowClear: true,
    multiple:true
  });
  function reloadPage() {
    window.location.reload();
  }
</script>
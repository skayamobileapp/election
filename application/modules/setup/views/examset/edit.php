<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Edit Examset</h1>

    <a href='../list' class="btn btn-link ml-auto">
      <i class="fa fa-chevron-left" aria-hidden="true"></i>
      Back
    </a>

  </div>

  <form id="form_main" action="" method="post" enctype="multipart/form-data">

    <div class="page-container">

      <div>
        <h4 class="form-title">Examset Details</h4>
      </div>

      <div class="form-container">


        <div class="row">
          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Examset Name <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="name" name="name" placeholder="Examset Name" value="<?php echo $examset->name; ?>">
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Duration <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                  <input type="text" class="form-control" id="duration" name="duration" placeholder="Examset Name" value="<?php echo $examset->duration; ?>">
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">TOS <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <select name="id_tos" id="id_tos" class="form-control">
                  <option value="">Select</option>
                  <?php
                  if (!empty($toslist)) {
                    foreach ($toslist as $record) {
                  ?>
                      <option value="<?php echo $record->id; ?>" <?php if($record->id == $examset->id_tos){ echo 'selected';} ?>>
                        <?php echo $record->name;  ?>
                      </option>
                  <?php
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>
      
          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Attempts <span class="text-danger">*</span></label>
              <div class="col-sm-8">
              <select name="attempts" id="attempts" class="form-control">
                  <option value="">Select</option>
                  <option value="1" <?php if($examset->attempts == 1){ echo 'selected'; } ?>>Once</option>
                  <option value="4" <?php if($examset->attempts == 4){ echo 'selected'; } ?>>Unlimited</option>
                </select>
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Grading Method <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <select name="grading_method" id="grading_method" class="form-control">
                  <option value="">Select</option>
                  <option value="1" <?php if($examset->grading_method == 1){ echo 'selected'; } ?>>Highest Grade</option>
                  <option value="2" <?php if($examset->grading_method == 2){ echo 'selected'; } ?>>Average Grade</option>
                  <option value="3" <?php if($examset->grading_method == 3){ echo 'selected'; } ?>>First Attempt</option>
                  <option value="4" <?php if($examset->grading_method == 4){ echo 'selected'; } ?>>Last Attempt</option>
                </select>
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Layout <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <select name="layout" id="layout" class="form-control">
                  <option value="">Select</option>
<!--                   <option value="1" <?php if($examset->layout == 1){ echo 'selected'; } ?>>Every Question</option>
                  <option value="2" <?php if($examset->layout == 2){ echo 'selected'; } ?>>Every 2 Questions</option>
                  <option value="3" <?php if($examset->layout == 3){ echo 'selected'; } ?>>Every 3 Questions</option>
 -->                  <option value="4" <?php if($examset->layout == 4){ echo 'selected'; } ?>>All Question in single page</option>
                </select>
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Question Behaviour <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <select name="behaviour" id="behaviour" class="form-control">
                  <option value="">Select</option>
                  <option value="1" <?php if($examset->behaviour == 1){ echo 'selected'; } ?>>Randomise and generate new set for every exam</option>
                  <option value="2" <?php if($examset->behaviour == 2){ echo 'selected'; } ?>>Randomise the previous attempt question by same candidate in next attempt (multiple attempts)</option>
                 <!--  <option value="3" <?php if($examset->behaviour == 3){ echo 'selected'; } ?>>Randomise for every student but generate one set only</option> -->
                </select>
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="form-group row align-items-center">
              <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1" <?php if ($examset->status == 1) {
                                                                                                                      echo "checked=checked";
                                                                                                                    }; ?>>
                  <label class="custom-control-label" for="customRadioInline1">Active</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0" <?php if ($examset->status == 0) {
                                                                                                                      echo "checked=checked";
                                                                                                                    }; ?>>
                  <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                </div>
              </div>
            </div>
          </div>

        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Instructions<span class="text-danger">*</span></label>
              <div class="col-sm-10">
                <textarea name="instructions" id="instructions"><?php echo $examset->instructions; ?></textarea>
              </div>
            </div>
          </div>
        </div>


        <div class="button-block clearfix">
          <div class="bttn-group">
            <button type="submit" class="btn btn-primary">Save</button>
          </div>

        </div>

      </div>
    </div>
  </form>
</main>
<script type="text/javascript" src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script>

<script>
  $(document).ready(function() {
    CKEDITOR.replace('instructions');
    $("#form_main").validate({
      rules: {
        name: {
          required: true
        },
        instructions: {
          required: true
        },
        duration: {
          required: true
        },
        pass_grade: {
          required: true
        },
        attempts: {
          required: true
        },
        grading_method: {
          required: true
        },
        layout: {
          required: true
        },
        behaviour: {
          required: true
        },
        id_tos: {
          required: true
        }
      },
      messages: {
        name: {
          required: "<p class='error-text'>Examset Name Required</p>",
        },
        instructions: {
          required: "<p class='error-text'>Instructions Required</p>",
        },
        duration: {
          required: "<p class='error-text'>Select Duration</p>",
        },
        pass_grade: {
          required: "<p class='error-text'>Pass Grade Required</p>",
        },
        attempts: {
          required: "<p class='error-text'>Select Attempts</p>",
        },
        grading_method: {
          required: "<p class='error-text'>Select Grading Method</p>",
        },
        layout: {
          required: "<p class='error-text'>Select Layout</p>",
        },
        behaviour: {
          required: "<p class='error-text'>Select Behaviour</p>",
        },
        id_tos: {
          required: "<p class='error-text'>Select TOS</p>",
        }
      },
      errorElement: "span",
      errorPlacement: function(error, element) {
        error.appendTo(element.parent());
      }

    });
  });
</script>
<script type="text/javascript">
  $('select').select2();

  function reloadPage() {
    window.location.reload();
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var attempts = $('#attempts').val();
    if(attempts == 4){
      $('#attempt_durationdiv').show();
    }
    else{
      $('#attempt_durationdiv').hide();
    }

    $('#attempts').change(function() {
      var id = $(this).val();
      if (id == 4) {
        $('#attempt_durationdiv').show();
      } else {
        $('#attempt_durationdiv').hide();
      }
    });

  });
</script>
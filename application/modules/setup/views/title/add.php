<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Add Election Title</h1>
        
        <a href='list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    
    <form id="form_main" action="" method="post">

        <div class="page-container">

          <div>
            <h4 class="form-title">Election Title details</h4>
          </div>

            <div class="form-container">


                <div class="row">



                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Name <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                        </div>
                      </div>
                    </div>



                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Election <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <select name="id_election" id="id_election" class="form-control"
                             
                             >
                            <option value="">Select</option>
                            <?php
                            // onchange="getElection(this.value)"
                            if (!empty($electionList))
                            {
                                foreach ($electionList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo date('d-m-Y', strtotime($record->election_date)) . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                          </select>
                          </div>
                        </div>
                    </div>

                </div>



                <div class="row">
                

                    <div class="col-lg-6">
                      <div class="form-group row align-items-center">
                        <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1" checked="checked">
                            <label class="custom-control-label" for="customRadioInline1">Active</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0">
                            <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                          </div>
                        </div>
                      </div>
                    </div>

                </div>       


                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <button type="submit" class="btn btn-primary">Save</button>
                      <a href='add' class="btn btn-link">Clear All Fields</a>
                      
                  </div>

                </div> 

            </div>                                
        </div>
    </form>
</main>

<script>

    $('select').select2();
  
    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                id_election: {
                    required: true
                },
                name: {
                    required: true
                }
            },
            messages: {
                id_election: {
                    required: "<p class='error-text'>Select Election</p>",
                },
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    function reloadPage()
    {
      window.location.reload();
    }
</script>
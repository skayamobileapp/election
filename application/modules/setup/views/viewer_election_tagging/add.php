<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Add Recepients To Group Message</h1>

    <a href='../list' class="btn btn-link ml-auto">
      <i class="fa fa-chevron-left" aria-hidden="true"></i>
      Back
    </a>

  </div>


  

  <form id="form_main" action="" method="post" enctype="multipart/form-data">

      <div class="page-container">

            <div>
              <h4 class="form-title">Election Title details</h4>
            </div>

              <div class="form-container">


                  <div class="row">



                      <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Name <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                          <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo $title->name; ?>" readonly>
                          </div>
                        </div>
                      </div>



                      <div class="col-lg-6">
                          <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Election <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                              <select name="id_election" id="id_election" class="form-control" disabled>
                              <option value="">Select</option>
                              <?php
                              if (!empty($electionList))
                              {
                                  foreach ($electionList as $record)
                                  {?>
                               <option value="<?php echo $record->id;  ?>"
                                  <?php
                                  if($record->id == $title->id_election)
                                  {
                                      echo 'selected';
                                  }
                                  ?>
                                  >
                                  <?php echo $record->election_date . " - " . $record->name;?>
                               </option>
                              <?php
                                  }
                              }
                              ?>
                            </select>
                            </div>
                          </div>
                      </div>

                  </div>



                  <div class="row">

                      <div class="col-lg-6">
                          <div class="form-group row align-items-center">
                            <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                              <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1" <?php if ($title->status == 1) {
                                  echo "checked=checked";
                                }; ?>>
                                <label class="custom-control-label" for="customRadioInline1">Active</label>
                              </div>
                              <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0" <?php if ($title->status == 0) {
                                  echo "checked=checked";
                                }; ?>>
                                <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                              </div>
                            </div>
                          </div>
                      </div>

                  </div>


                    
                  

              </div>                                
      
      </div>



        <br>


        <h4 class="form-title">Add Viewers</h4>
        <div class="form-container">
                          
                <div class="clearfix">
                    <ul class="nav nav-tabs" role="tablist" >
                        <li role="presentation" class="nav-item" ><a href="#invoice" class="nav-link active"
                                aria-controls="invoice" aria-selected="true"
                                role="tab" data-toggle="tab">Search</a>
                        </li>
                       <!--  <li role="presentation" class="nav-item"><a href="#receipt" class="nav-link"
                                aria-controls="receipt" role="tab" data-toggle="tab">Viewers List</a>
                        </li>
                         -->
                    </ul>

                    
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="invoice">

                            <h4 class="form-title">Viewer Search</h4>
                            <div class="form-container">


                                

                                    <div class="row" id="view_student_display">

                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Name / Email </label>
                                                    <input type="text" class="form-control" id="name" name="name">
                                                </div>
                                            </div>

                                    </div>

                                   


                                <div>
                                    <table border="0px" style="width: 100%">
                                        <tr>
                                            <td style="text-align: right;" colspan="6">
                                                <button type="button" id="btn_add_detail" onclick="getViewersListByData()" class="btn btn-primary btn-light btn-lg">Search</button>
                                            </td>
                                        </tr>

                                    </table>
                                    <br>
                                </div>


                                </div> 


                                <!-- <div class="form-container" id="view_visible" style="display: none;">
                                    <h4 class="form-group-title">Search Result</h4> -->

                                    <div id="view">
                                    </div>



                                    <div class="button-block clearfix">
                                      <div class="bttn-group">
                                          <button type="submit" class="btn btn-primary">Save</button>
                                          <!-- <a href='add' class="btn btn-link">Clear All Fields</a> -->
                                          
                                      </div>

                                    </div> 

                                <!-- </div> -->


                                <div class="custom-table" id="printReceipt">
                                    <table class="table" id="list-table">
                                        <thead>
                                        <tr>
                                            <th>Sl. no</th>
                                            <th>Viewer Name</th>
                                            <th>Email</th>
                                            <th>phone</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        if (!empty($viewerList))
                                        {
                                            $j=1;
                                            foreach ($viewerList as $record)
                                            {
                                        ?>
                                            <tr>
                                                <td><?php echo $j ?></td>
                                                <td><?php echo $record->name ?></td>
                                                <td><?php echo $record->email ?></td>
                                                <td><?php echo $record->phone ?></td>
                                                <td class="">
                                                <a onclick="deleteViewerElectionTagging(<?php echo $record->id ?>)">Delete</a>
                                                </td>
                                            </tr>
                                        <?php
                                            $j++;
                                            }
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>



                          </div> 
                    
                    </div>




                </div>
        </div>



  </form>


</main>
<!-- <script type="text/javascript" src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script> -->
<script>
  
  $('select').select2();

    function getViewersListByData()
    {
      // alert($("#name").val());
        var tempPR = {};
        tempPR['name'] = $("#name").val();
        tempPR['id_title'] = <?php echo $title->id ?>;
        tempPR['id_election'] = <?php echo $title->id_election ?>;

        $.ajax(
        {
           url: '/setup/ViewerElectionTagging/getViewersListByData',
           type: 'POST',
           data:
           {
            data: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
              // alert(result);
              $("#view").html(result);
             // $("#view_visible").show();
           }
        });
    }



     function deleteViewerElectionTagging(id)
    {
      // alert(id);
        $.get("/setup/ViewerElectionTagging/deleteViewerElectionTagging/"+id,
            function(data, status)
            {
              // alert(data);
                window.location.reload();
            });
    }



    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                name: {
                    required: true
                },
                type: {
                    required: true
                },
                id_template: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                type: {
                    required: "<p class='error-text'>Select Type</p>",
                },
                id_template: {
                    required: "<p class='error-text'>Select Template</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Participants_model extends CI_Model
{
    function electionListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('election');
        // $this->db->where('date(election_date) <', date('Y-m-d'));
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function countryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('country');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function stateListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getStateByCountryId($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getElectionTitleByElectionId($id_election)
    {
        $this->db->select('*');
        $this->db->from('election_title');
        $this->db->where('id_election', $id_election);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }


    function electionTitleListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('election_title');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }
    
    function participantsListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('participants as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function participantsListSearch($data)
    {
        $this->db->select('c.*, ele.name as election_name, ele.election_date, ele.city, t.name as election_title, cre.name as created_by, upd.name as updated_by');
        $this->db->from('participants as c');
        $this->db->join('election as ele', 'c.id_election = ele.id');
        $this->db->join('election_title as t', 'c.id_title = t.id');
        $this->db->join('users as cre', 'c.created_by = cre.id','left');
        $this->db->join('users as upd', 'c.updated_by = upd.id','left');
        // $this->db->join('staff as s', 'c.id_staff_coordinator = s.id');
        if (!empty($data['name']))
        {
            $likeCriteria = "(c.name  LIKE '%" . $data['name'] . "%' or c.description  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_title'] != '')
        {
            $this->db->where('c.id_title', $data['id_title']);
        }
        // if ($data['id_department'] != '')
        // {
        //     $this->db->where('c.id_department', $data['id_department']);
        // }
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

     function getParticipants($id)
    {
         $this->db->select('c.*');
        $this->db->from('participants as c');
        // $this->db->join('department as d', 'c.id_department = d.id','left');
        // $this->db->join('staff as s', 'c.id_staff_coordinator = s.id','left');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        $result = $query->row();
        // echo "<pre>";print_r($result);die;

        return $result;
    }

    function addNewParticipants($data)
    {
        $this->db->trans_start();
        $this->db->insert('participants', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editParticipants($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('participants', $data);

        return $result;
    }

    function deleteParticipants($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('participants', $data);

        return $this->db->affected_rows();
    }
}

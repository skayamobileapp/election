<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Difficult_level_model extends CI_Model
{

    function difficultLevelListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('difficult_level as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function difficultLevelListSearch($data)
    {
        $this->db->select('c.*, cre.name as created_by, upd.name as updated_by');
        $this->db->from('difficult_level as c');
        $this->db->join('users as cre', 'c.created_by = cre.id','left');
        $this->db->join('users as upd', 'c.updated_by = upd.id','left');
        if (!empty($data['name']))
        {
            $likeCriteria = "(c.name  LIKE '%" . $data['name'] . "%' or c.name_optional_language  LIKE '%" . $data['name'] . "%' or c.code  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

     function getDifficultLevel($id)
    {
        $this->db->select('c.*');
        $this->db->from('difficult_level as c');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function addNewDifficultLevel($data)
    {
        $this->db->trans_start();
        $this->db->insert('difficult_level', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editDifficultLevel($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('difficult_level', $data);

        return $result;
    }

    function deleteDifficultLevel($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('difficult_level', $data);
        return $this->db->affected_rows();
    }
}

<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Student_model extends CI_Model
{
    function studentList()
    {
        $this->db->select('a.*, b.name as stateName, c.name as countryName, ecl.name as location');
        $this->db->from('student as a');
        $this->db->join('state as b', 'a.id_state = b.id');
        $this->db->join('country as c', 'a.id_country = c.id');
        $this->db->join('student_location as ecl', 'a.id_location = ecl.id');
        $this->db->order_by("a.id", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();      
         return $result;
    }

    function studentLocationListForRegistration()
    {
        $this->db->select('a.*, ecl.name as location');
        $this->db->from('student as a');
        $this->db->join('state as b', 'a.id_state = b.id');
        $this->db->join('country as c', 'a.id_country = c.id');
        $this->db->join('student_location as ecl', 'a.id_location = ecl.id');
        $this->db->order_by("a.id", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();      
         return $result;
    }


    function studentListSearch($data)
    {
        // $date = 
        $this->db->select('a.*, b.name as stateName, c.name as countryName');
        $this->db->from('student as a');
        $this->db->join('state as b', 'a.permanent_state = b.id');
        $this->db->join('country as c', 'a.permanent_country = c.id');
        if ($data['name'] != '')
        {
            $likeCriteria = "(a.full_name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        // if ($data['id_location'] != '')
        // {
        //     $this->db->where('a.id_location', $data['id_location']);
        // }
        $this->db->order_by("a.full_name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getStudentList($id)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getSalutation($id)
    {
        $this->db->select('*');
        $this->db->from('salutation_setup');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    function studentExams($id)
    {
        $this->db->select('*');
        $this->db->from('user_exams as b');
        $this->db->join('student as a', 'a.id = b.user_id');
        $this->db->where('b.user_id', $id);
        $query = $this->db->get();
        return $query->result();
    }
    
    function addStudent($data)
    {
        $this->db->trans_start();
        $this->db->insert('student', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editStudent($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('student', $data);
        return $result;
    }



    function countryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('country');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function stateListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getStateByCountryId($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function salutationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('salutation_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function tosListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('tos');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function studentLocationList()
    {
        $this->db->select('*');
        $this->db->from('student_location');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function addStudentHasRoomCapacity($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_has_room', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getStudentHasRoom($id)
    {
        $this->db->select('*');
        $this->db->from('student_has_room');
        $this->db->where('id_student', $id);
        $this->db->order_by("room", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function deleteRoomCapacity($id)
    {
        $this->db->where('id', $id);
        $result = $this->db->delete('student_has_room');
        return $result;
    }
}
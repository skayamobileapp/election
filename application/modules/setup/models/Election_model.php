<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Election_model extends CI_Model
{
    function countryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('country');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function stateListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getStateByCountryId($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }
    
    function electionListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('election as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function electionListSearch($data)
    {
        $this->db->select('c.*, cre.name as created_by, upd.name as updated_by');
        $this->db->from('election as c');
        $this->db->join('users as cre', 'c.created_by = cre.id','left');
        $this->db->join('users as upd', 'c.updated_by = upd.id','left');
        // $this->db->join('staff as s', 'c.id_staff_coordinator = s.id');
        if (!empty($data['name']))
        {
            $likeCriteria = "(c.name  LIKE '%" . $data['name'] . "%' or c.name_optional_language  LIKE '%" . $data['name'] . "%' or c.code  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        // if ($data['id_staff'] != '')
        // {
        //     $this->db->where('c.id_staff_coordinator', $data['id_staff']);
        // }
        // if ($data['id_department'] != '')
        // {
        //     $this->db->where('c.id_department', $data['id_department']);
        // }
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

     function getElection($id)
    {
         $this->db->select('c.*');
        $this->db->from('election as c');
        // $this->db->join('department as d', 'c.id_department = d.id','left');
        // $this->db->join('staff as s', 'c.id_staff_coordinator = s.id','left');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        $result = $query->row();
        // echo "<pre>";print_r($result);die;

        return $result;
    }

    function addNewElection($data)
    {
        $this->db->trans_start();
        $this->db->insert('election', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editElection($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('election', $data);

        return $result;
    }

    function deleteElection($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('election', $data);

        return $this->db->affected_rows();
    }
}

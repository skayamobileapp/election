<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Viewer_election_tagging_model extends CI_Model
{

    function electionListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('election');
        // $this->db->where('date(election_date) <', date('Y-m-d'));
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }


    function titleListSearch($data)
    {
        $this->db->select('c.*, ele.name as election_name, ele.election_date, ele.city, cre.name as created_by, upd.name as updated_by');
        $this->db->from('election_title as c');
        $this->db->join('election as ele', 'c.id_election = ele.id');
        $this->db->join('users as cre', 'c.created_by = cre.id','left');
        $this->db->join('users as upd', 'c.updated_by = upd.id','left');
        if (!empty($data['name']))
        {
            $likeCriteria = "(c.name  LIKE '%" . $data['name'] . "%')";
            // $likeCriteria = "(c.name  LIKE '%" . $data['name'] . "%' or c.name_optional_language  LIKE '%" . $data['name'] . "%' or c.code  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_election'] != '')
        {
            $this->db->where('c.id_election', $data['id_election']);
        }
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getTitle($id)
    {
         $this->db->select('c.*');
        $this->db->from('election_title as c');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function viewerListByData($data)
    {
        // echo "<Pre>";print_r($data);exit();

        $this->db->select('v.*, c.id, ele.name as election_name, ele.election_date, ele.city, t.name as election_title');
        $this->db->from('viewer_vote_pooling as c');
        $this->db->join('election as ele', 'c.id_election = ele.id');
        $this->db->join('election_title as t', 'c.id_title = t.id');
        $this->db->join('viewers as v', 'c.id_viewer = v.id');
        if($data->id != '')
        {
            $this->db->where('c.id_title', $data->id);
        }
        if($data->id_election != '')
        {
            $this->db->where('c.id_election', $data->id_election);
        }
        // if (!empty($data['name']))
        // {
        //     $likeCriteria = "(c.name  LIKE '%" . $data['name'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
        // $this->db->order_by("c.name", "ASC");
        // $likeCriteria = "v.id NOT IN ( c.id_viewer and c.id_election=" . $data['id_election'] . ")";
        // $this->db->where($likeCriteria);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getViewerListByData($data)
    {
        $this->db->select('c.*');
        $this->db->from('viewers as c');
        // if (!empty($data['name']))
        // {
        //     $likeCriteria = "(c.name  LIKE '%" . $data['name'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function addViewerElectionTagging($data)
    {
        $this->db->trans_start();
        $this->db->insert('viewer_vote_pooling', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteViewerElectionTagging($id)
    {
        $this->db->where('id', $id);
        $result = $this->db->delete('viewer_vote_pooling');
        return $result;
    }
}
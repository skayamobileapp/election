<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Course_learning_objective_model extends CI_Model
{

    function courseLearningObjectiveListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('course_learning_objective as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function courseLearningObjectiveListSearch($data)
    {
        $this->db->select('c.*');
        $this->db->from('course_learning_objective as c');
        if (!empty($data['name']))
        {
            $likeCriteria = "(c.name  LIKE '%" . $data['name'] . "%' or c.name_optional_language  LIKE '%" . $data['name'] . "%' or c.code  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

     function getCourseLearningObjective($id)
    {
        $this->db->select('c.*');
        $this->db->from('course_learning_objective as c');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function addNewCourseLearningObjective($data)
    {
        $this->db->trans_start();
        $this->db->insert('course_learning_objective', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editCourseLearningObjective($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('course_learning_objective', $data);

        return $result;
    }

    function deleteCourseLearningObjective($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('course_learning_objective', $data);
        return $this->db->affected_rows();
    }
}

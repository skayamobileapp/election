<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Country_model (Country Model)
 * Country model class to get to handle country related data 
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Country_model extends CI_Model
{
    /**
     * This function is used to get the country listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function countryListingCount()
    {
        $this->db->select('BaseTbl.id as countryId, BaseTbl.name, BaseTbl.status');
        $this->db->from('country as BaseTbl');
        $this->db->order_by("BaseTbl.name", "ASC");
         $query = $this->db->get();
        
        return $query->num_rows();
    }
    /**
     * This function is used to get the country listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function latestCountryListing()
    {
        $this->db->select('c.id as countryId, c.name, BaseTbl.status');
        $this->db->from('country as c');
        $this->db->order_by("c.id", "desc");

         $query = $this->db->get();
         $result = $query->result();        
         return $result;
    }
    
    function countryListing($country)
    {
        $this->db->select('BaseTbl.id as countryId, BaseTbl.name, BaseTbl.status');
        $this->db->from('country as BaseTbl');
        if (!empty($country))
        {
            $likeCriteria = "(BaseTbl.name  LIKE '%" . $country . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("BaseTbl.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();        
         return $result;
    }

    
     /**
     * This function is used to add new country to system
     * @return number $insert_id : This is last inserted id
     */
    function addNewCountry($countryInfo)
    {
        $this->db->trans_start();
        $this->db->insert('country', $countryInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }

    
    /**
     * This function used to get country information by id
     * @param number $countryId : This is country id
     * @return array $result : This is country information
     */
    function getCountryInfo($countryId)
    {
        $this->db->select('id as countryId, name, status');
        $this->db->from('country');
        $this->db->where('id', $countryId);
        $query = $this->db->get();
            // echo "<pre>";print_r($query);die;
        
        return $query->row();
    }

    
    /**
     * This function is used to update the country information
     * @param array $countryInfo : This is countrys updated information
     * @param number $countryId : This is country id
     */
    function editCountry($countryInfo, $countryId)
    {
        $this->db->where('id', $countryId);
        $result = $this->db->update('country', $countryInfo);
        
        return $result;
    }
    
    
    
    /**
     * This function is used to delete the country information
     * @param number $countryId : This is country id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteCountry($countryId, $countryInfo)
    {
        $this->db->where('id', $countryId);
        $this->db->update('country', $countryInfo);
        
        return $this->db->affected_rows();
    }

    function countryList()
    {
        $this->db->select('*');
        $this->db->from('country');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();      
         return $result;
    }

}

  
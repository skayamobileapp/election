<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Title_model extends CI_Model
{
    function titleListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('election_title as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }


    function electionListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('election');
        // $this->db->where('date(election_date) <', date('Y-m-d'));
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }


    function titleListSearch($data)
    {
        $this->db->select('c.*, ele.name as election_name, ele.election_date, ele.city, cre.name as created_by, upd.name as updated_by');
        $this->db->from('election_title as c');
        $this->db->join('election as ele', 'c.id_election = ele.id');
        $this->db->join('users as cre', 'c.created_by = cre.id','left');
        $this->db->join('users as upd', 'c.updated_by = upd.id','left');
        // $this->db->join('department as d', 'c.id_department = d.id');
        // $this->db->join('staff as s', 'c.id_staff_coordinator = s.id');
        if (!empty($data['name']))
        {
            $likeCriteria = "(c.name  LIKE '%" . $data['name'] . "%')";
            // $likeCriteria = "(c.name  LIKE '%" . $data['name'] . "%' or c.name_optional_language  LIKE '%" . $data['name'] . "%' or c.code  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_election'] != '')
        {
            $this->db->where('c.id_election', $data['id_election']);
        }
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getTitle($id)
    {
         $this->db->select('c.*');
        $this->db->from('election_title as c');
        // $this->db->join('department as d', 'c.id_department = d.id','left');
        // $this->db->join('staff as s', 'c.id_staff_coordinator = s.id','left');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        $result = $query->row();
        // echo "<pre>";print_r($result);die;

        return $result;
    }

    function addNewTitle($data)
    {
        $this->db->trans_start();
        $this->db->insert('election_title', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editTitle($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('election_title', $data);

        return $result;
    }

    function deleteTitle($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('election_title', $data);

        return $this->db->affected_rows();
    }
}

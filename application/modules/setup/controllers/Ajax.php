<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Ajax extends BaseController
{
    public function __construct()
    { 
        parent::__construct();
        $this->load->model('question_model');
        $this->load->model('tos_model');
        $this->isLoggedIn();
    }

    function getCategoryTopics()
    {
        
            $id_course = $this->security->xss_clean($this->input->post('id'));

            $topicList = $this->question_model->topicListByCourse($id_course);

           echo json_encode($topicList);die;
    }
    function getTosQuestions(){
        $pools = $this->security->xss_clean($this->input->post('pools'));
        $questions = $this->tos_model->getTosQuestions($pools);
           echo json_encode($questions);die;
    }
    
   
}

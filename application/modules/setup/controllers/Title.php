<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Title extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('title_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('title.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_election'] = $this->security->xss_clean($this->input->post('id_election'));

            $data['searchParam'] = $formData;

            $data['electionList'] = $this->title_model->electionListByStatus('1');
            $data['titleList'] = $this->title_model->titleListSearch($formData);

            $this->global['pageTitle'] = 'Election Management System : Title List';
            $this->global['pageCode'] = 'election_title.list';
            $this->loadViews("title/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('title.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
            
                $name = $this->security->xss_clean($this->input->post('name'));
                $id_election = $this->security->xss_clean($this->input->post('id_election'));
            
                $data = array(
                    'name' => $name,
                    'id_election' => $id_election,
                    'status' => 1,
                    'created_by' => $user_id
                );
                
                $result = $this->title_model->addNewTitle($data);
                if ($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Title created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Title creation failed');
                }

                redirect('/setup/title/list');
            }

            $data['electionList'] = $this->title_model->electionListByStatus('1');
            
            // echo "<Pre>";print_r($data['electionList']);exit();
           
            $this->global['pageTitle'] = 'Election Management System : Add Title';
            $this->global['pageCode'] = 'election_title.add';
            $this->loadViews("title/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('title.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/title/list');
            }

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {

                $name = $this->security->xss_clean($this->input->post('name'));
                $id_election = $this->security->xss_clean($this->input->post('id_election'));
            
                $data = array(
                    'name' => $name,
                    'id_election' => $id_election,
                    'updated_by' => $user_id
                );
                
                $result = $this->title_model->editTitle($data,$id);

                if ($result)
                {
                    $this->session->set_flashdata('success', 'Title edited successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Title edit failed');
                }

                redirect('/setup/title/list');
            }

            $data['title'] = $this->title_model->getTitle($id);
            $data['electionList'] = $this->title_model->electionListByStatus('1');
            
            $this->global['pageTitle'] = 'Election Management System : Edit Title';
            $this->global['pageCode'] = 'election_title.list';

            $this->loadViews("title/edit", $this->global, $data, NULL);
        }
    }
}

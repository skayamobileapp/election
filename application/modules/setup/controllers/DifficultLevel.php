<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class DifficultLevel extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('difficult_level_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('difficultylevel.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {            
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));

            $data['searchParam'] = $formData;
            $data['difficultLevelList'] = $this->difficult_level_model->difficultLevelListSearch($formData);

            $this->global['pageTitle'] = 'Election Management System : Difficult Level List';
            $this->global['pageCode'] = 'difficultylevel.list';
            $this->loadViews("difficult_level/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('difficultylevel.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
            
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status,
                    'created_by' => $user_id
                );
                
                $result = $this->difficult_level_model->addNewDifficultLevel($data);
                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New Difficulty Level created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Difficulty Level creation failed');
                }
                redirect('/setup/difficultLevel/list');
            }
            $this->global['pageCode'] = 'difficultylevel.add';
            $this->global['pageTitle'] = 'Election Management System : Add Difficult Level';
            $this->loadViews("difficult_level/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('difficultylevel.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/difficultLevel/list');
            }

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {

                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status,
                    'updated_by' => $user_id
                );
                
                $result = $this->difficult_level_model->editDifficultLevel($data,$id);
                if ($result) {
                    $this->session->set_flashdata('success', 'Difficulty Level edited successfully');
                } else {
                    $this->session->set_flashdata('error', 'Difficulty Level edit failed');
                }
                redirect('/setup/difficultLevel/list');
            }

            $data['difficultylevel'] = $this->difficult_level_model->getDifficultLevel($id);
            $this->global['pageCode'] = 'difficultlevel.edit';
            $this->global['pageTitle'] = 'Election Management System : Edit Difficult Level';
            $this->loadViews("difficult_level/edit", $this->global, $data, NULL);
        }
    }
}

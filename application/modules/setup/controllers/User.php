<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class User extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('user.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['email'] = $this->security->xss_clean($this->input->post('email'));
            $formData['mobile'] = $this->security->xss_clean($this->input->post('mobile'));
            $formData['role'] = $this->security->xss_clean($this->input->post('role'));

            $data['searchParam'] = $formData;
            $data['roleList'] = $this->user_model->roleListByStatusForAdmin('1');
            $data['userList'] = $this->user_model->usersListSearch($formData);

            $this->global['pageTitle'] = 'Scholarship Management System : User List';
            $this->global['pageCode'] = 'user.list';
            
            $this->loadViews("user/list", $this->global, $data, NULL);
        }
    }

    function add()
    {
        if ($this->checkAccess('user.add') == 1) {
            $this->loadAccessRestricted();
        } else {

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->id_scholar;

            if ($this->input->post()) {

                $salutation = $this->security->xss_clean($this->input->post('salutation'));
                $first_name = $this->security->xss_clean($this->input->post('first_name'));
                $last_name = $this->security->xss_clean($this->input->post('last_name'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $mobile = $this->security->xss_clean($this->input->post('mobile'));
                $password = $this->security->xss_clean($this->input->post('password'));
                $role_id = $this->security->xss_clean($this->input->post('role'));
                $status = $this->security->xss_clean($this->input->post('status'));


                $salutationInfo = $this->user_model->getSalutation($salutation);

                $data = array(
                    'salutation' => $salutation,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'name' => $salutationInfo->name . ". " . $first_name . " " . $last_name,
                    'email' => $email,
                    'mobile' => $mobile,
                    'password' => md5($password),
                    'id_role' => $role_id,
                    'status' => $status,
                    'created_by' => $user_id
                );

                $result = $this->user_model->addNewUser($data);
                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New User created successfully');
                } else {
                    $this->session->set_flashdata('error', 'User creation failed');
                }
                redirect('/setup/user/list');
            }
            $data['roleList'] = $this->user_model->roleListByStatus('1');
            $data['salutationList'] = $this->user_model->salutationListByStatus('1');

            $this->global['pageTitle'] = 'Election Management System : Add User';
            $this->global['pageCode'] = 'user.add';
            $this->loadViews("user/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('user.edit') == 1) {
            $this->loadAccessRestricted();
        } else {
            if ($id == null) {
                redirect('/setup/user/list');
            }

           $user_id = $this->session->id_scholar;

            if ($this->input->post()) {

                $salutation = $this->security->xss_clean($this->input->post('salutation'));
                $first_name = $this->security->xss_clean($this->input->post('first_name'));
                $last_name = $this->security->xss_clean($this->input->post('last_name'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $mobile = $this->security->xss_clean($this->input->post('mobile'));
                $password = $this->security->xss_clean($this->input->post('password'));
                $role_id = $this->security->xss_clean($this->input->post('role'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $salutationInfo = $this->user_model->getSalutation($salutation);

                $data = array(
                    'salutation' => $salutation,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'name' => $salutationInfo->name . ". " . $first_name . " " . $last_name,
                    'email' => $email,
                    'mobile' => $mobile,
                    'id_role' => $role_id,
                    'status' => $status,
                    'updated_by' => $user_id
                );
                $result = $this->user_model->editUser($data, $id);
                if ($result) {
                    $this->session->set_flashdata('success', 'User edited successfully');
                } else {
                    $this->session->set_flashdata('error', 'User edit failed');
                }
                redirect('/setup/user/list');
            }

            $data['roleList'] = $this->user_model->roleListByStatus('1');
            $data['salutationList'] = $this->user_model->salutationListByStatus('1');
            $data['userInfo'] = $this->user_model->getUser($id);

            $this->global['pageTitle'] = 'Election Management System : Edit User';
            $this->global['pageCode'] = 'user.edit';
            $this->loadViews("user/edit", $this->global, $data, NULL);
        }
    }

    function decryptIt($q)
    {
        $cryptKey  = 'qJB0rGtIn5UB1xG03efyCp';
        $qDecoded      = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($q), base64_decode($q), MCRYPT_MODE_CBC, md5(md5($cryptKey))), "\0");
        return ($qDecoded);
    }
}

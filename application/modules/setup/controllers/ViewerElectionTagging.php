<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ViewerElectionTagging extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('viewer_election_tagging_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('viewer_election_tagging.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_election'] = $this->security->xss_clean($this->input->post('id_election'));
            
            $data['searchParam'] = $formData;

            $data['electionList'] = $this->viewer_election_tagging_model->electionListByStatus('1');
            $data['titleList'] = $this->viewer_election_tagging_model->titleListSearch($formData);

        // echo "<Pre>";print_r($data['examStudentTaggingList']);exit();


            $this->global['pageTitle'] = 'Election Management System : Election Tagging';
            $this->global['pageCode'] = 'viewer_election_tagging.list';
            $this->loadViews("viewer_election_tagging/list", $this->global, $data, NULL);
        }
    }
    
    function add($id)
    {
        if ($this->checkAccess('viewer_election_tagging.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['title'] = $this->viewer_election_tagging_model->getTitle($id);
            
            if($this->input->post())
            {
                
                // echo "<Pre>";print_r($this->input->post());exit();

                $user_id = $this->session->userId;

                $id_title = $data['title']->id;
                $id_election = $data['title']->id_election;
                $id_viewer = $this->security->xss_clean($this->input->post('id_viewer'));


                foreach ($id_viewer as $viewer)
                {

                $uniqueId = rand(0000000000,9999999999);
                $random_number = md5($uniqueId);

                $data = array(
                    'id_title' => $id_title,
                    'id_election' => $id_election,
                    'id_viewer' => $viewer,
                    'random_number' => $random_number,
                    'status' => 1,
                    'created_by' => $user_id
                );

                $result = $this->viewer_election_tagging_model->addViewerElectionTagging($data);

                }

                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New Exam Event created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Exam Event creation failed');
                }

                redirect('/setup/viewerElectionTagging/add/'.$id);
            }

            $data['electionList'] = $this->viewer_election_tagging_model->electionListByStatus('1');
            $data['viewerList'] = $this->viewer_election_tagging_model->viewerListByData($data['title']);

                // echo "<Pre>";print_r($data['viewerList']);exit();


            $this->global['pageCode'] = 'viewer_election_tagging.add';
            $this->global['pageTitle'] = 'Election Management System : Add Election Tagging';
            $this->loadViews("viewer_election_tagging/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('viewer_election_tagging.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/exam/examStudentTagging/list');
            }
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                redirect('/exam/examStudentTagging/list');
            }

            $data['studentList'] = $this->viewer_election_tagging_model->studentListByStatus('1');
            $data['examEventList'] = $this->viewer_election_tagging_model->examEventListByStatus('1');

            $data['examStudentTagging'] = $this->viewer_election_tagging_model->getExamStudentTagging($id);
            // $data['examStudentTagging'] = $this->viewer_election_tagging_model->getExamStudentTagging($id);
            

            $this->global['pageCode'] = 'viewer_election_tagging.edit';
            $this->global['pageTitle'] = 'Election Management System : Edit Exam Event';
            $this->loadViews("viewer_election_tagging/edit", $this->global, $data, NULL);
        }
    }

    function getViewersListByData()
    {
        $data = $this->security->xss_clean($this->input->post('data'));
        // echo "<Pre>";print_r($data);exit();

        $student_data = $this->viewer_election_tagging_model->getViewerListByData($data);
        
        // echo "<Pre>";print_r($student_data);exit();

        $table = "<table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Student Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll'> Check All</th>
                </tr>";

                    for($i=0;$i<count($student_data);$i++)
                    {

                    $id = $student_data[$i]->id;
                    $name = $student_data[$i]->name;
                    $email = $student_data[$i]->email;
                    $phone = $student_data[$i]->phone;

                    $j = $i+1;
                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$name</td>
                            <td>$email</td>
                            <td>$phone</td>                           
                            <td class='text-center'>
                          <input type='checkbox' name='id_viewer[]' class='check' value='".$id."'>
                        </td>
                       
                        </tr>";
                    }


        $table= $table .  "</table>";

        echo $table;exit;
    }

    function deleteViewerElectionTagging($id_pooling)
    {
        $staff_data = $this->viewer_election_tagging_model->deleteViewerElectionTagging($id_pooling);
        echo $id_pooling;
    }

    function getExamCenterByLocation($id_location)
    {
        // echo "<Pre>";print_r($id_location);exit();
            $results = $this->viewer_election_tagging_model->getExamCenterByLocation($id_location);

            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_exam_center' id='id_exam_center' class='form-control' onchange='getExamSetByLocationNCenter()'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }
}

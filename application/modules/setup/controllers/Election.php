<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Election extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('election_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('election.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));

            $data['searchParam'] = $formData;
            $data['electionList'] = $this->election_model->electionListSearch($formData);

            // echo "<Pre>";print_r($data['electionList']);exit();

            $this->global['pageTitle'] = 'Election Management System : Election List';
            $this->global['pageCode'] = 'election.list';
            $this->loadViews("election/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('election.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
            
                $name = $this->security->xss_clean($this->input->post('name'));
                // $name_optional_language = $this->security->xss_clean($this->input->post('name_optional_language'));
                $election_date = $this->security->xss_clean($this->input->post('election_date'));
                $from_time = $this->security->xss_clean($this->input->post('from_time'));
                $to_time = $this->security->xss_clean($this->input->post('to_time'));
                $organisation = $this->security->xss_clean($this->input->post('organisation'));
                $department = $this->security->xss_clean($this->input->post('department'));
                $address1 = $this->security->xss_clean($this->input->post('address1'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $contact_person = $this->security->xss_clean($this->input->post('contact_person'));
                $contact_email = $this->security->xss_clean($this->input->post('contact_email'));
                $phone = $this->security->xss_clean($this->input->post('phone'));



                $data = array(
                    'name' => $name,
                    // 'name_optional_language' => $name_optional_language,
                    'election_date' => date('Y-m-d', strtotime($election_date)),
                    'from_time' => $from_time,
                    'to_time' => $to_time,
                    'organisation' => $organisation,
                    'department' => $department,
                    'address1' => $address1,
                    'id_country' => $id_country,
                    'id_state' => $id_state,
                    'city' => $city,
                    'zipcode' => $zipcode,
                    'contact_person' => $contact_person,
                    'contact_email' => $contact_email,
                    'phone' => $phone,
                    'status' => 1,
                    'created_by' => $user_id
                );
                
                $result = $this->election_model->addNewElection($data);
                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New Election created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Election creation failed');
                }

                redirect('/setup/election/list');
            }

            $data['countryList'] = $this->election_model->countryListByStatus('1');
           
            $this->global['pageCode'] = 'election.add';
            $this->global['pageTitle'] = 'Election Management System : Add Election';
            $this->loadViews("election/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('election.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/election/list');
            }

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {

                $name = $this->security->xss_clean($this->input->post('name'));
                // $name_optional_language = $this->security->xss_clean($this->input->post('name_optional_language'));
                $election_date = $this->security->xss_clean($this->input->post('election_date'));
                $from_time = $this->security->xss_clean($this->input->post('from_time'));
                $to_time = $this->security->xss_clean($this->input->post('to_time'));
                $organisation = $this->security->xss_clean($this->input->post('organisation'));
                $department = $this->security->xss_clean($this->input->post('department'));
                $address1 = $this->security->xss_clean($this->input->post('address1'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $contact_person = $this->security->xss_clean($this->input->post('contact_person'));
                $contact_email = $this->security->xss_clean($this->input->post('contact_email'));
                $phone = $this->security->xss_clean($this->input->post('phone'));



                $data = array(
                    'name' => $name,
                    // 'name_optional_language' => $name_optional_language,
                    'election_date' => date('Y-m-d', strtotime($election_date)),
                    'from_time' => $from_time,
                    'to_time' => $to_time,
                    'organisation' => $organisation,
                    'department' => $department,
                    'address1' => $address1,
                    'id_country' => $id_country,
                    'id_state' => $id_state,
                    'city' => $city,
                    'zipcode' => $zipcode,
                    'contact_person' => $contact_person,
                    'contact_email' => $contact_email,
                    'phone' => $phone,
                    'updated_by' => $user_id
                );
                
                $result = $this->election_model->editElection($data,$id);

                if ($result)
                {
                    $this->session->set_flashdata('success', 'Election edited successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Election edit failed');
                }

                redirect('/setup/election/list');
            }

            $data['election'] = $this->election_model->getElection($id);
            $data['countryList'] = $this->election_model->countryListByStatus('1');


            $this->global['pageCode'] = 'election.list';
            $this->global['pageTitle'] = 'Election Management System : Edit Election';
            $this->loadViews("election/edit", $this->global, $data, NULL);
        }
    }


    function getStateByCountry($id_country)
    {
        if($id_country != '')
        {
            $results = $this->election_model->getStateByCountryId($id_country);

            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_state' id='id_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;

        }
        else
        {
            return array();exit;
        }
    }
}

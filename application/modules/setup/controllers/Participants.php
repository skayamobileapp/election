<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Participants extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('participants_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('participants.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_title'] = $this->security->xss_clean($this->input->post('id_title'));

            $data['searchParam'] = $formData;

            $data['titleList'] = $this->participants_model->electionTitleListByStatus('1');
            $data['participantsList'] = $this->participants_model->participantsListSearch($formData);

            // echo "<Pre>";print_r($data['participantsList']);exit();

            $this->global['pageTitle'] = 'Election Management System : Participants List';
            $this->global['pageCode'] = 'participants.list';
            $this->loadViews("participants/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('participants.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                // echo "<Pre>"; print_r($_FILES);exit;
                // $this->input->post()
            
                if($_FILES['image'])
                {
                // echo "<Pre>"; print_r($_FILES['image']);exit;

                    $image_name = $_FILES['image']['name'];
                    $image_size = $_FILES['image']['size'];
                    $image_tmp =$_FILES['image']['tmp_name'];
                    
                    // echo "<Pre>"; print_r($certificate_tmp);exit();

                    $image_ext=explode('.',$image_name);
                    $image_ext=end($image_ext);
                    $image_ext=strtolower($image_ext);


                    $this->fileFormatNSizeValidation($image_ext,$image_size,'Image File');

                    $image = $this->uploadFile($image_name,$image_tmp,'Image File');
                }



                if($_FILES['logo'])
                {
                // echo "<Pre>"; print_r($_FILES['image']);exit;

                    $logo_name = $_FILES['logo']['name'];
                    $logo_size = $_FILES['logo']['size'];
                    $logo_tmp =$_FILES['logo']['tmp_name'];
                    

                    $logo_ext=explode('.',$logo_name);
                    $logo_ext=end($logo_ext);
                    $logo_ext=strtolower($logo_ext);


                    $this->fileFormatNSizeValidation($logo_ext,$logo_size,'Image File');

                    $logo = $this->uploadFile($logo_name,$logo_tmp,'Image File');
                }



                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $id_election = $this->security->xss_clean($this->input->post('id_election'));
                $id_title = $this->security->xss_clean($this->input->post('id_title'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $phone = $this->security->xss_clean($this->input->post('phone'));



                $data = array(
                    'name' => $name,
                    // 'name_optional_language' => $name_optional_language,
                    'description' =>$description,
                    'id_election' => $id_election,
                    'id_title' => $id_title,
                    'email' => $email,
                    'phone' => $phone,
                    'status' => 1,
                    'created_by' => $user_id
                );



                if($image)
                {
                    $data['image'] = $image;
                }
                if($logo)
                {
                    $data['logo'] = $logo;
                }

                // echo "<Pre>"; print_r($data);exit();
                
                $result = $this->participants_model->addNewParticipants($data);
                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New Participants created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Participants creation failed');
                }

                redirect('/setup/participants/list');
            }

            $data['electionList'] = $this->participants_model->electionListByStatus('1');
            $data['countryList'] = $this->participants_model->countryListByStatus('1');
           
            $this->global['pageTitle'] = 'Election Management System : Add Participants';
            $this->global['pageCode'] = 'participants.add';
            $this->loadViews("participants/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('participants.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/participants/list');
            }

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {

                // echo "<Pre>"; print_r($_FILES['name']);exit();
                
                // if($_FILES['image'])
                // {
                // // echo "<Pre>"; print_r($_FILES['image']);exit;

                //     $image_name = $_FILES['image']['name'];
                //     $image_size = $_FILES['image']['size'];
                //     $image_tmp =$_FILES['image']['tmp_name'];
                    
                //     // echo "<Pre>"; print_r($certificate_tmp);exit();

                //     $image_ext=explode('.',$image_name);
                //     $image_ext=end($image_ext);
                //     $image_ext=strtolower($image_ext);


                //     $this->fileFormatNSizeValidation($image_ext,$image_size,'Image File');

                //     $image = $this->uploadFile($image_name,$image_tmp,'Image File');
                // }



                // if($_FILES['logo'])
                // {
                // // echo "<Pre>"; print_r($_FILES['image']);exit;

                //     $logo_name = $_FILES['logo']['name'];
                //     $logo_size = $_FILES['logo']['size'];
                //     $logo_tmp =$_FILES['logo']['tmp_name'];
                    
                //     // echo "<Pre>"; print_r($certificate_tmp);exit();

                //     $logo_ext=explode('.',$logo_name);
                //     $logo_ext=end($logo_ext);
                //     $logo_ext=strtolower($logo_ext);


                //     $this->fileFormatNSizeValidation($logo_ext,$logo_size,'Image File');

                //     $logo = $this->uploadFile($logo_name,$logo_tmp,'Image File');
                // }



                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $id_election = $this->security->xss_clean($this->input->post('id_election'));
                $id_title = $this->security->xss_clean($this->input->post('id_title'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $phone = $this->security->xss_clean($this->input->post('phone'));



                $data = array(
                    'name' => $name,
                    // 'name_optional_language' => $name_optional_language,
                    'description' =>$description,
                    'id_election' => $id_election,
                    'id_title' => $id_title,
                    'email' => $email,
                    'phone' => $phone,
                    'updated_by' => $user_id
                );

                // if($image)
                // {
                //     $data['image'] = $image;
                // }
                // if($logo)
                // {
                //     $data['logo'] = $logo;
                // }
                
                $result = $this->participants_model->editParticipants($data,$id);

                if ($result)
                {
                    $this->session->set_flashdata('success', 'Participants edited successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Participants edit failed');
                }

                redirect('/setup/participants/list');
            }

            $data['participants'] = $this->participants_model->getParticipants($id);
            $data['countryList'] = $this->participants_model->countryListByStatus('1');
            $data['electionList'] = $this->participants_model->electionListByStatus('1');


            $this->global['pageTitle'] = 'Election Management System : Edit Participants';
            $this->global['pageCode'] = 'participants.list';
            $this->loadViews("participants/edit", $this->global, $data, NULL);
        }
    }


    function getStateByCountry($id_country)
    {
        if($id_country != '')
        {
            $results = $this->participants_model->getStateByCountryId($id_country);

            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_state' id='id_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;

        }
        else
        {
            return array();exit;
        }
    }


    function getElectionTitleByElectionId($id_election)
    {
        if($id_election != '')
        {
            $results = $this->participants_model->getElectionTitleByElectionId($id_election);

            // echo "<Pre>";print_r($results);exit();

            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_title' id='id_title' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            $id = $results[$i]->id;
            $name = $results[$i]->name;

            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;

        }
        else
        {
            return array();exit;
        }
    }
}

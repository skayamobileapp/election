<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Examset extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('examset_model');
        $this->load->model('tos_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('examset.list') == 1) {
            $this->loadAccessRestricted();
        } else {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));

            $data['searchParam'] = $formData;
            $data['ExamsetList'] = $this->examset_model->examsetListSearch($formData);

            $this->global['pageTitle'] = 'Election Management System : Examset List';
            $this->global['pageCode'] = 'examset.list';
            $this->loadViews("examset/list", $this->global, $data, NULL);
        }
    }

    function add()
    {
        if ($this->checkAccess('examset.add') == 1) {
            $this->loadAccessRestricted();
        } else {

            $user_id = $this->session->userId;

            if ($this->input->post()) {

                $name = $this->security->xss_clean($this->input->post('name'));
                $id_tos = $this->security->xss_clean($this->input->post('id_tos'));
                $instructions = $this->security->xss_clean($this->input->post('instructions'));
                $duration = $this->security->xss_clean($this->input->post('duration'));
                $pass_grade = $this->security->xss_clean($this->input->post('pass_grade'));
                $attempts = $this->security->xss_clean($this->input->post('attempts'));
                $attempt_duration = $this->security->xss_clean($this->input->post('attempt_duration'));
                $grading_method = $this->security->xss_clean($this->input->post('grading_method'));
                $layout = $this->security->xss_clean($this->input->post('layout'));
                $behaviour = $this->security->xss_clean($this->input->post('behaviour'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'name' => $name,
                    'id_tos' => $id_tos,
                    'instructions' => $instructions,
                    'duration' => $duration,
                    'pass_grade' => $pass_grade,
                    'attempts' => $attempts,
                    'attempt_duration' => $attempt_duration,
                    'grading_method' => $grading_method,
                    'layout' => $layout,
                    'behaviour' => $behaviour,
                    'status' => $status,
                    'created_by' => $user_id
                );

                $result = $this->examset_model->addNewExamset($data);
                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New Examset added successfully');
                } else {
                    $this->session->set_flashdata('error', 'Examset add failed');
                }
                redirect('/setup/examset/list');
            }

            $data['toslist'] = $this->tos_model->tosListSearch(array());
            $this->global['pageCode'] = 'examset.add';
            $this->global['pageTitle'] = 'Election Management System : Add Examset';
            $this->loadViews("examset/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('examset.edit') == 1) {
            $this->loadAccessRestricted();
        } else {
            if ($id == null) {
                redirect('/setup/examset/list');
            }
            if ($this->input->post()) {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;


                $name = $this->security->xss_clean($this->input->post('name'));
                $id_tos = $this->security->xss_clean($this->input->post('id_tos'));
                $instructions = $this->security->xss_clean($this->input->post('instructions'));
                $duration = $this->security->xss_clean($this->input->post('duration'));
                $pass_grade = $this->security->xss_clean($this->input->post('pass_grade'));
                $attempts = $this->security->xss_clean($this->input->post('attempts'));
                $attempt_duration = $this->security->xss_clean($this->input->post('attempt_duration'));
                $grading_method = $this->security->xss_clean($this->input->post('grading_method'));
                $layout = $this->security->xss_clean($this->input->post('layout'));
                $behaviour = $this->security->xss_clean($this->input->post('behaviour'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'name' => $name,
                    'id_tos' => $id_tos,
                    'instructions' => $instructions,
                    'duration' => $duration,
                    'pass_grade' => $pass_grade,
                    'attempts' => $attempts,
                    'attempt_duration' => $attempt_duration,
                    'grading_method' => $grading_method,
                    'layout' => $layout,
                    'behaviour' => $behaviour,
                    'status' => $status,
                    'updated_by' => $user_id
                );

                $result = $this->examset_model->editExamset($data, $id);
                if ($result) {
                    $this->session->set_flashdata('success', 'Examset edited successfully');
                } else {
                    $this->session->set_flashdata('error', 'Examset edit failed');
                }
                redirect('/setup/examset/list');
            }
            $data['toslist'] = $this->tos_model->tosListSearch(array());
            $data['examset'] = $this->examset_model->getExamset($id);
            // echo "<pre>";print_r($data);die;
            $this->global['pageTitle'] = 'Election Management System : Edit Examset';
            $this->global['pageCode'] = 'examset.edit';
            $this->loadViews("examset/edit", $this->global, $data, NULL);
        }
    }
}

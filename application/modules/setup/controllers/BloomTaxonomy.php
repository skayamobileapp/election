<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class BloomTaxonomy extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('bloom_taxonomy_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('taxonomy.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {            
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));

            $data['searchParam'] = $formData;
            $data['bloomTaxonomyList'] = $this->bloom_taxonomy_model->bloomTaxonomyListSearch($formData);

            $this->global['pageTitle'] = 'Election Management System : Bloom Taxonomy List';
            $this->global['pageCode'] = 'taxonomy.list';
            $this->loadViews("bloom_taxonomy/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('taxonomy.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
            
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status,
                    'created_by' => $user_id
                );
                
                $result = $this->bloom_taxonomy_model->addNewBloomTaxonomy($data);
                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New Bloom Taxonomy created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Bloom Taxonomy creation failed');
                }
                redirect('/setup/bloomTaxonomy/list');
            }

            $this->global['pageTitle'] = 'Election Management System : Add Bloom Taxonomy';
            $this->global['pageCode'] = 'taxonomy.add';
            $this->loadViews("bloom_taxonomy/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('taxonomy.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/bloomTaxonomy/list');
            }

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {

                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status,
                    'updated_by' => $user_id
                );
                
                $result = $this->bloom_taxonomy_model->editBloomTaxonomy($data,$id);
                if ($result) {
                    $this->session->set_flashdata('success', 'Bloom Taxonomy edited successfully');
                } else {
                    $this->session->set_flashdata('error', 'Bloom Taxonomy edit failed');
                }
                redirect('/setup/bloomTaxonomy/list');
            }

            $data['bloomTaxonomy'] = $this->bloom_taxonomy_model->getBloomTaxonomy($id);

            $this->global['pageTitle'] = 'Election Management System : Edit Bloom Taxonomy';
            $this->global['pageCode'] = 'taxonomy.edit';
            $this->loadViews("bloom_taxonomy/edit", $this->global, $data, NULL);
        }
    }
}

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Role extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('role_model');
        $this->load->model('permission_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('role.list') == 1) {
            $this->loadAccessRestricted();
        } else {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $data['searchParam'] = $formData;
            $data['roleList'] = $this->role_model->roleListSearch($formData);
            $this->global['pageTitle'] = 'Election Management System : Role List';
            $this->global['pageCode'] = 'role.list';
            $this->loadViews("role/list", $this->global, $data, NULL);
        }
    }

    function add()
    {
        if ($this->checkAccess('role.add') == 1) {
            $this->loadAccessRestricted();
        } else {

            if ($this->input->post()) {

                $role = $this->security->xss_clean($this->input->post('role'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $permissions = $this->input->post('permissions');
                $data = array(
                    'role' => $role,
                    'status' => $status
                );
 
                $result = $this->role_model->addNewRole($data);
                $this->role_model->updateRolePermissions($permissions, $result);
                if ($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Role created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Role creation failed');
                }
                redirect('/setup/role/list');
            }
            $data['permissions'] = $this->permission_model->permissionListSearch(null);
            $this->global['pageTitle'] = 'Election Management System : Add Role';
            $this->global['pageCode'] = 'role.add';
            $this->loadViews("role/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('role.edit') == 1) {
            $this->loadAccessRestricted();
        } else {
            if ($id == null) {
                redirect('/setup/role/list');
            }
            if ($this->input->post()) {


                $roleList = $this->input->post('checkrole');

                $deleteoldrold = $this->role_model->deletePermission($id);

                for($i=0;$i<count($roleList);$i++) {
                      $data = array(
                            'id_role' => $id,
                            'id_permission' => $roleList[$i]
                        );
 
                $result = $this->role_model->addPermission($data);
                }

                
            }

            $getrolelist = $this->role_model->getMenu();
            $data['role'] = $getrolelist;
            $data['idrole'] = $id;
            $data['rolepermissions'] = $this->role_model->rolePermissions($id);
            $data['permissions'] = $this->permission_model->permissionListSearch(null);
            $this->global['pageTitle'] = 'Election Management System : Edit Role';
            $this->global['pageCode'] = 'role.edit';
            $this->loadViews("role/edit", $this->global, $data, NULL);
        }
    }
}

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Viewers extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('viewers_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('viewers.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));

            $data['searchParam'] = $formData;
            $data['viewersList'] = $this->viewers_model->viewersListSearch($formData);

            // echo "<Pre>";print_r($data['viewersList']);exit();

            $this->global['pageTitle'] = 'Election Management System : Viewers List';
            $this->global['pageCode'] = 'viewers.list';
            $this->loadViews("viewers/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('viewers.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                // echo "<Pre>"; print_r($_FILES);exit;
                // $this->input->post()
            
                // if($_FILES['image'])
                // {
                // // echo "<Pre>"; print_r($_FILES['image']);exit;

                //     $image_name = $_FILES['image']['name'];
                //     $image_size = $_FILES['image']['size'];
                //     $image_tmp =$_FILES['image']['tmp_name'];
                    
                //     // echo "<Pre>"; print_r($certificate_tmp);exit();

                //     $image_ext=explode('.',$image_name);
                //     $image_ext=end($image_ext);
                //     $image_ext=strtolower($image_ext);


                //     $this->fileFormatNSizeValidation($image_ext,$image_size,'Image File');

                //     $image = $this->uploadFile($image_name,$image_tmp,'Image File');
                // }



                // if($_FILES['logo'])
                // {
                // // echo "<Pre>"; print_r($_FILES['image']);exit;

                //     $logo_name = $_FILES['logo']['name'];
                //     $logo_size = $_FILES['logo']['size'];
                //     $logo_tmp =$_FILES['logo']['tmp_name'];
                    

                //     $logo_ext=explode('.',$logo_name);
                //     $logo_ext=end($logo_ext);
                //     $logo_ext=strtolower($logo_ext);


                //     $this->fileFormatNSizeValidation($logo_ext,$logo_size,'Image File');

                //     $logo = $this->uploadFile($logo_name,$logo_tmp,'Image File');
                // }


                // $uniqueId = rand(0000000000,9999999999);
                // $random_number = md5($uniqueId);



                $name = $this->security->xss_clean($this->input->post('name'));
                // $name_optional_language = $this->security->xss_clean($this->input->post('name_optional_language'));
                $id_election = $this->security->xss_clean($this->input->post('id_election'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $phone = $this->security->xss_clean($this->input->post('phone'));



                $data = array(
                    'name' => $name,
                    'id_election' => $id_election,
                    'email' => $email,
                    'phone' => $phone,
                    'status' => 1,
                    'created_by' => $user_id
                );



                // if($image)
                // {
                //     $data['image'] = $image;
                // }
                // if($logo)
                // {
                //     $data['logo'] = $logo;
                // }

                // echo "<Pre>"; print_r($data);exit();
                
                $result = $this->viewers_model->addNewViewers($data);
                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New Viewers created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Viewers creation failed');
                }

                redirect('/setup/viewers/list');
            }

            $data['electionList'] = $this->viewers_model->electionListByStatus('1');
            // $data['countryList'] = $this->viewers_model->countryListByStatus('1');
           
            $this->global['pageTitle'] = 'Election Management System : Add Viewers';
            $this->global['pageCode'] = 'viewers.add';
            $this->loadViews("viewers/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('viewers.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/viewers/list');
            }

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {

                $name = $this->security->xss_clean($this->input->post('name'));
                // $name_optional_language = $this->security->xss_clean($this->input->post('name_optional_language'));
                $id_election = $this->security->xss_clean($this->input->post('id_election'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $phone = $this->security->xss_clean($this->input->post('phone'));



                $data = array(
                    'name' => $name,
                    'id_election' => $id_election,
                    'email' => $email,
                    'phone' => $phone,
                    'updated_by' => $user_id
                );
                
                $result = $this->viewers_model->editViewers($data,$id);

                if ($result)
                {
                    $this->session->set_flashdata('success', 'Viewers edited successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Viewers edit failed');
                }

                redirect('/setup/viewers/list');
            }

            $data['viewers'] = $this->viewers_model->getViewers($id);
            // $data['countryList'] = $this->viewers_model->countryListByStatus('1');
            $data['electionList'] = $this->viewers_model->electionListByStatus('1');


            $this->global['pageTitle'] = 'Election Management System : Edit Viewers';
            $this->global['pageCode'] = 'viewers.list';
            $this->loadViews("viewers/edit", $this->global, $data, NULL);
        }
    }


    function getStateByCountry($id_country)
    {
        if($id_country != '')
        {
            $results = $this->viewers_model->getStateByCountryId($id_country);

            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_state' id='id_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;

        }
        else
        {
            return array();exit;
        }
    }


    function getElectionTitleByElectionId($id_election)
    {
        if($id_election != '')
        {
            $results = $this->viewers_model->getElectionTitleByElectionId($id_election);

            // echo "<Pre>";print_r($results);exit();

            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_title' id='id_title' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            $id = $results[$i]->id;
            $name = $results[$i]->name;

            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;

        }
        else
        {
            return array();exit;
        }
    }
}

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Permission extends BaseController
{
    public function __construct()
    { 
        parent::__construct();
        $this->load->model('permission_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('permission.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));

            $data['searchParam'] = $formData;
            $data['permissionList'] = $this->permission_model->permissionListSearch($formData);

            $this->global['pageTitle'] = 'Election Management System : Permission List';
            $this->global['pageCode'] = 'permission.list';
            $this->loadViews("permission/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('permission.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if($this->input->post())
            {
            
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'code' => $code,
                    'description' => $description,
                    'status' => $status
                );
                
                $result = $this->permission_model->addNewPermission($data);
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Permission created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Permission creation failed');
                }
                redirect('/setup/permission/list');
            }

            $this->global['pageTitle'] = 'Election Management System : Add Permission';
            $this->global['pageCode'] = 'permission.add';
            $this->loadViews("permission/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('permission.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/permission/list');
            }
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'code' => $code,
                    'description' => $description,
                    'status' => $status
                );
                
                $result = $this->permission_model->editPermission($data,$id);
                if($result)
                {
                    $this->session->set_flashdata('success', 'Permission edited successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Permission edit failed');
                }
                redirect('/setup/permission/list');
            }

            $data['permission'] = $this->permission_model->getPermission($id);

            $this->global['pageTitle'] = 'Election Management System : Edit Permission';
            $this->global['pageCode'] = 'permission.edit';
            $this->loadViews("permission/edit", $this->global, $data, NULL);
        }
    }
}

<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Invoice_model extends CI_Model
{    
    function mainInvoiceList()
    {
        $this->db->select('mi.*, s.full_name as student');
        $this->db->from('main_invoice as mi');
        $this->db->join('student as s', 'mi.id_student = s.id');
        // $this->db->join('country as c', 'sp.id_country = c.id');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function invoiceListSearch($data)
    {
        $this->db->select('mi.*, s.full_name, s.nric');
        $this->db->from('main_invoice as mi');
        $this->db->join('student as s', 'mi.id_student = s.id');
        if ($data['name'] != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);

        }
        if ($data['nric'] != '')
        {
            $likeCriteria = "(s.nric  LIKE '%" . $data['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['invoice_number'] != '')
        {
            $likeCriteria = "(mi.invoice_number  LIKE '%" . $data['invoice_number'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['type'] != '')
        {
            $this->db->where('mi.type', $data['type']);
        }
        // if ($data['id_program'] != '')
        // {
        //     $this->db->where('mi.id_program', $data['id_program']);
        // }
        // if ($data['id_cohert'] != '')
        // {
        //     $this->db->where('mi.id_cohert', $data['id_cohert']);
        // }
        // if ($data['id_scholarship'] != '')
        // {
        //     $this->db->where('mi.id_scholarship', $data['id_scholarship']);
        // }
        if ($data['status'] != '')
        {

            $this->db->where('mi.status', $data['status']);
        }
        $this->db->order_by("mi.id", "DESC");
        // $this->db->join('country as c', 'sp.id_country = c.id');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function currencyListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('currency_setup');
        $this->db->where('status', $status); 
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }

    function studentList()
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where("applicant_status !=", "Graduated");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getScholarshipByIdCohort($id_scheme)
    {
        $this->db->select('DISTINCT(icr.id_sub_thrust) as id_sub_thrust');
        $this->db->from('scheme_has_program as icr');
        $this->db->join('scholarship_programme as p', 'icr.id_program = p.id','left');
        $this->db->join('scholarship_sub_thrust as ichp', 'icr.id_sub_thrust = ichp.id','left');
        $this->db->where('icr.id_scholarship_scheme', $id_scheme);
        $this->db->where('icr.id_scholarship_scheme', $id_scheme);
        // $this->db->where('icr.status', '1');
        // $this->db->where('icr.to_dt <', date('Y-m-d'));
        $query = $this->db->get();
        $results = $query->result();

        $details = array();
        foreach ($results as $result)
        {
            $id_sub_thrust = $result->id_sub_thrust;
            $sub_thrust = $this->getSubThrust($id_sub_thrust);
            
            array_push($details, $sub_thrust);
        }

        return $details;
    }


    function getProgramByScholarshipNCohortId($data)
    {
        $this->db->select('DISTINCT(icr.id_program) as id_program');
        $this->db->from('scheme_has_program as icr');
        $this->db->join('scholarship_programme as p', 'icr.id_program = p.id','left');
        $this->db->join('scholarship_sub_thrust as ichp', 'icr.id_sub_thrust = ichp.id','left');
        $this->db->where('icr.id_scholarship_scheme', $data['id_cohert']);
        $this->db->where('icr.id_sub_thrust', $data['id_scholarship']);
        // $this->db->where('icr.status', '1');
        // $this->db->where('icr.to_dt <', date('Y-m-d'));
        $query = $this->db->get();
        $results = $query->result();

        $details = array();
        foreach ($results as $result)
        {
            $id_program = $result->id_program;
            $program = $this->getProgramData($id_program);
            
            array_push($details, $program);
        }

        return $details;
    }


    function getSubThrust($id)
    {
        $this->db->select('sa.*');
        // $this->db->join('scholarship_programme as sp', 'sa.id_program = sp.id');
        $this->db->from('scholarship_sub_thrust as sa');
        $this->db->where('sa.id', $id);
        $query = $this->db->get();
        return $query->row();
    }


    function getProgramData($id)
    {
        $this->db->select('sa.*');
        // $this->db->join('scholarship_programme as sp', 'sa.id_program = sp.id');
        $this->db->from('scholarship_programme as sa');
        $this->db->where('sa.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getStudentByData($data)
    {
        $this->db->select('sa.*');
        $this->db->from('student as sa');
        $this->db->where('sa.id_cohert', $data['id_cohert']);
        $this->db->where('sa.id_scholarship', $data['id_scholarship']);
        $this->db->where('sa.id_program', $data['id_program']);
        $query = $this->db->get();
        return $query->result();
    }

    function addNewTempMainInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function getTempMainInvoiceDetails($id_session)
    {
        $this->db->select('tmid.*, fs.name as fee_setup');
        $this->db->from('temp_main_invoice_details as tmid');
        $this->db->join('fee_setup as fs', 'tmid.id_fee_item = fs.id');        
        $this->db->where('tmid.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempData($id)
    { 
        // echo "<Pre>";  print_r($id);exit;
       $this->db->where('id', $id);
       $this->db->delete('temp_main_invoice_details');
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, p.code as programme_code,');
        $this->db->from('student as s');
        $this->db->join('scholarship_programme as p', 's.id_program = p.id');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getFeeSetupListByData($data)
    {
        $this->db->select('DISTINCT(fs.id_fee_item) as id_fee_item');
        $this->db->from('fee_structure as fs');
        $this->db->join('fee_structure_master as fsm', 'fs.id_fee_structure = fsm.id');
        $this->db->where('fsm.id_cohert', $data['id_cohert']);
        $this->db->where('fsm.id_scholarship', $data['id_scholarship']);
        $this->db->where('fsm.id_program', $data['id_program']);
        $query = $this->db->get();
        $results = $query->result();
        
        $details = array();

        foreach ($results as $key => $result)
        {
            $id_fee_item = $result->id_fee_item;
            $fee_item = $this->getFeeItem($id_fee_item);
            if($fee_item)
            {
                array_push($details, $fee_item);
            }
        }
        
        return $details;
    }

    function getFeeItem($id)
    {
        $this->db->select('sa.*');
        // $this->db->join('scholarship_programme as sp', 'sa.id_program = sp.id');
        $this->db->from('fee_setup as sa');
        $this->db->where('sa.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function addNewInvoice($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }


    function addNewMainInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

     function deleteTempDataBySession($id_session)
    { 
       $this->db->where('id_session', $id_session);
       $this->db->delete('temp_main_invoice_details');
    }


    function moveTempToDetails($id_invoice)
    {
        $id_session = $this->session->my_session_id;

        $temp_details = $this->getTempMainInvoiceDetails($id_session);

        foreach ($temp_details as $temp_detail)
        {
            $temp_detail->id_main_invoice = $id_invoice;
            unset($temp_detail->id);
            unset($temp_detail->id_session);
            unset($temp_detail->fee_setup);
                
            $added_detail = $this->addNewMainInvoiceDetails($temp_detail);

        }

        $deleted_temp = $this->deleteTempDataBySession($id_session);
    }

    function generateMainInvoiceNumber()
    {
        $year = date('y');
        $Year = date('Y');

        $this->db->select('j.*');
        $this->db->from('main_invoice as j');
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        $result = $query->num_rows();

 
        $count= $result + 1;
       $jrnumber = $number = "INV" .(sprintf("%'06d", $count)). "/" . $Year;
       return $jrnumber;        
    }

    function feeSetupListByStatus($status)
    {
        $this->db->select('fs.*');
        $this->db->from('fee_setup as fs');
        $this->db->where('status', $status);
        $this->db->order_by("fs.name", "ASC");
        $query = $this->db->get();
        $result = $query->result();  
        return $result;
    }

    function getMainInvoice($id)
    {
        $this->db->select('mi.*');
        $this->db->from('main_invoice as mi');
        // $this->db->join('student as s', 'mi.id_student = s.id');
        // $this->db->join('programme as p', 'mi.id_program = p.id','left');
        // $this->db->join('intake as i', 'mi.id_intake = i.id','left');
        $this->db->where('mi.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getMainInvoiceDetails($id)
    {
        $this->db->select('mid.*, fstp.name as fee_setup');
        $this->db->from('main_invoice_details as mid');
        // $this->db->join('fee_structure as fs', 'mid.id_fee_item = fs.id');        
        $this->db->join('fee_setup as fstp', 'mid.id_fee_item = fstp.id');       
        $this->db->where('mid.id_main_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getMainInvoiceDiscountDetails($id)
    {
        $this->db->select('*');
        $this->db->from('main_invoice_discount_details');   
        $this->db->where('id_main_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }


































































    function getMainInvoiceListByStatusForCancellation($data)
    {
        $and ='';

        $this->db->select('mi.*');
        $this->db->from('main_invoice as mi');
        // $this->db->join('student as s', 'mi.id_student = s.id');
        // $this->db->join('programme as p', 's.id_program = p.id');
        // $this->db->join('intake as i', 's.id_intake = i.id');
        if ($data['name'] != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
            $and =' and';

        }
        if ($data['nric'] != '')
        {
            $likeCriteria = "(s.nric  LIKE '%" . $data['nric'] . "%')";
            $this->db->where($likeCriteria);
            $and =' and';
        }
        if ($data['invoice_number'] != '')
        {
            $likeCriteria = "(mi.invoice_number  LIKE '%" . $data['invoice_number'] . "%')";
            $this->db->where($likeCriteria);
            $and =' and';
        }
        if ($data['type'] != '')
        {
            $this->db->where('mi.type', $data['type']);
            $and =' and';
        }
        if ($data['id_programme'] != '')
        {
            $this->db->where('mi.id_program', $data['id_programme']);
            $and =' and';
        }
        if ($data['id_intake'] != '')
        {
            $this->db->where('mi.id_intake', $data['id_intake']);
            $and =' and';
        }
        if ($data['status'] != '')
        {

            $this->db->where('mi.status', $data['status']);
            if($data['status'] == 1)
            {
                if($and =='')
                {
                    $and = ' ';
                }
                $likeCriteria = " mi.id NOT IN (SELECT id_main_invoice FROM receipt_details)";
                $this->db->where($likeCriteria);                
            }
        }
        $this->db->where("mi.type !=", 'Sponsor');
        $this->db->order_by("mi.id", "DESC");
        // $this->db->join('country as c', 'sp.id_country = c.id');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function editMainInvoiceList($data,$id)
    {
        // $status = ['status'=>'1'];
      $this->db->where_in('id', $id);
      $this->db->update('main_invoice', $data);
    }

    
    
    

    function editMainInvoice($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('main_invoice', $data);
        return TRUE;
    }

    

    function applicantList()
    {
        $this->db->select('*');
        $this->db->from('applicant');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    

    

    

    function addTempDetails($data)
    {
        // echo "<Pre>";  print_r($data);exit;

        $this->db->trans_start();
        $this->db->insert('temp_main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateTempDetails($data,$id) {
        $this->db->where('id', $id);
        $this->db->update('temp_main_invoice_details', $data);
        return TRUE;
    }

    

    function intakeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', $status);
        $this->db->order_by("year", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function intakeList()
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->order_by("year", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getStudentByProgrammeId($id_programme)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id_program', $id_programme);
        $this->db->order_by("full_name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getApplicantListByData($data)
    {
        $this->db->select('*');
        $this->db->from('applicant');
        $this->db->where('id_program', $data['id_program']);
        $this->db->where('id_intake', $data['id_intake']);
        $this->db->where('applicant_status', 'Approved');
        $this->db->order_by("full_name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getStudentListByData($data)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id_program', $data['id_program']);
        $this->db->where('id_intake', $data['id_intake']);
        $this->db->where('applicant_status', 'Approved');
        $this->db->order_by("full_name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getProgrammeById($id_programme)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('id', $id_programme);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    

    function getApplicantByApplicantId($id_applicant)
    {
        $this->db->select('s.*, p.name as programme_name, i.name as intake_name');
        $this->db->from('applicant as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->where('s.id', $id_applicant);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    

    function getMainInvoiceApplicantData($id_applicant)
    {
        $this->db->select('app.full_name, app.nric, app.id_degree_type');
        $this->db->from('applicant as app');
        $this->db->where('app.id', $id_applicant);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getMainInvoiceStudentData($id_student)
    {
        $this->db->select('stu.full_name, stu.nric, stu.id_degree_type');
        $this->db->from('student as stu');
        $this->db->where('stu.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getMainInvoiceSponserData($id_sponser)
    {
        $this->db->select('stu.name full_name, stu.code as nric');
        $this->db->from('sponser as stu');
        $this->db->where('stu.id', $id_sponser);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function qualificationList()
    {
        $this->db->select('*');
        $this->db->from('qualification_setup');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    

    function sponserListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('sponser');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getStudentByStudent($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, i.name as intake_name, qs.name as qualification_name, qs.code as qualification_code, adv.ic_no, adv.name as advisor');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->join('qualification_setup as qs', 's.id_degree_type = qs.id');
        $this->db->join('staff as adv', 's.id_advisor = adv.id','left');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getApplicantInformation($id)
    {
        $this->db->select('a.*, p.code as program_code, p.name as program_name, i.year as intake_year, i.name as intake_name');
        $this->db->from('applicant as a');
        $this->db->join('programme as p', 'a.id_program = p.id');
        $this->db->join('intake as i', 'a.id_intake = i.id');
        $this->db->where('a.id', $id);
        $query = $this->db->get();
        $applicant = $query->row();
        return $applicant;
    }

    function gettemplate($id) {
        $this->db->select('*');
        $this->db->from('communication_template');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $intake = $query->row();
        return $intake;
    }

    function getMainInvoiceDetailsForCourseRegistrationShow($id)
    {
        $this->db->select('mid.*, fstp.name as fee_setup, fm.code as frequency_mode, amt.code as amount_calculation_type');
        $this->db->from('main_invoice_details as mid');
        // $this->db->join('fee_structure as fs', 'mid.id_fee_item = fs.id');        
        $this->db->join('fee_setup as fstp', 'mid.id_fee_item = fstp.id');        
        $this->db->join('frequency_mode as fm', 'fstp.id_frequency_mode = fm.id');      
        $this->db->join('amount_calculation_type as amt', 'fstp.id_amount_calculation_type = amt.id');        
        $this->db->where('mid.id_main_invoice', $id);
        $query = $this->db->get();
        $results = $query->result();
        $details = array();
        foreach ($results as $result)
        {
        // echo "<Pre>";print_r($results);exit;

            if($result->id_reference > 0 &&  $result->description == 'CREDIT HOUR MULTIPLICATION')
            {
               $course_registered = $this->getCourseRegistered($result->id_reference);
               $result->course_code = $course_registered->code;
               $result->course_name = $course_registered->name;

            }

           array_push($details, $result);
        }

        return $details;
    }

    function getCourseRegistered($id)
    {
        $this->db->select('c.*');
        $this->db->from('course_registration as cr');
        $this->db->join('course as c', 'cr.id_course = c.id');      
        $this->db->where('cr.id', $id);
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    function getSemesterByMainInvoiceDetailsForCourseRegistrationShow($id)
    {
        $this->db->select('s.*');
        $this->db->from('main_invoice_details as mid');      
        $this->db->join('course_registration as cr', 'mid.id_reference = cr.id');        
        $this->db->join('semester as s', 'cr.id_semester = s.id');
        $this->db->where('mid.description', 'CREDIT HOUR MULTIPLICATION');
        $this->db->where('mid.id_main_invoice', $id);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function getBankRegistration()
    {
        $this->db->select('fc.*, c.name as country, s.name as state');
        $this->db->from('bank_registration as fc');
        $this->db->join('country as c', 'fc.id_country = c.id');
        $this->db->join('state as s', 'fc.id_state = s.id');
        $this->db->where('fc.status', 1);
        $this->db->order_by("fc.id", "DESC");
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    function getOrganisation()
    {
        $this->db->select('fc.*');
        $this->db->from('organisation as fc');
        $this->db->where('fc.status', 1);
        $this->db->order_by("fc.id", "DESC");
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }
}
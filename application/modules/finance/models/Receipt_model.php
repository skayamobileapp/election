<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Receipt_model extends CI_Model
{
    function studentList()
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where("applicant_status !=", "Graduated");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function receiptList()
    {
        $this->db->select('r.*');
        $this->db->from('receipt as r');
        // $this->db->join('country as c', 'sp.id_country = c.id');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getReceipt($id)
    {
        $this->db->select('re.*, s.full_name, s.nric');
        $this->db->from('receipt as re');
        $this->db->join('student as s', 're.id_student = s.id');
        $this->db->where('re.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    function getReceiptInvoiceDetails($id)
    {
        $this->db->select('r.*,mi.*, r.paid_amount');
        $this->db->from('receipt_details as r');
        $this->db->join('main_invoice as mi', 'mi.id = r.id_main_invoice');
        $this->db->where('r.id_receipt', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }
    

    function getReceiptPaymentDetails($id)
    {
        $this->db->select('r.*');
        $this->db->from('receipt_paid_details as r');
        $this->db->where('r.id_receipt', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }



    function getReceiptListBySearch($data)
    {
        $this->db->select('re.*, s.full_name, s.nric');
        $this->db->from('receipt as re');
        $this->db->join('student as s', 're.id_student = s.id');
        if ($data['name'] != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['nric'] != '')
        {
            $likeCriteria = "(s.nric  LIKE '%" . $data['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['receipt_number'] != '')
        {
            $likeCriteria = "(re.receipt_number  LIKE '%" . $data['receipt_number'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['type'] != '')
        {
            $this->db->where('re.type', $data['type']);
        }
        if ($data['status'] != '')
        {
            $this->db->where('re.status', $data['status']);
        }
        $this->db->order_by("re.id", "DESC");
        // $this->db->join('country as c', 'sp.id_country = c.id');
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function editReceiptList($array)
    {
        $status = ['status'=>'1'];
      $this->db->where_in('id', $array);
      $this->db->update('receipt', $status);
    }
    
    function addNewReceipt($data)
    {
        $this->db->trans_start();
        $this->db->insert('receipt', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }


    function addNewReceiptDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('receipt_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }


     function addNewReceiptPaymentDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('receipt_paid_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return '1';;
    }

    function editReceipt($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('receipt', $data);
        return TRUE;
    }

    // function studentList()
    // {
    //     $this->db->select('*');
    //     $this->db->from('student');
    //     $this->db->where('status', '1');
    //     $this->db->order_by("full_name", "ASC");
    //      $query = $this->db->get();
    //      $result = $query->result();  
    //      return $result;
    // }

    function addNewTempReceiptDetails($data)
    {
        // echo "<Pre>";  print_r($data);exit;
        $this->db->trans_start();
        $this->db->insert('temp_receipt_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
// echo "<Pre>";  print_r($db);exit;
        return $insert_id;
    }

    function getTempReceiptDetails($id_session)
    {
         $this->db->select('trd.*');
        $this->db->from('temp_receipt_details as trd');
        $this->db->where('trd.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempDataBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
       $this->db->delete('temp_receipt_details');
    }

     function deleteTempData($id)
    { 
       $this->db->where('id', $id);
       $this->db->delete('temp_receipt_details');
    }

    function addTempDetails($data)
    {
        // echo "<Pre>";  print_r($data);exit;

        $this->db->trans_start();
        $this->db->insert('temp_receipt_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateTempDetails($data,$id) {
        $this->db->where('id', $id);
        $this->db->update('temp_receipt_details', $data);
        return TRUE;
    }

    function programmeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getStudentByProgrammeId($id_programme)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id_program', $id_programme);
        $this->db->order_by("full_name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getProgrammeById($id_programme)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('id', $id_programme);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, i.name as intake_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id');
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getApplicantByApplicantId($id_applicant)
    {
        $this->db->select('s.*, p.name as programme_name, i.name as intake_name');
        $this->db->from('applicant as s');
        $this->db->join('programme as p', 's.id_program = p.id');
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->where('s.id', $id_applicant);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function generateReceiptNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('j.*');
            $this->db->from('receipt as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "REC" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }

    function getInvoicesByStudentId($id_student)
    {
        $balance_amount = "0";
        $type = "Student";
        $this->db->select('mi.*');
        $this->db->from('main_invoice as mi');        
        // $this->db->where('mi.id_student', $id_student);
        $this->db->where('mi.status', '1');
        $this->db->where('mi.type !=', 'Sponsor');
        $likeCriteria = "(mi.balance_amount  > '" . $balance_amount . "' and mi.id_student  ='" . $id_student . "' and mi.type  ='" . $type . "')";
        $this->db->where($likeCriteria);
        $query = $this->db->get();
        return $query->result();

    }

    function getInvoicesByApplicantId($id_student)
    {
        $balance_amount = "0";
        $type = "Applicant";
        $this->db->select('mi.*');
        $this->db->from('main_invoice as mi');        
        // $this->db->where('mi.id_student', $id_student);
        $this->db->where('mi.status', '1');
        $likeCriteria = "(mi.balance_amount  > '" . $balance_amount . "' and mi.id_student  ='" . $id_student . "' and mi.type  ='" . $type . "')";
        $this->db->where($likeCriteria);
        $query = $this->db->get();
        return $query->result();

    }

    function insertAmountToTempData($data)
    {
        
        $this->db->trans_start();
        $this->db->insert('add_temp_receipt_paid_amount', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getAmountFromTempTableReceiptBySession($id_session)
    {
        $this->db->select('*');
        $this->db->from('add_temp_receipt_paid_amount');
        $this->db->where('id_session', $id_session);
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function deleteTempAmountDataBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
       $this->db->delete('temp_receipt_details');
    }

    function updateMainInvoiceAmount($id_invoice, $now_paid_amount)
    {
        $this->db->select('mi.*');
        $this->db->from('main_invoice as mi');
        $this->db->where('mi.id', $id_invoice);
        $query = $this->db->get();
        $invoice_row = $query->row();

         // echo "<Pre>"; print_r($invoice_row);exit();     


        $total_amount = $invoice_row->total_amount;
        $paid_amount = $invoice_row->paid_amount;
        $balance_amount = $invoice_row->balance_amount;

        $new_paid = $paid_amount + $now_paid_amount;
        $new_balance = $total_amount - $new_paid;

        $data['paid_amount'] = $new_paid;
        $data['balance_amount'] = $new_balance;


         $this->db->where('id', $id_invoice);
        $this->db->update('main_invoice', $data);
        return TRUE;
    }

    function sponserListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('sponser');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getStudentBySponser($id_sponser)
    {
        $this->db->select('DISTINCT(id_student) as id_student');
        $this->db->from('sponser_has_students');
        $this->db->where('end_date >', date('Y-m-d'));
        $this->db->where('id_sponser', $id_sponser);
         $query = $this->db->get();
         $results = $query->result();
         // print_r($results);exit();

         $details = array();
         foreach ($results as $key => $result)
         {
            $student_data = $this->getStudentData($result->id_student);

            array_push($details, $student_data);
         }     
         return $details;
    }

    function getStudentData($id_student)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('applicant_status !=', 'Graduated');
         $this->db->where('id', $id_student);
         $query = $this->db->get();
         $result = $query->row();
         // print_r($result);exit();     
         return $result;
    }


    function getInvoicesByStudentIdNSponser($id_student)
    {
        $balance_amount = "0";
        $type = "Sponsor";
        $this->db->select('mi.*');
        $this->db->from('main_invoice as mi');        
        // $this->db->where('mi.id_student', $id_student);
        $this->db->where('mi.status', '1');
        $this->db->where('mi.type', 'Sponsor');
        $likeCriteria = "(mi.balance_amount  > '" . $balance_amount . "' and mi.id_student  ='" . $id_student . "' and mi.type  ='" . $type . "')";
        $this->db->where($likeCriteria);
        $query = $this->db->get();
        return $query->result();

    }

    function getBankRegistration()
    {
        $this->db->select('fc.*, c.name as country, s.name as state');
        $this->db->from('bank_registration as fc');
        $this->db->join('country as c', 'fc.id_country = c.id');
        $this->db->join('state as s', 'fc.id_state = s.id');
        $this->db->where('fc.status', 1);
        $this->db->order_by("fc.id", "DESC");
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    function getOrganisation()
    {
        $this->db->select('fc.*');
        $this->db->from('organisation as fc');
        $this->db->where('fc.status', 1);
        $this->db->order_by("fc.id", "DESC");
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    function getSemesterCourseRegistrationByInvoiceId($id_invoice)
    {
        $this->db->select('fc.*, s.name as semester_name, s.code as semester_code');
        $this->db->from('main_invoice_details as fc');
        $this->db->join('course_registration as c', 'fc.id_reference = c.id');
        $this->db->join('semester as s', 'c.id_semester = s.id');
        $this->db->order_by("fc.id", "DESC");
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    function getSingleReceiptPaymentDetails($id)
    {
        $this->db->select('r.*');
        $this->db->from('receipt_paid_details as r');
        $this->db->where('r.id_receipt', $id);
        $this->db->order_by("r.id", "DESC");
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }
}
 <div class="container-fluid">
      <div class="row">
        <nav id="sidebarMenu" class="col-md-3 d-md-block sidebar collapse">
          <div class="sidebar-sticky pt-3">
            <div class="nav-fold">
              <a href="#" class="d-flex align-items-center">
                <span class="user-avatar"><img src="<?php echo BASE_PATH; ?>assets/img/user.jpg"/></span>
                <span>
                  <?php echo $name; ?>
                  <small class="d-block"><?php echo $role; ?></small>
                  <small class="d-block"></small>                  
                </span>
              </a>
            </div>
            <ul class="nav flex-column mb-2">
             
              <li class="nav-item">
                <a class="nav-link  <?php 
                  if(in_array($pageCode,array('currency.list','currency.add','currency.edit','currency_rate_setup.list','currency_rate_setup.add'))){  ?>
                  <?php 
                  }
                  else
                  { 
                  ?>
                  collapsed
                  <?php
                  }
                  ?>"  data-toggle="collapse" href="#collapseExamApplication" role="button">
                  <i class="fa fa-list-alt"></i>
                  <span>Finance Setup</span>
                  <i class="fa fa-angle-right ml-auto" aria-hidden="true"></i>
                  <i class="fa fa-angle-down ml-auto" aria-hidden="true"></i>
                </a>

                <ul class="collapse nav <?php 
                  if(in_array($pageCode,array('currency.list','currency.add','currency.edit','currency_rate_setup.list','currency_rate_setup.add'))){  ?>
                  show
                 <?php 
                  } else{ 
                  ?>
                  
                  <?php
                  }
                  ?>" id="collapseExamApplication">
                  <li class="nav-item">
                    <a href="/finance/currency/list" class="nav-link <?php if(in_array($pageCode,array('currency.list','currency.add','currency.edit'))){echo 'active';}?>">
                      <i class="fa fa-list-alt"></i>
                      <span>Currency Setup</span>                      
                    </a>
                  </li>

                  <li class="nav-item">
                    <a href="/finance/currencyRateSetup/list" class="nav-link <?php if(in_array($pageCode,array('currency_rate_setup.list','currency_rate_setup.add','currency_rate_setup.edit'))){echo 'active';}?>">
                      <i class="fa fa-list-alt"></i>
                      <span>Currency Rate Setup</span>                      
                    </a>
                  </li>

                </ul>
              </li>  
              
              <li class="nav-item">
                <a class="nav-link  <?php 
                  if(in_array($pageCode,array('account_code.list','account_code.add','account_code.edit','bank_registration.list','bank_registration.add','bank_registration.edit','learningobjective.list','learningobjective.add','learningobjective.edit','taxonomy.list','taxonomy.add','taxonomy.edit','difficultylevel.list','difficultylevel.add','difficultylevel.edit','question.list','question.add','question.edit'))){  ?>
                  <?php 
                  } else{ 
                  ?>
                  collapsed
                  <?php
                  }
                  ?>" data-toggle="collapse" href="#collapseResults" role="button">
                  <i class="fa fa-file-text"></i>
                  <span>General Setup</span>
                  <i class="fa fa-angle-right ml-auto" aria-hidden="true"></i>
                  <i class="fa fa-angle-down ml-auto" aria-hidden="true"></i>
                </a>
                <ul class="collapse nav <?php 
                  if(in_array($pageCode,array('account_code.list','account_code.add','account_code.edit','bank_registration.list','bank_registration.add','bank_registration.edit','payment_type.list','payment_type.add','payment_type.edit','fee_category.list','fee_category.add','fee_category.edit','fee_setup.list','fee_setup.add','fee_setup.edit','fee_structure.list','fee_structure.add','fee_structure.edit'))){  ?>
                  show
                 <?php 
                  } else{ 
                  ?>
                  
                  <?php
                  }
                  ?>" id="collapseResults">
                  <li class="nav-item">
                    <a href="/finance/accountCode/list" class="nav-link <?php if(in_array($pageCode,array('account_code.list','account_code.add','account_code.edit',))){echo 'active';}?>">
                      <i class="fa fa-file-text"></i>
                      <span>Account Code</span>                      
                    </a>
                  </li>

                  <li class="nav-item">
                    <a href="/finance/bankRegistration/list" class="nav-link <?php if(in_array($pageCode,array('bank_registration.list','bank_registration.add','bank_registration.edit'))){echo 'active';}?>">
                      <i class="fa fa-file-text"></i>
                      <span>Bank Registration</span>                      
                    </a>
                  </li>  

                  <li class="nav-item">
                    <a href="/finance/paymentType/list" class="nav-link <?php if(in_array($pageCode,array('payment_type.list','payment_type.add','payment_type.edit'))){echo 'active';}?>">
                      <i class="fa fa-file-text"></i>
                      <span>Payment Type</span>                      
                    </a>
                  </li>  

                  <li class="nav-item">
                    <a href="/finance/feeCategory/list" class="nav-link <?php if(in_array($pageCode,array('fee_category.list','fee_category.add','fee_category.edit'))){echo 'active';}?>">
                      <i class="fa fa-file-text"></i>
                      <span>Fee Category</span>                      
                    </a>
                  </li>                  

                  <li class="nav-item">
                    <a href="/finance/feeSetup/list" class="nav-link <?php if(in_array($pageCode,array('fee_setup.list','fee_setup.add','fee_setup.edit'))){echo 'active';}?>">
                      <i class="fa fa-file-text"></i>
                      <span>Fee Setup</span>                      
                    </a>
                  </li>




                  <!-- <li class="nav-item">
                    <a href="/finance/feeStructure/list" class="nav-link <?php if(in_array($pageCode,array('fee_structure.list','fee_structure.add','fee_structure.edit'))){echo 'active';}?>">
                      <i class="fa fa-file-text"></i>
                      <span>Fee Structure</span>                      
                    </a>
                  </li> -->
                </ul>
              </li>



              <li class="nav-item">
                <a class="nav-link  <?php 
                  if(in_array($pageCode,array('invoice.list','invoice.add','invoice.view','invoice.approve','receipt.list','receipt.add','receipt.view','receipt.approve'))){ ?>
                  <?php 
                  } else{ 
                  ?>
                  collapsed
                  <?php
                  }
                  ?>" data-toggle="collapse" href="#collapse_invoice_result" role="button">
                  <i class="fa fa-file-text"></i>
                  <span>Invoice & Receipt</span>
                  <i class="fa fa-angle-right ml-auto" aria-hidden="true"></i>
                  <i class="fa fa-angle-down ml-auto" aria-hidden="true"></i>
                </a>
                <ul class="collapse nav <?php 
                  if(in_array($pageCode,array('invoice.list','invoice.add','invoice.view','invoice.approve','receipt.list','receipt.add','receipt.view','receipt.approve'))){  ?>
                  show
                 <?php 
                  } else{ 
                  ?>
                  
                  <?php
                  }
                  ?>" id="collapse_invoice_result">

                  <li class="nav-item">
                    <a href="/finance/invoice/list" class="nav-link <?php if(in_array($pageCode,array('invoice.list','invoice.add','invoice.view',))){echo 'active';}?>">
                      <i class="fa fa-file-text"></i>
                      <span>Generate Invoice</span>                      
                    </a>
                  </li>

                  <li class="nav-item">
                    <a href="/finance/receipt/list" class="nav-link <?php if(in_array($pageCode,array('receipt.list','receipt.add','receipt.view','receipt.approve',))){echo 'active';}?>">
                      <i class="fa fa-file-text"></i>
                      <span>Receipt</span>                      
                    </a>
                  </li>

                </ul>
              </li>
                                     
          </ul>         
        </div>
      </nav>
    </div>
  </div>
<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Edit Bank Registration</h1>
        
        <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    
    <form id="form_bank" action="" method="post">

        <div class="page-container">

          <div>
            <h4 class="form-title">Bank Registration Details</h4>
          </div>

            <div class="form-container">


                <div class="row">


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Name <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo $bankRegistration->name; ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Swift Code <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="code" name="code" placeholder="Code" value="<?php echo $bankRegistration->code; ?>">
                        </div>
                      </div>
                    </div>
                    
                </div>


                <div class="row">


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Bank ID <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="bank_id" name="bank_id" placeholder="Bank ID" value="<?php echo $bankRegistration->bank_id; ?>">
                        </div>
                      </div>
                    </div>



                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Country</label>
                          <div class="col-sm-8">
                            <select name="id_country" id="id_country" class="form-control" onchange="getStateByCountry(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($countryList))
                            {
                                foreach ($countryList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                <?php 
                                if($record->id == $bankRegistration->id_country)
                                {
                                    echo "selected=selected";
                                } ?>
                                >        
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                          </select>
                          </div>
                        </div>
                    </div>                    

                </div>

                <div class="row">


                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">State <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <span id="view_state">
                                <select class="form-control" id='id_state' name='id_state'>
                                    <option value=''></option>
                                  </select>
                            </span>
                          </div>
                        </div>
                    </div>                    

                

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Land Mark <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="landmark" name="landmark" placeholder="Landmark" value="<?php echo $bankRegistration->landmark; ?>">
                        </div>
                      </div>
                    </div>


                </div>

                <div class="row">


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">City <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="city" name="city" placeholder="City" value="<?php echo $bankRegistration->city; ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Zipcode <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="zipcode" name="zipcode" placeholder="Zipcode" value="<?php echo $bankRegistration->zipcode; ?>">
                        </div>
                      </div>
                    </div>


                </div>



                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Account No. <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="number" class="form-control" id="account_no" name="account_no" placeholder="Ex. 88888888" value="<?php echo $bankRegistration->account_no; ?>">
                        </div>
                      </div>
                    </div>

               
                    
                    <div class="col-lg-6">
                      <div class="form-group row align-items-center">
                        <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1" <?php if($bankRegistration->status == 1){
                                echo "checked=checked";
                            } ?> >
                            <label class="custom-control-label" for="customRadioInline1">Active</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0" <?php if($bankRegistration->status == 0){
                                echo "checked=checked";
                            } ?> >
                            <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                          </div>
                        </div>
                      </div>
                    </div>

                </div>

                  


                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <button type="submit" class="btn btn-primary">Save</button>
                  </div>

                </div> 

            </div>                                
        </div>
    </form>
</main>

<script>

    function reloadPage()
    {
      window.location.reload();
    }

    $('select').select2();



    function getStateByCountry(id)
    {
        $.get("/finance/bankRegistration/getStateByCountry/"+id, function(data, status){
       
            $("#view_state").html(data);
        });
    }



    $(document).ready(function()
    {
        var id_country = "<?php echo $bankRegistration->id_country;?>";

        if(id_country!='')
        {
             $.get("/finance/bankRegistration/getStateByCountry/"+id_country, function(data, status)
                {
                    var id_state = "<?php echo $bankRegistration->id_state;?>";

                    $("#view_state").html(data);
                    $("#id_state").find('option[value="'+id_state+'"]').attr('selected',true);
                    $('select').select2();
                });
        }

        $("#form_bank").validate({
            rules: {
                code: {
                    required: true
                },
                name: {
                    required: true
                },
                bank_id: {
                    required: true
                },
                address: {
                    required: true
                },
                id_country: {
                    required: true
                },
                id_state: {
                    required: true
                },
                landmark: {
                    required: true
                },
                city: {
                    required: true
                },
                zipcode: {
                    required: true
                },
                cr_fund: {
                    required: true
                },
                cr_department: {
                    required: true
                },
                cr_activity: {
                    required: true
                },
                cr_account: {
                    required: true
                },
                account_no: {
                    required: true
                }
            },
            messages: {
                code: {
                    required: "<p class='error-text'>Swift Code Required</p>",
                },
                name: {
                    required: "<p class='error-text'>Bank Name Required</p>",
                },
                bank_id: {
                    required: "<p class='error-text'>Bank ID Required</p>",
                },
                address: {
                    required: "<p class='error-text'>Address Required</p>",
                },
                id_country: {
                    required: "<p class='error-text'>Select Country</p>",
                },
                id_state: {
                    required: "<p class='error-text'>Select State</p>",
                },
                landmark: {
                    required: "<p class='error-text'>Enter Landmark</p>",
                },
                city: {
                    required: "<p class='error-text'>Enter City Name</p>",
                },
                zipcode: {
                    required: "<p class='error-text'>Enter Zipcode</p>",
                },
                cr_fund: {
                    required: "<p class='error-text'>Select Credit Fund Code</p>",
                },
                cr_department: {
                    required: "<p class='error-text'>Select Credit Department Code</p>",
                },
                cr_activity: {
                    required: "<p class='error-text'>Select Credit Activity Code</p>",
                },
                cr_account: {
                    required: "<p class='error-text'>Select Credit Account Code</p>",
                },
                account_no: {
                    required: "<p class='error-text'>Account No Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


</script>
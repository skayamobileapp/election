<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Invoice List</h1>
    <a href="add" class="btn btn-primary ml-auto">+ Add Invoice</a>
  </div>
  <div class="page-container">
    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="">
              Advanced Search
              <i class="fa fa-angle-right" aria-hidden="true"></i>
              <i class="fa fa-angle-down" aria-hidden="true"></i>
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">

          <div id="collapseOne" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true" style="">
            <div class="panel-body p-2">

              <div class="row">

                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Invoice Number</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="name" name="name" placeholder="Type / Code" value="<?php echo $searchParam['name'] ?>">
                    </div>
                  </div>
                </div>

              </div>

              <hr />
              <div class="d-flex justify-content-center">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="list" class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>

        </form>

      </div>
    </div>
    <?php
    if ($this->session->flashdata('success')) {
    ?>
      <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
      </div>
    <?php
    }
    if ($this->session->flashdata('error')) {
    ?>
      <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Failure!</strong> <?php echo $this->session->flashdata('error'); ?>
      </div>
    <?php
    }
    ?>
    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Invoice Number</th>
            <th>Student</th>
            <th>Type</th>
            <th>Invoice Total</th>
            <th>Total Discount</th>
            <th>Total Payable</th>
            <th>Paid </th>
            <th>Balance </th>
            <!-- <th>Remarks</th> -->
            <th>Invoice Date</th>
            <th style="text-align: center;">Status</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($invoiceList)) {
            $i = 1;
            foreach ($invoiceList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->invoice_number ?></td>
                <td><?php echo $record->full_name . " - " . $record->nric ?></td>
                <td><?php echo $record->type ?></td>
                <td><?php echo $record->invoice_total ?></td>
                <td><?php echo $record->total_discount ?></td>
                <td><?php echo $record->total_amount ?></td>
                <td><?php echo $record->paid_amount ?></td>
                <td><?php echo $record->balance_amount ?></td>
                <!-- <td><?php echo $record->remarks ?></td> -->
                <td><?php echo date("d-m-Y", strtotime($record->date_time)) ?></td>
                <td style="text-align: center;">
                <?php if( $record->status == '1')
                  {
                    echo "Approved";
                  }
                  elseif( $record->status == '0')
                  {
                    echo "Pending";
                  }
                  elseif( $record->status == '2')
                  {
                    echo "Rejected";
                  }
                  ?></td>
                  <td style="text-align: center;">
                      <a href="<?php echo 'edit/'.$record->id; ?>" title="View Invoice">View</a>
                  </td>
              </tr>
          <?php
              $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</main>
<script>
  $('select').select2();

  function clearSearchForm()
  {
    window.location.reload();
  }

  function makeDefault(id)
  {
     $.ajax(
        {
           url: '/finance/currency/makeDefault/'+id,
           type: 'GET',
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
            window.location.reload();
           }
        });
  }
</script>
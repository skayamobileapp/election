<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">View Invoice</h1>
        
        <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    
    <form id="form_fee_setup" action="" method="post">

        <div class="page-container">

          <div>
            <h4 class="form-title">Invoice Details</h4>
          </div>

          <div class="form-container">


                <div class="row">


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Type</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="type" name="type" value="<?php echo $invoice->type; ?>" readonly>
                        </div>
                      </div>
                    </div>


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Invoice Date <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" autocomplete="off" value="<?php echo date('d-m-Y', strtotime($invoice->date_time)); ?>" readonly>
                        </div>
                      </div>
                    </div>


                </div>




                <div class="row">

                    

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Student <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <select name="id_student" id="id_student" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($studentList))
                            {
                                foreach ($studentList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                              <?php
                              if($record->id == $invoice->id_student)
                              {
                                echo 'selected';
                              }
                              ?>
                              >
                                <?php echo $record->nric . " - " . $record->full_name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                          </select>
                          </div>
                        </div>
                    </div>   




                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Total Amount <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                                <input type="text" class="form-control" id="invoice_total" name="invoice_total" readonly value="<?php echo $invoice->invoice_total ?>">
                          </div>
                        </div>
                    </div>


                </div>

                <div class="row"> 

                  <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Paid Amount <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                                <input type="text" class="form-control" id="paid_amount" name="paid_amount" readonly value="<?php echo $invoice->paid_amount ?>">
                          </div>
                        </div>
                  </div>


                  <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Balance Amount <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                                <input type="text" class="form-control" id="balance_amount" name="balance_amount" readonly value="<?php echo $invoice->balance_amount ?>">
                          </div>
                        </div>
                  </div>

                </div>


                <div class="row">
                  
                  <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Status</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="status" name="status" value="<?php if($invoice->status == 0){
                              echo 'Pending';
                            }elseif($invoice->status == 1)
                            {
                              echo 'Approved';
                            }if($invoice->status == 2)
                            {
                              echo 'Rejected';
                            }
                             ?>
                             " readonly>
                        </div>
                      </div>
                    </div>

                    
                </div>

            </div>

                  


                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <!-- <button type="submit" class="btn btn-primary">Save</button> -->
                  </div>

                </div> 

            </div>                                
        </div>
    </form>




      <form id="form_detail" action="" method="post">

        <div class="page-container">

          <div>
            <h4 class="form-title">Fee Item details</h4>
          </div>

            <div class="form-container">



                <?php

                if(!empty($invoiceDetails))
                {
                    ?>
                    <br>

                            <h4 class="form-group-title">Invoice Fee Item Details</h4>

                        

                          <div class="custom-table">
                            <table class="table">
                                <thead>
                                    <tr>
                                    <th>Sl. No</th>
                                    <th>Fee Item</th>
                                    <th>Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                     <?php
                                 $total = 0;
                                  for($i=0;$i<count($invoiceDetails);$i++)
                                 { ?>
                                    <tr>
                                    <td><?php echo $i+1;?></td>
                                    <td><?php echo $invoiceDetails[$i]->fee_setup; ?></td>
                                    <td><?php echo $invoiceDetails[$i]->amount; ?></td>
                                    </tr>
                                  <?php 
                              } 
                              ?>
                                </tbody>
                            </table>
                          </div>


                <?php
                
                }
                 ?>



            </div>                                
        </div>

      </form>










</main>

<script>
    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
    });


    $('select').select2();


    function reloadPage()
    {
      window.location.reload();
    }


    function saveData()
    {
        if($('#form_detail').valid())
        {
        var tempPR = {};
        tempPR['id_fee_item'] = $("#id_fee_item").val();
        tempPR['amount'] = $("#amount").val();
        // tempPR['id'] = $("#id").val();
            $.ajax(
            {
               url: '/finance/invoice/tempadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);

                var ta = $("#inv_total_amount").val();
                // alert(ta);
                $("#invoice_total").val(ta);
               }
            });
        }
    }

    function deleteTempData(id) {
         $.ajax(
            {
               url: '/finance/invoice/tempDelete/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
                var ta = $("#inv_total_amount").val();
                $("#total_amount").val(ta);
               }
            });
    }





    $(document).ready(function() {
        $("#form_detail").validate({
            rules: {
                id_fee_item: {
                    required: true
                },
                amount: {
                    required: true
                }
            },
            messages: {
                id_fee_item: {
                    required: "<p class='error-text'>Select Fee Item</p>",
                },
                amount: {
                    required: "<p class='error-text'>Amount Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function() {
        $("#form_fee_setup").validate({
            rules: {
                type: {
                    required: true
                },
                invoice_date: {
                    required: true
                },
                id_student: {
                    required: true
                },
                invoice_total: {
                    required: true
                }
            },
            messages: {
                type: {
                    required: "<p class='error-text'>Select Type</p>",
                },
                invoice_date: {
                    required: "<p class='error-text'>Invoice Date Required</p>",
                },
                id_student: {
                    required: "<p class='error-text'>Select Student</p>",
                },
                invoice_total: {   
                    required: "<p class='error-text'>Invoice Total Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


</script>
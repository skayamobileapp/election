<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">View Receipt</h1>
        
        <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    
    <form id="form_fee_setup" action="" method="post">

        <div class="page-container">

          <div>
            <h4 class="form-title">Receipt Details</h4>
          </div>

          <div class="form-container">


                <div class="row">


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Type</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="type" name="type" value="<?php echo $receipt->type; ?>" readonly>
                        </div>
                      </div>
                    </div>


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Receipt Date <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control datepicker" id="receipt_date" name="receipt_date" autocomplete="off" value="<?php echo date('d-m-Y', strtotime($receipt->receipt_date)); ?>" readonly>
                        </div>
                      </div>
                    </div>


                </div>




                <div class="row">

                    

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Student <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <select name="id_student" id="id_student" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($studentList))
                            {
                                foreach ($studentList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                              <?php
                              if($record->id == $receipt->id_student)
                              {
                                echo 'selected';
                              }
                              ?>
                              >
                                <?php echo $record->nric . " - " . $record->full_name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                          </select>
                          </div>
                        </div>
                    </div>   




                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Receipt Amount <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                                <input type="text" class="form-control" id="receipt_amount" name="receipt_amount" readonly value="<?php echo $receipt->receipt_amount ?>">
                          </div>
                        </div>
                    </div>


                </div>


                <div class="row">


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Remarks</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="remarks" name="remarks" value="<?php echo $receipt->remarks; ?>" readonly>
                        </div>
                      </div>
                    </div>



                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Status</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="status" name="status" value="<?php if($receipt->status == 0){
                              echo 'Pending';
                            }elseif($receipt->status == 1)
                            {
                              echo 'Approved';
                            }if($receipt->status == 2)
                            {
                              echo 'Rejected';
                            }
                             ?>
                             " readonly>
                        </div>
                      </div>
                    </div>



                </div>

                <!-- <div class="row"> 

                  <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Paid Amount <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                                <input type="text" class="form-control" id="paid_amount" name="paid_amount" readonly value="<?php echo $invoice->paid_amount ?>">
                          </div>
                        </div>
                  </div>


                  <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Balance Amount <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                                <input type="text" class="form-control" id="balance_amount" name="balance_amount" readonly value="<?php echo $invoice->balance_amount ?>">
                          </div>
                        </div>
                  </div>

                </div> -->

            </div>

                  


                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <!-- <button type="submit" class="btn btn-primary">Save</button> -->
                  </div>

                </div> 

            </div>                                
        </div>
    </form>




      <form id="form_detail" action="" method="post">

        <div class="page-container">

          <div>
            <h4 class="form-title">Receipt Invoice (Paid) Details</h4>
          </div>

            <div class="form-container">



                <?php

                if(!empty($invoiceDetails))
                {
                    ?>
                    <br>

                            <h4 class="form-group-title">Invoice Fee Item Details</h4>

                        

                          <div class="custom-table">
                            <table class="table">
                                <thead>
                                    <tr>
                                    <th>Sl. No</th>
                                    <th>Fee Item</th>
                                    <th>Paid Amount</th>
                                    <th>Invoice Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                     <?php
                                 $total = 0;
                                  for($i=0;$i<count($invoiceDetails);$i++)
                                 { ?>
                                    <tr>
                                    <td><?php echo $i+1;?></td>
                                    <td><?php echo $invoiceDetails[$i]->invoice_number; ?></td>
                                    <td><?php echo $invoiceDetails[$i]->paid_amount; ?></td>
                                    <td><?php echo date('d-m-Y', strtotime($invoiceDetails[$i]->date_time)); ?></td>

                                    </tr>
                                  <?php 
                              } 
                              ?>
                                </tbody>
                            </table>
                          </div>


                <?php
                
                }
                 ?>



            </div>                                
        </div>

      </form>










</main>

<script>
    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
    });


    $('select').select2();


    function reloadPage()
    {
      window.location.reload();
    }

    $(document).ready(function() {
        $("#form_detail").validate({
            rules: {
                id_fee_item: {
                    required: true
                },
                amount: {
                    required: true
                }
            },
            messages: {
                id_fee_item: {
                    required: "<p class='error-text'>Select Fee Item</p>",
                },
                amount: {
                    required: "<p class='error-text'>Amount Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function() {
        $("#form_fee_setup").validate({
            rules: {
                type: {
                    required: true
                },
                invoice_date: {
                    required: true
                },
                id_student: {
                    required: true
                },
                invoice_total: {
                    required: true
                }
            },
            messages: {
                type: {
                    required: "<p class='error-text'>Select Type</p>",
                },
                invoice_date: {
                    required: "<p class='error-text'>Invoice Date Required</p>",
                },
                id_student: {
                    required: "<p class='error-text'>Select Student</p>",
                },
                invoice_total: {   
                    required: "<p class='error-text'>Invoice Total Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


</script>
<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Add Receipt</h1>
        
        <a href='list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    
    <form id="form_fee_setup" action="" method="post">

        <div class="page-container">

          <div>
            <h4 class="form-title">Receipt Details</h4>
          </div>

          <div class="form-container">


                <div class="row">


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Type</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="type" name="type" value="Student" readonly>
                        </div>
                      </div>
                    </div>


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Receipt Date <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control datepicker" id="invoice_date" name="invoice_date" autocomplete="off" value="<?php echo date('d-m-Y'); ?>" readonly>
                        </div>
                      </div>
                    </div>


                </div>

                <div class="row">



                    

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Student <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <select name="id_student" id="id_student" class="form-control" onchange="getInvoiceByStudentId(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($studentList))
                            {
                                foreach ($studentList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->nric . " - " . $record->full_name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                          </select>
                          </div>
                        </div>
                    </div>  


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Remarks</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="remarks" name="remarks">
                        </div>
                      </div>
                    </div> 


                </div>



            </div>



            <div id="display_receipt_details" style="display: none;">
              <h4 class="form-title">Receipt Details</h4>

              <div class="form-container">
                <div id="view_invoice_details">      
                </div>
              </div>

            </div>


                  


                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <button type="submit" class="btn btn-primary">Save</button>
                      <a href='add' class="btn btn-link">Clear All Fields</a>
                  </div>

                </div> 

            </div>                                
        </div>
    </form>




</main>

<script>
    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
    });


    $('select').select2();


    function reloadPage()
    {
      window.location.reload();
    }


    function getInvoiceByStudentId(id_student)
    {
     
      var tempPR = {};
      tempPR['id_student'] = $("#id_student").val();
      tempPR['type'] = $("#type").val();
      // tempPR['id'] = $("#id").val();
          $.ajax(
          {
             url: '/finance/receipt/getInvoiceByStudentId',
              type: 'POST',
             data:
             {
              tempData: tempPR
             },
             error: function()
             {
              alert('Something is wrong');
             },
             success: function(result)
             {
              $("#view_invoice_details").html(result);
              $("#display_receipt_details").show();
             }
          });
    }

    function deleteTempData(id) {
         $.ajax(
            {
               url: '/finance/invoice/tempDelete/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
                var ta = $("#inv_total_amount").val();
                $("#total_amount").val(ta);
               }
            });
    }





    $(document).ready(function() {
        $("#form_detail").validate({
            rules: {
                id_fee_item: {
                    required: true
                },
                amount: {
                    required: true
                }
            },
            messages: {
                id_fee_item: {
                    required: "<p class='error-text'>Select Fee Item</p>",
                },
                amount: {
                    required: "<p class='error-text'>Amount Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function() {
        $("#form_fee_setup").validate({
            rules: {
                type: {
                    required: true
                },
                invoice_date: {
                    required: true
                },
                id_student: {
                    required: true
                },
                invoice_total: {
                    required: true
                }
            },
            messages: {
                type: {
                    required: "<p class='error-text'>Select Type</p>",
                },
                invoice_date: {
                    required: "<p class='error-text'>Receipt Date Required</p>",
                },
                id_student: {
                    required: "<p class='error-text'>Select Student</p>",
                },
                invoice_total: {   
                    required: "<p class='error-text'>Invoice Total Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


</script>
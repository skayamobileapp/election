<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Add Fee Category</h1>
        
        <a href='list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    
    <form id="form_fee_category" action="" method="post">

        <div class="page-container">

          <div>
            <h4 class="form-title">Fee Category details</h4>
          </div>

            <div class="form-container">


                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Code <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="code" name="code" placeholder="Code">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Name <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                        </div>
                      </div>
                    </div>

                    
                </div>


                <div class="row">
                  
                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Name Optional Language</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="name_optional_language" name="name_optional_language" placeholder="">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Fee Group <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <select name="fee_group" id="fee_group" class="form-control">
                                <option value="">Select</option>
                                <option value="Rental">Rental</option>
                                <option value="Statement Of Account">Statement Of Account</option>
                          </select>
                          </div>
                        </div>
                    </div>      



                </div>


                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Sequence <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="number" class="form-control" id="sequence" name="sequence" placeholder="1,2,3 ...">
                        </div>
                      </div>
                    </div>

                    
                    <div class="col-lg-6">
                      <div class="form-group row align-items-center">
                        <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1" checked="checked">
                            <label class="custom-control-label" for="customRadioInline1">Active</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0">
                            <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                          </div>
                        </div>
                      </div>
                    </div>

                </div>

                  


                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <button type="submit" class="btn btn-primary">Save</button>
                  </div>

                </div> 

            </div>                                
        </div>
    </form>
</main>

<script>

  $('select').select2();

  function reloadPage()
  {
    window.location.reload();
  }

    
    $(document).ready(function() {
        $("#form_fee_category").validate({
            rules: {
                code: {
                    required: true
                },
                name: {
                    required: true
                },
                status: {
                    required: true
                },
                fee_group: {
                    required: true
                },
                sequence: {
                    required: true
                }
            },
            messages: {
                code: {
                    required: "<p class='error-text'>Category Code Required</p>",
                },
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                },
                fee_group: {
                    required: "<p class='error-text'>Select Fee Group</p>",
                },
                sequence: {
                    required: "<p class='error-text'>Enter Sequence</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Edit Payment Type</h1>
        
        <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    
    <form id="form_payment_type" action="" method="post">

        <div class="page-container">

          <div>
            <h4 class="form-title">Payment Type details</h4>
          </div>

            <div class="form-container">


                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Code <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="code" name="code" placeholder="Code" value="<?php echo $paymentType->code ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Description <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="description" name="description" placeholder="Descripton" value="<?php echo $paymentType->description ?>">
                        </div>
                      </div>
                    </div>

                    
                </div>


                <div class="row">
                  
                  <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Description Optional Language</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="description_optional_language" name="description_optional_language" placeholder="" value="<?php echo $paymentType->description_optional_language ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Payment Group <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <select name="payment_group" id="payment_group" class="form-control">
                                <option value="">Select</option>
                                <option value="Sponsership"
                                <?php
                                if($paymentType->payment_group == 'Sponsership')
                                {
                                    echo 'selected';
                                }?>
                                >Sponsership</option>
                                <option value="Scholarship"
                                <?php
                                if($paymentType->payment_group == 'Scholarship')
                                {
                                    echo 'selected';
                                }?>
                                >Scholarship</option>
                                <option value="Checque"
                                <?php
                                if($paymentType->payment_group == 'Checque')
                                {
                                    echo 'selected';
                                }?>
                                >Checque</option>
                                <option value="Transfer Payment"
                                <?php
                                if($paymentType->payment_group == 'Transfer Payment')
                                {
                                    echo 'selected';
                                }?>
                                >Transfer Payment</option>
                                <option value="Cash"
                                <?php
                                if($paymentType->payment_group == 'Cash')
                                {
                                    echo 'selected';
                                }?>
                                >Cash</option>
                                <option value="Credit Card"<?php
                                if($paymentType->payment_group == 'Credit Card')
                                {
                                    echo 'selected';
                                }?>
                                >Credit Card</option>
                                <option value="Others"
                                <?php
                                if($paymentType->payment_group == 'Others')
                                {
                                    echo 'selected';
                                }?>
                                >Others</option>
                          </select>
                          </div>
                        </div>
                    </div>      



                </div>


                <div class="row">
                    
                    <div class="col-lg-6">
                      <div class="form-group row align-items-center">
                        <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1" <?php if($paymentType->status == 1){
                                echo "checked=checked";
                            } ?> >
                            <label class="custom-control-label" for="customRadioInline1">Active</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0" <?php if($paymentType->status == 0){
                                echo "checked=checked";
                            } ?> >
                            <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                          </div>
                        </div>
                      </div>
                    </div>

                </div>

                  


                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <button type="submit" class="btn btn-primary">Save</button>
                  </div>

                </div> 

            </div>                                
        </div>
    </form>
</main>

<script>

  $('select').select2();

  function reloadPage()
  {
    window.location.reload();
  }

    
    $(document).ready(function()
    {
        $("#form_payment_type").validate(
        {
            rules:
            {
                description:
                {
                    required: true
                },
                payment_group:
                {
                    required: true
                },
                code:
                {
                    required: true
                }
            },
            messages:
            {
                description:
                {
                    required: "<p class='error-text'>Description Required</p>",
                },
                payment_group:
                {
                    required: "<p class='error-text'>Payment Group Required</p>",
                },
                code:
                {
                    required: "<p class='error-text'>Payment Mode Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element)
            {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Add Currency Rate Setup</h1>
        
        <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>

        <div class="page-container">

          <div>
            <h4 class="form-title">Currency Setup details</h4>
          </div>

            <div class="form-container">


                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Code <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="code" name="code" placeholder="Code" value="<?php echo $currency->code ?>" readonly>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Name <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo $currency->name ?>" readonly>
                        </div>
                      </div>
                    </div>

                    
                </div>


                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Default Language</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="name_optional_language" name="name_optional_language" placeholder=" Default Language Name" value="<?php echo $currency->name_optional_language ?>" readonly>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Symbol Prefix <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="prefix" name="prefix" placeholder="Symbol Prefix" value="<?php echo $currency->prefix ?>" readonly>
                        </div>
                      </div>
                    </div>

                    

                </div>


                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Symbol Syffix <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="suffix" name="suffix" placeholder="Symbol Suffix" value="<?php echo $currency->suffix ?>" readonly>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Decimal Places <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="number" class="form-control" id="decimal_place" name="decimal_place" placeholder="Ex. 2 (Like .00)" value="<?php echo $currency->decimal_place ?>" readonly>
                        </div>
                      </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row align-items-center">
                        <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1" <?php if($currency->status == 1){
                                echo "checked=checked";
                            } ?> >
                            <label class="custom-control-label" for="customRadioInline1">Active</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0" <?php if($currency->status == 0){
                                echo "checked=checked";
                            } ?> >
                            <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                          </div>
                        </div>
                      </div>
                    </div>

                </div>

                  


                  
                

            </div>  

            <br>




        <form id="form_currency" action="" method="post">

        <div class="page-container">

          <div>
            <h4 class="form-title">Currency Rate Setup details</h4>
          </div>

            <div class="form-container">


                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Exchange Rate <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="number" class="form-control" id="exchange_rate" name="exchange_rate" placeholder="Exchange Rate">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Min Rate <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="number" class="form-control" id="min_rate" name="min_rate" placeholder="Min Rate">
                        </div>
                      </div>
                    </div>

                    
                </div>


                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Max Rate <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="number" class="form-control" id="max_rate" name="max_rate" placeholder=" Max Rate">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Effective Date <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control datepicker" id="effective_date" name="effective_date" placeholder="Effective Date">
                        </div>
                      </div>
                    </div>

                    

                </div>


                  


                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <button type="submit" class="btn btn-primary">Save</button>
                  </div>

                </div> 

            </div>                                
        </div>
    </form>



     <?php

        if(!empty($currencyRateSetup))
        {
            ?>
            <br>

            <div class="form-container">
                    <h4 class="form-group-title">Currency Rate Setup Details</h4>

                

                  <div class="custom-table">
                    <table class="table">
                        <thead>
                            <tr>
                            <th>Sl. No</th>
                            <th>Exchange Rate</th>
                            <th>Min. Rate</th>
                            <th>Max. Rate</th>
                            <th>Effective Date</th>
                            <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                             <?php
                         $total = 0;
                          for($i=0;$i<count($currencyRateSetup);$i++)
                         { ?>
                            <tr>
                            <td><?php echo $i+1;?></td>
                            <td><?php echo $currencyRateSetup[$i]->exchange_rate; ?></td>
                            <td><?php echo $currencyRateSetup[$i]->min_rate; ?></td>
                            <td><?php echo $currencyRateSetup[$i]->max_rate; ?></td>
                            <td><?php echo date('d-m-Y', strtotime($currencyRateSetup[$i]->effective_date)); ?></td>

                            <td class="text-center">
                            <a onclick="deleteRateSetup(<?php echo $currencyRateSetup[$i]->id; ?>)">Delete</a>
                            </td>
                             </tr>
                          <?php 
                      } 
                      ?>
                        </tbody>
                    </table>
                  </div>

                </div>

        <?php
        
        }
         ?>





        </div>
</main>

<script>

    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
    });


    function deleteRateSetup(id)
    {
         $.ajax(
            {
               url: '/finance/currencyRateSetup/deleteRateSetup/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                window.location.reload();
               }
            });
    }

    function reloadPage()
    {
      window.location.reload();
    }





  $(document).ready(function() {
        $("#form_currency").validate({
            rules: {
                exchange_rate: {
                    required: true
                },
                min_rate: {
                    required: true
                },
                max_rate: {
                    required: true
                },
                effective_date: {
                    required: true
                }
            },
            messages: {
                exchange_rate: {
                    required: "<p class='error-text'>Exchange Rate Required</p>",
                },
                min_rate: {
                    required: "<p class='error-text'>Min. Rate Required</p>",
                },
                max_rate: {
                    required: "<p class='error-text'>Max. Rate Required</p>",
                },
                effective_date: {
                    required: "<p class='error-text'>Effective Date Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


</script>

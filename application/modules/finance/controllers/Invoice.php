<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Invoice extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('invoice_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('invoice.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['invoice_number'] = $this->security->xss_clean($this->input->post('invoice_number'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['status'] = $this->security->xss_clean($this->input->post('status'));

            $data['searchParam'] = $formData;

            $data['invoiceList'] = $this->invoice_model->invoiceListSearch($formData);

            // echo "<Pre>"; print_r($data['invoiceList']);exit;

            $this->global['pageTitle'] = 'Election Management System : Invoice List';
            $this->global['pageCode'] = 'invoice.list';

            $this->loadViews("invoice/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('invoice.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;   


            if($this->input->post())
            {

                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $invoice_total = $this->security->xss_clean($this->input->post('invoice_total'));
                $status = $this->security->xss_clean($this->input->post('status'));
                
                $generated_number = $this->invoice_model->generateMainInvoiceNumber();

                $data = array(
                    'invoice_number' => $generated_number,
                    'type' => $type,
                    'id_student' => $id_student,
                    'invoice_total' => $invoice_total,
                    'total_amount' => $invoice_total,
                    'balance_amount' => $invoice_total,
                    'currency' => 1,
                    'status' => 1,
                    'created_by' => $user_id
                );
                // echo "<Pre>"; print_r($data);exit;

                $result = $this->invoice_model->addNewInvoice($data);

                if($result)
                {
                    $result = $this->invoice_model->moveTempToDetails($result);
                }

                redirect('/finance/invoice/list');
            }
            else
            {
                $deleted_temp = $this->invoice_model->deleteTempDataBySession($id_session);
            }

            $data['studentList'] = $this->invoice_model->studentList();
            $data['feeSetupList'] = $this->invoice_model->feeSetupListByStatus('1');

            $this->global['pageTitle'] = 'Election Management System : Add Invoice';
            $this->global['pageCode'] = 'invoice.add';

            $this->loadViews("invoice/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('invoice.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId; 

            if ($id == null)
            {
                redirect('/setup/invoice/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'type' => $type,
                    'status' => $status,
                    'updated_by' => $user_id
                );

                $result = $this->invoice_model->editAccountCode($data,$id);
                redirect('/finance/invoice/list');
            }
            
            $data['invoice'] = $this->invoice_model->getMainInvoice($id);
            $data['invoiceDetails'] = $this->invoice_model->getMainInvoiceDetails($id);
            $data['studentList'] = $this->invoice_model->studentList();
            // $data['feeSetupList'] = $this->invoice_model->feeSetupListByStatus('1');

                // echo "<Pre>"; print_r($data['invoice']);exit;


            $this->global['pageTitle'] = 'Election Management System : View Invoice';
            $this->global['pageCode'] = 'invoice.view';

            $this->loadViews("invoice/edit", $this->global, $data, NULL);
        }
    }

    function tempadd()
    {
        $id_session = $this->session->my_session_id;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));


        // echo "<Pre>";print_r($tempData);exit;

        $tempData['id_session'] = $id_session;
        $inserted_id = $this->invoice_model->addTempDetails($tempData);

        $data = $this->displaytempdata();
        
        echo $data;        
    }

    function displaytempdata()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->invoice_model->getTempMainInvoiceDetails($id_session); 
        // echo "<Pre>";print_r($details);exit;
        if(!empty($temp_details))
        {

        $table = "<table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Fee Item</th>
                    <th>Amount</th>
                    <th>Action</th>
                </tr>";
                $total_amount = 0;
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $fee_setup = $temp_details[$i]->fee_setup;
                    $amount = $temp_details[$i]->amount;
                    $j = $i+1;
                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$fee_setup</td>
                            <td>$amount</td>                           
                            <td>
                                <a onclick='deleteTempData($id)'>Delete</a>
                            </td>
                        </tr>";
                        $total_amount = $total_amount + $amount;
                    }

                    $table .= "
                        <tr>
                            <td></td>
                            <td style='text-align: right'>Total : </td>
                            <td><input type='hidden' id='inv_total_amount' name='inv_total_amount' value='$total_amount' />$total_amount</td>                           
                            <td></td>
                        </tr>";

        $table.= "</table>";
        }
        else
        {
            $table= "";
        }
        return $table;
    }

    function tempDelete($id)
    {
        // echo "<Pre>";  print_r($id);exit;
        $id_session = $this->session->my_session_id;
        $inserted_id = $this->invoice_model->deleteTempData($id);
        $data = $this->displaytempdata();
        echo $data; 
    } 

}

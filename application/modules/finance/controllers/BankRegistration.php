<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class BankRegistration extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('bank_registration_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('bank_registration.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $data['searchParam'] = $formData;

            $data['bankRegistrationList'] = $this->bank_registration_model->bankRegistrationListSearch($formData);

            $this->global['pageTitle'] = 'Election Management System : Bank Registration List';
            $this->global['pageCode'] = 'bank_registration.list';

            $this->loadViews("bank_registration/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('bank_registration.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $formData = $this->input->post();

               // echo "<Pre>"; print_r($formData);exit;
            	$id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $account_no = $this->security->xss_clean($this->input->post('account_no'));
                $bank_id = $this->security->xss_clean($this->input->post('bank_id'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $landmark = $this->security->xss_clean($this->input->post('landmark'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $cr_fund = $this->security->xss_clean($this->input->post('cr_fund'));
                $cr_department = $this->security->xss_clean($this->input->post('cr_department'));
                $cr_activity = $this->security->xss_clean($this->input->post('cr_activity'));
                $cr_account = $this->security->xss_clean($this->input->post('cr_account'));
                
                $data = array(
					'name' => $name,
                    'code' => $code,
					'account_no' => $account_no,
					'bank_id' => $bank_id,
					'address' => $address,
					'landmark' => $landmark,
					'city' => $city,
					'id_state' => $id_state,
					'id_country' => $id_country,
                    'zipcode' => $zipcode,
                    'cr_fund' => $cr_fund,
                    'cr_department' => $cr_department,
                    'cr_activity' => $cr_activity,
					'cr_account' => $cr_account,
					'status' => $status,
					'created_by' => $user_id
                );

                if($status == 1)
                {
                    $update['status'] = 0;
                    $inserted_id = $this->bank_registration_model->editBankRegistrationForStatus($update);

                }
                $inserted_id = $this->bank_registration_model->addNewBankRegistration($data);
                redirect('/finance/bankRegistration/list');
            }
            $data['countryList'] = $this->bank_registration_model->countryListByStatus('1');
            
               // echo "<Pre>"; print_r($data);exit;

            
            $this->global['pageTitle'] = 'Election Management System : Add Bank Registration';
            $this->global['pageCode'] = 'bank_registration.add';

            $this->loadViews("bank_registration/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('bank_registration.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/bankRegistration/list');
            }
            if($this->input->post())
            {
                $formData = $this->input->post();

               // echo "<Pre>"; print_r($formData);exit;

	            $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $account_no = $this->security->xss_clean($this->input->post('account_no'));
                $bank_id = $this->security->xss_clean($this->input->post('bank_id'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $landmark = $this->security->xss_clean($this->input->post('landmark'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $cr_fund = $this->security->xss_clean($this->input->post('cr_fund'));
                $cr_department = $this->security->xss_clean($this->input->post('cr_department'));
                $cr_activity = $this->security->xss_clean($this->input->post('cr_activity'));
                $cr_account = $this->security->xss_clean($this->input->post('cr_account'));

                $data = array(
					'name' => $name,
					'code' => $code,
                    'account_no' => $account_no,
					'bank_id' => $bank_id,
					'address' => $address,
					'landmark' => $landmark,
					'city' => $city,
					'id_state' => $id_state,
					'id_country' => $id_country,
					'zipcode' => $zipcode,
                    'cr_fund' => $cr_fund,
                    'cr_department' => $cr_department,
                    'cr_activity' => $cr_activity,
                    'cr_account' => $cr_account,
					'status' => $status,
					'updated_by' => $user_id
                );

                if($status == 1)
                {
                    $update['status'] = 0;
                    $inserted_id = $this->bank_registration_model->editBankRegistrationForStatus($update);

                }

                //print_r($data);exit;
                $result = $this->bank_registration_model->editBankRegistration($data,$id);
                redirect('/finance/bankRegistration/list');
            }
            // $data['studentList'] = $this->bank_registration_model->studentList();
            $data['bankRegistration'] = $this->bank_registration_model->getBankRegistration($id);
            $data['countryList'] = $this->bank_registration_model->countryListByStatus('1');
            $data['stateList'] = $this->bank_registration_model->stateListByStatus('1');

               // echo "<Pre>"; print_r($data);exit;

            $this->global['pageTitle'] = 'Election Management System : Edit Bank Registration';
            $this->global['pageCode'] = 'bank_registration.edit';

            $this->loadViews("bank_registration/edit", $this->global, $data, NULL);
        }
    }

    function getStateByCountry($id_country)
    {
        $results = $this->bank_registration_model->getStateByCountryId($id_country);

        // echo "<Pre>"; print_r($programme_data);exit;
     //    $table="   
     //        <script type='text/javascript'>
     //             $('select').select2();
     //         </script>
     // ";

        $table="

         <script type='text/javascript'>
                 $('select').select2();
         </script>


        <select name='id_state' id='id_state' class='form-control'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $results[$i]->id;
        $name = $results[$i]->name;
        $table.="<option value=".$id.">".$name.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;
    }
}

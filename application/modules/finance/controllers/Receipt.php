<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Receipt extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('receipt_model');
        $this->load->model('invoice_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('receipt.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['receipt_number'] = $this->security->xss_clean($this->input->post('receipt_number'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;

            $data['receiptList'] = $this->receipt_model->getReceiptListBySearch($formData);

            $data['studentList'] = $this->invoice_model->studentList();

            $this->global['pageTitle'] = 'Election Management System : Receipt List';
            $this->global['pageCode'] = 'receipt.list';

            $this->loadViews("receipt/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        $user_id = $this->session->userId;
        if ($this->checkAccess('receipt.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            

            if($this->input->post())
            {

                $formData = $this->input->post();

                // echo "<Pre>";print_r($formData);exit;

                // $receipt_amount = $this->security->xss_clean($this->input->post('receipt_amount'));
                $remarks = $this->security->xss_clean($this->input->post('remarks'));
                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $receipt_date = $this->security->xss_clean($this->input->post('receipt_date'));

                

                $receipt_number = $this->receipt_model->generateReceiptNumber();

                $data = array(
                    'id_student' => $id_student,
                    'type' => $type,
                    'receipt_number' => $receipt_number,
                    'receipt_amount' => 0,
                    'remarks' => $remarks,
                    'status' => 1
                );
                // echo "<Pre>";print_r($data);exit;
                $inserted_id = $this->receipt_model->addNewReceipt($data);

                if($inserted_id)
                {
                    $total_amount = 0;
                 for($i=0;$i<count($formData['invoice_id']);$i++)
                 {
                    $invoice_id = $formData['invoice_id'][$i];
                    $payable_amount = $formData['payable_amount'][$i];

                    if($payable_amount > 0)
                    {

                    $updated_main_invoice = $this->receipt_model->updateMainInvoiceAmount($invoice_id,$payable_amount);

                        if ($updated_main_invoice)
                        {
                        // echo "<Pre>";print_r($updated_main_invoice);exit();
                        
                            $detailsData = array(
                            'id_receipt' => $inserted_id,
                            'id_main_invoice' => $invoice_id,
                            'invoice_amount' => '0',
                            'paid_amount' => $payable_amount,                        
                            'status' => '1',
                            'created_by' => $user_id

                            );
                            //print_r($details);exit;
                            $result = $this->receipt_model->addNewReceiptDetails($detailsData);
                        }
                    }

                    $total_amount = $total_amount + $payable_amount;
                 }
                
                $update_data['receipt_amount'] = $total_amount;

                $result = $this->receipt_model->editReceipt($update_data,$inserted_id);



                }
                // $this->receipt_model->deleteTempDataBySession($id_session);
                 
                $this->receipt_model->deleteTempAmountDataBySession($id_session);
                redirect('/finance/receipt/list');
            }
            else
            {
                $this->receipt_model->deleteTempAmountDataBySession($id_session);
            }
            // $data['status'] = '1';
            // $data['nric'] = '';
            // $data['type'] = '';
            // $data['invoice_number'] = '';
            // $data['id_programme'] = '';
            // $data['id_intake'] = '';
            // $data['name'] = '';

            // $data['receiptList'] = $this->invoice_model->getMainInvoiceListByStatus($data);

            $data['studentList'] = $this->invoice_model->studentList();


            $this->global['pageTitle'] = 'Election Management System : Add Receipt';
            $this->global['pageCode'] = 'receipt.list';

            $this->loadViews("receipt/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('receipt.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/receipt/list');
            }
            if($this->input->post())
            {
                redirect('/finance/receipt/list');
            }
            
            $data['studentList'] = $this->receipt_model->studentList();


            $data['receipt'] = $this->receipt_model->getReceipt($id);
            $data['invoiceDetails'] = $this->receipt_model->getReceiptInvoiceDetails($id);

            // $data['paymentDetails'] = $this->receipt_model->getReceiptPaymentDetails($id);
            // echo "<pre>";print_R($data['receiptFor']);exit;

            // echo "<pre>";print_R($data['invoiceDetails']);exit;

            $this->global['pageTitle'] = 'Election Management System : View Receipt';
            $this->global['pageCode'] = 'receipt.view';

            $this->loadViews("receipt/edit", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('receipt.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/receipt/approvalList');
            }
            if($this->input->post())
            {
                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'status' => $status,
                    'reason' => $reason
                );

                $result = $this->receipt_model->editReceipt($data,$id);

                redirect('/finance/receipt/approvalList');
            }

            $data['receipt'] = $this->receipt_model->getReceipt($id);
            if($data['receipt']->type == 'Applicant')
            {
                $data['receiptFor'] = $this->invoice_model->getMainInvoiceApplicantData($data['receipt']->id_student);
            }elseif($data['receipt']->type == 'Student')
            {
                $data['receiptFor'] = $this->invoice_model->getMainInvoiceStudentData($data['receipt']->id_student);
            }elseif($data['receipt']->type == 'Sponser')
            {
                $data['receiptFor'] = $this->invoice_model->getMainInvoiceSponserData($data['receipt']->id_sponser);
                $data['studentDetails'] = $this->invoice_model->getStudentByStudent($data['receipt']->id_student);
            }
            // echo "<pre>";print_R($data['receiptFor']);exit;
            $data['invoiceDetails'] = $this->receipt_model->getReceiptInvoiceDetails($id);
            $data['paymentDetails'] = $this->receipt_model->getReceiptPaymentDetails($id);
            $data['degreeTypeList'] = $this->invoice_model->qualificationList();
            $data['sponserList'] = $this->receipt_model->sponserListByStatus('1');
            // echo "<Pre>"; print_r($data['receipt']);exit;

            $this->global['pageTitle'] = 'Election Management System : View Receipt';
            $this->global['pageCode'] = 'receipt.list';

            $this->loadViews("receipt/view", $this->global, $data, NULL);
        }
    }

    function approvalList()
    {
        if ($this->checkAccess('pr_entry_approval.list') == 0)
        // if ($this->checkAccess('receipt.approval_list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['receipt_number'] = $this->security->xss_clean($this->input->post('receipt_number'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['status'] = '0';
 
            $data['searchParam'] = $formData;

            $data['receiptList'] = $this->receipt_model->getReceiptListBySearch($formData);

            $data['programmeList'] = $this->invoice_model->programmeListByStatus('1');
            $data['intakeList'] = $this->invoice_model->intakeListByStatus('1');
            $data['applicantList'] = $this->invoice_model->applicantList();
            $data['studentList'] = $this->invoice_model->studentList();
            $data['sponserList'] = $this->receipt_model->sponserListByStatus('1');


            $this->global['pageTitle'] = 'Election Management System : Approve Receipt';
            $this->loadViews("receipt/approval_list", $this->global, $data, NULL);
        }
    }

    function tempadd()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_session;
        unset($tempData['id']);
        $inserted_id = $this->receipt_model->addTempDetails($tempData);
        
        $data = $this->displaytempdata();
        echo $data;        
    }

    function displaytempdata()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->receipt_model->getTempReceiptDetails($id_session); 

        if(!empty($temp_details))
        {

         // echo "<Pre>";print_r($temp_details);exit;
        
        $table = "<table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Payment Mode</th>
                    <th>Amount </th>
                    <th>Reference Number</th>
                    <th>Action</th>
                </tr>";
                $invoice_total_amount = 0;
                $invoice_paid_amount = 0;
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $id_payment_mode = $temp_details[$i]->id_payment_mode;
                    $payment_mode_amount = $temp_details[$i]->payment_mode_amount;
                    $payment_reference_number = $temp_details[$i]->payment_reference_number;
                    $j = $i+1;
                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$id_payment_mode</td>
                            <td>$payment_mode_amount</td>
                            <td>$payment_reference_number</td>                            
                            <td>
                                <a onclick='deleteTempData($id)'>Delete</a>
                            <td>
                        </tr>";
                        $invoice_total_amount = $invoice_total_amount + $payment_mode_amount;
                    }

                    $table .= "
                        <tr>
                            <td></td>
                            <td style='text-align: right'>Total : </td>
                            <td><input type='hidden' id='invoice_total_amount' value='$invoice_total_amount'/>$invoice_total_amount</td>
                                               
                            <td></td>
                        </tr>
                        </table>";

            }
            else
            {
                $table = '';
            }
        return $table;
    }



    function getInvoiceByStudentId()
    {
         
        $data = $this->security->xss_clean($this->input->post('tempData'));
        
            // $student_data = $this->receipt_model->getStudentByStudentId($id);
            // // echo "<Pre>"; print_r($student_data);exit;

            // $student_name = $student_data->full_name;
            // $student_nric = $student_data->nric;
            // $email = $student_data->email_id;
            // $nric = $student_data->nric;
            // $intake_name = $student_data->intake_name;
            // $programme_name = $student_data->programme_name;


            // $table  = "



            //  <h4 class='sub-title'>Student Details</h4>

            //     <div class='data-list'>
            //         <div class='row'>
    
            //             <div class='col-sm-6'>
            //                 <dl>
            //                     <dt>Student Name :</dt>
            //                     <dd>$student_name</dd>
            //                 </dl>
            //                 <dl>
            //                     <dt>Student Email :</dt>
            //                     <dd>$email</dd>
            //                 </dl>
            //                 <dl>
            //                     <dt>Student NRIC :</dt>
            //                     <dd>$nric</dd>
            //                 </dl>
            //             </div>        
                        
            //             <div class='col-sm-6'>
            //                 <dl>
            //                     <dt>Intake :</dt>
            //                     <dd>
            //                         $intake_name
            //                     </dd>
            //                 </dl>
            //                 <dl>
            //                     <dt>Programme :</dt>
            //                     <dd>$programme_name</dd>
            //                 </dl>
            //                 <dl>
            //                     <dt></dt>
            //                     <dd></dd>
            //                 </dl>
            //             </div>
    
            //         </div>
            //     </div>
            //     <br>";

            $type = $data['type'];
            $id_student = $data['id_student'];

            $invoice_data = $this->receipt_model->getInvoicesByStudentId($id_student);
            // echo "<Pre>";print_r($invoice_data);exit;

            $table = "";
            if(!$invoice_data)
            {
                $table .= "
                <br>
                <br>
                <div class='custom-table'>
                    <table align='center' class='table' id='list-table'>
                      <tr>
                        <h3 style='text-align: center;'>No Balance Invoices Available For This Student</h3>
                    </tr>
                    </table>
                </div>
                <br>
                <br>";
                // echo "<Pre>";print_r("No Data");exit;
            }
            else
            {
                $table .= "
                <h3>Receipt Details</h3>
                <div class='custom-table'>
                    <table class='table' id='list-table'>
                      <tr>
                        <th>Sl. No</th>
                        <th>Invoice Number</th>
                        <th>Total Amount</th>
                        <th>Balance Amount</th>
                        <th>Payable Amount</th>
                    </tr>";


                for($i=0;$i<count($invoice_data);$i++)
                    {
                        $id = $invoice_data[$i]->id;
                        $invoice_number = $invoice_data[$i]->invoice_number;
                        $total_amount = $invoice_data[$i]->total_amount;
                        $balance_amount = $invoice_data[$i]->balance_amount;
                        $paid_amount = $invoice_data[$i]->paid_amount;
                        $currency = $invoice_data[$i]->currency;
                        $j=$i+1;

                        $table .= "
                    <tr>
                        <td>$j
                        <input type='number' hidden='hidden' readonly='readonly' id='invoice_id[]' name='invoice_id[]' value='$id'>
                        <td>$invoice_number</td>
                        <td>$total_amount</td>
                        <td>$balance_amount</td>
                        <td style='text-align: center;'>
                        <div class='form-group'>
                            <input type='number' class='form-control' id='payable_amount[]' name='payable_amount[]' >
                        </div>
                        </td>
                    </tr>";
                    }
                            
                $table .= "
                </table>";
            }


            echo $table;
            exit;
    }


    function generateReceipt($id_receipt)
    {

        $this->getMpdfLibrary();


           // echo "<pre>"; print_r($receiptFor);exit;

            // include("/home/camsedu/public_html/assets/mpdf/vendor/autoload.php");
            //  require_once __DIR__ . '/vendor/autoload.php';
            
            $mpdf=new \Mpdf\Mpdf(); 

            // $mpdf->SetHeader("<div style='text-align: left;'>Election Management System
            //                    </div>");

        $organisationDetails = $this->receipt_model->getOrganisation();

        // echo "<Pre>";print_r($organisationDetails);exit;


        $signature = $_SERVER['DOCUMENT_ROOT']."/assets/images/logo.png";

        if($organisationDetails->image != '')
        {
            $signature = $_SERVER['DOCUMENT_ROOT']."/assets/images/" . $organisationDetails->image;
        }

            $currentDate = date('d-m-Y');
            $currentTime = date('h:i:s a');

        

         $receipt = $this->receipt_model->getReceipt($id_receipt);

        if($receipt->type == 'Applicant')
        {
            $receiptFor = $this->invoice_model->getMainInvoiceApplicantData($receipt->id_student);
        }elseif($receipt->type == 'Student')
        {
            $receiptFor = $this->invoice_model->getMainInvoiceStudentData($receipt->id_student);
        }elseif($receipt->type == 'Sponser')
        {
            $receiptFor = $this->invoice_model->getMainInvoiceSponserData($receipt->id_sponser);
            $studentDetails = $this->invoice_model->getStudentByStudent($receipt->id_student);
        }
        // echo "<pre>";print_R($data['receiptFor']);exit;
        

        // echo "<Pre>";print_r($receiptFor);exit;


        $type = $receipt->type;
        $receipt_number = $receipt->receipt_number;
        $receipt_date = $receipt->receipt_date;
        $remarks = $receipt->remarks;
        $receipt_amount = $receipt->receipt_amount;
        $programme_name = $receipt->programme_name;
        $programme_code = $receipt->programme_code;
        $intake_name = $receipt->intake_name;
        $intake_year = $receipt->intake_year;


        if($receipt_date)
        {
            $receipt_date = date('d-m-Y', strtotime($receipt_date));
        }


        $receipt_generation_name = $receiptFor->full_name;
        $receipt_generation_nric = $receiptFor->nric;



            $file_data = "";


            $file_data .= "
    <table align='center' width='100%'>
        <tr>
          <td style='text-align: left' width='20%' ><b>RECEIPT</b></td>
          <td style='text-align: center' width='30%' ></td>
          <td style='text-align: right' width='40%' ><img src='$signature' width='120px' /></td>
          
        </tr>
       
        
        <tr>
          <td style='text-align: center' width='100%'  colspan='3'></td>
        </tr>
    </table>


    <table width='100%'>
        <tr>
             <td>Student Name : $receipt_generation_name </td>
             <td></td>
             <td>Receipt No: $receipt_number</td>
             <td></td>
        </tr>
        <tr>
             <td>Matric No:  </td>
             <td></td>
             <td>Receipt Date: $receipt_date</td>
             <td></td>
        </tr>
    </table>

  <br>

<table align='center' width='100%' border='1' style='border-collapse: collapse;'>
  <tr>
   <td><b>No</b></td>
   <td><b>DESCRIPTION</b></td>
   <td><b>TOTAL (RM)</b></td>
  </tr>";
  $i = 0;


  
    $invoiceDetails = $this->receipt_model->getReceiptInvoiceDetails($id_receipt);
    
    // echo "<pre>"; print_r($invoiceDetails);exit;
    

        $total_receipt_detail = 0;
        $i = 1;
      foreach ($invoiceDetails as $value)
      {

        $id_invoice = $value->id;
        $remarks = $value->remarks;
        $invoice_number = $value->invoice_number;
        $invoice_total = $value->invoice_total;
        $total_amount = $value->total_amount;
        $paid_amount = $value->paid_amount;
        $balance_amount = $value->balance_amount;
        $total_discount = $value->total_discount;
        $balance_amount = $value->balance_amount;
        $currency = $value->currency;

        // $amount = number_format($amount, 2, '.', ',');

            // $acqDate   = date("d/m/Y", strtotime($acqDate));

        if($remarks == 'Student Course Registration')
        {
            $semester = $this->receipt_model->getSemesterCourseRegistrationByInvoiceId($id_invoice);
    // echo "<pre>"; print_r($semester);exit;

            $remarks = "Semester : " . $semester->semester_code . " - " . $semester->semester_name;


        }

       $file_data .= "
      <tr>
        <td>$i</td>
        <td>$remarks</td>
        <td>$paid_amount</td>
      </tr>";

      $total_receipt_detail = $total_receipt_detail + $paid_amount;
      $i++;

      }


      $paymentDetails = $this->receipt_model->getSingleReceiptPaymentDetails($id_receipt);

      $payment_type = $paymentDetails->id_payment_type;


  $file_data .= "
    <tr>
        <td>$i</th>
        <td>Payment Mode : $payment_type</th>
        <td></th>
    </tr>
  ";




    $amount_word = $this->getAmountWordings($receipt_amount);

    $amount_word = ucwords($amount_word);

    $receipt_amount = number_format($receipt_amount, 2, '.', ',');


  
   $file_data .= "
  <tr>
    <td></td>
    <td><b>TOTAL AMOUNT RECEIVED:</b></td>
    <td>$receipt_amount</td>
  </tr>
  <tr>
    <td colspan='2'  style='text-align:center;'><font size='3'><b>$amount_word</b></font></td>
    <td></td>
  </tr>
  <tr>
    <td colspan='3'  style='text-align:left;'><font size='3'>Issued by:<br>Finance & Accounts Department</font></td>
  </tr>
  </table>";

  // $paymentDetails = $this->receipt_model->getReceiptPaymentDetails($id_receipt);

  



//    $total_receipt_detail = 0;
//       foreach ($paymentDetails as $payment)
//       {

//         $id_payment_type = $payment->id_payment_type;
//         $payment_reference_number = $payment->payment_reference_number;
//         $paid_amount = $payment->paid_amount;

        

//        $file_data = $file_data ."
//       <tr>
//         <td style='text-align:left;' width='30%'><font size='3'>$id_payment_type</font></td>
//         <td style='text-align:left;' width='40%'><font size='3'>$payment_reference_number</font></td>
//         <td style='text-align:center;' width='30%'><font size='3'>$paid_amount</font></td>
//       </tr>";

//       }


//       $file_data = $file_data ."
// </table>";
    
    // echo "<pre>"; print_r($paymentDetails);exit;


  $bankDetails = $this->receipt_model->getBankRegistration();

        if($bankDetails)
        {
            $bank_name = $bankDetails->name;
            $bank_code = $bankDetails->code;
            $account_no = $bankDetails->account_no;
            $state = $bankDetails->state;
            $country = $bankDetails->country;
            $address = $bankDetails->address;
            $city = $bankDetails->city;
            $zipcode = $bankDetails->zipcode;

             $file_data .= "


        <p>1. All Receipts are Checked and Paid to : </p>
    <table align='center' width='100%'>
      <tr>
            <td style='text-align: left' width='30%'>PAYEE</td>
            <td style='text-align: center' width='5%'>:</td>
            <td style='text-align: left' width='65%'>AGILE DIGITAL TECHNOLOGIES</td>
      </tr>

      <tr>
            <td style='text-align: left' width='30%'>BANK</td>
            <td style='text-align: center' width='5%'>:</td>
            <td style='text-align: left' width='65%'>$bank_name</td>
      </tr>

      <tr>
            <td style='text-align: left' width='30%'>ADDRESS</td>
            <td style='text-align: center' width='5%'>:</td>
            <td style='text-align: left' width='65%'>$address , $city , $state , $country - $zipcode</td>
      </tr>

      <tr>
            <td style='text-align: left' width='30%'>ACCOUNT NO</td>
            <td style='text-align: center' width='5%'>:</td>
            <td style='text-align: left' width='65%'>$account_no</td>
      </tr>
      <tr>
            <td style='text-align: left' width='30%'>SWIFT CODE</td>
            <td style='text-align: center' width='5%'>:</td>
            <td style='text-align: left' width='65%'>$bank_code</td>
      </tr>

      
    </table>
    <p> 2. This is auto generated Receipt. No signature is required : </p>






    
      ";


        }

  


    // echo "<pre>"; print_r($file_data);exit;

    
  $file_data .="
<pagebreak>";



            // $mpdf->SetFooter('<div>Election Management System</div>');
            // echo $file_data;exit;

            $mpdf->WriteHTML($file_data);

            $mpdf->Output($type . '_RECEIPT_'.$receipt_number.'.pdf', 'D');
            exit;
    }
}

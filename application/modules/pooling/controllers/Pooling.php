<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Pooling extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('pooling_model');
        // $this->election_pooling();
    }
    
    function electionPooling($uniq_id)
    {
        // echo "<Pre>";print_r($uniq_id);exit();
        
        if ($uniq_id == '')
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $pooling = $this->pooling_model->getPoolingDataByRandomNumber($uniq_id);
            if(!$pooling)
            {
                redirect('/pooling/pooling/pageNotFound');
            }
            $is_voted = $pooling->is_voted;
            if($is_voted != 0)
            {
                redirect('/pooling/pooling/votingRecorded/'.$uniq_id);
            }

            $id_election = $pooling->id_election;
            $id_title = $pooling->id_title;
            $id_viewer = $pooling->id_viewer;
            $id_pooling = $pooling->id;


            if($this->input->post())
            {
            
                // echo "<Pre>";print_r($this->input->post());exit();

                $id_participant = $this->security->xss_clean($this->input->post('id_participant'));

                $voting_data = array(
                    "machine_ip"=>$_SERVER['REMOTE_ADDR'],
                    "browser_info"=>getBrowserAgent(),
                    "agent_string"=>$this->agent->agent_string(),
                    "platform"=>$this->agent->platform(),
                    "id_participant"=>$id_participant,
                    "is_voted"=>$id_participant,
                    "voting_date_time"=>date('Y-m-d H:i:s')

                );
                
                // echo "<Pre>";print_r($voting_data);exit();

                $result = $this->pooling_model->updateVotingPool($voting_data,$id_pooling);
                redirect('/pooling/pooling/votingRecorded/'.$uniq_id);
            }
            
            


            $data['title'] = $this->pooling_model->getTitle($id_title);
            $data['viewer'] = $this->pooling_model->getViewer($id_viewer);
            $data['election'] = $this->pooling_model->getElection($id_election);
            $data['electionPooling'] = $pooling;

            $data['electionPoolingCandidates'] = $this->pooling_model->getElectionPoolingCandidates($id_title,$id_election);

            // echo "<Pre>";print_r($data);exit();

            $this->global['pageTitle'] = 'Election Pooling : Add Communication Group';

            $this->loadViews("election_pooling/pooling", $this->global, $data, NULL);
        }
    }

    function pageNotFound()
    {
        $data['message'] = 'No User String Available ';

        $this->global['pageTitle'] = 'Election Management : User Not Autherized';
        $this->loadViews("election_pooling/page_not_found", $this->global, $data, NULL);
    }

    function votingRecorded($uniq_id)
    {
        $pooling = $this->pooling_model->getPoolingDataByRandomNumber($uniq_id);
        $is_voted = $pooling->is_voted;
        if($is_voted == 0)
        {
            redirect('/pooling/pooling/electionPooling/'.$uniq_id);
        }

        $id_election = $pooling->id_election;
        $id_title = $pooling->id_title;
        $id_viewer = $pooling->id_viewer;
        $id_pooling = $pooling->id;

        $data['title'] = $this->pooling_model->getTitle($id_title);
        $data['viewer'] = $this->pooling_model->getViewer($id_viewer);
        $data['election'] = $this->pooling_model->getElection($id_election);
        $data['electionPooling'] = $pooling;

        $data['message'] = ' Thank You For Voting <br> Your Vote Recorded ';

        $this->global['pageTitle'] = 'Election Management System : Voting Already Recorded';
        $this->loadViews("election_pooling/pooling_thankyou", $this->global, $data, NULL);
    }
}
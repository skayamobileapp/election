<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Pooling_model extends CI_Model
{

    function getPoolingDataByRandomNumber($uniq_id)
    {
        $this->db->select('*');
        $this->db->from('viewer_vote_pooling');
        $this->db->where('random_number', $uniq_id);
        $query = $this->db->get();
        return $query->row();
    }


    function getTitle($id)
    {
         $this->db->select('c.*');
        $this->db->from('election_title as c');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function getViewer($id)
    {
        $this->db->select('c.*');
        $this->db->from('viewers as c');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function getElection($id)
    {
        $this->db->select('c.*');
        $this->db->from('election as c');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function getElectionPoolingCandidates($id_title,$id_election)
    {
        $this->db->select('p.*');
        $this->db->from('participants as p');
        $this->db->where('p.id_title', $id_title);
        $this->db->where('p.id_election', $id_election);
         $query = $this->db->get();
         $result = $query->result();  
         //echo "<Pre>"; print_r($result);exit;
         return $result;
    }

    function updateVotingPool($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('viewer_vote_pooling', $data);
        return TRUE;
    }

    function templateList()
    {
        $this->db->select('sp.*');
        $this->db->from('communication_template as sp');
        $this->db->order_by("sp.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         //echo "<Pre>"; print_r($result);exit;
         return $result;
    }

    function templateListSearch($data)
    {
        $this->db->select('sp.*');
        $this->db->from('communication_template as sp');
        if (!empty($data['name']))
        {
            $likeCriteria = "(sp.name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("sp.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         //echo "<Pre>"; print_r($result);exit;
         return $result;
    }

    
    function addNewTemplate($data)
    {
        $this->db->trans_start();
        $this->db->insert('communication_template', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editTemplate($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('communication_template', $data);
        return TRUE;
    }

    function stateList()
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('status', '1');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function countryList()
    {
        $this->db->select('c.id, c.name');
        $this->db->from('country as c');
        $this->db->where('c.status', '1');
        $this->db->order_by("c.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getStateByCountryId($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

}


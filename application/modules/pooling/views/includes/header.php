<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="description" content="" />
  <title><?php echo $pageTitle; ?></title>
  <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

  <!-- Bootstrap core CSS -->
  <link href="<?php echo BASE_PATH; ?>assets/pooling/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?php echo BASE_PATH; ?>assets/pooling/css/font-awesome.min.css" rel="stylesheet" />
  <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">
  <!-- Custom styles for this template -->
  <link href="<?php echo BASE_PATH; ?>assets/pooling/css/main.css" rel="stylesheet" />
  
</head>

<body>


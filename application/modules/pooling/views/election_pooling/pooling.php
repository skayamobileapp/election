<!DOCTYPE html>
<html lang="en">


<body>

    <form id="form_main" action="" method="post">


        <nav class="navbar navbar-default main-header">
            <div class="container">
                <div class="navbar-header">
                    <div class="main-logos">
                        <a class="navbar-brand rv-trust-logo" href="#">RV Institutes</a>
                    </div>
                </div>
                <h3 class="header-title">
                    Welcome to <span><?php echo $election->name . " ON " . date('d-m-Y',strtotime($election->election_date)) ?></span>
                </h3>
                <!--/.nav-collapse -->
            </div>
        </nav>

        <div class="container main-container">
            <h3 class="section-title"><?php echo $title->name ?></h3>
            <table class="table table-bordered vote-table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Profile</th>
                        <th>Name</th>
                        <th>Logo</th>
                        <th width="100px">Vote</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                      if (!empty($electionPoolingCandidates))
                      {
                        $i = 1;
                        foreach ($electionPoolingCandidates as $record)
                        {
                      ?>
                    <tr>
                        <td scope="row"><?php echo $i ?></td>
                        <td class="text-center">
                            <img src="<?php echo '/assets/images/' . $record->image; ?>" class="img-responsive" /></td>
                        <td>
                            <h4><?php echo $record->name; ?></h4>
                           <!--  <a href="#" data-toggle="modal" data-target="#myModal" title="<?php echo $record->description ?>">Click here view description</a> -->
                        </td>
                        <td class="text-center">
                            <img src="<?php echo '/assets/images/' . $record->logo; ?>" class="img-responsive" />
                        </td>
                        <td>
                            <div class="radio vote-radio">
                                <label class="radio">
                                    <input type="radio" name="id_participant" id="id_participant" value="<?php echo $record->id; ?>">
                                    <span class="check-radio"></span>
                                </label>
                            </div>
                        </td>
                    </tr>
                    <?php
                      $i++;
                    }
                  }
                  ?>
                </tbody>
            </table>
            <div class="clearfix">
                <button type="submit" class="btn btn-primary pull-right">SUBMIT</button>
            </div>
            
        </div>

    </form>




    <footer class="footer-wrapper">
        <div class="container text-center">
            <p>&copy; 2020 Nanochip Solutions | RV-VLSI Design Center</p>
        </div>
    </footer>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Profile Details</h4>
            </div>
            <div class="modal-body">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias facilis voluptates, consequatur aut optio corporis magnam? Ea consequatur repellendus quaerat similique, dolorem, iusto consequuntur assumenda ad illum saepe earum sapiente.</p>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias facilis voluptates, consequatur aut optio corporis
                    magnam? Ea consequatur repellendus quaerat similique, dolorem, iusto consequuntur assumenda ad illum saepe earum
                    sapiente.</p>
            </div>
        </div>
    </div>
</div>

    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.steps.js"></script>
    <script src="js/main.js"></script>
</body>

</html>
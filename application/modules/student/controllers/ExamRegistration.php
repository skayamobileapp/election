<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ExamRegistration extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('setup/semester_model');        
        $this->load->model('grade_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('grade.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['gradeList'] = $this->grade_model->gradeListSearch($name);
            $this->global['pageTitle'] = 'Election Management System : Grade';
            $this->global['pageCode'] = 'grade.list';
            $this->loadViews("grade/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('grade.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $data['semesterList'] = $this->semester_model->semesterList();
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'status' => $status
                );
            
                $result = $this->grade_model->addNewGrade($data);
                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New Grade created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Grade creation failed');
                }
                redirect('/exam/grade/list');
            }
            
            $this->global['pageTitle'] = 'Election Management System : Add Grade';
            $this->global['pageCode'] = 'grade.add';
            $this->loadViews("grade/add", $this->global,  $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('grade.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/exam/grade/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'status' => $status
                );
                
                $result = $this->grade_model->editGradeDetails($data,$id);
                if ($result) {
                    $this->session->set_flashdata('success', 'Grade edited successfully');
                } else {
                    $this->session->set_flashdata('error', 'Grade edit failed');
                }
                redirect('/exam/grade/list');
            }
            $data['gradeList'] = $this->grade_model->getGradeDetails($id);
            $this->global['pageCode'] = 'grade.edit';
            $this->global['pageTitle'] = 'Election Management System : Edit Grade';
            $this->loadViews("grade/edit", $this->global, $data, NULL);
        }
    }
}

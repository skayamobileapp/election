<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ExamReport extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('exam_reports_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('exam_report.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_exam_location'] = '';
            $formData['id_exam_event'] = '';
            $formData['id_exam_name'] = '';
            $formData['exam_from_date'] = '';
            $formData['exam_to_date'] = '';




            $data['searchParam'] = $formData;

            if($this->input->post())
            {
                $formData['id_exam_location'] = $this->security->xss_clean($this->input->post('id_exam_location'));
                $formData['id_exam_event'] = $this->security->xss_clean($this->input->post('id_exam_event'));
                $formData['id_exam_name'] = $this->security->xss_clean($this->input->post('id_exam_name'));
                $formData['exam_from_date'] = date('Y-m-d', strtotime($this->security->xss_clean($this->input->post('exam_from_date'))));
                $formData['exam_to_date'] = date('Y-m-d', strtotime($this->security->xss_clean($this->input->post('exam_to_date'))));
                
                $data['searchParam'] = $formData;
     
                $data['examList'] = $this->exam_reports_model->examListSearch($formData);

                // echo "<Pre>"; print_r($data['examList']);exit;
            }


            $data['examNameList'] = $this->exam_reports_model->examListByStatus('1');
            $data['examLocationList'] = $this->exam_reports_model->examLocationListByStatus('1');
            $data['examEventList'] = $this->exam_reports_model->examEventListByStatus('1');


            $this->global['pageTitle'] = 'Election Management System : Exam Reports List';
            $this->global['pageCode'] = 'exam_report.list';
            //print_r($subjectDetails);exit;
            $this->loadViews("exam_report/list", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('exam_report.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/records/ExamReport/list');
            }
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->session_id;

                echo "<Pre>";print_r($this->input->post());exit;
                
                $applicant_status = $this->security->xss_clean($this->input->post('applicant_status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'applicant_status' => $applicant_status,
                    'reason' => $reason,
                    'approved_by' => $id_user
                );

                redirect('/records/ExamReport/list');
            }

            $data['examDetails'] = $this->exam_reports_model->getExamTaggingDetails($id);

            $data['studentDetails'] = $this->exam_reports_model->getStudentDetails($data['examDetails']->id_student);
            $data['examAttemptsByData'] = $this->exam_reports_model->getExamAttemptsByData($data['examDetails']->id_student,$id);

            // echo "<Pre>";print_r($data['studentDetails']);exit;

            $data['id_exam_tagging'] = $id;


            
            $this->global['pageTitle'] = 'Election Management System : View Exam Reports';
            $this->global['pageCode'] = 'exam_report.view';

            $this->loadViews("exam_report/edit", $this->global, $data, NULL);
        }
    }


    function viewQuestoins($id_student_tagging, $id_attemp)
    {
            // echo "<Pre>";print_r($id_student_tagging);exit;

        if ($this->checkAccess('exam_report.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id_student_tagging == null)
            {
                redirect('/records/ExamReport/list');
            }
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->session_id;

                echo "<Pre>";print_r($this->input->post());exit;
                
                $applicant_status = $this->security->xss_clean($this->input->post('applicant_status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'applicant_status' => $applicant_status,
                    'reason' => $reason,
                    'approved_by' => $id_user
                );

                redirect('/records/ExamReport/list');
            }

            $data['examDetails'] = $this->exam_reports_model->getExamTaggingDetails($id_student_tagging);

            $data['studentDetails'] = $this->exam_reports_model->getStudentDetails($data['examDetails']->id_student);

            $data['examQuestionsByData'] = $this->exam_reports_model->examQuestionsByData($id_student_tagging,$id_attemp);

            // echo "<Pre>";print_r($data['examQuestionsByData']);exit;

            $data['id_exam_tagging'] = $id_student_tagging;


            
            $this->global['pageTitle'] = 'Election Management System : View Exam Reports';
            $this->global['pageCode'] = 'exam_report.view';

            $this->loadViews("exam_report/view_question", $this->global, $data, NULL);
        }
    }

    function displaytempdata()
    {
        $data = $this->security->xss_clean($this->input->post('tempData'));

        
        $temp_details = $this->exam_reports_model->getExamStudentQuestionListByData($data); 

        echo "<Pre>";print_r($temp_details);exit;
        
        if(!empty($temp_details))
        {

        
        $table = "<table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Payment Mode</th>
                    <th>Amount </th>
                    <th>Reference Number</th>
                    <th>Action</th>
                </tr>";
                $invoice_total_amount = 0;
                $invoice_paid_amount = 0;
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $id_payment_mode = $temp_details[$i]->id_payment_mode;
                    $payment_mode_amount = $temp_details[$i]->payment_mode_amount;
                    $payment_reference_number = $temp_details[$i]->payment_reference_number;
                    $j = $i+1;
                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$id_payment_mode</td>
                            <td>$payment_mode_amount</td>
                            <td>$payment_reference_number</td>                            
                            <td>
                                <a onclick='deleteTempData($id)'>Delete</a>
                            <td>
                        </tr>";
                        $invoice_total_amount = $invoice_total_amount + $payment_mode_amount;
                    }

                    $table .= "
                        <tr>
                            <td></td>
                            <td style='text-align: right'>Total : </td>
                            <td><input type='hidden' id='invoice_total_amount' value='$invoice_total_amount'/>$invoice_total_amount</td>
                                               
                            <td></td>
                        </tr>
                        </table>";

            }
            else
            {
                $table = '';
            }
        echo $table;exit;
    }
}

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ExamStudentTagging extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('exam_student_tagging_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('exam_student_tagging.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_exam_event'] = $this->security->xss_clean($this->input->post('id_exam_event'));

            // $formData['id_exam_location'] = $this->security->xss_clean($this->input->post('id_exam_location'));
            // $formData['id_exam_center'] = $this->security->xss_clean($this->input->post('id_exam_center'));
            // $formData['id_exam_set'] = $this->security->xss_clean($this->input->post('id_exam_set'));

            $data['searchParam'] = $formData;

            $data['examStudentTaggingList'] = $this->exam_student_tagging_model->examStudentTaggingListSearch($formData);

            $data['examEventList'] = $this->exam_student_tagging_model->examEventListByStatus('1');

            // $data['locationList'] = $this->exam_student_tagging_model->examCenterLocationList();
            // $data['examCenterList'] = $this->exam_student_tagging_model->examCenterList();
            // $data['examSetList'] = $this->exam_student_tagging_model->examSetListByStatus('1');


        // echo "<Pre>";print_r($data['examStudentTaggingList']);exit();


            $this->global['pageCode'] = 'exam_student_tagging.list';
            $this->global['pageTitle'] = 'Election Management System : Exam Events';
            $this->loadViews("exam_student_tagging/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('exam_student_tagging.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $user_id = $this->session->userId;

                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_exam_center = $this->security->xss_clean($this->input->post('id_exam_center'));
                $id_exam_location = $this->security->xss_clean($this->input->post('id_exam_location'));
                $id_exam_event = $this->security->xss_clean($this->input->post('id_exam_event'));
                $id_exam_set = $this->security->xss_clean($this->input->post('id_exam_set'));
                $id_exam_name = $this->security->xss_clean($this->input->post('id_exam_name'));
                
                $data = array(
                    'id_student' => $id_student,
                    'id_exam_event' => $id_exam_event,
                    // 'id_exam_center' => $id_exam_center,
                    // 'id_exam_location' => $id_exam_location,
                    // 'id_exam_set' => $id_exam_set,
                    // 'id_exam_name' => $id_exam_name,
                    'status' => 1,
                    'created_by' => $user_id
                );

                $result = $this->exam_student_tagging_model->addExamStudentTagging($data);

                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New Exam Event created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Exam Event creation failed');
                }

                redirect('/student/examStudentTagging/list');
            }

            $data['studentList'] = $this->exam_student_tagging_model->studentListByStatus('1');
            $data['examEventList'] = $this->exam_student_tagging_model->examEventListByStatus('1');

            // $data['locationList'] = $this->exam_student_tagging_model->examCenterLocationListByStatus('1');
            // $data['examCenterList'] = $this->exam_student_tagging_model->examCenterListByStatus('1');
            // $data['examSetList'] = $this->exam_student_tagging_model->examSetListByStatus('1');
            // $data['examNameList'] = $this->exam_student_tagging_model->examNameListByStatus('1');



            $this->global['pageCode'] = 'exam_student_tagging.add';
            $this->global['pageTitle'] = 'Election Management System : Add Exam Event';
            $this->loadViews("exam_student_tagging/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('exam_student_tagging.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/exam/examStudentTagging/list');
            }
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                redirect('/exam/examStudentTagging/list');
            }

            $data['studentList'] = $this->exam_student_tagging_model->studentListByStatus('1');
            $data['examEventList'] = $this->exam_student_tagging_model->examEventListByStatus('1');

            $data['examStudentTagging'] = $this->exam_student_tagging_model->getExamStudentTagging($id);
            // $data['examStudentTagging'] = $this->exam_student_tagging_model->getExamStudentTagging($id);
            

            $this->global['pageCode'] = 'exam_student_tagging.edit';
            $this->global['pageTitle'] = 'Election Management System : Edit Exam Event';
            $this->loadViews("exam_student_tagging/edit", $this->global, $data, NULL);
        }
    }


    function getExamCenterByLocation($id_location)
    {
        // echo "<Pre>";print_r($id_location);exit();
            $results = $this->exam_student_tagging_model->getExamCenterByLocation($id_location);

            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_exam_center' id='id_exam_center' class='form-control' onchange='getExamSetByLocationNCenter()'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }


    function getExamSetByLocationNCenter()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $results = $this->exam_student_tagging_model->getExamSetByLocationNCenter($tempData);

        // echo "<Pre>";print_r($results);exit();
        if(!empty($results))
        {

            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_exam_set' id='id_exam_set' class='form-control' onchange='getExamEvent()'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;

        }

    }

    function getExamEvent()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $results = $this->exam_student_tagging_model->getExamEvents($tempData);

        // echo "<Pre>";print_r($results);exit();
        if(!empty($results))
        {

            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_exam_event' id='id_exam_event' class='form-control' onchange='getExamEventByID()'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $from_dt = date('d-m-Y', strtotime($results[$i]->from_dt));

            $table.="<option value=".$id.">".$from_dt . " - " . $name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;

        }
    }

    function getExamEventByID()
    {

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $results = $this->exam_student_tagging_model->getExamEventById($tempData['id_exam_event']);

        // echo "<Pre>";print_r($results);exit();

        if(!empty($results))
        {


            $date_time = date('d-m-Y',strtotime($results->from_dt));
            $from_tm = $results->from_tm;
            $name = $results->name;
            $to_tm = $results->to_tm;
            $type = $results->type;


         $table = "

            <h4 class='sub-title'>Event Details</h4>
            <br>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Event Name :</dt>
                                <dd>$name</dd>
                            </dl>
                            <dl>
                                <dt>Event Type :</dt>
                                <dd>$type</dd>
                            </dl>
                            <dl>
                                <dt>Event Date :</dt>
                                <dd>$date_time</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Event Start Time :</dt>
                                <dd> $from_tm
                                </dd>
                            </dl>
                            <dl>
                                <dt>Event End Time :</dt>
                                <dd>$to_tm</dd>
                            </dl>
                        </div>
    
                    </div>
                </div>

                ";
        }
        else
        {
            $table = "<h4 class='sub-title'>Event Not Available</h4>";
        }

        print_r($table);exit();
    }
}

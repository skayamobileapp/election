<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ExamName extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('exam_name_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('exam_name.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name')); 
            $data['searchParam'] = $formData;


            $data['examNameList'] = $this->exam_name_model->examNameListSearch($formData);
            $this->global['pageCode'] = 'exam_name.list';
            $this->global['pageTitle'] = 'Election Management System : Exam Name';
            $this->loadViews("exam_name/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('exam_name.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $total_marks = $this->security->xss_clean($this->input->post('total_marks'));
                $total_question = $this->security->xss_clean($this->input->post('total_question'));
                $total_time = $this->security->xss_clean($this->input->post('total_time'));
                $min_marks = $this->security->xss_clean($this->input->post('min_marks'));
                $display_result_immediately = $this->security->xss_clean($this->input->post('display_result_immediately'));
                $status = $this->security->xss_clean($this->input->post('status'));



                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'total_marks' => $total_marks,
                    'total_question' => $total_question,
                    'total_time' => $total_time,
                    'min_marks' => $min_marks,
                    'display_result_immediately' => $display_result_immediately,
                    'status' => $status,
                    'created_by' => $user_id,
                );
            
                $result = $this->exam_name_model->addNewExamName($data);
                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New Exam Name created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Exam Name creation failed');
                }
                redirect('/exam/examName/list');
            }

            $this->global['pageCode'] = 'exam_name.add';
            $this->global['pageTitle'] = 'Election Management System : Add Exam';
            $this->loadViews("exam_name/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('exam_name.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/exam/examName/list');
            }
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $total_marks = $this->security->xss_clean($this->input->post('total_marks'));
                $total_question = $this->security->xss_clean($this->input->post('total_question'));
                $total_time = $this->security->xss_clean($this->input->post('total_time'));
                $min_marks = $this->security->xss_clean($this->input->post('min_marks'));
                $display_result_immediately = $this->security->xss_clean($this->input->post('display_result_immediately'));
                $status = $this->security->xss_clean($this->input->post('status'));


                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'total_marks' => $total_marks,
                    'total_question' => $total_question,
                    'total_time' => $total_time,
                    'display_result_immediately' => $display_result_immediately,
                    'min_marks' => $min_marks,
                    'status' => $status,
                    'updated_by' => $user_id,
                );

                $result = $this->exam_name_model->editExamNameDetails($data,$id);
                if ($result) {
                    $this->session->set_flashdata('success', 'Exam Name edited successfully');
                } else {
                    $this->session->set_flashdata('error', 'Exam Name edit failed');
                }
                redirect('/exam/examName/list');
            }
            $data['examName'] = $this->exam_name_model->getExamNameDetails($id);
            $this->global['pageCode'] = 'exam_name.edit';
            $this->global['pageTitle'] = 'Election Management System : Edit Exam';
            $this->loadViews("exam_name/edit", $this->global, $data, NULL);
        }
    }
}

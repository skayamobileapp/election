<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ExamHasQuestion extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('exam_has_question_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('exam_has_question.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_course'] = $this->security->xss_clean($this->input->post('id_course'));
            $formData['id_topic'] = $this->security->xss_clean($this->input->post('id_topic'));
            $formData['id_course_learning_objective'] = $this->security->xss_clean($this->input->post('id_course_learning_objective'));
            $formData['id_bloom_taxonomy'] = $this->security->xss_clean($this->input->post('id_bloom_taxonomy'));
            $formData['id_difficult_level'] = $this->security->xss_clean($this->input->post('id_difficult_level'));

            $data['courseList'] = $this->exam_has_question_model->courseListByStatus('1');
            $data['topicList'] = $this->exam_has_question_model->topicListByStatus('1');
            $data['courseLearningObjectList'] = $this->exam_has_question_model->courseLearningObjectListByStatus('1');
            $data['bloomTaxonomyList'] = $this->exam_has_question_model->bloomTaxonomyListByStatus('1');
            $data['difficultLevelList'] = $this->exam_has_question_model->difficultLevelListByStatus('1');


            // echo "<Pre>";print_r($data['bloomTaxonomyList']);exit();

            $data['searchParam'] = $formData;


            $data['examHasQuestionList'] = $this->exam_has_question_model->examHasQuestionListSearch($formData);

            $this->global['pageCode'] = 'exam_has_question.list';
            $this->global['pageTitle'] = 'Election Management System : Exam Has Question List';
            $this->loadViews("exam_has_question/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('exam_has_question.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
              $user_id = $this->session->userId;

            if($this->input->post())
            {
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                $id_topic = $this->security->xss_clean($this->input->post('id_topic'));
                $id_course_learning_objective = $this->security->xss_clean($this->input->post('id_course_learning_objective'));
                $id_bloom_taxonomy = $this->security->xss_clean($this->input->post('id_bloom_taxonomy'));
                $id_difficult_level = $this->security->xss_clean($this->input->post('id_difficult_level'));
                $question_count = $this->security->xss_clean($this->input->post('question_count'));


                $data = array(
                    'id_course' => $id_course,
                    'id_topic' => $id_topic,
                    'id_course_learning_objective' => $id_course_learning_objective,
                    'id_bloom_taxonomy' => $id_bloom_taxonomy,
                    'id_difficult_level' => $id_difficult_level,
                    'question_count' => $question_count,
                    'status' => 1,
                    'created_by' => $user_id,
                );
            
                $inserted_count_id = $this->exam_has_question_model->addNewExamHasQuestion($data);

                if($inserted_count_id)
                {
                    $inserted_id = $this->exam_has_question_model->moveQuestionsIdToExam($data,$inserted_count_id);
                }
                if ($inserted_id > 0) {
                    $this->session->set_flashdata('success', 'New Exam Has Question created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Exam Has Question creation failed');
                }
                redirect('/exam/examHasQuestion/list');
            }

            $data['courseList'] = $this->exam_has_question_model->courseListByStatus('1');
            $data['topicList'] = $this->exam_has_question_model->topicListByStatus('1');
            $data['courseLearningObjectList'] = $this->exam_has_question_model->courseLearningObjectListByStatus('1');
            $data['bloomTaxonomyList'] = $this->exam_has_question_model->bloomTaxonomyListByStatus('1');
            $data['difficultLevelList'] = $this->exam_has_question_model->difficultLevelListByStatus('1');


            $this->global['pageCode'] = 'exam_has_question.add';
            $this->global['pageTitle'] = 'Election Management System : Add Exam Has Question';
            $this->loadViews("exam_has_question/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('exam_has_question.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/exam/examHasQuestion/list');
            }
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $total_marks = $this->security->xss_clean($this->input->post('total_marks'));
                $total_question = $this->security->xss_clean($this->input->post('total_question'));
                $total_time = $this->security->xss_clean($this->input->post('total_time'));
                $min_marks = $this->security->xss_clean($this->input->post('min_marks'));
                $status = $this->security->xss_clean($this->input->post('status'));


                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'total_marks' => $total_marks,
                    'total_question' => $total_question,
                    'total_time' => $total_time,
                    'min_marks' => $min_marks,
                    'status' => $status,
                    'updated_by' => $user_id,
                );

                $result = $this->exam_has_question_model->editExamHasQuestionDetails($data,$id);
                if ($result) {
                    $this->session->set_flashdata('success', 'Question edited successfully');
                } else {
                    $this->session->set_flashdata('error', 'Question edit failed');
                }
                redirect('/exam/examHasQuestion/list');
            }

            $data['courseList'] = $this->exam_has_question_model->courseListByStatus('1');
            $data['topicList'] = $this->exam_has_question_model->topicListByStatus('1');
            $data['courseLearningObjectList'] = $this->exam_has_question_model->courseLearningObjectListByStatus('1');
            $data['bloomTaxonomyList'] = $this->exam_has_question_model->bloomTaxonomyListByStatus('1');
            $data['difficultLevelList'] = $this->exam_has_question_model->difficultLevelListByStatus('1');


            $data['examHasQuestion'] = $this->exam_has_question_model->getExamHasQuestion($id);
            $data['examHasQuestionDetails'] = $this->exam_has_question_model->getExamHasQuestionDetails($id);

            $this->global['pageCode'] = 'exam_has_question.edit';
            $this->global['pageTitle'] = 'Election Management System : Edit Exam Has uestion';
            $this->loadViews("exam_has_question/edit", $this->global, $data, NULL);
        }
    }
}

<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Exam_student_tagging_model extends CI_Model
{

    function examEventListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('exam_event');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;   
    }

    function examSetListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('examset');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getExamCenterByLocation($id_location)
    {
        $this->db->select('DISTINCT(a.id) as id, a.*');
        $this->db->from('exam_center as a');
        $this->db->join('exam_center_location as ecl', 'a.id_location = ecl.id');
        $this->db->where('a.id_location', $id_location);
        $query = $this->db->get();
        $result = $query->result();
        // print_r($result);exit();      
        return $result;
    }

    function getExamSetByLocationNCenter($data)
    {
        $this->db->select('DISTINCT(a.id_exam_set) as id_exam_set');
        $this->db->from('exam_event as a');
        $this->db->where('a.id_location', $data['id_exam_location']);
        $this->db->where('a.id_exam_center', $data['id_exam_center']);
        $query = $this->db->get();
        $results = $query->result();


        // print_r($result);exit();      

        $details = array();
        foreach ($results as $value) {
            $id_exam_set = $value->id_exam_set;

            $exam_set = $this->getExamSet($id_exam_set);
            array_push($details, $exam_set);
        }
        return $details;
    }

    function getExamEvents($data)
    {
        $this->db->select('a.*');
        $this->db->from('exam_event as a');
        $this->db->where('a.id_location', $data['id_exam_location']);
        $this->db->where('a.id_exam_center', $data['id_exam_center']);
        $this->db->where('a.id_exam_set', $data['id_exam_set']);
        $query = $this->db->get();
        $results = $query->result();

        return $results;
    }

    function getExamSet($id)
    {
        $this->db->select('*');
        $this->db->from('exam_set');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getExamEventById($id)
    {
        $this->db->select('*');
        $this->db->from('exam_event');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function examStudentTaggingListSearch($data)
    {
        // $date = 
        $this->db->select('a.*, s.full_name as student_name, s.nric, ee.name as exam_event, ee.exam_date');
        $this->db->from('exam_student_tagging as a');
        $this->db->join('exam_event as ee', 'a.id_exam_event = ee.id');
        $this->db->join('student as s', 'a.id_student = s.id');
        // $this->db->join('exam_center_location as ecl', 'a.id_exam_location = ecl.id');
        // $this->db->join('exam_center as ec', 'a.id_exam_center = ec.id');
        if ($data['name'] != '') {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['name'] . "%' or s.email_id  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_exam_event'] != '') {
            $this->db->where('a.id_exam_event', $data['id_exam_event']);
        }
        // if ($data['id_exam_center'] != '') {
        //     $this->db->where('a.id_exam_center', $data['id_exam_center']);
        // }
        // if ($data['id_exam_set'] != '') {
        //     $this->db->where('a.id_exam_set', $data['id_exam_set']);
        // }
        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }


    function getExamDetails($studentId) {

         $this->db->select('b.*,es.instructions');
        $this->db->from('exam_student_tagging as b');
        $this->db->join('exam_event as e', 'b.id_exam_event=e.id');
        $this->db->join('examset as es', 'e.id_exam_set=es.id');
        $this->db->where('b.id_student', $studentId);
        $query = $this->db->get();
        $result = $query->row();
        return $result;

        
    }
    function checkExamStarted($examEventId) {
         $this->db->select('b.*');
        $this->db->from('exam_center_start_exam as b');
        $this->db->where('b.id_exam_event', $examEventId);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function getStudentExam($studentId)
    {
        $this->db->select('a.*,e.*,es.*,b.*,a.id as examstudenttagging');
        $this->db->from('exam_student_tagging as a');
        $this->db->join('exam_event as e', 'a.id_exam_event=e.id');
        $this->db->join('examset as es', 'e.id_exam_set=es.id');
        $this->db->join('tos as b', 'es.id_tos=b.id');
        $this->db->where('a.id_student', $studentId);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function getAnswersByQuestionId($questionid) {
        $this->db->select('a.*');
        $this->db->from('question_has_option as a');
        $this->db->where('a.id_question', $questionid);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function checkifAnswered($questionid,$id_student_exam_attempt) {
            $this->db->select('a.*');
        $this->db->from('student_question_set as a');
        $this->db->where('a.id_question', $questionid);
        $this->db->where('a.id_student_exam_attempt', $id_student_exam_attempt);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }
    function getPreviousUserExam($exam_id,$user_id)
    {
        $this->db->select('a.*');
        $this->db->from('examset_questions as a');
        $this->db->where('a.id_examset', $exam_id);
        $this->db->where('a.id_user', $user_id);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }
    function getPreviousExam($exam_id)
    {
        $this->db->select('a.*');
        $this->db->from('examset_questions as a');
        $this->db->where('a.id_examset', $exam_id);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }
    function getExamQuestions($id_tos)
    {
        $this->db->select('a.*');
        $this->db->from('tos_details as a');
        $this->db->where('a.id_tos', $id_tos);
        $query = $this->db->get();
        $result = $query->result();

        $final_questions = array();
        foreach ($result as $row) {
            $question_count = $row->questions_selected;
            $id_course = $row->id_course;
            $id_topic = $row->id_topic;
            $id_bloom_taxonomy = $row->id_bloom_taxonomy;
            $id_difficult_level = $row->id_difficult_level;

            $query1 = $this->db->query("select * from question where id_course = " . $id_course . " AND id_topic = " . $id_topic . " AND id_bloom_taxonomy =" . $id_bloom_taxonomy . " AND id_difficult_level =" . $id_difficult_level . " order by rand() limit " . $question_count);
            $result1 = $query1->result_array();
            foreach ($result1 as $row1) {
                $question_options = $this->db->query("select * from question_has_option where id_question = " . $row1['id'])->result_array() ;
                $row1['options'] = $question_options;
                array_push($final_questions, $row1);
            }
        }
        return $final_questions;
    }

    function getExamEvent($id)
    {
        $this->db->select('*');
        $this->db->from('exam_student_tagging');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function addExamStudentTagging($data)
    {
        $this->db->trans_start();
        $this->db->insert('exam_student_tagging', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }
    function saveExamsetQuestions($exam,$questions,$userId,$examset_code)
    { 
        $this->db->trans_start();
        foreach($questions as $question){
            $data = array();
            $data['examset_code'] = $examset_code;
            $data['id_examset'] = $exam->id;
            $data['id_question'] = $question['id'];
            $data['id_user'] = $userId;
            $data['status'] = 1;
            $this->db->insert('examset_questions', $data);
        }
        $this->db->trans_complete();
        return ;
    }

    function editExamEvent($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('exam_student_tagging', $data);
        return $result;
    }


    function getStateByCountryId($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function examCenterLocationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('exam_center_location');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }


    function examCenterLocationList()
    {
        $this->db->select('*');
        $this->db->from('exam_center_location');
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function examCenterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('exam_center');
        $this->db->order_by("name", "ASC");
        $this->db->where('status', $status);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function studentListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->order_by("full_name", "ASC");
        $this->db->where('status', $status);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function examNameListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('exam_name');
        $this->db->order_by("name", "ASC");
        $this->db->where('status', $status);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }


    function examCenterList()
    {
        $this->db->select('*');
        $this->db->from('exam_center');
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getExamCenterNLocationByCenterId($id)
    {
        $this->db->select('*');
        $this->db->from('exam_center');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $data['exam_center'] = $query->row()->name;



        $id_location = $query->row()->id_location;

        $this->db->select('*');
        $this->db->from('exam_center_location');
        $this->db->where('id', $id_location);
        $query = $this->db->get();
        $data['location'] = $query->row()->name;

        return $data;
    }

    function getExamStudentTagging($id)
    {
        $this->db->select('*');
        $this->db->from('exam_student_tagging');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $data = $query->row();

        return $data;
    }
}

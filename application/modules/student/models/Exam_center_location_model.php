<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Exam_center_location_model extends CI_Model
{
    function locationList()
    {
        $this->db->select('*');
        $this->db->from('exam_center_location');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function locationListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('exam_center_location');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%' or code  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getLocation($id)
    {
        $this->db->select('*');
        $this->db->from('exam_center_location');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewLocation($data)
    {
        $this->db->trans_start();
        $this->db->insert('exam_center_location', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editLocation($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('exam_center_location', $data);
        return $result ;
    }
}


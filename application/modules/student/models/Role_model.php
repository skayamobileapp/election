<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Role_model extends CI_Model
{
    function roleListingCount()
    {
        $this->db->select('BaseTbl.id as roleId, BaseTbl.role');
        $this->db->from('roles as BaseTbl');
         $query = $this->db->get();
        
        return $query->num_rows();
    }
    
    function latestRoleListing()
    {
        $this->db->select('BaseTbl.id as roleId, BaseTbl.role');
        $this->db->from('roles as BaseTbl');
        $this->db->limit(10, 0);
        $this->db->order_by("roleId", "desc");

         $query = $this->db->get();
         $result = $query->result();        
         return $result;
    }
    
    function roleListing($role)
    {
        $this->db->select('BaseTbl.id as roleId, BaseTbl.role');
        $this->db->from('roles as BaseTbl');
        if(!empty($role)) {
        $likeCriteria = "(BaseTbl.role  LIKE '%".$role."%')";
        $this->db->where($likeCriteria);
        }
         $query = $this->db->get();
         $result = $query->result();        
         return $result;
    }

    function rolePermissions($roleId)
    {
        $this->db->select('BaseTbl.permissionId');
        $this->db->from('tbl_role_permissions as BaseTbl');
        $this->db->where('roleId', $roleId);
        $query = $this->db->get();
         $result = $query->result();        
         return $result;
    }

    function addNewRole($roleInfo)
    {
        $this->db->trans_start();
        $this->db->insert('roles', $roleInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }

    function updateRolePermissions($permissions,$roleId)
    {
        $this->db->trans_start();
        $SQL = "delete from tbl_role_permissions where roleId = ".$roleId;
        $query = $this->db->query($SQL);
        foreach($permissions as $permission){
            $info = array('roleId'=>$roleId,'permissionId'=>$permission);
            $this->db->insert('tbl_role_permissions', $info);
        }
        
        $this->db->trans_complete();
        
        return ;
    }
    
    function getRoleInfo($roleId)
    {
        $this->db->select('id as roleId, role');
        $this->db->from('roles');
        $this->db->where('id', $roleId);
        $query = $this->db->get();
            // echo "<pre>";print_r($query);die;
        
        return $query->row();
    }

    function checkAccess($roleId,$code)
    {
         return 0;

        $this->db->select('id');
        $this->db->from('tbl_role_permissions');
        $this->db->join('tbl_permissions', 'tbl_role_permissions.permissionId = tbl_permissions.permissionId');

        $this->db->where('roleId', $roleId);
        $this->db->where('code', $code);
        $query = $this->db->get();
        if(empty($query->row())){
            return 1;
        }
        else{
            return 0;
        }
    }
    
    function editRole($roleInfo, $roleId)
    {
        $this->db->where('id', $roleId);
        $this->db->update('roles', $roleInfo);
        
        return TRUE;
    }
    
    function deleteRole($roleId, $roleInfo)
    {
        $this->db->where('id', $roleId);
        $this->db->update('roles', $roleInfo);
        
        return $this->db->affected_rows();
    }

}

  
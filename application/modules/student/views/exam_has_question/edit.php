<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Edit Exam Event</h1>
        
        <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    
    <form id="form_main" action="" method="post">

        <div class="page-container">

          <div>
            <h4 class="form-title">Exam Event details</h4>
          </div>

            <div class="form-container">


                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">No. Of Questions <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="question_count" name="question_count" placeholder="No. Questions" value="<?php echo $examHasQuestion->question_count; ?>" readonly>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Course <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <select name="id_course" id="id_course" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($courseList))
                            {
                                foreach ($courseList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                      <?php 
                                    if($record->id == $examHasQuestion->id_course)
                                        { echo "selected"; }
                                    ?>
                                      >
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                          </select>
                          </div>
                        </div>
                    </div>


                </div>

                <div class="row">

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Topic </label>
                          <div class="col-sm-8">
                            <select name="id_topic" id="id_topic" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($topicList))
                            {
                                foreach ($topicList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                      <?php 
                                    if($record->id == $examHasQuestion->id_topic)
                                        { echo "selected"; }
                                    ?>
                                      >
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                          </select>
                          </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Course Learning Objective </label>
                          <div class="col-sm-8">
                            <select name="id_course_learning_objective" id="id_course_learning_objective" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($courseLearningObjectList))
                            {
                                foreach ($courseLearningObjectList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                      <?php 
                                    if($record->id == $examHasQuestion->id_course_learning_objective)
                                        { echo "selected"; }
                                    ?>
                                      >
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                          </div>
                        </div>
                    </div>

                </div>


                <div class="row">


                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Bloom Taxonomy </label>
                          <div class="col-sm-8">
                            <select name="id_bloom_taxonomy" id="id_bloom_taxonomy" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($bloomTaxonomyList))
                            {
                                foreach ($bloomTaxonomyList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                      <?php 
                                    if($record->id == $examHasQuestion->id_bloom_taxonomy)
                                        { echo "selected"; }
                                    ?>
                                      >
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                            </select>
                          </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Difficult Level </label>
                          <div class="col-sm-8">
                            <select name="id_difficult_level" id="id_difficult_level" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($difficultLevelList))
                            {
                                foreach ($difficultLevelList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                      <?php 
                                    if($record->id == $examHasQuestion->id_difficult_level)
                                        { echo "selected"; }
                                    ?>
                                      >
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                            </select>
                          </div>
                        </div>
                    </div>

                </div>
                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <!-- <button type="submit" class="btn btn-primary">Save</button> -->
                        <!-- <a onclick="reloadPage()" class="btn btn-link">Clear All Fields</a> -->
                      <!-- <button onclick="reloadPage()" class="btn btn-link">Clear All Fields</button> -->
                  </div>

                </div> 

            </div>                                
        </div>
    </form>


    <?php

        if(!empty($examHasQuestionDetails))
        {
            ?>
            <br>

            <div class="form-container">
                    <h4 class="form-group-title">Question Details</h4>

                <h8>Note : No. Of Questions May Be Vary </h8>

                  <div class="custom-table">
                    <table class="table">
                        <thead>
                            <tr>
                            <th>Sl. No</th>
                             <th>Question</th>
                             <th class="text-center">Image</th>
                             <!-- <th class="text-center">Action</th> -->
                            </tr>
                        </thead>
                        <tbody>
                             <?php
                         $total = 0;
                          for($i=0;$i<count($examHasQuestionDetails);$i++)
                         { ?>
                            <tr>
                            <td><?php echo $i+1;?></td>
                            <td><?php echo $examHasQuestionDetails[$i]->question;?></td>
                            <td class="text-center">
                            <?php 
                            if($examHasQuestionDetails[$i]->image)
                            {
                            ?>
                            <a href="<?php echo '/assets/images/' . $examHasQuestionDetails[$i]->image; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $examHasQuestionDetails[$i]->image; ?>)" title="<?php echo $examHasQuestionDetails[$i]->image; ?>">
                                View
                            </a>
                            <?php
                            }else
                            {
                                echo "<a title='No File '> No File Uploaded </a>";
                            }
                            ?>

                            </td>


                           <!--  <td class="text-center">
                            <a onclick="deleteQuestion(<?php echo $examHasQuestionDetails[$i]->id; ?>)">Delete</a>
                            </td> -->

                             </tr>
                          <?php 
                      } 
                      ?>
                        </tbody>
                    </table>
                  </div>

                </div>




        <?php
        
        }
         ?>




</main>

<script>

    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                question_count: {
                    required: true
                },
                id_course: {
                    required: true
                }
            },
            messages: {
                question_count: {
                    required: "<p class='error-text'>No. Of Questions Required</p>",
                },
                id_course: {
                    required: "<p class='error-text'>Select Course</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">


    $( function() {
      $( ".datepicker" ).datepicker({
          changeYear: true,
          changeMonth: true,
          yearRange: "1960:2001"
      });
    } );

    $('select').select2();

    function reloadPage()
    {
      window.location.reload();
    }
</script>
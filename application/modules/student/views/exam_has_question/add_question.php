<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Add Questions To Exam</h1>
        
        <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>

        <div class="page-container">

          <div>
            <h4 class="form-title">Exam Name details</h4>
          </div>

            <div class="form-container">


                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Name <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo $examName->name; ?>" readonly>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Descrption <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="description" name="description" placeholder="Description" value="<?php echo $examName->description; ?>" readonly>
                        </div>
                      </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Total Marks <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="number" class="form-control" id="total_marks" name="total_marks" placeholder="Total Marks" value="<?php echo $examName->total_marks; ?>" readonly>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Total Question <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="number" class="form-control" id="total_question" name="total_question" placeholder="Total Question" value="<?php echo $examName->total_question; ?>" readonly>
                        </div>
                      </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Total Time <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="number" class="form-control" id="total_time" name="total_time" placeholder="Total Time" value="<?php echo $examName->total_time; ?>" readonly>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Min. Marks <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="number" class="form-control" id="min_marks" name="min_marks" placeholder="Min. Marks" value="<?php echo $examName->min_marks; ?>" readonly>
                        </div>
                      </div>
                    </div>

                </div>
   

                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row align-items-center">
                        <label class="col-sm-4 col-form-label">Display Result Immediately <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioResult1" name="display_result_immediately" class="custom-control-input" value="0" 
                            <?php if($examName->display_result_immediately==0) {
                                 echo "checked=checked";
                              };?> disabled
                            >
                            <label class="custom-control-label" for="customRadioResult1">No</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioResult2" name="display_result_immediately" class="custom-control-input" value="1" 
                            <?php if($examName->display_result_immediately==1) {
                                 echo "checked=checked";
                              };?> disabled
                            >
                            <label class="custom-control-label" for="customRadioResult2">Yes</label>
                          </div>
                        </div>
                      </div>
                    </div>


                    <div class="col-lg-6">
                      <div class="form-group row align-items-center">
                        <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1" 
                            <?php if($examName->status==1) {
                                 echo "checked=checked";
                              };?> disabled
                            >
                            <label class="custom-control-label" for="customRadioInline1">Active</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0"
                            <?php if($examName->status==0) {
                                 echo "checked=checked";
                              };?> disabled
                            >
                            <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                          </div>
                        </div>
                      </div>
                    </div>

                </div> 




                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <!-- <button type="submit" class="btn btn-primary">Save</button> -->
                        <!-- <a onclick="reloadPage()" class="btn btn-link">Clear All Fields</a> -->
                      <!-- <button onclick="reloadPage()" class="btn btn-link">Clear All Fields</button> -->
                  </div>

                </div> 

            </div>                                
        </div>





        <form id="form_main" action="" method="post">

        <div class="page-container">

          <div>
            <h4 class="form-title">Exam Has Question details</h4>
          </div>

            <div class="form-container">


                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">No. Of Questions <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="question_count" name="question_count" placeholder="No. Questions">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Course <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <select name="id_course" id="id_course" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($courseList))
                            {
                                foreach ($courseList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>">
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                          </select>
                          </div>
                        </div>
                    </div>


                </div>

                <div class="row">

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Topic </label>
                          <div class="col-sm-8">
                            <select name="id_topic" id="id_topic" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($topicList))
                            {
                                foreach ($topicList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>">
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                          </select>
                          </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Course Learning Objective </label>
                          <div class="col-sm-8">
                            <select name="id_course_learning_objective" id="id_course_learning_objective" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($courseLearningObjectList))
                            {
                                foreach ($courseLearningObjectList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>">
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                          </div>
                        </div>
                    </div>

                </div>


                <div class="row">


                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Bloom Taxonomy </label>
                          <div class="col-sm-8">
                            <select name="id_bloom_taxonomy" id="id_bloom_taxonomy" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($bloomTaxonomyList))
                            {
                                foreach ($bloomTaxonomyList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>">
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                            </select>
                          </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Difficult Level </label>
                          <div class="col-sm-8">
                            <select name="id_difficult_level" id="id_difficult_level" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($difficultLevelList))
                            {
                                foreach ($difficultLevelList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>">
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                            </select>
                          </div>
                        </div>
                    </div>

                </div>
                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <button type="submit" class="btn btn-primary">Save</button>
                        <!-- <a onclick="reloadPage()" class="btn btn-link">Clear All Fields</a> -->
                      <button onclick="reloadPage()" class="btn btn-link">Clear All Fields</button>
                  </div>

                </div> 

            </div>                                
        </div>
    </form>








    <?php

        if(!empty($examHasQuestionCountList))
        {
            ?>
            <br>

            <div class="form-container">
                    <h4 class="form-group-title">Questions For Exam</h4>

                <!-- <h8>Note : No. Of Questions May Be Vary </h8> -->

                  <div class="custom-table">
                    <table class="table">
                        <thead>
                            <tr>
                            <th>Sl. No</th>
                            <th>Total Questions</th>
                            <th>Course</th>
                            <th>Topic</th>
                            <th>Bloom Taxonomy</th>
                            <th>Course Learning Objective</th>
                            <th>Difficult Level</th>
                            <!-- <th style="text-align: center;">Action</th> -->
                            </tr>
                        </thead>
                        <tbody>
                             <?php
                         $total = 0;
                         $i=1;
                          foreach ($examHasQuestionCountList as $record)
                         { ?>
                            <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo ucfirst($record->question_count) ?></td>
                            <td><?php echo $record->course_code . " - " . $record->course_name ?></td>
                            <td><?php echo $record->topic_code . " - " . $record->topic_name ?></td>
                            <td><?php echo $record->bloom_taxonomy_code . " - " . $record->bloom_taxonomy_name ?></td>
                            <td><?php echo $record->course_learning_objective_code . " - " . $record->course_learning_objective_name ?></td>
                            <td><?php echo $record->difficult_level_code . " - " . $record->difficult_level_name ?></td>
                            <!-- <td class="text-center">
                                <a href="<?php echo 'edit/'.$record->id; ?>" class="btn" type="" data-toggle="tooltip" data-placement="top" title="View" >
                                  <i class="fa fa-eye" style="color:magenta" aria-hidden="true">
                                  </i>
                                </a>

                              </td> -->
                            </tr>

                             </tr>
                          <?php 
                          $i++;
                      } 
                      ?>
                        </tbody>
                    </table>
                  </div>

                </div>




        <?php
        
        }
         ?>








</main>

<script>

    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                question_count: {
                    required: true
                },
                id_course: {
                    required: true
                }
            },
            messages: {
                question_count: {
                    required: "<p class='error-text'>No. Of Questions Required</p>",
                },
                id_course: {
                    required: "<p class='error-text'>Select Course</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
  
    $('select').select2();

    function reloadPage()
    {
      window.location.reload();
    }
</script>
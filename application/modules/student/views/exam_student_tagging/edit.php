<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">View Exam Student Tagging</h1>
        
        <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    
    <form id="form_main" action="" method="post">

        <div class="page-container">

          <div>
            <h4 class="form-title">Exam Student Tagging Details</h4>
          </div>

            <div class="form-container">


                <div class="row">

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Student <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <select name="id_student" id="id_student" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($studentList))
                            {
                                foreach ($studentList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                              <?php 
                              if($record->id == $examStudentTagging->id_student)
                                  { echo "selected"; }
                              ?>
                              >
                                <?php echo $record->nric . " - " . $record->full_name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                          </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Event <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <select name="id_exam_event" id="id_exam_event" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($examEventList))
                            {
                                foreach ($examEventList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                              <?php 
                              if($record->id == $examStudentTagging->id_exam_event)
                                  { echo "selected"; }
                              ?>
                              >
                                <?php echo date('d-m-Y', strtotime($record->exam_date)) . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                          </div>
                        </div>
                    </div>


                </div>


                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <!-- <button type="submit" class="btn btn-primary">Save</button> -->
                  </div>

                </div> 

                </div>

                        


                  

            </div>                                
        </div>
    </form>
</main>

<script>

    function getExamCenterByLocation(id)
    {
      if(id != '')
      {


        $.get("/student/examStudentTagging/getExamCenterByLocation/"+id, function(data, status){
       
        // alert(data);
            $('#dummy_exam_center').hide();
            $('#view_exam_center').show();
            $("#view_exam_center").html(data);
            // $("#view_programme_details").html(data);
            // $("#view_programme_details").show();
        });
      }
      else
      {
        $('#dummy_exam_center').show();
        $('#view_exam_center').hide();
      }
    }





    function getExamSetByLocationNCenter()
    {
      
        var tempPR = {};
        tempPR['id_exam_center'] = $("#id_exam_center").val();
        tempPR['id_exam_location'] = $("#id_exam_location").val();

        if($("#id_exam_center").val() != '' && $("#id_exam_location").val() != '')
        {
  
            $.ajax(
            {
               url: '/student/examStudentTagging/getExamSetByLocationNCenter',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                 $('#dummy_exam_set').hide();
              $('#view_exam_set').show();
              $("#view_exam_set").html(result);

                // $('#myModal').modal('hide');
                // var ta = $("#total_detail").val();
                // alert(ta);
                // $("#amount").val(ta);
               }
            });

      }
      else
      {
        $('#dummy_exam_set').show();
        $('#view_exam_set').hide();
      }
    }

    function getExamEvent()
    {
        var tempPR = {};
        tempPR['id_exam_center'] = $("#id_exam_center").val();
        tempPR['id_exam_location'] = $("#id_exam_location").val();
        tempPR['id_exam_set'] = $("#id_exam_set").val();

        if($("#id_exam_center").val() != '' && $("#id_exam_set").val() != '' && $("#id_exam_location").val() != '')
        {
  
            $.ajax(
            {
               url: '/student/examStudentTagging/getExamEvent',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                 $('#dummy_exam_event').hide();
              $('#view_exam_event').show();
              $("#view_exam_event").html(result);

                // $('#myModal').modal('hide');
                // var ta = $("#total_detail").val();
                // alert(ta);
                // $("#amount").val(ta);
               }
            });

      }
      else
      {
        $('#dummy_exam_event').show();
        $('#view_exam_event').hide();
      }

    }


    function getExamEventByID()
    {
      var tempPR = {};
        tempPR['id_exam_event'] = $("#id_exam_event").val();

        if($("#id_exam_event").val() != '')
        {
  
            $.ajax(
            {
               url: '/student/examStudentTagging/getExamEventByID',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
               
            $('#view_event_details').show();
              $("#view_event_details").html(result);

                // $('#myModal').modal('hide');
                // var ta = $("#total_detail").val();
                // alert(ta);
                // $("#amount").val(ta);
               }
            });

      }
      else
      {
        $('#view_event_details').hide();
      }
    }


    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                id_student: {
                    required: true
                },
                 id_exam_center: {
                    required: true
                },
                 id_exam_location: {
                    required: true
                },
                 id_exam_event: {
                    required: true
                },
                 id_exam_set: {
                    required: true
                },
                 id_exam_name: {
                    required: true
                }
            },
            messages: {
                id_student: {
                    required: "<p class='error-text'>Select Student</p>",
                },
                id_exam_center: {
                    required: "<p class='error-text'>Select Exam Center</p>",
                },
                id_exam_location: {
                    required: "<p class='error-text'>Select Exam Location</p>",
                },
                id_exam_event: {
                    required: "<p class='error-text'>Select Exam Event</p>",
                },
                id_exam_set: {
                    required: "<p class='error-text'>Select Exam Set</p>",
                },
                id_exam_name: {
                    required: "<p class='error-text'>Select Exam Name</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">


    $( function() {
      $( ".datepicker" ).datepicker({
          changeYear: true,
          changeMonth: true,
          yearRange: "1960:2001"
      });
    } );

    $('select').select2();

    function reloadPage()
    {
      window.location.reload();
    }
</script>
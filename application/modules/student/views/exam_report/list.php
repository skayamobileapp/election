<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Exam Attempt Student List</h1>
    <!-- <a href="add" class="btn btn-primary ml-auto">+ Add Fee Structure</a> -->
  </div>
  <div class="page-container">
    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="">
              Advanced Search
              <i class="fa fa-angle-right" aria-hidden="true"></i>
              <i class="fa fa-angle-down" aria-hidden="true"></i>
            </a>
          </h4>
        </div>
        <form action="" method="post" id="form_search">

          <div id="collapseOne" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true" style="">
            <div class="panel-body p-2">

              <div class="row">

                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">From Date <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control datepicker" id="exam_from_date" name="exam_from_date" placeholder="01/10/2020" value="<?php if($searchParam['exam_from_date'] != ''){ echo date('d-m-Y', strtotime($searchParam['exam_from_date'])); } ?>">
                    </div>
                  </div>
                </div>


                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">To Date <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control datepicker" id="exam_to_date" name="exam_to_date" placeholder="11/10/2020" value="<?php if($searchParam['exam_to_date'] != ''){ echo date('d-m-Y', strtotime($searchParam['exam_to_date'])); } ?>">
                    </div>
                  </div>
                </div>


              </div>



              <!-- <div class="row">


                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Exam Name <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                      <select name="id_exam_name" id="id_exam_name" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($examNameList)) {
                          foreach ($examNameList as $record) {
                            $selected = '';
                            if ($record->id == $searchParam['id_exam_name']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>" <?php echo $selected;  ?>>
                              <?php echo  $record->name;  ?>
                            </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>



                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Location </label>
                    <div class="col-sm-8">
                      <select name="id_exam_location" id="id_exam_location" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if(!empty($examLocationList)) {
                          foreach($examLocationList as $record) {
                            $selected = '';
                            if ($record->id == $searchParam['id_exam_location']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>" <?php echo $selected;  ?>>
                              <?php echo  $record->name;  ?>
                            </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

                


              </div> -->

              <div class="row">

                
                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Exam Event <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                      <select name="id_exam_event" id="id_exam_event" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($examEventList)) {
                          foreach ($examEventList as $record) {
                            $selected = '';
                            if ($record->id == $searchParam['id_exam_event']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>" <?php echo $selected;  ?>>
                              <?php echo  $record->name;  ?>
                            </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>


              </div>




              <hr />
              <div class="d-flex justify-content-center">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="list" class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>

        </form>

      </div>
    </div>
    <?php
    if ($this->session->flashdata('success')) {
    ?>
      <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
      </div>
    <?php
    }
    if ($this->session->flashdata('error')) {
    ?>
      <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Failure!</strong> <?php echo $this->session->flashdata('error'); ?>
      </div>
    <?php
    }
    ?>
    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Student</th>
            <th>Location</th>
            <th>Exam Event</th>
            <th>Exam Name</th>
            <th>Exam Date</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($examList)) {
            $i = 1;
            foreach ($examList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->full_name . " - " . $record->nric ?></td>
                <td><?php echo $record->exam_location ?></td>
                <td><?php echo $record->exam_event ?></td>
                <td><?php echo $record->exam_name ?></td>
                <td><?php echo date('d-m-Y', strtotime($record->exam_date)); ?></td>
                <!-- <td><?php echo $record->phone?></td> -->
                <!-- <td style="text-align: center;"><?php echo $record->applicant_status ?></td> -->
                <td style="text-align: center;">
                  <a href="<?php echo 'edit/' . $record->id; ?>" class="btn" type="" data-toggle="tooltip" data-placement="top" title="View Attempts">
                    <i class="fa fa-eye" aria-hidden="true">
                    </i>
                  </a>
                </td>
              </tr>
          <?php
              $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</main>
<script>
  $('select').select2();

   $(function(){
      $(".datepicker").datepicker(
      {
          changeYear: true,
          changeMonth: true,
      });
    });


   $(document).ready(function()
   {
      $("#form_search").validate({
          rules: {
              exam_from_date: {
                  required: true
              },
              exam_to_date: {
                  required: true
              },
              id_exam_event: {
                  required: true
              }
              // ,
              // exam_date: {
              //   required: true
              // }
          },
          messages: {
              exam_from_date: {
                  required: "<p class='error-text'>Select From Date</p>",
              },
              exam_to_date: {
                  required: "<p class='error-text'>Select To Date</p>",
              },
              id_exam_event: {
                  required: "<p class='error-text'>Select Exam Event</p>",
              }
              // ,
              // exam_date: {
              //   required: "<p class='error-text'>Select Course</p>",
              // }
          },
          errorElement: "span",
          errorPlacement: function(error, element) {
              error.appendTo(element.parent());
          }
        });
    });


</script>
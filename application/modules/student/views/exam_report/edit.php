<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Student Examination Report View</h1>

        <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

        
    </div>



          <div class="form-container">
              <h4 class="form-group-title">Student Details</h4>
              <div class='data-list'>
                  <div class='row'> 
                      <div class='col-sm-6'>
                          <dl>
                              <dt>Student Name :</dt>
                              <dd><?php echo ucwords($studentDetails->full_name);?></dd>
                          </dl>
                          <dl>
                              <dt>Student NRIC :</dt>
                              <dd><?php echo $studentDetails->nric ?></dd>
                          </dl>                        
                      </div>        
                      
                      <div class='col-sm-6'>                           
                          <dl>
                              <dt>Student Email :</dt>
                              <dd><?php echo $studentDetails->email_id; ?></dd>
                          </dl>
                          <dl>
                              <dt>Phone :</dt>
                              <dd><?php echo $studentDetails->phone ?></dd>
                          </dl>   
                      </div>
                  </div>
              </div>
          </div>







            


<form id="form_applicant" action="" method="post" enctype="multipart/form-data">


            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>


            <div class="clearfix">

                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#tab_one" class="nav-link border rounded text-center"
                            aria-controls="tab_one" aria-selected="true"
                            role="tab" data-toggle="tab">Exam Attemp Details</a>
                    </li>
                    <!-- <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                            aria-controls="tab_two" role="tab" data-toggle="tab">Exam Registation</a>
                    </li>
                    <li role="presentation"><a href="#tab_three" class="nav-link border rounded text-center"
                            aria-controls="tab_three" role="tab" data-toggle="tab">Invoice Details</a>
                    </li>
                    <li role="presentation"><a href="#tab_four" class="nav-link border rounded text-center"
                            aria-controls="tab_four" role="tab" data-toggle="tab">Receipt Details</a>
                    </li>
                    <li role="presentation"><a href="#tab_five" class="nav-link border rounded text-center"
                            aria-controls="tab_five" role="tab" data-toggle="tab">Exam Attempts</a>
                    </li> -->
                                     
                </ul>


            





            
                
                <div class="tab-content offers-tab-content">



                    <div role="tabpanel" class="tab-pane active" id="tab_one">
                        
                        <div class="col-12 mt-4">



                         <!-- <div class="form-container">
                            <h5 class="form-group-title">Coming Soon ....!!!</h5>
                          </div> -->


                          <?php

                                if(!empty($examAttemptsByData))
                                {
                                    ?>

                                    <div class="form-container">
                                            <h5 class="form-group-title">Exam Attemp Details</h5>

                                        

                                          <div class="custom-table">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                    <th>Sl. No</th>
                                                    <th>Attempt Number</th>
                                                    <th>Start Date</th>
                                                    <th>End Date</th>
                                                    <th>Submitted Time</th>
                                                    <th>Result</th>
                                                    <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                  for($i=0;$i<count($examAttemptsByData);$i++)
                                                 { ?>
                                                    <tr>
                                                    <td><?php echo $i+1;?></td>
                                                    <td><?php echo $examAttemptsByData[$i]->attempt_number; ?></td>
                                                    <td><?php echo date('d-m-Y H:i:s', strtotime($examAttemptsByData[$i]->start_time)); ?></td>
                                                    <td><?php echo date('d-m-Y', strtotime($examAttemptsByData[$i]->end_time)); ?></td>
                                                    <td><?php echo date('h:i:s A', strtotime($examAttemptsByData[$i]->submitted_time)); ?></td>
                                                    <td><?php echo $examAttemptsByData[$i]->exam_result; ?></td>
                                                    <td style="text-align: center;">
                                                      <a href="<?php echo '../viewQuestoins/' . $id_exam_tagging . '/' . $examAttemptsByData[$i]->id; ?>" class="btn" type="" data-toggle="tooltip" data-placement="top" title="View Attempts">
                                                        <i class="fa fa-eye" aria-hidden="true">
                                                        </i>
                                                      </a>
                                                    </td>

                                                    <!-- <td style="text-align: center;">
                                                      <a onclick="viewAttemptQuestioDetails(<?php echo $examAttemptsByData[$i]->exam_result; ?>" class="btn" type="" data-toggle="tooltip" data-placement="top" title="View Questions">
                                                        <i class="fa fa-eye" aria-hidden="true">
                                                        </i>
                                                      </a>
                                                    </td> -->
                                                    </tr>
                                                  <?php
                                              } 
                                              ?>
                                                </tbody>
                                            </table>
                                          </div>

                                        </div>

                                <?php
                                
                                }
                                else
                                {
                                    ?>

                                    <h4>No Attempts Found</h4>
                                    <?php

                                }
                                 ?>



                        </div> 
                    
                    </div>









                    <div role="tabpanel" class="tab-pane" id="tab_two">
                        
                        <div class="col-12 mt-4">



                          <div class="form-container">
                            <h5 class="form-group-title">Coming Soon .....!!!</h5>
                          </div>
                         



                        </div> 
                    
                    </div>







                    <div role="tabpanel" class="tab-pane" id="tab_three">
                        
                        <div class="mt-4">


                            
                            


                        </div>
                    
                    </div>






                    <div role="tabpanel" class="tab-pane" id="tab_four">
                        <div class="mt-4">


                            <?php

                                if(!empty($receiptList))
                                {
                                    ?>

                                    <div class="form-container">
                                            <h5 class="form-group-title">Receipt Details</h5>

                                        

                                          <div class="custom-table">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                    <th>Sl. No</th>
                                                    <th>Receipt Number</th>
                                                    <th>Receipt Date</th>
                                                    <th>Type</th>
                                                    <th>Paid Amount</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                  for($i=0;$i<count($receiptList);$i++)
                                                 { ?>
                                                    <tr>
                                                    <td><?php echo $i+1;?></td>
                                                    <td><?php echo $receiptList[$i]->receipt_number; ?></td>
                                                    <!-- <td><?php echo date('d-m-Y H:i:s', strtotime($receiptList[$i]->receipt_date)); ?></td> -->
                                                    <td><?php echo date('d-m-Y', strtotime($receiptList[$i]->receipt_date)); ?></td>
                                                    <td><?php echo $receiptList[$i]->type; ?></td>
                                                    <td><?php echo $receiptList[$i]->receipt_amount; ?></td>
                                                    </tr>
                                                  <?php
                                              } 
                                              ?>
                                                </tbody>
                                            </table>
                                          </div>

                                        </div>

                                <?php
                                
                                }
                                else
                                {
                                    ?>

                                    <h4>No Receipt Found</h4>
                                    <?php

                                }
                                 ?>

                        



                        </div>
                    
                    </div>











                    <div role="tabpanel" class="tab-pane" id="tab_five">
                        <div class="mt-4">


                          <div class="form-container">
                            <h5 class="form-group-title">Coming Soon .....!!!</h5>
                          </div>
                        



                        </div>
                    
                    </div>




                











                </div>






                


        </div>


    </form>





     <!--  <div class="chat-bot-container" style="display: ;">
          <div class="cb-header">
              Hi, <strong><?php print_r($_SESSION['applicant_name']);?></strong> <br />We are happy to help you,
          </div>
          <ul class="cb-help">
              <li>Type '<strong>1</strong>' : For Application Status</li>
              <li>Type '<strong>2</strong>' : For Fee Amount</li>
              <li>Type '<strong>3</strong>' : For Offer Letter</li>
              <li>Type '<strong>4</strong>' : To know the start date of the semester</li>
              <li>Type '<strong>9</strong>' : To get a call from the representative</li>
          </ul>
         
          <div id='attach'></div>
          <div class="form-group cb-form">
              <input type="text" class="form-control" placeholder="Type here..." name='message' id='message' value=''>
              <button class="btn btn-primary" type="button" onclick='validateData()'>Start</button>
            </div>                                   
      </div> -->




        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer> 
      






</main>


<script>

    $('select').select2();


    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
    });
</script>
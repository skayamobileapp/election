<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Result_model extends CI_Model
{
    function titleListSearch($data)
    {
        $this->db->select('c.*, ele.name as election_name, ele.election_date, ele.city, cre.name as created_by, upd.name as updated_by');
        $this->db->from('election_title as c');
        $this->db->join('election as ele', 'c.id_election = ele.id');
        $this->db->join('users as cre', 'c.created_by = cre.id','left');
        $this->db->join('users as upd', 'c.updated_by = upd.id','left');
        if (!empty($data['name']))
        {
            $likeCriteria = "(c.name  LIKE '%" . $data['name'] . "%')";
            // $likeCriteria = "(c.name  LIKE '%" . $data['name'] . "%' or c.name_optional_language  LIKE '%" . $data['name'] . "%' or c.code  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['status'] != '')
        {
            $this->db->where('c.is_completed', $data['status']);
        }
        if ($data['id_election'] != '')
        {
            $this->db->where('c.id_election', $data['id_election']);
        }
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $results = $query->result();

        $details = array();

        foreach ($results as $result)
        {
            $id_title = $result->id;
            $id_election = $result->id_election;

            $count = 0;

            $participants = $this->getParticipantsCount($id_title,$id_election);
            if($participants)
            {
                $count = $participants;
            }

            $result->participants_count = $count;

            array_push($details, $result);
        }

        return $details;
    }

    function getParticipantsCount($id_title,$id_election)
    {
        $this->db->select('*');
        $this->db->from('participants as c');
        $this->db->where('c.id_title', $id_title);
        $this->db->where('c.id_election', $id_election);
        $query = $this->db->get();
        $result = $query->num_rows();
        // echo "<pre>";print_r($result);die;

        return $result;
    }


    function electionListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('election');
        $this->db->where('status', $status);
        // $this->db->where('date(election_date) <', date('Y-m-d'));
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getTitle($id)
    {
         $this->db->select('c.*');
        $this->db->from('election_title as c');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function getElectionPoolingCandidates($id_title,$id_election)
    {
        $this->db->select('p.*');
        $this->db->from('participants as p');
        $this->db->where('p.id_title', $id_title);
        $this->db->where('p.id_election', $id_election);
        $query = $this->db->get();
        $results = $query->result();  

        $details = array();

        foreach ($results as $result)
        {
            $id_particpant = $result->id;
            $votes = $this->getParticipantsVotingCount($id_title,$id_election,$id_particpant);
            $result->votes = $votes;
            array_push($details, $result);
        }

         //echo "<Pre>"; print_r($result);exit;
         return $details;
    }

    function getParticipantsVotingCount($id_title,$id_election,$id_particpants)
    {
        $this->db->select('*');
        $this->db->from('viewer_vote_pooling as c');
        $this->db->where('c.id_title', $id_title);
        $this->db->where('c.id_election', $id_election);
        $this->db->where('c.is_voted', $id_particpants);
        $query = $this->db->get();
        $result = $query->num_rows();
        // echo "<pre>";print_r($result);die;

        return $result;
    }

    function editTitle($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('election_title', $data);

        return $result;
    }

    function addElectionResults($data)
    {
        $this->db->trans_start();
        $this->db->insert('election_results', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }
}


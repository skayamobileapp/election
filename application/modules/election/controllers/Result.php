<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Result extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('result_model');
        $this->isLoggedIn();
    }

    function pendingList()
    {
        if ($this->checkAccess('result.pending_list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_election'] = $this->security->xss_clean($this->input->post('id_election'));
            $formData['status'] = '0';

            $data['searchParam'] = $formData;

            $data['electionList'] = $this->result_model->electionListByStatus('1');
            $data['titleList'] = $this->result_model->titleListSearch($formData);

            $this->global['pageTitle'] = 'Election Management System : Result Pending List';
            $this->global['pageCode'] = 'result.pending_list';
            $this->loadViews("results/pending_list", $this->global, $data, NULL);
        }
    }


    function summaryList()
    {
        if ($this->checkAccess('result.summary_list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_election'] = $this->security->xss_clean($this->input->post('id_election'));
            $formData['status'] = '';

            $data['searchParam'] = $formData;
            
            $data['electionList'] = $this->result_model->electionListByStatus('1');
            $data['titleList'] = $this->result_model->titleListSearch($formData);

            $this->global['pageTitle'] = 'Election Management System : Result Summary List';
            $this->global['pageCode'] = 'result.summary_list';
            $this->loadViews("results/summary_list", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('result.approve') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/election/result/pendinglist');
            }

            $data['title'] = $this->result_model->getTitle($id);

            if($this->input->post())
            {
                $user_id = $this->session->userId;
            
                // echo "<Pre>";print_r($this->input->post());exit();

                // $name = $this->security->xss_clean($this->input->post('name'));
                // $code = $this->security->xss_clean($this->input->post('code'));
                $is_completed = $this->security->xss_clean($this->input->post('is_completed'));
            
                $title_data = array(
                    'status' => 1,
                    'is_completed' => $is_completed,
                    'updated_by' => $user_id
                );

                $result = $this->result_model->editTitle($title_data,$id);

                if($result)
                {

                    $votes = $this->result_model->getParticipantsVotingCount($data['title']->id,$data['title']->id_election,$is_completed);


                    // echo "<Pre>";print_r($votes);exit();


                    $result_data = array(
                        'id_title'=>$id,
                        'id_election'=>$data['title']->id_election,
                        'id_participant'=> $is_completed,
                        'votes'=>$votes,
                        'status'=>1
                        );

                    $result = $this->result_model->addElectionResults($result_data);
                
                }

                if ($result)
                {
                    $this->session->set_flashdata('success', 'Election Result Approved successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Election Result Approve Failed');
                }

                redirect('/election/result/pendinglist');
            }

            $data['electionList'] = $this->result_model->electionListByStatus('1');

            $data['getElectionPoolingCandidates'] = $this->result_model->getElectionPoolingCandidates($id,$data['title']->id_election);

            // echo "<Pre>";print_r($data['getElectionPoolingCandidates']);exit();
            
            $this->global['pageTitle'] = 'Election Management System : Edit Location';
            $this->global['pageCode'] = 'result.approve';
            $this->loadViews("results/edit", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('result.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/election/result/summarylist');
            }
            if($this->input->post())
            {
                redirect('/election/result/summarylist');
            }

            $data['title'] = $this->result_model->getTitle($id);
            $data['electionList'] = $this->result_model->electionListByStatus('1');
            $data['getElectionPoolingCandidates'] = $this->result_model->getElectionPoolingCandidates($id,$data['title']->id_election);


            // echo "<Pre>";print_r($data);exit();


            $this->global['pageTitle'] = 'Election Management System : View Election Results';
            $this->global['pageCode'] = 'result.view';
            $this->loadViews("results/view", $this->global, $data, NULL);
        }
    }
}

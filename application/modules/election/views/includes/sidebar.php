 <div class="container-fluid">
      <div class="row">
        <nav id="sidebarMenu" class="col-md-3 d-md-block sidebar collapse">
          <div class="sidebar-sticky pt-3">
            <div class="nav-fold">
              <a href="#" class="d-flex align-items-center">
                <span class="user-avatar"><img src="<?php echo BASE_PATH; ?>assets/img/user.jpg"/></span>
                <span>
                  <?php echo $name; ?>
                  <small class="d-block"><?php echo $role; ?></small>
                  <small class="d-block"></small>                  
                </span>
              </a>
            </div>
            <ul class="nav flex-column mb-2">
            
              
              <li class="nav-item">
                <a class="nav-link <?php 
                  if(in_array($pageCode,array('result.pending_list','result.approve','result.summary_list','result.view'))){  ?>
                  <?php 
                  } else{ 
                  ?>
                  collapsed
                  <?php
                  }
                  ?>" data-toggle="collapse" href="#collapseResults" role="button">
                  <i class="fa fa-file-text"></i>
                  <span>Examination Setup</span>
                  <i class="fa fa-angle-right ml-auto" aria-hidden="true"></i>
                  <i class="fa fa-angle-down ml-auto" aria-hidden="true"></i>
                </a>
                <ul class="collapse nav <?php 
                  if(in_array($pageCode,array('result.pending_list','result.approve','result.summary_list','result.view'))){  ?>
                  show
                 <?php 
                  } else{ 
                  ?>
                  
                  <?php
                  }
                  ?>" id="collapseResults">
                   <li class="nav-item">
                    <a href="/election/result/pendingList" class="nav-link <?php if(in_array($pageCode,array('result.pending_list','result.approve'))){echo 'active';}?>">
                      <i class="fa fa-file-text"></i>
                      <span>Approve Results</span>                      
                    </a>
                  </li>  
                  
                  <li class="nav-item">
                    <a href="/election/result/summaryList" class="nav-link <?php if(in_array($pageCode,array('result.summary_list','result.view'))){echo 'active';}?>">
                      <i class="fa fa-file-text"></i>
                      <span>Results Summary</span>                      
                    </a>
                  </li>
                                  

                  <!-- <li class="nav-item">
                    <a href="/setup/question/list" class="nav-link">
                      <i class="fa fa-file-text"></i>
                      <span>Question</span>                      
                    </a>
                  </li> -->
                </ul>
              </li>                
                                     
          </ul>         
        </div>
      </nav>
    </div>
  </div>
<!-- 
<li><a href="/exam/location/list">Exam Locaction Setup</a></li>
                        <li><a href="/exam/examCenter/list">Exam Center Setup</a></li>
                        <li><a href="/exam/examEvent/list">Exam Event</a></li>
                        <li><a href="/exam/examName/list">Exam Name</a></li>
                        <li><a href="/exam/examHasQuestion/list">Exam Has Question</a></li>


 -->
<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Approve Election Results</h1>
        
        <a href='../pendingList' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    
    <form id="form_main" action="" method="post">

        <div class="page-container">

          <div>
            <h4 class="form-title">Election Title details</h4>
          </div>

            <div class="form-container">


                <div class="row">



                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Name <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo $title->name; ?>" readonly>
                        </div>
                      </div>
                    </div>



                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Election <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <select name="id_election" id="id_election" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($electionList))
                            {
                                foreach ($electionList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                <?php
                                if($record->id == $title->id_election)
                                {
                                    echo 'selected';
                                }
                                ?>
                                >
                                <?php echo $record->election_date . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                          </select>
                          </div>
                        </div>
                    </div>

                </div>


                <?php
                if($title->is_completed != 0)
                {
                    ?>

                    <div class="row">

                        <div class="col-lg-6">
                          <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php if($title->is_completed > 0){ echo 'Approved'; }else{ echo 'Pending';}  ?>" readonly>
                            </div>
                          </div>
                        </div>

                    </div>


                 <?php
                }
                ?>


            </div>  





            <h4 class="form-title">Election Voting Details</h4>
                <div class="form-container">
                                        

                                
                <div class="custom-table">
                    <table class="table" width="100%">
                        <thead>
                            <tr>
                            <th>Sl. No</th>
                             <th>Participant Name</th>
                             <th>Description</th>
                             <th>Email</th>
                             <th>Phone</th>
                             <!-- <th>Image</th>
                             <th>Logo</th> -->
                             <th class="text-center">Votes</th>
                             <th class="text-center">Result</th>
                            </tr>
                        </thead>
                        <tbody>
                             <?php
                         $total = 0;
                          for($i=0;$i<count($getElectionPoolingCandidates);$i++)
                         { ?>
                            <tr>
                            <td><?php echo $i+1;?></td>
                            <td><?php echo $getElectionPoolingCandidates[$i]->name;?></td>
                            <td>
                            <a href="#" data-toggle="modal" title="<?php echo $getElectionPoolingCandidates[$i]->description ?>">Click here view description</a>
                            </td>
                            <td><?php echo $getElectionPoolingCandidates[$i]->email;
                              ?>
                            </td>
                            <td><?php echo $getElectionPoolingCandidates[$i]->phone;
                              ?>
                            <!-- </td>
                            <td class="text-center">
                                <img src="<?php echo '/assets/images/' . $getElectionPoolingCandidates[$i]->image; ?>" class="img-responsive" />
                            </td>
                            <td class="text-center">
                                <img src="<?php echo '/assets/images/' . $getElectionPoolingCandidates[$i]->logo; ?>" class="img-responsive" />
                            </td> -->
                            <td>
                                <h5><?php echo $getElectionPoolingCandidates[$i]->votes; ?></h5>
                            </td>
                            <td>
                              <div class="radio vote-radio">
                                  <label class="radio">
                                      <input type="radio" name="is_completed" id="is_completed" value="<?php echo $getElectionPoolingCandidates[$i]->id; ?>">
                                      <span class="check-radio"> Winner</span>
                                  </label>
                              </div>
                            </td>
                            </tr>
                          <?php 
                      } 
                      ?>
                        </tbody>
                    </table>
                  </div>

                </div>


                <?php
                if($title->is_completed == 0)
                {
                 ?>
                    <div class="button-block clearfix">
                      <div class="bttn-group">
                          <button type="submit" class="btn btn-primary">Save</button>
                          <!-- <a href='add' class="btn btn-link">Clear All Fields</a> -->
                      </div>

                    </div>

                <?php
                }
                ?>




        </div>
    </form>
</main>

<script>
    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                status: {
                    required: true
                },
                is_completed: {
                    required: true
                }
            },
            messages: {
                status: {
                    required: "<p class='error-text'>Status Required</p>",
                },
                is_completed: {
                    required: "<p class='error-text'>Select Winner</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    function reloadPage()
    {
      window.location.reload();
    }
</script>
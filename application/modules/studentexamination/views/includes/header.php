<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="description" content="" />
  <title><?php echo $pageTitle; ?></title>

  <!-- Bootstrap core CSS -->
  <link href="<?php echo BASE_PATH; ?>assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?php echo BASE_PATH; ?>assets/css/font-awesome.min.css" rel="stylesheet" />
  <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">
  <!-- Custom styles for this template -->
  <link href="<?php echo BASE_PATH; ?>assets/css/studentmain.css" rel="stylesheet" />
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-light main-header">
    <div class="container">
      <a class="navbar-brand" href="#">LOGO</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExample02">
        <ul class="navbar-nav ml-auto align-items-center">
          <li class="nav-item">
            Welcome <?php echo $userInfo['student_name'];?>
          </li>
          <li class="nav-item active">
            <a href="#" class="nav-link">Logout</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
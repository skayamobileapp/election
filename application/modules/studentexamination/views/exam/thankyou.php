
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <title>Quiz</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/main.css" rel="stylesheet" />
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-light main-header">
      <div class="container">
        <a class="navbar-brand" href="#">LOGO</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarsExample02">
          <ul class="navbar-nav ml-auto align-items-center">
            <li class="nav-item">
              Welcome <?php echo $this->session->userdata['student_name'];?>
            </li>            
            <li class="nav-item active">
              <a href="#" class="nav-link">Logout</a>
            </li>          
          </ul>          
        </div>
      </div>
    </nav> 

    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-8 text-center congrats-block">
          <br/>          <br/>
          <br/>
          <br/>
          <br/>
          <br/>

           <h3> Dear <?php echo $userInfo['student_name'];?></h3>
          <?php if($result_status=='1') { ?>
          <h3>Congrats <?php echo $userInfo['student_name'];?></h3>
          <h3>You have <?php echo $result;?></h3>

          <?php   } else { ?>
          <h3>Sorry You have <?php echo $result;?></h3>

           <?php  } ?>
           <h3>Your Score is : <?php echo $correctAnswer." / ".$totalQuestionInExam;?></h3>


         <?php if($exam->attempts=='4') {  ?>
           <h3>You can attempt multiple time within your time range, 
            <a href="/studentexamination/exam/instructions">Click to answer again</a>
          <?php } ?>
        </div>
      </div>
    </div>
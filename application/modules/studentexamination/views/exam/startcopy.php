<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <title>Quiz</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/main.css" rel="stylesheet" />
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-light main-header">
      <div class="container">
        <a class="navbar-brand" href="#">LOGO</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarsExample02">
          <ul class="navbar-nav header-stats align-items-center">
            <li class="nav-item active">
              <img src="<?php echo BASE_PATH; ?>assets/onlineportal/img/total_questions_icon.svg" />Total Questions <span><?php echo count($question)?></span>
            </li>
            <li class="nav-item">
              <img src="<?php echo BASE_PATH; ?>assets/onlineportal/img/total_time_icon.svg" />Total Time(min) <span><?php echo $exam->duration;?></span>
            </li>

            <?php if($extraTime) { ?>
            <li class="nav-item">
              <img src="<?php echo BASE_PATH; ?>assets/onlineportal/img/total_time_icon.svg" />Extra Time(min) <span><?php echo $extraTime;?></span>
            </li>
          <?php } ?> 

            <li class="nav-item">
              <img src="<?php echo BASE_PATH; ?>assets/onlineportal/img/total_remainingtime_icon.svg" />Remaining Time  
<span  id="countdown"></span>
            </li>            
          </ul>
          <ul class="navbar-nav ml-auto align-items-center">
            <li class="nav-item">
              Welcome <?php echo $this->session->userdata['student_name'];?>
            </li>            
            <li class="nav-item active">
              <a href="#" class="nav-link">Logout</a>
            </li>          
          </ul>          
        </div>
      </div>
    </nav> 
    
    <div class="quiz-wrapper">
      <div class="container">
        <div class="row">
          <?php $this->load->model('student/exam_student_tagging_model');?>
          <?php for ($i=0;$i<count($question);$i++) {?>
          <div class="col-md-8 col-lg-9" id="questionDiv<?php echo $question[$i]['id']?>">
            <span><?php echo $i+1;?></span><h4 class="pt-3"><?php echo $question[$i]['question'];?></h4> 

            <?php 
                        $answerList = $this->exam_student_tagging_model->getAnswersByQuestionId($question[$i]['id']);

                         $answeredData = $this->exam_student_tagging_model->checkifAnswered($question[$i]['id'],$this->session->id_student_exam_attempt);

            for ($a=0;$a<count($answerList);$a++) { ?>
            <div class="row mt-2" >
              <div class="col-md-6">
                <div class="answer-radio">
                  <div class="custom-control custom-radio">
                    <input type="radio" id="answer<?php echo $answerList[$a]->id;?>" name="quesion<?php echo $question[$i][id];?>" class="custom-control-input" 
                       <?php if($answeredData->id_answer==$answerList[$a]->id) { echo "checked=checked";} ?> onclick="updateAnswer(<?php echo $answerList[$a]->id;?>,<?php echo $question[$i]['examstudentid'];?>)">
                    <label class="custom-control-label" for="answer<?php echo $answerList[$a]->id;?>"><?php echo $answerList[$a]->option_description;?></label>
                  </div>
                </div>
              </div>
              <?php $a++;?>
              <div class="col-md-6">
                <div class="answer-radio">
                  <div class="custom-control custom-radio">
                    <input type="radio" id="answer<?php echo $answerList[$a]->id;?>" name="quesion<?php echo $question[$i][id];?>" class="custom-control-input" 
                       <?php if($answeredData->id_answer==$answerList[$a]->id) { echo "checked=checked";} ?> onclick="updateAnswer(<?php echo $answerList[$a]->id;?>,<?php echo $question[$i]['examstudentid'];?>)">
                    <label class="custom-control-label" for="answer<?php echo $answerList[$a]->id;?>"><?php echo $answerList[$a]->option_description;?></label>
                  </div>
                </div>
              </div>
                                       
            </div>  

          <?php } ?>
            <hr />   
            <?php 
            if(count($question)==$i+1) { ?>
            <div class="d-flex">
              <button type="submit" class="btn btn-primary btn-lg" onclick="confirmsubmit()">Next</button>
            </div>    
           <?php } ?>
                     </div>

        <?php } ?> 
          <div class="col-md-4 col-lg-3">
           <div id="attachQuestionNumber"></div>
          </div>          
        </div>
      </div>
    </div>

    <script src="<?php echo BASE_PATH; ?>assets/onlineportal/js/jquery-1.12.4.min.js"></script>

    <script src="http://www.espacescomprises.com/wp-content/themes/espacescomprises/js/scrollTo.js"></script>

    

  </body>
</html>

<script>

  $( document ).ready(function() {

  getAllQuestionNumber();
  timer();


});


  function scrolldiv(id) {

    $('html, body').animate({scrollTop:$('#questionDiv'+id).position().top}, 'slow');


  }


   var seconds, upgradeTime;
   var remainingminute = parseInt(<?php echo $remainingTime + $extraTime;?>);
   console.log(remainingminute);
    seconds = upgradeTime = parseInt(remainingminute)*(60);

  function timer() {
    var days = Math.floor(seconds / 24 / 60 / 60);
    var hoursLeft = Math.floor((seconds) - (days * 86400));
    var hours = Math.floor(hoursLeft / 3600);
    var minutesLeft = Math.floor((hoursLeft) - (hours * 3600));
    var minutes = Math.floor(minutesLeft / 60);
    var remainingSeconds = seconds % 60;
    // add a 0 in front of single digit seconds
    if (remainingSeconds < 10) {
        remainingSeconds = "0" + remainingSeconds;
    }
    // add a 0 in front of single digit minutes
    if (minutes < 10) {
        minutes = "0" + minutes;
    }

    document.getElementById('countdown').innerHTML =  hours + ":" +  minutes + ":" + remainingSeconds;

    if (seconds === 0) {
        clearInterval(countdownTimer);
        document.getElementById('countdown').innerHTML = "Completed";
        //    reload page
    } else {
        seconds--;
    }
}
var countdownTimer = setInterval(timer, 1000);



  function getAllQuestionNumber() {
    $.get("/studentexamination/exam/getAllQuestionNumber", function(data, status){
           
                $("#attachQuestionNumber").html(data);
            });
  }

  function updateAnswer(answerid,examquestionid){
    $.get("/studentexamination/exam/markanswer/"+answerid+"/"+examquestionid, function(data, status){
           
                getAllQuestionNumber();
            });
  }

  function confirmsubmit() {
    var cnf = confirm("Do you really want to submit the answers");
    if(cnf==true) {
       alert("Submitted successfully");
        $.get("/studentexamination/exam/submitmarks/", function(data, status){
           
                  parent.location='/studentexamination/exam/thankyou';

            });

    } else {

    }
  }
</script>
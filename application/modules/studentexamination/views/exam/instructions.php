<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <title>Quiz</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/main.css" rel="stylesheet" />
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-light main-header">
      <div class="container">
        <a class="navbar-brand" href="#">LOGO</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarsExample02">
          <ul class="navbar-nav ml-auto align-items-center">
            <li class="nav-item">
              Welcome <?php echo $this->session->userdata['student_name'];?>
            </li>            
            <li class="nav-item active">
              <a href="#" class="nav-link">Logout</a>
            </li>          
          </ul>          
        </div>
      </div>
    </nav> 

    <div class="container">
    <div class="row align-items-center">
      <div class="col-md-4 mt-5">
        <img src="<?php echo BASE_PATH; ?>assets/img/instructions.svg" alt="" class="img-responsive">
      </div>
      <div class="col-md-8 mt-5">
        <h4>Instructions</h4>
        <ul class="instructions-list">
        <?php
            echo $instruction;
          ?>
          </ul>
        <a href="#" id='startButtonDisabled' style="pointer-events: none;">
          <button type="button" class="btn btn-primary btn-lg mt-2" style="background: #dddddd;" >Click to Start</button>
        </a>

         <a href="start" id='startButtonenabled'>
          <button type="button" class="btn btn-primary btn-lg mt-2" onclick='redirectexam()'>Click to Start</button>
        </a>
      </div>
    </div>
  </div>

 <script src="<?php echo BASE_PATH; ?>assets/onlineportal/js/jquery-1.12.4.min.js"></script>


  <script>

    $( document ).ready(function() {

  checkExamStarted();
});


  function checkExamStarted() {
    $("#startButtonDisabled").hide();
    $("#startButtonenabled").hide();
    $.get("/studentexamination/exam/checkExamStartedOrNot", function(data, status){
         if(data=='1') {
            $("#startButtonenabled").show();
         }
         if(data=='0') {
            $("#startButtonDisabled").show();
         }
    });
  }

  window.setInterval(function(){
  checkExamStarted();
}, 5000);


 function redirectexam() {
  parent.location='studentexamination/exam/start';
 }


  </script>
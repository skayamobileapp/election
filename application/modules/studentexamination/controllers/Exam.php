<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Exam extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('student/exam_student_tagging_model');
        $this->load->model('student/exam_set_model');
        $this->isStudentLoggedIn();
        error_reporting(0);
    }

    function instructions($id=NULL)
    {
        
        echo date('Y-m-d H:i:s');
        $user_id = $this->session->id_student;
        $examDetails = $this->exam_student_tagging_model->getExamDetails($user_id);


        $attemptnumber = 1;

            $user_id = $this->session->id_student;
            $examset_code = $this->session->examset_code;
            $exam = $this->exam_student_tagging_model->getStudentExam($user_id);

            $examDetails = $this->exam_student_tagging_model->getExamDetails($user_id);



            $examEvent = $examDetails->id_exam_event;
            //check the exam started or not for that exam event
            

            $studentExamAttemptData = $this->exam_set_model->studentExamAttempt($exam->examstudenttagging,$user_id);



             $this->session->id_student_exam_attempt = $studentExamAttemptData->id;

           if($studentExamAttemptData->exam_submitted==1) {
           
               if($exam->attempts==1) {
                 echo "<script>alert('You have already completed the exam')</script>";
                 echo "<script>parent.location='/studentLogin'</script>";
                 exit;
               }
           }



        $examEvent = $examDetails->id_exam_event;
            //check the exam started or not for that exam event
        $examCenterDetails = $this->exam_student_tagging_model->checkExamStarted($examEvent);
        $data['instruction'] = $examDetails->instructions;
        $data['error'] = $id;

            $this->loadExamViews("exam/instructions", $this->global, $data, NULL);
        
    }

    function checkExamStartedOrNot(){

        $examStarted = 0; 
        $user_id = $this->session->id_student;
        $examDetails = $this->exam_student_tagging_model->getExamDetails($user_id);

        $examEvent = $examDetails->id_exam_event;
            //check the exam started or not for that exam event
        $examCenterDetails = $this->exam_student_tagging_model->checkExamStarted($examEvent);

        if($examCenterDetails && $examDetails->attendence_status=='1') {
            $examStarted = 1;
        }

        echo $examStarted;
        exit;
    }

    function start()
    {
        
            $attemptnumber = 1;

            $user_id = $this->session->id_student;
            $examset_code = $this->session->examset_code;
            $exam = $this->exam_student_tagging_model->getStudentExam($user_id);

            $examDetails = $this->exam_student_tagging_model->getExamDetails($user_id);



            $examEvent = $examDetails->id_exam_event;
            //check the exam started or not for that exam event
            $examStarted = 0;
            $examCenterDetails = $this->exam_student_tagging_model->checkExamStarted($examEvent);

            if($examCenterDetails) {
                $examStarted = 1;
            }
            if($examStarted==0) {
                redirect('/studentexamination/exam/instructions/1');

            }





            $idtos = $exam->id_tos;
            $time1 = strtotime($exam->exam_date.' '.$exam->to_tm);  
            $time2 = strtotime(date('Y-m-d H:i:s'));
            $completedTimeValid =  ($time2 - $time1);
          

             if($completedTimeValid>0) {
                echo "<script>alert('The examination time is over.')</script>";
                echo "<script>parent.location='/studentLogin'</script>";
                exit;
             } else {

                 $remainingTime = abs($completedTimeValid)/60 + 1;
                
             }
           

            ///check the question assigned to student or not
            $studentExamAttemptData = $this->exam_set_model->studentExamAttempt($exam->examstudenttagging,$user_id);


             $this->session->id_student_exam_attempt = $studentExamAttemptData->id;

           if($studentExamAttemptData->exam_submitted==1) {
           
               if($exam->attempts==1) {
                redirect('/studentLogin');
               }

              

               $nextattemptnumber = $studentExamAttemptData->attempt_number + 1;
               $this->creationofQuestion($nextattemptnumber);  
               
               // create new set of questions
               $attemptnumber = $nextattemptnumber;

           }


            $checkQustion = $this->exam_set_model->checkQuestionAssigned($studentExamAttemptData->id);   



           if(count($checkQustion)>0) {
             $question = $this->exam_set_model->getAssignedQuestions($studentExamAttemptData->id);


           } else {
             $this->creationofQuestion($attemptnumber);        
             $question = $this->exam_set_model->getAssignedQuestions($studentExamAttemptData->id);

           } 



            // save question set to student
            $data['completedTime'] = $completedTime;
            $data['remainingTime'] = $remainingTime;
            $data['extraTime'] = $exam->extra_time;
            $data['exam'] = $exam;
            $data['question'] = $question;
            $data['student_id'] = $user_id;
            $data['idexamtagging'] = $exam->examstudenttagging;
            $data['question'] = $question;

            $this->global['pageTitle'] = 'Student Exam : Start';
            $this->global['userInfo'] = $this->session->get_userdata();
            $this->loadExamViews("exam/start", $this->global, $data, NULL);
        
    }

    function creationofQuestion($attemptnumber){
          $user_id = $this->session->id_student;
            $examset_code = $this->session->examset_code;

            $exam = $this->exam_student_tagging_model->getStudentExam($user_id);




            $idtos = $exam->id_tos;



            $examdatastarted = array(
                    'id_student' => $user_id,
                    'id_exam_student_tagging' => $exam->examstudenttagging,
                    'start_time' =>date('Y-m-d H:i:s'),
                    'end_time' => $exam->examid,
                    'attempt_number' => $attemptnumber
                );
            $id_student_exam_attempt = $this->exam_set_model->studentStartedExam($examdatastarted);




        if($exam->behaviour=='2' && $attemptnumber>1) {
            $oldattemptNumber = $attemptnumber-1;
            $idoldStudentExamAttempt = $this->exam_set_model->getOldExamAttempt($exam->examid,$oldattemptNumber);


            $getDataFromOld = $this->exam_set_model->getOldQuestionByAttemptId($idoldStudentExamAttempt->id);
            shuffle($getDataFromOld);
            for($q=0;$q<count($getDataFromOld);$q++) {

                     $examdata = array(
                    'id_student' => $user_id,
                    'id_question' => $getDataFromOld[$q]['id_question'],
                    'question_order' => $q+1,
                    'id_exam_student_tagging' => $exam->examstudenttagging,
                    'id_answer' => 0,
                    'id_student_exam_attempt'=>$id_student_exam_attempt
                );

                $current_exam = $this->exam_set_model->saveUserExam($examdata);
            }




        } else {

        $tosdetails = $this->exam_set_model->getQuestionByTos($idtos);
            $question = array();
            for($i=0;$i<count($tosdetails);$i++) {
                  $questionList = $this->exam_set_model->getQuestionFromLogic($tosdetails[$i]);
                  for($l=0;$l<count($questionList);$l++) {
                        array_push($question, $questionList[$l]);
                  }
            }

            for($q=0;$q<count($question);$q++) {

                     $examdata = array(
                    'id_student' => $user_id,
                    'id_question' => $question[$q]['id'],
                    'question_order' => $q+1,
                    'id_exam_student_tagging' => $exam->examstudenttagging,
                    'id_answer' => 0,
                    'id_student_exam_attempt'=>$id_student_exam_attempt
                );

                $current_exam = $this->exam_set_model->saveUserExam($examdata);
            }


        }





            

             
            $this->session->student_exam_time =  $data['student_exam_time'];

    }

    function markanswer($answerid = NULL,$examid=NULL) {
       $data['id_answer'] = $answerid;
       $data['datetime'] = date('Y-m-d H:i:s');
       $tosdetails = $this->exam_set_model->updateAnswerOption($data,$examid);
       return 1;     
    }

    function getAllQuestionNumber(){

         $user_id = $this->session->id_student;
            $examset_code = $this->session->examset_code;
            $question = $this->exam_set_model->getAssignedQuestions($this->session->id_student_exam_attempt);

        $table = "<div class='questions-list-container'>
              <h5>Questions <button class='btn btn-primary btn-sm close-btn' onclick='togglemenu()'>Close</button></h5>
              <ul class='questions-list'>";
              for($i=0;$i<count($question);$i++) {
                $j = $i+1;
                $idquestion = $question[$i]['id'];
                  $class ='';
                  if($question[$i]['id_answer']!=0) {
                      $class ='answered';
                  }
                $table.="<li class='$class' onclick='scrolldiv($idquestion)'>$j</li>";
              }

                
               
              $table.="</ul>
               <ul class='question-list-help'>
                <li class='current'>Current</li>
                <li>Not Attempted</li>
                <li class='answered'>Answered</li>
                <li class='not-answered'>Not Answered</li>
              </ul>
            </div>";
            echo $table;
            exit;
    }


    function getAllQuestionNumberOldWrking() {
      $table = " <div class='questions-list-container'>
              <h5>Questions <button class='btn btn-primary btn-sm close-btn' onclick='togglemenu()'>Close</button></h5>
              <ul class='questions-list'>
                <li class='answered'>1</li>
                <li class='answered'>2</li>
                <li class='not-answered'>3</li>
                <li class='answered'>4</li>
                <li class='not-answered'>5</li>
                <li class='current'>6</li>
                <li>7</li>
                <li>8</li>
                <li>9</li>
                <li>10</li>
                <li>11</li>
                <li>12</li>   
                <li>13</li>
                <li>14</li>
                <li>15</li>
                <li>16</li>
                <li>17</li>
                <li>18</li> 
                <li>19</li>
                <li>20</li>
                <li>21</li>
                <li>22</li>
                <li>23</li>
                <li>24</li>   
                <li>25</li>
                <li>26</li>
                <li>27</li>
                <li>28</li>
                <li>29</li>
                <li>30</li>  
                <li>31</li>
                <li>32</li>
                <li>33</li>   
                <li>34</li>
                <li>35</li>
                <li>36</li>
                <li>37</li>
                <li>38</li>
                <li>39</li> 
                <li>40</li>
                <li>41</li>
                <li>42</li>
                <li>43</li>
                <li>44</li>
                <li>45</li>   
                <li>46</li>
                <li>47</li>
                <li>48</li>
                <li>49</li>
                <li>50</li>                                                                               
              </ul>
              <ul class='question-list-help'>
                <li class='current'>Current</li>
                <li>Not Attempted</li>
                <li class='answered'>Answered</li>
                <li class='not-answered'>Not Answered</li>
              </ul>
            </div>";
            echo $table;
            exit;  
    }


    function submitmarks() {

                 $user_id = $this->session->id_student;
            $examset_code = $this->session->examset_code;


            $exam = $this->exam_student_tagging_model->getStudentExam($user_id);
  $examdatastarted = array(
                    'exam_submitted' => 1,
                    'submit_type' => 1,
                    'submitted_time' =>date('Y-m-d H:i:s')
                );
            $this->exam_set_model->studentExamClosed($examdatastarted,$this->session->student_exam_time);

         echo "1";
         return;


    }

    function submitmarksauto() {

                 $user_id = $this->session->id_student;
            $examset_code = $this->session->examset_code;


            $exam = $this->exam_student_tagging_model->getStudentExam($user_id);
  $examdatastarted = array(
                    'exam_submitted' => 1,
                    'submit_type' => 2,
                    'submitted_time' =>date('Y-m-d H:i:s')
                );
            $this->exam_set_model->studentExamClosed($examdatastarted,$this->session->student_exam_time);

         echo "1";
         return;


    }


    function pendingquestion() {
        $user_id = $this->session->id_student;
            $examset_code = $this->session->examset_code;
            $exam = $this->exam_student_tagging_model->getStudentExam($user_id);
        $totalQuestionInExam = $exam->question_count;
        $id_student_exam_attempt = $this->session->id_student_exam_attempt;
        $answeredQuestion = $this->exam_set_model->getAnsweredByStudent($id_student_exam_attempt);
    if($totalQuestionInExam==count($answeredQuestion)) {
        $answeredFull = 0;
    } else {
        $answeredFull = $totalQuestionInExam - count($answeredQuestion);
    }

     echo $answeredFull;
     return;


    }
    function thankyou()
    {

          $user_id = $this->session->id_student;
            $examset_code = $this->session->examset_code;
            $exam = $this->exam_student_tagging_model->getStudentExam($user_id);
        $totalQuestionInExam = $exam->question_count;
        $id_student_exam_attempt = $this->session->id_student_exam_attempt;
        $correctAnswerList = $this->exam_set_model->getCorrectAnswer($id_student_exam_attempt);
        $percentage = (count($correctAnswerList))/$totalQuestionInExam * 100;
        $data['correctAnswer'] = count($correctAnswerList);
        $data['percentage'] = $percentage;
        $data['totalQuestionInExam'] = $totalQuestionInExam;
        $data['exam'] = $exam;

        if($percentage>50) {
            $data['result'] = 'Pass';
            $data['result_status'] = '1';
        } else {
            $data['result'] = 'Fail';
            $data['result_status'] = '0';            
        }

        $this->global['pageTitle'] = 'Student Exam : Thank You';
        $this->global['userInfo'] = $this->session->get_userdata();
        $data['userInfo'] = $this->session->get_userdata();
            $this->loadExamViews("exam/thankyou", $this->global, $data, NULL);
    }
    
}

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="description" content="" />
  <title><?php echo $pageTitle; ?></title>
  <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

  <!-- Bootstrap core CSS -->
  <link href="<?php echo BASE_PATH; ?>assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?php echo BASE_PATH; ?>assets/css/font-awesome.min.css" rel="stylesheet" />
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap" rel="stylesheet" />
  <!-- Custom styles for this template -->
  <link href="<?php echo BASE_PATH; ?>assets/css/main.css" rel="stylesheet" />
  
</head>

<body>
  <nav class="main-header navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
    <a class="navbar-brand col-md-3 mr-0 px-3" href="/setup/user/list">LOGO</a>
    <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-toggle="collapse" data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <ul class="nav px-3 justify-content-end">
      <li class="nav-item">
        <a class="nav-link" href="/setup/role/list">
          Setup
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="/election/result/pendingList">
          Election
        </a>
      </li>

      <!-- <li class="nav-item">
        <a class="nav-link" href="/exam/examName/list">
          Exam
        </a>
      </li>
      <li class="nav-item">
          <a class="nav-link" href="/student/studentRegistration/list">
            Student
          </a>
      </li>
      <li class="nav-item">
          <a class="nav-link" href="/finance/currency/list">
            Finance
          </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/records/student/list">
          Records
        </a>
      </li> -->

      <li class="nav-item">
        <a class="nav-link active" href="/communication/template/list">
          Communication
        </a>
      </li>
      
      <li class="nav-item">
          <a href='/exam/user/logout' class="nav-link" href="#">
            <i class="fa fa-sign-out" aria-hidden="true"></i> Logout
          </a>
        </li>
    </ul>
  </nav>
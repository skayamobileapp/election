<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Add Recepients To Group</h1>

    <a href='../recepientList' class="btn btn-link ml-auto">
      <i class="fa fa-chevron-left" aria-hidden="true"></i>
      Back
    </a>

  </div>

  <form id="form_main" action="" method="post" enctype="multipart/form-data">

    <div class="page-container">

      <div>
        <h4 class="form-title">Group details</h4>
      </div>

      <div class="form-container">


        <div class="row">

          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Name <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="name" name="name" value="<?php echo $group->name; ?>" readonly>
              </div>
            </div>
          </div>


          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Type <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="type" name="type"  value="<?php echo $group->type; ?>" readonly>
              </div>
            </div>
          </div>


        </div>

        <div class="row">


          <div class="col-lg-6">
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Template <span class="text-danger">*</span></label>
                <div class="col-sm-8">
                  <select name="id_template" id="id_template" class="form-control" disabled>
                    <option value="">Select</option>
                    <?php
                    if (!empty($templateList)) {
                      foreach ($templateList as $record) { ?>
                        <option value="<?php echo $record->id;  ?>"
                          <?php if ($group->id_template == $record->id)
                          {
                            echo "selected";
                          } ?>>
                          <?php echo $record->name;  ?>
                        </option>
                    <?php
                      }
                    }
                    ?>
                  </select>
                </div>
              </div>
            </div>

        


          <div class="col-lg-6">
            <div class="form-group row align-items-center">
              <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1" <?php if($group->status == 1){
                      echo "checked=checked";
                  } ?> >
                  <label class="custom-control-label" for="customRadioInline1">Active</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0" <?php if($group->status == 0){
                      echo "checked=checked";
                  } ?> >
                  <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                </div>
              </div>
            </div>
          </div>

        </div>



        <div class="button-block clearfix">
          <div class="bttn-group">
            <button type="submit" class="btn btn-primary">Save</button>
          </div>

        </div>

      </div>
    </div>


    <h4 class="form-title">Add Recepients</h4>
    <div class="form-container">
                      
            <div class="clearfix">
                <ul class="nav nav-tabs" role="tablist" >
                    <li role="presentation" class="nav-item" ><a href="#invoice" class="nav-link active"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Search</a>
                    </li>
                    <li role="presentation" class="nav-item"><a href="#receipt" class="nav-link"
                            aria-controls="receipt" role="tab" data-toggle="tab">Recepient List</a>
                    </li>
                    
                </ul>

                
                <div class="tab-content">

                <div role="tabpanel" class="tab-pane active" id="invoice">



                        <h4 class="form-title"><?php echo $group->type; ?> Search</h4>
                        <div class="form-container">
                            


                        <?php
                        if($group->type == 'Applicant' || $group->type == 'Student')
                        {

                            ?>


                            

                                <div class="row" id="view_student_display">

                                       <!--  <div class="col-sm-3">
                                            <div class="form-group">
                                              <label>Program </label>
                                                <select name="id_programme" id="id_programme" class="form-control">
                                                    <option value="">Select</option>
                                                    <?php
                                                    if (!empty($programmeList))
                                                    {
                                                        foreach ($programmeList as $record)
                                                        {?>
                                                     <option value="<?php echo $record->id;  ?>">
                                                        <?php echo $record->code ." - ".$record->name;?>
                                                     </option>
                                                    <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <div class="form-group">
                                              <label>Intake </label>
                                                <select name="id_intake" id="id_intake" class="form-control">
                                                    <option value="">Select</option>
                                                    <?php
                                                    if (!empty($intakeList))
                                                    {
                                                        foreach ($intakeList as $record)
                                                        {?>
                                                     <option value="<?php echo $record->id;  ?>">
                                                        <?php echo $record->year ." - ".$record->name;?>
                                                     </option>
                                                    <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div> -->

                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Name / NRIC </label>
                                                <input type="text" class="form-control" id="student" name="student">
                                            </div>
                                        </div>

                                </div>

                                <?php 
                                }
                                else
                                {

                                ?>



                                <div class="row" id="view_staff_display">


                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Staff Name</label>
                                            <input type="text" class="form-control" id="staff_name" name="staff_name">
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Staff IC No.</label>
                                            <input type="text" class="form-control" id="staff_ic_no" name="staff_ic_no">
                                        </div>
                                    </div>

                                </div> 
                                <?php

                                }
                            ?> 


                            <div>
                                <table border="0px" style="width: 100%">
                                    <tr>
                                        <td style="text-align: right;" colspan="6">
                                            <button type="button" id="btn_add_detail" onclick="showstudentlist()" class="btn btn-primary btn-light btn-lg">Search</button>
                                        </td>
                                    </tr>

                                </table>
                                <br>
                            </div>


                            </div> 


                            <div class="form-container" id="view_visible" style="display: none;">
                                <h4 class="form-group-title">Search Result</h4>

                                <div id="view">
                                </div>

                            </div>



                        </div> 
                    </div>










                    <div role="tabpanel" class="tab-pane" id="receipt">
                        <div class="mt-4">




                    <?php
                    if($group->type == 'Student')
                    {
                    
                    ?>


                           <div>
                                <table border="0px" style="width: 100%">
                                    <tr>
                                        <td style="text-align: right;" colspan="6">
                                            <button type="button" id="btn_add_detail" onclick="sendMail()" class="btn btn-primary btn-light">Send Mail</button>
                                        </td>
                                    </tr>

                                </table>
                                <br>
                            </div>








                            <div class="custom-table" id="printReceipt">
                                <table class="table" id="list-table">
                                    <thead>
                                    <tr>
                                        <th>Sl. no</th>
                                        <th><?php echo $group->type; ?> Name</th>
                                        <th>Email</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if (!empty($groupRecepientsList))
                                    {
                                        $j=1;
                                        foreach ($groupRecepientsList as $record)
                                        {
                                    ?>
                                        <tr>
                                            <td><?php echo $j ?></td>
                                            <td><?php echo $record->nric . " - " . $record->full_name ?></td>
                                            <td><?php echo $record->email_id ?></td>
                                            <td class="">
                                            <a onclick="deleteStaffRecepient(<?php echo $record->id ?>)">Delete</a>
                                            </td>
                                        </tr>
                                    <?php
                                        $j++;
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>



                             <?php 
                                }
                                else
                                {

                                ?>

                                <div>
                                    <table border="0px" style="width: 100%">
                                        <tr>
                                            <td style="text-align: right;" colspan="6">
                                                <button type="button" id="btn_add_detail" onclick="sendMail()" class="btn btn-primary btn-light">Send Mail</button>
                                            </td>
                                        </tr>

                                    </table>
                                    <br>
                                </div>



                                <div class="custom-table" id="printReceipt">
                                <table class="table" id="list-table">
                                    <thead>
                                    <tr>
                                        <th>Sl. no</th>
                                        <th>Staff Name</th>
                                        <th>Email</th>
                                        <th>Staff ID</th>
                                        <th>Gender</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if (!empty($groupRecepientsList))
                                    {
                                        $j=1;
                                        foreach ($groupRecepientsList as $record)
                                        {
                                    ?>
                                        <tr>
                                            <td><?php echo $j ?></td>
                                            <td><?php echo $record->ic_no . " - " . $record->name ?></td>
                                            <td><?php echo $record->email ?></td>
                                            <td><?php echo $record->staff_id ?></td>
                                            <td><?php echo $record->gender ?></td>
                                            <td class="">
                                            <a onclick="deleteStaffRecepient(<?php echo $record->id ?>)">Delete</a>
                                            </td>
                                        </tr>
                                    <?php
                                        $j++;
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>



                            <?php
                            }
                            ?>




                    </div>
                </div>
            </div>
            </div>

            


        </div>  




  </form>
</main>
<!-- <script type="text/javascript" src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script> -->
<script>
  
  $('select').select2();

  function showstudentlist()
    // $("button").click(function()
    {
        var type = $("#type").val();

        if(type == 'Staff')
        {
            getStaffList();
        }
        else
        {
            getStudentListByType();
        }
    }


    function getStaffList()
    {
        var tempPR = {};
        tempPR['type'] = $("#type").val();
        tempPR['staff_name'] = $("#staff_name").val();
        tempPR['staff_ic_no'] = $("#staff_ic_no").val();

        $.ajax(
        {
           url: '/communication/group/getStaffList',
           type: 'POST',
           data:
           {
            data: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
            // alert(result);
             //location.reload();
             $("#view").html(result);
             $("#view_visible").show();
           }
        });
    }



    function getStudentListByType()
    {
        var tempPR = {};
        tempPR['type'] = $("#type").val();
        // tempPR['id_programme'] = $("#id_programme").val();
        // tempPR['id_intake'] = $("#id_intake").val();
        tempPR['student'] = $("#student").val();

        $.ajax(
        {
           url: '/communication/group/getStudentListByType',
           type: 'POST',
           data:
           {
            data: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
             //location.reload();
             $("#view").html(result);
             $("#view_visible").show();
           }
        });
    }



     function deleteStaffRecepient(id)
    {

        $.get("/communication/group/deleteStaffRecepient/"+id,
            function(data, status)
            {
                window.location.reload();
            });
    }


    function sendMail()
    {
        var tempPR = {};
        tempPR['id_group'] = <?php echo $group->id ?>;
        tempPR['type'] = $("#type").val();
        // alert(tempPR);
        $.ajax(
        {
           url: '/communication/group/sendMail',
           type: 'POST',
           data:
           {
            data: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
             alert(result);
           }
        });
    }





    

    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                name: {
                    required: true
                },
                type: {
                    required: true
                },
                id_template: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                type: {
                    required: "<p class='error-text'>Select Type</p>",
                },
                id_template: {
                    required: "<p class='error-text'>Select Template</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
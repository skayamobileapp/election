<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Edit Group Message</h1>

    <a href='../list' class="btn btn-link ml-auto">
      <i class="fa fa-chevron-left" aria-hidden="true"></i>
      Back
    </a>

  </div>

  <form id="form_main" action="" method="post" enctype="multipart/form-data">

    <div class="page-container">

      <div>
        <h4 class="form-title">Group Message details</h4>
      </div>

      <div class="form-container">


        <div class="row">

          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Name <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="name" name="name" value="<?php echo $group->name; ?>">
              </div>
            </div>
          </div>


          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Type <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="type" name="type"  value="<?php echo $group->type; ?>" readonly>
              </div>
            </div>
          </div>


        </div>

        <div class="row">


          <div class="col-lg-6">
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Template <span class="text-danger">*</span></label>
                <div class="col-sm-8">
                  <select name="id_template" id="id_template" class="form-control">
                    <option value="">Select</option>
                    <?php
                    if (!empty($templateList)) {
                      foreach ($templateList as $record) { ?>
                        <option value="<?php echo $record->id;  ?>"
                          <?php if ($group->id_template == $record->id)
                          {
                            echo "selected";
                          } ?>>
                          <?php echo $record->name;  ?>
                        </option>
                    <?php
                      }
                    }
                    ?>
                  </select>
                </div>
              </div>
            </div>

        


          <div class="col-lg-6">
            <div class="form-group row align-items-center">
              <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1" <?php if($group->status == 1){
                      echo "checked=checked";
                  } ?> >
                  <label class="custom-control-label" for="customRadioInline1">Active</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0" <?php if($group->status == 0){
                      echo "checked=checked";
                  } ?> >
                  <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                </div>
              </div>
            </div>
          </div>

        </div>



        <div class="button-block clearfix">
          <div class="bttn-group">
            <button type="submit" class="btn btn-primary">Save</button>
          </div>

        </div>

      </div>
    </div>
  </form>
</main>
<script type="text/javascript" src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script>
<script>
    $('select').select2();


    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                name: {
                    required: true
                },
                type: {
                    required: true
                },
                id_template: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                type: {
                    required: "<p class='error-text'>Subject Required</p>",
                },
                id_template: {
                    required: "<p class='error-text'>Description Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
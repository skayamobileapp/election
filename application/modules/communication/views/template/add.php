<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Add Template Message</h1>

    <a href='list' class="btn btn-link ml-auto">
      <i class="fa fa-chevron-left" aria-hidden="true"></i>
      Back
    </a>

  </div>

  <form id="form_main" action="" method="post" enctype="multipart/form-data">

    <div class="page-container">

      <div>
        <h4 class="form-title">Template Message details</h4>
      </div>

      <div class="form-container">


        <div class="row">

          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Name <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="name" name="name">
              </div>
            </div>
          </div>


          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Subject <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="subject" name="subject" >
              </div>
            </div>
          </div>

        </div>

        <div class="row">


          <div class="col-lg-6">
            <div class="form-group row align-items-center">
              <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1" checked="checked">
                  <label class="custom-control-label" for="customRadioInline1">Active</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0">
                  <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                </div>
              </div>
            </div>
          </div>

        </div>
        
        <h4 class="form-title">Table Variables</h4>
        <div class="form-container">
            
             <div class="custom-table">
                <table class="table">
                  <thead>
                      <tr>
                          <th>Text</th>
                          <th>Variables</th>
                          <th>Text</th>
                          <th>Variables</th>
                      </tr>
                  </thead>
                  <tbody>
                    <tr>
                         <td>Student NAme</td>
                         <td>@studentname</td>
                         <td>Program</td>
                         <td>@program</td>
                    </tr>
                    <tr>
                        <td>NRIC/Passport</td>
                        <td>@nric</td>
                        <td>Email</td>
                        <td>@email</td>
                    </tr>
                  </tbody>
                </table>
             </div>
        </div>
                
        <div class="row">

          <div class="col-lg-12">
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Message<span class="text-danger">*</span></label>
              <div class="col-sm-10">
               <textarea name="message" id="message"></textarea>
              </div> 
            </div>
          </div>
          
        </div>



        <div class="button-block clearfix">
          <div class="bttn-group">
            <button type="submit" class="btn btn-primary">Save</button>
            <a href='add' class="btn btn-link">Clear All Fields</a>
          </div>

        </div>

      </div>
    </div>
  </form>
</main>
<script type="text/javascript" src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script>
<script>
    $('select').select2();

  $(document).ready(function() {
    CKEDITOR.replace( 'message' );
    $("#form_main").validate({
      rules: {
        name: {
          required: true
        },
        subject: {
          required: true
        }
      },
      messages: {
        name: {
          required: "<p class='error-text'>Name Required</p>",
        },
        subject: {
          required: "<p class='error-text'>Subject Required</p>",
        }
      },
      errorElement: "span",
      errorPlacement: function(error, element) {
        error.appendTo(element.parent());
      }

    });
  });
</script>
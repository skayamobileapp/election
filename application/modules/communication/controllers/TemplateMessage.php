<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class TemplateMessage extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('template_message_model');
        $this->isLoggedIn();
    }


    function list()
    {
        if ($this->checkAccess('template_message.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $data['searchParam'] = $formData;

            $data['templateList'] = $this->template_message_model->templateListSearch($formData);
            //echo "<Pre>"; print_r($data);exit;
            $this->global['pageTitle'] = 'Election Management System : Communication Message Template List';
            $this->global['pageCode'] = 'template_message.list';

            $this->loadViews("template_message/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('template_message.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $subject = $this->security->xss_clean($this->input->post('subject'));
                $message = $this->security->xss_clean($this->input->post('message'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'name' => $name,
                    'subject' => $subject,
                    'message' => $message,
                    'status' => $status,
                    'created_by' => $user_id
                );
                $result = $this->template_message_model->addNewTemplate($data);
                redirect('/communication/templateMessage/list');
            }

            $this->global['pageTitle'] = 'Election Management System : Add Communication Message Template';
            $this->global['pageCode'] = 'template_message.add';

            $this->loadViews("template_message/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('template_message.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/communication/templateMessage/list');
            }
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;
            
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $subject = $this->security->xss_clean($this->input->post('subject'));
                $message = $this->security->xss_clean($this->input->post('message'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'name' => $name,
                    'subject' => $subject,
                    'message' => $message,
                    'status' => $status,
                    'created_by' => $user_id
                );                
                $result = $this->template_message_model->editTemplate($data,$id);
                redirect('/communication/templateMessage/list');
            }

            $data['template'] = $this->template_message_model->getTemplate($id);
            
            $this->global['pageTitle'] = 'Election Management System : Edit Communication Message Template';
            $this->global['pageCode'] = 'template_message.edit';

            $this->loadViews("template_message/edit", $this->global, $data, NULL);
        }
    }
}

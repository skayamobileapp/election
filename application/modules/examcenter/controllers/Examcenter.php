<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Examcenter extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('examcenter_model');


                $this->examCenterID = $this->session->id_exam_center;

    }

    function instructions($id=NULL)
    {
              $data['asdf'] = "asfsd";

           
            $this->loadViews("exam_center/instructions", $this->global, $data, NULL);
        
    }

    function dashboard(){
      $currentDate = date('Y-m-d');
      $todaysExamDetails = $this->examcenter_model->getAllExamsByExamCenterId($this->examCenterID,$currentDate);

      if($todaysExamDetails) {

      $studentsList = $this->examcenter_model->getAllStudentsByExamEventId($todaysExamDetails->id);

   // print_r($studentsList);exit;
      }
      else {
        $studentsList = array();
      }

                        $data['currentexam'] = $todaysExamDetails;
                  $data['studentList'] = $studentsList;

            $this->loadViews("exam_center/dashboard", $this->global, $data, NULL);

    }

    function updateattendenceforstudent($examtagid,$status) {
       $data['attendence_status'] = $status;
       $tosdetails = $this->examcenter_model->updateExamAttendence($data,$examtagid);
       return 1;  
    }


    function updateextratime($examtagid,$status) {
       $data['extra_time'] = $status;
       $tosdetails = $this->examcenter_model->updateExamAttendence($data,$examtagid);
       return 1;  
    }


    function updateextratimeforall($status) {
      $currentDate = date('Y-m-d');
            $todaysExamDetails = $this->examcenter_model->getAllExamsByExamCenterId($this->examCenterID,$currentDate);
       $checkstartedExam = $this->examcenter_model->checkexamStarted($todaysExamDetails->id);
       $data['extra_time'] = $status;
       $tosdetails = $this->examcenter_model->updateExtratimeForAllStudents($data,$todaysExamDetails->id);
       return 1;  
    }


    function checkexamstart() {
      $currentDate = date('Y-m-d');
      $todaysExamDetails = $this->examcenter_model->getAllExamsByExamCenterId($this->examCenterID,$currentDate);
      $checkstartedExam = $this->examcenter_model->checkexamStarted($todaysExamDetails->id);
      if($checkstartedExam) {
        echo "1";
      } else {
        echo "0";
      }
      exit;

    }


    function examstart() {
      $currentDate = date('Y-m-d');
      $todaysExamDetails = $this->examcenter_model->getAllExamsByExamCenterId($this->examCenterID,$currentDate);
       $data = array(
        'id_exam_event' => $todaysExamDetails->id,
        'exam_start_time' => date('Y-m-d H:i:s'),
        'exam_status' => 1,
        'id_exam_center' =>$this->examCenterID
       );
      $checkstartedExam = $this->examcenter_model->addExamStart($data);

    }

   
}

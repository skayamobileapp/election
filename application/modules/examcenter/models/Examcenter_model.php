<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Examcenter_model extends CI_Model
{
    function getAllExamsByExamCenterId($id,$examdate)
    {
        $this->db->select('e.*,en.name as examname,es.duration');
        $this->db->from('exam_event as e');
        $this->db->join('exam_name as en', 'e.id_exam_name=en.id');
        $this->db->join('examset as es', 'e.id_exam_set=es.id');
        $this->db->where('e.exam_date', $examdate);
        $this->db->where('e.id_exam_center', $id);

         $query = $this->db->get();

         $result = $query->row();  
         return $result;
    }


    function getAllStudentsByExamEventId($id) {
         $this->db->select('s.*,e.id as examstudentaggingid,e.attendence_status,e.extra_time');
        $this->db->from('student as s');
        $this->db->join('exam_student_tagging as e', 'e.id_student=s.id');
        $this->db->where('e.id_exam_event', $id);

         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function updateExamAttendence($data,$examstudentaggingid) {
        $this->db->where('id =', $examstudentaggingid);
        $this->db->update('exam_student_tagging', $data);
        return TRUE;
    }

    function updateExtratimeForAllStudents($data,$id) {
                $this->db->where('id_exam_event =', $id);
        $this->db->update('exam_student_tagging', $data);
        return TRUE;

    }

    function checkexamStarted($id) {
        $this->db->select('e.*');
        $this->db->from('exam_center_start_exam as e');
        $this->db->where('e.id_exam_event', $id);

         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }

    function addExamStart($data) {
         $this->db->trans_start();
        $this->db->insert('exam_center_start_exam', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    
}


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <title>Dashboard</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet" />
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-light main-header">
      <div class="container">
        <a class="navbar-brand" href="#">LOGO</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarsExample02">
          <ul class="navbar-nav admin-nav ml-auto align-items-center">
            <li class="nav-item">
              <a href="#" class="nav-link">Home</a>
            </li> 
            <li class="nav-item">
              <a href="#" class="nav-link active">Dashboard</a>
            </li>   
            <li class="nav-item">
              <a href="#" class="nav-link">Students</a>
            </li>                         
            <li class="nav-item">
              Welcome Kiran
            </li>            
            <li class="nav-item">
              <a href="#" class="nav-link">Logout</a>
            </li>          
          </ul>          
        </div>
      </div>
    </nav> 
    
    <div class="container">
      <div class="pt-4">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item" role="presentation">
            <a class="nav-link active" id="studentlist-tab" data-toggle="tab" href="#studentlist" role="tab" aria-controls="studentlist" aria-selected="true">Student List</a>
          </li>
          <li class="nav-item" role="presentation">
            <a class="nav-link" id="futureexam-tab" data-toggle="tab" href="#futureexam" role="tab" aria-controls="futureexam" aria-selected="false">Future Exam</a>
          </li>
        </ul>
        <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" id="studentlist" role="tabpanel" aria-labelledby="studentlist-tab">
            <table class="table">
              <thead class="thead-light">
                <tr>
                  <th scope="col">Student Name</th>
                  <th scope="col">Email</th>
                  <th scope="col">Phone Number</th>
                  <th scope="col">Attendance</th>
                  <th scope="col">Extra Time</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Kiran Kumar</td>
                  <td>kiran@gmail.com</td>
                  <td>9742043110</td>
                  <td>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="customRadioInline1" name="customRadioInline1" class="custom-control-input">
                      <label class="custom-control-label" for="customRadioInline1">Yes</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="customRadioInline2" name="customRadioInline1" class="custom-control-input">
                      <label class="custom-control-label" for="customRadioInline2">No</label>
                    </div>
                  </td>
                  <td>15 min</td>
                </tr>
                <tr>
                  <td>Kiran Kumar</td>
                  <td>kiran@gmail.com</td>
                  <td>9742043110</td>
                  <td>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="customRadioInline1" name="customRadioInline1" class="custom-control-input">
                      <label class="custom-control-label" for="customRadioInline1">Yes</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="customRadioInline2" name="customRadioInline1" class="custom-control-input">
                      <label class="custom-control-label" for="customRadioInline2">No</label>
                    </div>
                  </td>
                  <td>15 min</td>
                </tr> 
                <tr>
                  <td>Kiran Kumar</td>
                  <td>kiran@gmail.com</td>
                  <td>9742043110</td>
                  <td>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="customRadioInline1" name="customRadioInline1" class="custom-control-input">
                      <label class="custom-control-label" for="customRadioInline1">Yes</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="customRadioInline2" name="customRadioInline1" class="custom-control-input">
                      <label class="custom-control-label" for="customRadioInline2">No</label>
                    </div>
                  </td>
                  <td>15 min</td>
                </tr>
                <tr>
                  <td>Kiran Kumar</td>
                  <td>kiran@gmail.com</td>
                  <td>9742043110</td>
                  <td>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="customRadioInline1" name="customRadioInline1" class="custom-control-input">
                      <label class="custom-control-label" for="customRadioInline1">Yes</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="customRadioInline2" name="customRadioInline1" class="custom-control-input">
                      <label class="custom-control-label" for="customRadioInline2">No</label>
                    </div>
                  </td>
                  <td>15 min</td>
                </tr>                                
              </tbody>
            </table>
          </div>
          <div class="tab-pane fade" id="futureexam" role="tabpanel" aria-labelledby="futureexam-tab">
            <table class="table">
              <thead class="thead-light">
                <tr>
                  <th scope="col">Student Name</th>
                  <th scope="col">Email</th>
                  <th scope="col">Phone Number</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Kiran Kumar</td>
                  <td>kiran@gmail.com</td>
                  <td>9742043110</td>
                </tr>
                <tr>
                  <td>Kiran Kumar</td>
                  <td>kiran@gmail.com</td>
                  <td>9742043110</td>
                </tr> 
                <tr>
                  <td>Kiran Kumar</td>
                  <td>kiran@gmail.com</td>
                  <td>9742043110</td>
                </tr>
                <tr>
                  <td>Kiran Kumar</td>
                  <td>kiran@gmail.com</td>
                  <td>9742043110</td>
                </tr>                                
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
  </body>
</html>

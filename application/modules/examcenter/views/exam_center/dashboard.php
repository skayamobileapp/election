
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <title>Exam</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/main.css" rel="stylesheet" />
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-light main-header">
      <div class="container">
        <a class="navbar-brand" href="#">LOGO</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarsExample02">
          <ul class="navbar-nav admin-nav ml-auto align-items-center">

            <li class="nav-item">
              <a href="/examcenter/examcenter/dashboard" class="nav-link">Dashboard</a>
            </li>   
                                 
            <li class="nav-item">
              Welcome <?php echo $this->session->exam_center_name;?>
            </li>            
            <li class="nav-item">
              <a href="#" class="nav-link">Logout</a>
            </li>          
          </ul>          
        </div>
      </div>
    </nav> 
    
    <?php if($studentList) {?> 
    <div class="container">
      <div class="pt-4">
        <h3 style="text-align: center;color: #00bcd2;"><?php echo $currentexam->examname;?></h3>
        <div class="row admin-stats py-3">
          <div class="col-md-4">
            <h4>Total Students - <span><?php echo count($studentList);?></span></h4>
          </div>
          <div class="col-md-4">
            <h4>Duration of Exam (Min)- <span><?php echo $currentexam->duration;?></span></h4>
          </div>          
        </div>
        <table class="table">
          <thead class="thead-light">
            <tr>
              <th scope="col">Student Name</th>
              <th scope="col">Email</th>
              <th scope="col">Phone Number</th>
              <th scope="col">Attendence</th>
              <th>Add Extra Time</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>

            <?php for($l=0;$l<count($studentList);$l++) { ?>
            <tr>
              <td><?php echo $studentList[$l]->full_name;?></td>
              <td><?php echo $studentList[$l]->email_id;?></td>
              <td><?php echo $studentList[$l]->nric;?></td>
              <td>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="customRadioInline<?php echo $studentList[$l]->examstudentaggingid;?>1" name="customRadioInline<?php echo $studentList[$l]->examstudentaggingid;?>" class="custom-control-input" onclick="updateAttendence(<?php echo $studentList[$l]->examstudentaggingid;?>,'1')" <?php if($studentList[$l]->attendence_status=='1') { echo "checked=checked";}?>>
                      <label class="custom-control-label" for="customRadioInline<?php echo $studentList[$l]->examstudentaggingid;?>1">Yes</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="customRadioInline<?php echo $studentList[$l]->examstudentaggingid;?>2" name="customRadioInline<?php echo $studentList[$l]->examstudentaggingid;?>" class="custom-control-input" onclick="updateAttendence(<?php echo $studentList[$l]->examstudentaggingid;?>,'0')" <?php if($studentList[$l]->attendence_status=='0') { echo "checked=checked";}?>>
                      <label class="custom-control-label" for="customRadioInline<?php echo $studentList[$l]->examstudentaggingid;?>2">No</label>
                    </div>
                  </td>
              <td><input type="text" class="form-control" value="<?php echo $studentList[$l]->extra_time;?>"  id="extratime<?php echo $studentList[$l]->examstudentaggingid;?>"> </td>
              <td><button type="button" class="btn btn-primary" onclick="updateExtratime(<?php echo $studentList[$l]->examstudentaggingid;?>)">update</button>
</td>
            </tr>
          <?php } ?> 
                                       
          </tbody>
        </table>  
        <hr/>
        <div class="d-flex align-items-center mb-3">
          <button type="button" class="btn btn-primary btn-lg" id="examstartButton" style="display: none;" onclick="startExam()">Start</button>
          <h3 id='examstartdisabled' style="display: none;">Exam has been started</h3>
          <div class="ml-auto mr-2">
            <div class="form-inline">
              <label for="inlineFormInputName2" class="mr-2">Extra Time for All students</label>
              <input type="text" class="form-control mr-sm-3" id="extra_time" placeholder="">
            </div>            
          </div>
          <button type="button" class="btn btn-outline-secondary" onclick="updateextratimeforall()">Save</button>
        </div>      
      </div>
    </div>
  <?php } else { ?>
   <div class="container">
     <h3>No Exam has been scheduled for today</h3>
   </div>
  <?php } ?> 

       <script src="<?php echo BASE_PATH; ?>assets/onlineportal/js/jquery-1.12.4.min.js"></script>

  </body>
</html>

<script>

function updateAttendence(examstudentaggingid,status) {
      $.get("/examcenter/examcenter/updateattendenceforstudent/"+examstudentaggingid+"/"+status, function(data, status){
           
            });
}


function updateExtratime(id) {
  var time = $("#extratime"+id).val();
  $.get("/examcenter/examcenter/updateextratime/"+id+"/"+time, function(data, status){
           
            });
}

function updateextratimeforall(id) {
   var time = $("#extra_time").val();
  $.get("/examcenter/examcenter/updateextratimeforall/"+time, function(data, status){
           
            });
}


function startExam() {
   $.get("/examcenter/examcenter/examstart", function(data, status){
          checkExamStarted();
    });
}


function checkExamStarted() {
    $("#examstartdisabled").hide();
    $("#examstartButton").hide();
    $.get("/examcenter/examcenter/checkexamstart", function(data, status){
         if(data=='1') {
            $("#examstartdisabled").show();
         }
         if(data=='0') {
            $("#examstartButton").show();
         }
    });
  }

  window.setInterval(function(){
  checkExamStarted();
}, 5000);


</script>
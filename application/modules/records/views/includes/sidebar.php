 <div class="container-fluid">
      <div class="row">
        <nav id="sidebarMenu" class="col-md-3 d-md-block sidebar collapse">
          <div class="sidebar-sticky pt-3">
            <div class="nav-fold">
              <a href="#" class="d-flex align-items-center">
                <span class="user-avatar"><img src="<?php echo BASE_PATH; ?>assets/img/user.jpg"/></span>
                <span>
                  <?php echo $name; ?>
                  <small class="d-block"><?php echo $role; ?></small>
                  <small class="d-block"></small>                  
                </span>
              </a>
            </div>
            <ul class="nav flex-column mb-2">
             
              <li class="nav-item">
                <a class="nav-link  <?php 
                  if(in_array($pageCode,array('student.list','student.view'))){  ?>
                  <?php 
                  }
                  else
                  { 
                  ?>
                  collapsed
                  <?php
                  }
                  ?>"  data-toggle="collapse" href="#collapseExamApplication" role="button">
                  <i class="fa fa-list-alt"></i>
                  <span>Student Records</span>
                  <i class="fa fa-angle-right ml-auto" aria-hidden="true"></i>
                  <i class="fa fa-angle-down ml-auto" aria-hidden="true"></i>
                </a>

                <ul class="collapse nav <?php 
                  if(in_array($pageCode,array('student.list','student.view'))){  ?>
                  show
                 <?php 
                  } else{ 
                  ?>
                  
                  <?php
                  }
                  ?>" id="collapseExamApplication">
                  <li class="nav-item">
                    <a href="/records/student/list" class="nav-link <?php if(in_array($pageCode,array('student.list','student.view'))){echo 'active';}?>">
                      <i class="fa fa-list-alt"></i>
                      <span>Student Record</span>                      
                    </a>
                  </li>
                  
                </ul>
              </li> 

              <li class="nav-item">
                <a class="nav-link  <?php 
                  if(in_array($pageCode,array('most_wrong_question.list','most_wrong_question.view','most_repeated_question.list','most_repeated_question.view'))){  ?>
                  <?php 
                  }
                  else
                  { 
                  ?>
                  collapsed
                  <?php
                  }
                  ?>"  data-toggle="collapse" href="#collapse_exam_reports" role="button">
                  <i class="fa fa-list-alt"></i>
                  <span>Reports</span>
                  <i class="fa fa-angle-right ml-auto" aria-hidden="true"></i>
                  <i class="fa fa-angle-down ml-auto" aria-hidden="true"></i>
                </a>

                <ul class="collapse nav <?php 
                  if(in_array($pageCode,array('most_wrong_question.list','most_wrong_question.view','most_repeated_question.list','most_repeated_question.view'))){  ?>
                  show
                 <?php 
                  } else{ 
                  ?>
                  
                  <?php
                  }
                  ?>" id="collapse_exam_reports">
                  <li class="nav-item">
                    <a href="/records/reports/mostWrongQuestions" class="nav-link <?php if(in_array($pageCode,array('most_wrong_question.list','most_wrong_question.view'))){echo 'active';}?>">
                      <i class="fa fa-list-alt"></i>
                      <span>Most Wrong Questions</span>                      
                    </a>
                  </li>

                  <li class="nav-item">
                    <a href="/records/reports/mostQuestions" class="nav-link <?php if(in_array($pageCode,array('most_repeated_question.list','most_repeated_question.view'))){echo 'active';}?>">
                      <i class="fa fa-list-alt"></i>
                      <span>Most Repeated Questions</span>                      
                    </a>
                  </li>
                  
                </ul>
              </li>       
                                     
          </ul>         
        </div>
      </nav>
    </div>
  </div>
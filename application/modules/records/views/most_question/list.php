<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Most Repeated Questions</h1>
        
        <!-- <a href='list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a> -->

    </div>
    
    <form id="form_main" action="" method="post">

        <div class="page-container">

          <div>
            <h4 class="form-title">Most Repeated Question Details</h4>
          </div>

            <div class="form-container">


                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Start Date <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control datepicker" id="start_date" name="start_date" autocomplete="off">
                        </div>
                      </div>
                    </div>


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">End Date <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control datepicker" id="end_date" name="end_date" autocomplete="off">
                        </div>
                      </div>
                    </div>


                </div>

                  
                <div class="button-block clearfix">

                  <div class="bttn-group">
                      <button type="submit" class="btn btn-primary">View</button>
                      <a href='mostQuestions' class="btn btn-link">Clear All Fields</a>
                  </div>

                </div> 


               

            </div>  


            <?php
            if(!empty($accountCodeList))
            {
              ?>

             <div class="custom-table">
              <table class="table" id="list-table">
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Question</th>
                    <th>Count</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $i=1;
                    foreach ($accountCodeList as $record) {
                  ?>
                      <tr>
                        <td><?php echo $i ?></td>
                        <td><?php echo $record->question ?></td>
                        <td><?php echo $record->totalquestion ?></td>
                      </tr>
                  <?php
                      $i++;
                  }
                  ?>
                </tbody>
              </table>
            </div>   

            <?php
            }
            ?>


        </div>
    </form>
</main>

<script type="text/javascript">


    $( function() {
      $( ".datepicker" ).datepicker({
          changeYear: true,
          changeMonth: true,
      });
    } );

   
</script>
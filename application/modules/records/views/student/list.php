<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Student Records List</h1>
    <!-- <a href="add" class="btn btn-primary ml-auto">+ Add Fee Structure</a> -->
  </div>
  <div class="page-container">
    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="">
              Advanced Search
              <i class="fa fa-angle-right" aria-hidden="true"></i>
              <i class="fa fa-angle-down" aria-hidden="true"></i>
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">

          <div id="collapseOne" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true" style="">
            <div class="panel-body p-2">

              <div class="row">

                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Student Name</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="full_name" name="full_name" placeholder="Full Name" value="<?php echo $searchParam['full_name'] ?>">
                    </div>
                  </div>
                </div>

                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Email Id</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="email_id" name="email_id" placeholder="abc@xyz.com" value="<?php echo $searchParam['email_id'] ?>">
                    </div>
                  </div>
                </div>

              </div>

              <div class="row">

                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">NRIC</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="nric" name="nric" placeholder="BSHHOA888" value="<?php echo $searchParam['nric'] ?>">
                    </div>
                  </div>
                </div>



                  <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Status </label>
                        <div class="col-sm-8">
                          <select name="status" id="status" class="form-control">
                          <option value="">Select</option>
                          <option value="Draft"
                          <?php
                          if($searchParam['applicant_status'] == 'Draft')
                          {
                            echo 'selected';
                          }
                          ?>
                          >Draft</option>
                          <option value="Approved"
                          <?php
                          if($searchParam['applicant_status'] == 'Approved')
                          {
                            echo 'selected';
                          }
                          ?>
                          >Approved</option>
                          <option value="Rejected"
                          <?php
                          if($searchParam['applicant_status'] == 'Rejected')
                          {
                            echo 'selected';
                          }
                          ?>
                          >Rejected</option>
                        </select>
                        </div>
                      </div>
                  </div>

              </div>

              <hr />
              <div class="d-flex justify-content-center">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="list" class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>

        </form>

      </div>
    </div>
    <?php
    if ($this->session->flashdata('success')) {
    ?>
      <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
      </div>
    <?php
    }
    if ($this->session->flashdata('error')) {
    ?>
      <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Failure!</strong> <?php echo $this->session->flashdata('error'); ?>
      </div>
    <?php
    }
    ?>
    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Student Name</th>
            <th>Email</th>
            <!-- <th>Phone Number</th> -->
            <th>NRIC</th>
            <!-- <th>Applicant Status</th> -->
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($studentList)) {
            $i = 1;
            foreach ($studentList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->first_name ?></td>
                <td><?php echo $record->email_id ?></td>
                <!-- <td><?php echo $record->phone?></td> -->
                <td><?php echo $record->nric ?></td>
                <!-- <td style="text-align: center;"><?php echo $record->applicant_status ?></td> -->
                <td style="text-align: center;">
                  <a href="<?php echo 'edit/' . $record->id; ?>" class="btn" type="" data-toggle="tooltip" data-placement="top" title="View">
                    <i class="fa fa-eye" aria-hidden="true">
                    </i>
                  </a>
                </td>
              </tr>
          <?php
              $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</main>
<script>
  $('select').select2();
</script>
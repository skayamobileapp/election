<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Student Record View</h1>

        <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

        
    </div>





            


<form id="form_applicant" action="" method="post" enctype="multipart/form-data">


            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>


            <div class="clearfix">

                <ul class="nav nav-tabs" role="tablist" >
                    <li role="presentation" class="nav-item" ><a href="#tab_one" class="nav-link active"
                            aria-controls="tab_one" aria-selected="true"
                            role="tab" data-toggle="tab">Profile Details</a>
                    </li>
                    <li role="presentation" class="nav-item"><a href="#tab_two" class="nav-link"
                            aria-controls="tab_two" role="tab" data-toggle="tab">Exam Registation</a>
                    </li>
                    <li role="presentation" class="nav-item"><a href="#tab_three" class="nav-link"
                            aria-controls="tab_three" role="tab" data-toggle="tab">Invoice Details</a>
                    </li>
                    <li role="presentation" class="nav-item"><a href="#tab_four" class="nav-link"
                            aria-controls="tab_four" role="tab" data-toggle="tab">Receipt Details</a>
                    </li>
                    <li role="presentation" class="nav-item"><a href="#tab_five" class="nav-link"
                            aria-controls="tab_five" role="tab" data-toggle="tab">Exam Attempts</a>
                    </li>
                                     
                </ul>


            





            
                
                <div class="tab-content offers-tab-content">



                    <div role="tabpanel" class="tab-pane active" id="tab_one">

                            <div class='data-list'>
                                <div class='row'> 
                                    <div class='col-sm-6'>
                                        <dl>
                                            <dt>Student Name :</dt>
                                            <dd><?php echo ucwords($studentDetails->full_name);?></dd>
                                        </dl>
                                        <dl>
                                            <dt>Student NRIC :</dt>
                                            <dd><?php echo $studentDetails->nric ?></dd>
                                        </dl> 
                                        <dl>
                                            <dt>Gender :</dt>
                                            <dd><?php echo $studentDetails->gender ?></dd>
                                        </dl> 
                                        <dl>
                                            <dt>Address 1 :</dt>
                                            <dd><?php echo $studentDetails->permanent_address1 ?></dd>
                                        </dl>
                                        <dl>
                                            <dt>Zipcode :</dt>
                                            <dd><?php echo $studentDetails->permanent_zipcode ?></dd>
                                        </dl>            
                                    </div>        
                                    
                                    <div class='col-sm-6'>                           
                                        <dl>
                                            <dt>Student Email :</dt>
                                            <dd><?php echo $studentDetails->email_id; ?></dd>
                                        </dl>
                                        <dl>
                                            <dt>Phone :</dt>
                                            <dd><?php echo $studentDetails->phone ?></dd>
                                        </dl> 
                                        <dl>
                                            <dt>City :</dt>
                                            <dd><?php echo $studentDetails->permanent_city ?></dd>
                                        </dl> 
                                        <dl>
                                            <dt>Address 2 :</dt>
                                            <dd><?php echo $studentDetails->permanent_address2 ?></dd>
                                        </dl>  
                                         
                                    </div>
                                </div>
                            </div>                    
                    
                    </div>









                    <div role="tabpanel" class="tab-pane" id="tab_two">
                        



                          <?php

                                if(!empty($examTaggingByIdStudent))
                                {
                                    ?>

                                          <div class="custom-table">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                    <th>Sl. No</th>
                                                    <th>Event Name</th>
                                                    <th>Exam Name</th>
                                                    <th>Event Center Location</th>
                                                    <th>Exam Date</th>
                                                    <th>Exam Registered On</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                  for($i=0;$i<count($examTaggingByIdStudent);$i++)
                                                 { ?>
                                                    <tr>
                                                    <td><?php echo $i+1;?></td>
                                                    <td><?php echo $examTaggingByIdStudent[$i]->exam_event_name; ?></td>
                                                    <td><?php echo $examTaggingByIdStudent[$i]->exam_name; ?></td>
                                                    <td><?php echo $examTaggingByIdStudent[$i]->exam_location; ?></td>
                                                    <td><?php echo date('d-m-Y', strtotime($examTaggingByIdStudent[$i]->exam_date)); ?></td>
                                                    <td><?php echo date('d-m-Y h:i:s A', strtotime($examTaggingByIdStudent[$i]->created_dt_tm)); ?></td>
                                                    </tr>
                                                  <?php
                                              } 
                                              ?>
                                                </tbody>
                                            </table>
                                          </div>


                                <?php
                                
                                }
                                else
                                {
                                    ?>

                                    <h5 class="m-4">No Invoice Found</h5>
                                    <?php

                                }
                                 ?>


                    
                    </div>







                    <div role="tabpanel" class="tab-pane" id="tab_three">



                            <?php

                                if(!empty($invoiceList))
                                {
                                    ?>



                                        

                                          <div class="custom-table">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                    <th>Sl. No</th>
                                                    <th>Invoice Number</th>
                                                    <th>Invoice Date</th>
                                                    <th>Type</th>
                                                    <th>Total Amount</th>
                                                    <th>Paid Amount</th>
                                                    <th>Balance Amount</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                  for($i=0;$i<count($invoiceList);$i++)
                                                 { ?>
                                                    <tr>
                                                    <td><?php echo $i+1;?></td>
                                                    <td><?php echo $invoiceList[$i]->invoice_number; ?></td>
                                                    <!-- <td><?php echo date('d-m-Y H:i:s', strtotime($invoiceList[$i]->date_time)); ?></td> -->
                                                    <td><?php echo date('d-m-Y', strtotime($invoiceList[$i]->date_time)); ?></td>
                                                    <td><?php echo $invoiceList[$i]->type; ?></td>
                                                    <td><?php echo $invoiceList[$i]->total_amount; ?></td>
                                                    <td><?php echo $invoiceList[$i]->paid_amount; ?></td>
                                                    <td><?php echo $invoiceList[$i]->balance_amount; ?></td>
                                                    </tr>
                                                  <?php
                                              } 
                                              ?>
                                                </tbody>
                                            </table>
                                          </div>


                                <?php
                                
                                }
                                else
                                {
                                    ?>

                                    <h4 class="m-4">No Invoice Found</h4>
                                    <?php

                                }
                                 ?>
                            


                    
                    </div>






                    <div role="tabpanel" class="tab-pane" id="tab_four">


                            <?php

                                if(!empty($receiptList))
                                {
                                    ?>



                                        

                                          <div class="custom-table">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                    <th>Sl. No</th>
                                                    <th>Receipt Number</th>
                                                    <th>Receipt Date</th>
                                                    <th>Type</th>
                                                    <th>Paid Amount</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                  for($i=0;$i<count($receiptList);$i++)
                                                 { ?>
                                                    <tr>
                                                    <td><?php echo $i+1;?></td>
                                                    <td><?php echo $receiptList[$i]->receipt_number; ?></td>
                                                    <!-- <td><?php echo date('d-m-Y H:i:s', strtotime($receiptList[$i]->receipt_date)); ?></td> -->
                                                    <td><?php echo date('d-m-Y', strtotime($receiptList[$i]->receipt_date)); ?></td>
                                                    <td><?php echo $receiptList[$i]->type; ?></td>
                                                    <td><?php echo $receiptList[$i]->receipt_amount; ?></td>
                                                    </tr>
                                                  <?php
                                              } 
                                              ?>
                                                </tbody>
                                            </table>
                                          </div>


                                <?php
                                
                                }
                                else
                                {
                                    ?>

                                    <h4 class="m-4">No Receipt Found</h4>
                                    <?php

                                }
                                 ?>

                        


                    
                    </div>











                    <div role="tabpanel" class="tab-pane" id="tab_five">


                          <?php

                            if(!empty($examAttemptsByData))
                            {
                                ?>



                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                <th>Event Name</th>
                                                <th>Exam Name</th>
                                                <th>Event Center Location</th>
                                                <th>Exam Date</th>
                                                <th>Attempt Number</th>
                                                <th>Start Date</th>
                                                <!-- <th>End Date</th> -->
                                                <th>Submitted Time</th>
                                                <th>Result</th>
                                                <th>View</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                              for($i=0;$i<count($examAttemptsByData);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $examAttemptsByData[$i]->exam_event_name; ?></td>
                                                <td><?php echo $examAttemptsByData[$i]->exam_name; ?></td>
                                                <td><?php echo $examAttemptsByData[$i]->exam_location; ?></td>
                                                <td><?php echo date('d-m-Y', strtotime($examAttemptsByData[$i]->exam_date)); ?></td>
                                                <td><?php echo $examAttemptsByData[$i]->attempt_number; ?></td>
                                                <td><?php echo date('d-m-Y H:i:s', strtotime($examAttemptsByData[$i]->start_time)); ?></td>
                                                <!-- <td><?php echo date('d-m-Y', strtotime($examAttemptsByData[$i]->end_time)); ?></td> -->
                                                <td><?php echo date('h:i:s A', strtotime($examAttemptsByData[$i]->submitted_time)); ?></td>
                                                <td><?php echo $examAttemptsByData[$i]->exam_result; ?></td>
                                                <td style="text-align: center;">
                                                    <a href="<?php echo '../viewQuestoins/' . $examAttemptsByData[$i]->id_exam_student_tagging . '/' . $examAttemptsByData[$i]->id; ?>" class="btn" type="" data-toggle="tooltip" data-placement="top" title="View Attempts">
                                                      <i class="fa fa-eye" aria-hidden="true">
                                                      </i>
                                                    </a>
                                                    </td>
                                                </tr>
                                              <?php
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>



                            <?php
                            
                            }
                            else
                            {
                                ?>

                                <h4 class="m-4">No Attempts Found</h4>
                                <?php

                            }
                             ?>
                        


                    
                    </div>




                











                </div>






                


        </div>


    </form>





     <!--  <div class="chat-bot-container" style="display: ;">
          <div class="cb-header">
              Hi, <strong><?php print_r($_SESSION['applicant_name']);?></strong> <br />We are happy to help you,
          </div>
          <ul class="cb-help">
              <li>Type '<strong>1</strong>' : For Application Status</li>
              <li>Type '<strong>2</strong>' : For Fee Amount</li>
              <li>Type '<strong>3</strong>' : For Offer Letter</li>
              <li>Type '<strong>4</strong>' : To know the start date of the semester</li>
              <li>Type '<strong>9</strong>' : To get a call from the representative</li>
          </ul>
         
          <div id='attach'></div>
          <div class="form-group cb-form">
              <input type="text" class="form-control" placeholder="Type here..." name='message' id='message' value=''>
              <button class="btn btn-primary" type="button" onclick='validateData()'>Start</button>
            </div>                                   
      </div> -->
      






</main>


<script>

   $('select').select2();

  function getDocumentByProgramme(id)
    {
      // alert(id);
        $.get("/applicant/applicant/getDocumentByProgramme/"+id, function(data, status)
        {
          // alert(data);
       
            if(data != '')
            {
                // $("#doc_upload").html(data);
                // $("#view_document").show();
            }else
            {
                // $("#view_document").hide();
                alert('No Records Defined To Upload For The Selected Program');
            }
        });
    }


    $(document).ready(function() {

        var id_program = <?php echo $getApplicantDetails->id_program; ?>;

        if(id_program != '')
        {
          getDocumentByProgramme(id_program);
        }

        var id_intake = $("#id_intake").val();
        $.get("/applicant/applicant/getIntakeDetails/"+id_intake, function(data, status)
        {
                    // alert(data);
                    if(data != '')
                    {
                         // $("#view_intake_discounts").hide();
                        $("#view_intake_discounts").html(data);

                        $("#view_is_intake_employee_discount").hide();
                        $("#view_is_intake_sibbling_discount").hide();
                        $("#view_is_intake_alumni_discount").hide();

                        var is_intake_alumni_discount = $("#is_intake_alumni_discount").val();
                        var is_intake_employee_discount = $("#is_intake_employee_discount").val();
                        var is_intake_sibbling_discount = $("#is_intake_sibbling_discount").val();
                // alert(is_intake_alumni_discount);
                // alert(is_intake_employee_discount);
                // alert(is_intake_sibbling_discount);

                      // alert(student_allotment_count);
                        if(is_intake_alumni_discount == 1)
                        {
                             $("#view_is_intake_alumni_discount").show();
                        }

                        if(is_intake_employee_discount == 1)
                        {
                             $("#view_is_intake_employee_discount").show();
                        }

                        if(is_intake_sibbling_discount == 1)
                        {
                             $("#view_is_intake_sibbling_discount").show();
                        }
                     }
                 

            });


         $("#form_applicant").validate({
            rules: {
                salutation: {
                    required: true
                },
                 first_name: {
                    required: true
                },
                 last_name: {
                    required: true
                },
                 phone: {
                    required: true
                },
                 email_id: {
                    required: true
                },
                 password: {
                    required: true
                },
                 nric: {
                    required: true
                },
                 gender: {
                    required: true
                },
                 id_program: {
                    required: true
                },
                 id_intake: {
                    required: true
                },
                employee_discount :{
                    required : true
                },
                sibbling_discount :{
                    required : true
                },
                 sibbling_name: {
                    required: true
                },
                 sibbling_nric: {
                    required: true
                },
                 employee_name: {
                    required: true
                },
                 employee_nric: {
                    required: true
                },
                 employee_designation: {
                    required: true
                },
                 date_of_birth: {
                    required: true
                },
                 nationality: {
                    required: true
                },
                 id_race: {
                    required: true
                },
                 mail_address1: {
                    required: true
                },
                 mailing_city: {
                    required: true
                },
                 mailing_country: {
                    required: true
                },
                 mailing_state: {
                    required: true
                },
                 mailing_zipcode: {
                    required: true
                },
                 permanent_address1: {
                    required: true
                },
                 permanent_city: {
                    required: true
                },
                 permanent_country: {
                    required: true
                },
                 permanent_state: {
                    required: true
                },
                 permanent_zipcode: {
                    required: true
                },
                 is_submitted: {
                    required: true
                },
                is_hostel: {
                    required: true
                },
                id_degree_type: {
                    required: true
                },
                passport: {
                    required: true
                },
                program_scheme: {
                    required: true
                },
                 alumni_discount: {
                    required: true
                },
                alumni_name: {
                    required: true
                },
                alumni_email: {
                    required: true
                },
                alumni_nric: {
                    required: true
                },
                id_program_scheme: {
                    required: true
                },
                id_branch: {
                    required: true
                }
            },
            messages: {
                salutation: {
                    required: "<p class='error-text'>Salutation required</p>",
                },
                first_name: {
                    required: "<p class='error-text'>First Name required</p>",
                },
                last_name: {
                    required: "<p class='error-text'>Last Name required</p>",
                },
                email_id: {
                    required: "<p class='error-text'>Email required</p>",
                },
                phone: {
                    required: "<p class='error-text'>Phone required</p>",
                },
                gender: {
                    required: "<p class='error-text'>Gender required</p>",
                },
                nric: {
                    required: "<p class='error-text'>NRIC required</p>",
                },
                password: {
                    required: "<p class='error-text'>Password required</p>",
                },
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                employee_discount: {
                    required: "<p class='error-text'>Employee Discount required</p>",
                },
                sibbling_discount: {
                    required: "<p class='error-text'>Sibbling Discount required</p>",
                },
                sibbling_name: {
                    required: "<p class='error-text'>Sibling Name required</p>",
                },
                sibbling_nric: {
                    required: "<p class='error-text'>Sibling NRIC required</p>",
                },
                employee_name: {
                    required: "<p class='error-text'>Employee Name required</p>",
                },
                employee_nric: {
                    required: "<p class='error-text'>Employee NRIC required</p>",
                },
                employee_designation: {
                    required: "<p class='error-text'>Employee Designation required</p>",
                },
                date_of_birth: {
                    required: "<p class='error-text'>Select Date Of Birth</p>",
                },
                nationality: {
                    required: "<p class='error-text'>Select Type Of Nationality</p>",
                },
                id_race: {
                    required: "<p class='error-text'>Select Race</p>",
                },
                mail_address1: {
                    required: "<p class='error-text'>Enter Mailing Address 1</p>",
                },
                mailing_city: {
                    required: "<p class='error-text'>Enter Mailimg City</p>",
                },
                mailing_country: {
                    required: "<p class='error-text'>Select Mailing Country</p>",
                },
                mailing_state: {
                    required: "<p class='error-text'>Select Mailing State</p>",
                },
                mailing_zipcode: {
                    required: "<p class='error-text'>Enter Mailing Zipcode</p>",
                },
                permanent_address1: {
                    required: "<p class='error-text'>Enter Permanent Address 1</p>",
                },
                permanent_city: {
                    required: "<p class='error-text'>Enter Permanent City</p>",
                },
                permanent_country: {
                    required: "<p class='error-text'>Select Permanent Country</p>",
                },
                permanent_state: {
                    required: "<p class='error-text'>Select Permanent State</p>",
                },
                permanent_zipcode: {
                    required: "<p class='error-text'>Enter Permanent Zipcode</p>",
                },
                is_submitted: {
                    required: "<p class='error-text'>Check Indicate that you accept the Terms and Conditions</p>",
                },
                is_hostel: {
                    required: "<p class='error-text'>Select Accomodation Required</p>",
                },
                id_degree_type: {
                    required: "<p class='error-text'>Select Degree Level</p>",
                },
                passport: {
                    required: "<p class='error-text'>Passport No. Required</p>",
                },
                program_scheme: {
                    required: "<p class='error-text'>Select Program Scheme</p>",
                },
                alumni_discount: {
                    required: "<p class='error-text'>Select Alumni Discount Applicable </p>",
                },
                alumni_name: {
                    required: "<p class='error-text'>Alumni Name Required</p>",
                },
                alumni_email: {
                    required: "<p class='error-text'>Alumni Email Required </p>",
                },
                alumni_nric: {
                    required: "<p class='error-text'>Alumni NRIC Required</p>",
                },
                id_program_scheme: {
                    required: "<p class='error-text'>Select Program Scheme</p>",
                },
                id_branch: {
                    required: "<p class='error-text'>Select Branch</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



    function validateData() {
        if($("#message").val()=='') {
           $("#error-textfordisplay").text('Enter some messgae');
        }
        if($("#message").val()!='0' && $("#message").val()!='1' && 
           $("#message").val()!='2' && 
           $("#message").val()!='3' && 
           $("#message").val()!='4' && 
           $("#message").val()!='9' ) {
           $("#error-textfordisplay").text('Please enter valid input');
          
        } else {
            messageid = $("#message").val();
            id_applicant = <?php echo $_SESSION['id_applicant'];?>;
             $.get("/applicant/applicant/getcustomreplay/"+messageid+"/"+id_applicant, function(data, status){
           
                $("#attach").append(data);
            });
        }

        
    }

    function submitApp()
    {
        $('#id_intake').prop('disabled', false);
        $('#id_program').prop('disabled', false);
        if($('#form_applicant').valid())
        {    
            $('#form_applicant').submit();
        }
    }

    $('select').select2();


    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
    });
</script>
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Reports extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('reports_model');
        $this->isLoggedIn();
    }

    function mostWrongQuestions()
    {
        if ($this->checkAccess('most_wrong_question.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['accountCodeList'] = array();

            $formData['full_name'] = $this->security->xss_clean($this->input->post('full_name'));
            $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['applicant_status'] = $this->security->xss_clean($this->input->post('applicant_status'));
            
            $data['searchParam'] = $formData;

            $data['studentList'] = $this->reports_model->studentListSearch($formData);

            if($_POST)
            {
                $data['accountCodeList'] = $this->reports_model->getMostWrongQuestions($formData);
            }

            $this->global['pageTitle'] = 'Election Management System : Most Wrong Questions List';
            $this->global['pageCode'] = 'most_wrong_question.list';
            //print_r($subjectDetails);exit;
            $this->loadViews("most_wrong_question/list", $this->global, $data, NULL);
        }
    }

    function mostQuestions()
    {
        if ($this->checkAccess('most_question.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['accountCodeList']=array();
            $formData['full_name'] = $this->security->xss_clean($this->input->post('full_name'));
            $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['applicant_status'] = $this->security->xss_clean($this->input->post('applicant_status'));
            
            $data['searchParam'] = $formData;

 
            if($_POST)
            {
                $data['accountCodeList'] = $this->reports_model->getMostRepeatedQuestions($formData);
            }

            $this->global['pageTitle'] = 'Election Management System : Most Questions List';
            $this->global['pageCode'] = 'most_repeated_question.list';
            //print_r($subjectDetails);exit;
            $this->loadViews("most_question/list", $this->global, $data, NULL);
        }
    }

}

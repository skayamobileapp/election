<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Student extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('applicant.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {


            $formData['full_name'] = $this->security->xss_clean($this->input->post('full_name'));
            $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['applicant_status'] = $this->security->xss_clean($this->input->post('applicant_status'));
            
            $data['searchParam'] = $formData;

 
            $data['studentList'] = $this->student_model->studentListSearch($formData);



            $this->global['pageTitle'] = 'Election Management System : Student List';
            $this->global['pageCode'] = 'student.list';
            //print_r($subjectDetails);exit;
            $this->loadViews("student/list", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('applicatoin.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/admission/applicant/list');
            }
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->session_id;

                echo "<Pre>";print_r($this->input->post());exit;
                
                $applicant_status = $this->security->xss_clean($this->input->post('applicant_status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'applicant_status' => $applicant_status,
                    'reason' => $reason,
                    'approved_by' => $id_user
                );

                redirect('/admission/applicant/list');
            }

            $data['studentDetails'] = $this->student_model->getStudentDetails($id);

            // $data['courseList'] = $this->student_model->courseListByStudentId($id);
            
            $data['invoiceList'] = $this->student_model->invoiceListByStudentId($id);
            $data['receiptList'] = $this->student_model->receiptListByStudentId($id);
            $data['examAttemptsByData'] = $this->student_model->getExamAttemptsByData($id);
            $data['examTaggingByIdStudent'] = $this->student_model->getStudentExamTaggingByIdStudent($id);




            // $data['applicantUploadedFiles'] = $this->student_model->getApplicantUploadedFiles($id);

            // echo "<Pre>";print_r($data['studentDetails']);exit;


            
            $this->global['pageTitle'] = 'Election Management System : Edit Applicant Approval';
            $this->global['pageCode'] = 'student.view';

            $this->loadViews("student/edit", $this->global, $data, NULL);
        }
    }

    function viewQuestoins($id_student_tagging, $id_attempt)
    {
            // echo "<Pre>";print_r($id_student_tagging);exit;

        if ($this->checkAccess('student.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id_student_tagging == null)
            {
                redirect('/records/ExamReport/list');
            }
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->session_id;

                echo "<Pre>";print_r($this->input->post());exit;
                
                $applicant_status = $this->security->xss_clean($this->input->post('applicant_status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'applicant_status' => $applicant_status,
                    'reason' => $reason,
                    'approved_by' => $id_user
                );

                redirect('/records/ExamReport/list');
            }

            $data['examDetails'] = $this->student_model->getExamTaggingDetails($id_student_tagging);

            $data['studentDetails'] = $this->student_model->getStudentDetails($data['examDetails']->id_student);

            $data['examQuestionsByData'] = $this->student_model->examQuestionsByData($id_student_tagging,$id_attempt);

            // echo "<Pre>";print_r($data['examQuestionsByData']);exit;

            $data['id_exam_tagging'] = $id_student_tagging;
            $data['id_attempt'] = $id_attempt;

            
            $this->global['pageTitle'] = 'Election Management System : Student Exam View';
            $this->global['pageCode'] = 'student.view';

            $this->loadViews("student/view_question", $this->global, $data, NULL);
        }
    }

    function downloadQuestion($id_student_tagging, $id_attempt)
    {

        // To Get Mpdf Library
        $this->getMpdfLibrary();

        // print_r($id_student);exit;
            
        $mpdf=new \Mpdf\Mpdf(); 

        $currentDate = date('d-m-Y');
        $currentTime = date('H:i:s');

        // $organisationDetails = $this->student_model->getOrganisation();

        // echo "<Pre>";print_r($organisationDetails);exit;
        

        $signature = $_SERVER['DOCUMENT_ROOT']."/assets/images/logo.png";

        // if($organisationDetails->image != '')
        // {
        //     $signature = $_SERVER['DOCUMENT_ROOT']."/assets/images/" . $organisationDetails->image;
        // }

        $examDetails = $this->student_model->getExamTaggingDetails($id_student_tagging);

        $student_data = $this->student_model->getStudentDetails($examDetails->id_student);



        // $student_data = $this->student_model->getStudentDetails($id_student);



        // echo "<Pre>";print_r($examDetails);exit;


        $exam_name = $examDetails->exam_name;
        $exam_event = $examDetails->exam_event;
        $exam_location = $examDetails->exam_location;
        $exam_date = date('d-m-Y', strtotime($examDetails->exam_date));


        $student_name = $student_data->full_name;
        $nric = $student_data->nric;
        $phone = $student_data->phone;
        $email_id = $student_data->email_id;
        $city = $student_data->permanent_city;
        $zipcode = $student_data->permanent_zipcode;
        // $branch_code = $student_data->branch_code;

        // if($date_time)
        // {
        //     $date_time = date('d-m-Y', strtotime($date_time));
        // }


            $file_data = "";


            $file_data.="<table align='center' width='100%' style='color:#00bcd2;'>
                <tr>
                          <td style='text-align: left' width='20%' ><b></b></td>
                          <td style='text-align: center' width='30%' ></td>

                  <td style='text-align: right' width='40%' ><img src='$signature' width='120px' /></td>
                  
                </tr>
               
                
                <tr>
                  <td style='text-align: center' width='100%'  colspan='3'> 
                  <b>$exam_event</b> ,<br/> <b>$exam_name</b> <br/> <b>$exam_date</b> <br><br></td>
                </tr>
            </table>


            <table width='100%'>
                <tr>
                 <td>Student Name : $student_name </td>
                 <td></td>
                 <td>NRIC: $nric</td>
                 <td></td>
                </tr>
                <tr>
                 <td>Email Id:  $email_id</td>
                 <td></td>
                 <td>Phone: $phone</td>
                 <td></td>
                </tr>
                <tr>
                 <td>Address:  $city</td>
                 <td></td>
                 <td>Zipcode: $zipcode</td>
                 <td></td>
                </tr>
             </table>
             ";


        $examQuestions = $this->student_model->examQuestionsByData($id_student_tagging,$id_attempt);


        // echo "<Pre>";print_r($examQuestions);exit;

        $file_data = $file_data ."
        <br>
        <br>
                <table>
                ";



        $i = 1;
        foreach ($examQuestions as $examQuestion)
        {
            $question = $examQuestion->question;
            $id_question = $examQuestion->id_question;
            $answer = $examQuestion->answer;
            $correct_answer = $examQuestion->correct_answer;
            $is_correct_answer = $examQuestion->is_correct_answer;

            $options = $this->student_model->getOptionsByQuestionId($id_question);

            // echo "<Pre>";print_r($options);exit;

            // $question = $examQuestion->question;

            $file_data = $file_data ."

                <tr>
                   <td valign='top'>
                   $i.
                   </td>
                   <td> $question
                   </td>
                </tr>
                ";

                $j = 1;
                foreach ($options as $option)
                {
                    $opt = $option->option_description;
                    $is_correct_answer = $option->is_correct_answer;

                    $file_data = $file_data ."
                    <tr>
                       <td colspan='2'>";

                    if($is_correct_answer == 1)
                    {
                        $file_data = $file_data ."
                        <b>
                        ";
                    }


                       $file_data = $file_data ."
                       &nbsp;&nbsp;&nbsp;&nbsp; $j . $opt
                       ";

                    if($is_correct_answer == 1)
                    {
                        $file_data = $file_data ."
                        </b>
                        ";
                    }
                    
                    
                    $file_data = $file_data ."
                       </td>
                    </tr>
                    ";
                    $j++;
                }

            $file_data = $file_data ."
                <tr>
                   <td colspan='2'>
                   <b> Answer </b> =  $correct_answer
                   </td>
                </tr>
                ";

            $file_data = $file_data ."
            <tr>
               <td colspan='2'>
               <br>
               </td>
            </tr>
            ";

            $i++;
        }

        $file_data = $file_data ."
        </table>";


        $file_data = $file_data ."
        <pagebreak>";

    


            // $mpdf->SetFooter('<div>Campus Management System</div>');
            // echo $file_data;exit;

            $mpdf->WriteHTML($file_data);
            $mpdf->Output($nric . '_Submitted_Answer_'.$currentDate . '_' . $currentTime.'.pdf', 'D');
            exit;

        echo $id_student_tagging;exit();
    }
}

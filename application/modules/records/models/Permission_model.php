<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


class Permission_model extends CI_Model
{
    function permissionListingCount()
    {
        $this->db->select('BaseTbl.permissionId, BaseTbl.code,BaseTbl.description');
        $this->db->from('tbl_permissions as BaseTbl');
         $query = $this->db->get();
        
        return $query->num_rows();
    }
  
    function latestPermissionListing()
    {
        $this->db->select('BaseTbl.permissionId, BaseTbl.code,BaseTbl.description');
        $this->db->from('tbl_permissions as BaseTbl');
        $this->db->limit(10, 0);
        $this->db->order_by("permissionId", "desc");

         $query = $this->db->get();
         $result = $query->result();        
         return $result;
    }
    
    function permissionListSearch($data)
    {
        $this->db->select('BaseTbl.id, BaseTbl.code, BaseTbl.description,BaseTbl.status');
        $this->db->from('permissions as BaseTbl');
        if ($data['name'] != '')
        {
            $likeCriteria = "(BaseTbl.code  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
       
        $this->db->order_by("BaseTbl.id", "ASC");
         $query = $this->db->get();
         $result = $query->result();        
         return $result;
    }
   
    function addNewPermission($permissionInfo)
    {
        $this->db->trans_start();
        $this->db->insert('permissions', $permissionInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
    function getPermission($permissionId)
    {
        $this->db->select('id, code,description,status');
        $this->db->from('permissions');
        $this->db->where('id', $permissionId);
        $query = $this->db->get();
        
        return $query->row();
    }
    
    function editPermission($permissionInfo, $permissionId)
    {
        $this->db->where('id', $permissionId);
        $result = $this->db->update('permissions', $permissionInfo);
        
        return $result;
    }
    
    function deletePermission($permissionId, $permissionInfo)
    {
        $this->db->where('id', $permissionId);
        $this->db->update('permissions', $permissionInfo);
        
        return $this->db->affected_rows();
    }

}

  
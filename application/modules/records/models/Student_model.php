<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Student_model extends CI_Model
{
    function studentListSearch($applicantList)
    {

        $this->db->select('a.*');
        $this->db->from('student as a');
        if($applicantList['full_name'])
        {
            $likeCriteria = "(a.full_name  LIKE '%" . $applicantList['full_name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($applicantList['email_id'])
        {
            $likeCriteria = "(a.email_id  LIKE '%" . $applicantList['email_id'] . "%')";
            $this->db->where($likeCriteria);
        }
        
        if($applicantList['nric'])
        {
            $likeCriteria = "(a.nric  LIKE '%" . $applicantList['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($applicantList['applicant_status'])
        {
            $this->db->where('a.applicant_status', $applicantList['applicant_status']);
        }
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function countryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('country');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();      
         return $result;
    }
   

    function categoryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function courseListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('course');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function stateList()
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('status', 1);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

     function raceListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('race_setup');
        $this->db->where('status', $status);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function salutationListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('salutation_setup as a');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

     function religionListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('religion_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    
    function getStudentDetails($id)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $sd = $query->row();
        return $sd;
    }

    function courseListByStudentId($id_student)
    {
        $this->db->select('mi.*');
        $this->db->from('main_invoice as mi');
        $this->db->where('mi.id_student', $id_student);
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function invoiceListByStudentId($id_student)
    {
        $this->db->select('*');
        $this->db->from('main_invoice');
        $this->db->where('id_student', $id_student);
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function receiptListByStudentId($id_student)
    {
        $this->db->select('*');
        $this->db->from('receipt');
        $this->db->where('id_student', $id_student);
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getExamAttemptsByData($id_student)
    {
        $this->db->select('sea.*, ee.name as exam_event_name, ee.exam_date, es.name as exam_name, ecl.name as exam_location, ee.exam_date');
        $this->db->from('student_exam_attempt as sea');
        $this->db->join('exam_student_tagging as est', 'sea.id_exam_student_tagging = est.id');
        $this->db->join('exam_event as ee', 'est.id_exam_event = ee.id');
        $this->db->join('examset as es','ee.id_exam_set = es.id');
        $this->db->join('exam_center_location as ecl','ee.id_exam_center = ecl.id');
        // $this->db->where('sea.id_exam_student_tagging', $id_exam_tagging);
        $this->db->where('sea.id_student', $id_student);
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function getStudentExamTaggingByIdStudent($id_student)
    {
        $this->db->select('est.*, ee.name as exam_event_name, ee.exam_date, es.name as exam_name, ecl.name as exam_location, ee.exam_date ');
        $this->db->from('exam_student_tagging as est');
        $this->db->join('exam_event as ee', 'est.id_exam_event = ee.id');
        $this->db->join('examset as es','ee.id_exam_set = es.id');
        $this->db->join('exam_center_location as ecl','ee.id_exam_center = ecl.id');
        $this->db->where('est.id_student', $id_student);
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function getExamTaggingDetails($id)
    {
        $this->db->select('a.*, s.full_name, s.nric, ee.name as exam_event, es.name as exam_name, ecl.name as exam_location, ee.exam_date as exam_date');
        $this->db->from('exam_student_tagging as a');
        $this->db->join('student as s','a.id_student = s.id');
        $this->db->join('exam_event as ee','a.id_exam_event = ee.id');
        $this->db->join('examset as es','ee.id_exam_set = es.id');
        $this->db->join('exam_center_location as ecl','ee.id_exam_center = ecl.id');
        $this->db->where('a.id', $id);
        $query = $this->db->get();
        $sd = $query->row();
        return $sd;
    }

    function examQuestionsByData($id_exam_tagging, $id_attempt)
    {
        $this->db->select('sqs.*, que.question, qho.option_description as answer');
        $this->db->from('student_question_set as sqs');
        $this->db->join('question as que','sqs.id_question = que.id','left');
        $this->db->join('question_has_option as qho','sqs.id_answer = qho.id','left');
        $this->db->join('exam_student_tagging as est','sqs.id_exam_student_tagging = est.id','left');
        $this->db->join('student_exam_attempt as sea','sqs.id_student_exam_attempt = sea.id','left');
        // $this->db->where('sqs.id_student', $data['id_student']);
        $this->db->where('sqs.id_exam_student_tagging', $id_exam_tagging);
        $this->db->where('sqs.id_student_exam_attempt', $id_attempt);
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $results = $query->result();
        
        // echo "<Pre>"; print_r($results);exit();

         $details = array();

         foreach ($results as $result)
         {

            $id_question = $result->id_question;
            $id_answer = $result->id_answer;

            // echo "<Pre>"; print_r($id_answer);exit();


            $result->correct_answer = '';
            $result->is_correct_answer = '0';

            $answer = $this->getAnswerToTheQuestion($id_question);

            if($answer)
            {
                $result->correct_answer = $answer->option_description;

                if($id_answer == $answer->id)
                {
                    $result->is_correct_answer = '1';
                }
            }

            array_push($details, $result);

            // echo "<Pre>"; print_r($result);exit();
         }

         return $details;
    }

    function getAnswerToTheQuestion($id_question)
    {
        $this->db->select('qho.*');
        $this->db->from('question_has_option as qho');
        $this->db->where('qho.id_question', $id_question);
        $this->db->where('qho.is_correct_answer', 1);
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->row();

         return $result;
    }

    function getOptionsByQuestionId($id_question)
    {
        $this->db->select('qho.*');
        $this->db->from('question_has_option as qho');
        $this->db->where('qho.id_question', $id_question);
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();

         return $result;
    }
    

}
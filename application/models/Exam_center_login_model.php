<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login_model (Login Model)
 * Login model class to get to authenticate user credentials 
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Exam_center_login_model extends CI_Model
{
    
    function loginExamCenter($email, $password)
    {
        $this->db->select('stu.*');
        $this->db->from('exam_center as stu');
        $this->db->where('stu.email', $email);
        $query = $this->db->get();
        
        $user = $query->row();
        // echo "<Pre>";print_r($user);exit();

        
        if(!empty($user))
        {
        $entered_md5 = md5($password);
            if($entered_md5 == $user->password)
            {
        // echo "<Pre>";print_r($user);exit();
                return $user;
            }else
            {
        // echo "<Pre>";print_r('sa');exit();

                return array();
            }
        }
        else
        {
            return array();
        }
    }

    function examCenterLastLoginInfo($id_student)
    {
        $this->db->select('created_dt_tm');
        $this->db->where('id_exam_center', $id_student);
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get('exam_center_last_login');

        return $query->row();
    }


    function checkScholarEmailExist($email)
    {
        $this->db->select('userId');
        $this->db->where('email', $email);
        $this->db->where('isDeleted', 0);
        $query = $this->db->get('scholar');

        if ($query->num_rows() > 0){
            return true;
        } else {
            return false;
        }
    }

    function resetPasswordUser($data)
    {
        $result = $this->db->insert('tbl_reset_password', $data);

        if($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function getScholarInfoByEmail($email)
    {
        $this->db->select('userId, email, name');
        $this->db->from('student');
        $this->db->where('isDeleted', 0);
        $this->db->where('email', $email);
        $query = $this->db->get();

        return $query->row();
    }

    function checkActivationDetails($email, $activation_id)
    {
        $this->db->select('id');
        $this->db->from('tbl_reset_password');
        $this->db->where('email', $email);
        $this->db->where('activation_id', $activation_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function createPasswordUser($email, $password)
    {
        $this->db->where('email', $email);
        $this->db->where('isDeleted', 0);
        $this->db->update('tbl_users', array('password'=>getHashedPassword($password)));
        $this->db->delete('tbl_reset_password', array('email'=>$email));
    }

    function addExamCenterLastLogin($loginInfo)
    {
        $this->db->trans_start();
        $this->db->insert('exam_center_last_login', $loginInfo);
        $this->db->trans_complete();
    }
}

?>
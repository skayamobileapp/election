<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


class Login_model extends CI_Model
{
    

    function loginMe($email, $password)
    {
        $this->db->select('usr.id as userId, usr.password, usr.name, usr.id_role as roleId, Roles.role');
        $this->db->from('users as usr');
        $this->db->join('roles as Roles','usr.id_role = Roles.id');
        $this->db->where('usr.email', $email);
        $this->db->where('usr.is_deleted', 0);
        $query = $this->db->get();
        
        $user = $query->row();
        // echo "<Pre>";print_r($email);exit();
        
        if(!empty($user)){
            $entered_md5 = md5($password);
            if($entered_md5 == $user->password)
            {
        // echo "<Pre>";print_r($user);exit();
                return $user;
            }
            else
            {
                return array();
            }
        } else {
            return array();
        }
    }

    function checkEmailExist($email)
    {
        $this->db->select('userId');
        $this->db->where('email', $email);
        $this->db->where('isDeleted', 0);
        $query = $this->db->get('users');

        if ($query->num_rows() > 0){
            return true;
        } else {
            return false;
        }
    }

    function resetPasswordUser($data)
    {
        $result = $this->db->insert('tbl_reset_password', $data);

        if($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }


    function getCustomerInfoByEmail($email)
    {
        $this->db->select('id as userId, email, name');
        $this->db->from('users');
        $this->db->where('isDeleted', 0);
        $this->db->where('email', $email);
        $query = $this->db->get();

        return $query->row();
    }

   

    function checkActivationDetails($email, $activation_id)
    {
        $this->db->select('id');
        $this->db->from('tbl_reset_password');
        $this->db->where('email', $email);
        $this->db->where('activation_id', $activation_id);
        $query = $this->db->get();
        return $query->num_rows();
    }


    function createPasswordUser($email, $password)
    {
        $this->db->where('email', $email);
        $this->db->where('is_deleted', 0);
        $this->db->update('users', array('password'=>getHashedPassword($password)));
        $this->db->delete('tbl_reset_password', array('email'=>$email));
    }


    function lastLogin($loginInfo)
    {
        $this->db->trans_start();
        $this->db->insert('user_last_login', $loginInfo);
        $this->db->trans_complete();
    }


    function lastLoginInfo($userId)
    {
        $this->db->select('usr.created_dt_tm');
        $this->db->where('usr.id_user', $userId);
        $this->db->order_by('usr.id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get('user_last_login as usr');

        return $query->row();
    }
}

?>
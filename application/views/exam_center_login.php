<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="description" content="" />
  <title>Student Login</title>

  <!-- Bootstrap core CSS -->
  <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/font-awesome.min.css" rel="stylesheet" />
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap" rel="stylesheet" />
  <!-- Custom styles for this template -->
  <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/main.css" rel="stylesheet" />
</head>

<body style="padding-top:0px !important;">

  <div class="login-wrapper" >
    <div class="container-fluid">
      <form action="<?php echo BASE_PATH; ?>examcenterLogin/centerLogin" method="post">
        <div class="row">
          <div class="col-sm-8 col-lg-6 col-xl-5 ml-auto">
            <div class="login-container d-flex align-items-center">
              <div>
                <h1>Logo</h1>
                <h3><strong>Sign In</strong><br />Welcome To CIIF Online Exam</h3>
                <?php
                if ($this->session->flashdata('success')) {
                ?>
                  <div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                  </div>
                <?php
                }
                if ($this->session->flashdata('error')) {
                ?>
                  <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Failure!</strong> <?php echo $this->session->flashdata('error'); ?>
                  </div>
                <?php
                }
                ?>
                <div class="form-group">
                  <label for="exampleFormControlInput1">Exam Center Login</label>
                  <input type="email" class="form-control" id="email" name="email" placeholder="kl@examcenter.com">
                </div>
                <div class="form-group">
                  <label for="examplePasswordInput1">Password</label>
                  <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                </div>
                <button type="submit" class="btn btn-primary btn-lg btn-block my-4">Sign In</button>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>

  <script src="<?php echo BASE_PATH; ?>assets/onlineportal/js/jquery-1.12.4.min.js"></script>
  <script src="<?php echo BASE_PATH; ?>assets/onlineportal/js/bootstrap.min.js"></script>
  <script src="<?php echo BASE_PATH; ?>assets/onlineportal/js/main.js"></script>
</body>

</html>
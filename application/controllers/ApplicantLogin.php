<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class ApplicantLogin extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('applicant_login_model');
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
        $this->checkApplicantLoggedIn();
    }

    function checkApplicantLoggedIn()
    {
        $isApplicantLoggedIn = $this->session->userdata('isApplicantLoggedIn');
        
        if(!isset($isApplicantLoggedIn) || $isApplicantLoggedIn != TRUE)
        {
            // echo "Not Login";exit();
            $this->load->view('applicant_login');
        }
        else
        {
            // echo "Login";exit();
            redirect('applicant/applicant');
        }
    }

    public function applicantLogin()
    {
        $formData = $this->input->post();
        // echo "<Pre>"; print_r($formData);exit;
        $domain = $this->getDomainName();

        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|max_length[128]|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[32]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->index();
        }
        else
        {
            $email = strtolower($this->security->xss_clean($this->input->post('email')));
            $password = $this->input->post('password');
            
            $result = $this->applicant_login_model->loginApplicant($email, $password);
            
            
            if(!empty($result))
            {
                if($result->email_verified == 0)
                {
                    echo "Email Verification Pending Check Your Email To Verify";exit();
                }
                $lastLogin = $this->applicant_login_model->applicantLastLoginInfo($result->id_applicant);

                if($lastLogin == '')
                {
                    $applicant_login = date('Y-m-d h:i:s');
                }
                else
                {
                    $applicant_login = $lastLogin->created_dt_tm;
                }

                $sessionArray = array('id_applicant'=>$result->id_applicant,                    
                                        'applicant_name'=>$result->applicant_name,
                                        'email_id'=>$result->email_id,
                                        'applicant_last_login'=> $applicant_login,
                                        'isApplicantLoggedIn' => TRUE
                                );
        // echo "<Pre>";print_r($sessionArray);exit();

                $this->session->set_userdata($sessionArray);

                unset($sessionArray['id_applicant'], $sessionArray['isApplicantLoggedIn'], $sessionArray['applicant_last_login']);

                $loginInfo = array("id_applicant"=>$result->id_applicant, "session_data" => json_encode($sessionArray), "machine_ip"=>$_SERVER['REMOTE_ADDR'], "user_agent"=>getBrowserAgent(), "agent_string"=>$this->agent->agent_string(), "platform"=>$this->agent->platform());


                $uniqueId = rand(0000000000,9999999999);
                $this->session->set_userdata("my_applicant_session_id", md5($uniqueId));


                $this->applicant_login_model->addApplicantLastLogin($loginInfo);

                // echo "Login To Applicant";exit();
                // echo md5($uniqueId);exit();
                redirect('/applicant/applicant/welcome');
            }
            else
            {
                $this->session->set_flashdata('error', 'Email or password mismatch');
                
                $this->index();
            }
        }
    }

    public function applicantRegistration()
    {
        
        if($this->input->post())
        {
            $salutation = $this->security->xss_clean($this->input->post('salutation'));
            $first_name = $this->security->xss_clean($this->input->post('first_name'));
            $last_name = $this->security->xss_clean($this->input->post('last_name'));
            $phone = $this->security->xss_clean($this->input->post('phone'));
            $nric = $this->security->xss_clean($this->input->post('nric'));
            $email_id = $this->security->xss_clean($this->input->post('email_id'));
            $password = $this->security->xss_clean($this->input->post('password'));

            $data = array(
                    'salutation' => $salutation,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'full_name' => $salutation.". ".$first_name." ".$last_name,
                    'phone' => $phone,
                    'nric' => $nric,
                    'email_id' => $email_id,
                    'password' => $password,
                    'email_verified' => 1
            );
            $duplicate = $this->applicant_login_model->checkDuplicateApplicantRegistration($data);
            if($duplicate)
            {
                echo "Email / Phone / NRIC Already Exist";exit();
            }

            $inserted_id = $this->applicant_login_model->addNewApplicantRegistration($data);
            redirect('/applicantLogin');
        }

        $this->load->view('applicant_registration');
    }
}

?>